﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.SessionManagement;
using StreamingMMSystemBase = Kreslik.Integrator.BusinessLayer.Systems.StreamingMMSystemBase<Kreslik.Integrator.BusinessLayer.Systems.IVenueStreamMMSystemSettings>;
//using StreamingTakerMMSystemBase = Kreslik.Integrator.BusinessLayer.Systems.StreamingTakerMMSystemBase<Kreslik.Integrator.BusinessLayer.Systems.IVenueStreamMMSystemSettings>;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class IntegratedTradingSystemsManger: IIntegratedTradingSystemsManger
    {
        //GC root this
        private IntegratedSystemSettingsProvidersManager _integratedSystemSettingsProvidersManager;
        private ToBReceiver _toBReceiver;
        private ITradingGroupsCache _tradingGroupsCache = new TradingGroupsCache();
        private BankpoolToVenueSessionAdapter _bankpoolToLGASessionAdapter;
        private OrderRateCheckersHolder _orderRateCheckersHolder;

        private static bool DealRejectionSupported(IOrderFlowSession session)
        {
            return (session as OrderFlowSession).DealRejectionSupported;
        }

        public IntegratedTradingSystemsManger() { }

        public void Initialize(ILogger logger, IBookTopProvidersStore priceBookStoreProvider, IOrderManagement orderManagement,
                                IRiskManager riskManager, ISymbolsInfo symbolsInfo, IPriceStreamMonitor priceStreamMonitor, ToBReceiver toBReceiver, IEnumerable<IOrderFlowSession> orderFlowSessions,
            IEnumerable<IStreamingChannel> streamingChannels, ITickerProvider lmaxTickerProvider, IUnicastInfoForwardingHub unicastInfoForwardingHub)
        {
            IOrderFlowSession htfOrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.HTF);
            IOrderFlowSession htaOrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.HTA);
            IOrderFlowSession ht3OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.HT3);
            IOrderFlowSession h4mOrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.H4M);
            IOrderFlowSession fxallOrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.FA1);
            IOrderFlowSession l01OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.L01);
            IOrderFlowSession lm2OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.LM2);
            IOrderFlowSession lm3OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.LM3);
            IOrderFlowSession lx1OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.LX1);
            IOrderFlowSession lgcOrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.LGC);
            IOrderFlowSession lgaOrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.LGA);
            IOrderFlowSession fc1OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.FC1);
            IOrderFlowSession fc2OrdChannel = orderFlowSessions.FirstOrDefault(
                    sess => sess != null && sess.Counterparty == Counterparty.FC2);

            IStreamingChannel fs1StreamChannel =
                streamingChannels.FirstOrDefault(sess => sess != null && sess.Counterparty == Counterparty.FS1);
            IStreamingChannel fs2StreamChannel =
                streamingChannels.FirstOrDefault(sess => sess != null && sess.Counterparty == Counterparty.FS2);
            //IStreamingChannel ht3StreamChannel =
            //    streamingChannels.FirstOrDefault(sess => sess != null && sess.Counterparty == Counterparty.HT3);
            IStreamingChannel fl1StreamChannel =
                streamingChannels.FirstOrDefault(sess => sess != null && sess.Counterparty == Counterparty.FL1);

            if (htfOrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "Hotspot ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result Hotspot MM and Cross systems will not be created");
            }

            if (htaOrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "Hotspot HTA ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result Hotspot MM and Cross systems will not be created");
            }

            if (h4mOrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "Hotspot H4M ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result Hotspot MM and Cross systems will not be created");
            }

            if (ht3OrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "Hotspot HT3 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result Hotspot MM and Cross systems will not be created");
            }

            if (fxallOrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "FXall ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result FXall MM and Cross systems will not be created");
            }

            if (l01OrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "LM1 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result LM1 MM and Cross systems will not be created");
            }

            if (lm2OrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "LM2 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result LM2 MM and Cross systems will not be created");
            }

            if (lm3OrdChannel == null)
            {
                logger.Log(LogLevel.Error,
                           "LM3 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result LM3 MM and Cross systems will not be created");
            }

            if (lx1OrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "LX1 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result LX1 quoting systems will not be created");
            }

            if (lgcOrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "LGC ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result LGC quoting systems will not be created");
            }

            if (lgaOrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "LGA ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result LGC quoting systems will not be created");
            }

            if (fc1OrdChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "FC1 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result FXCM MM and Cross systems will not be created");
            }

            if (fc2OrdChannel == null)
            {
                logger.Log(LogLevel.Error,
                           "FC2 ORD channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result FXCM MM and Cross systems will not be created");
            }

            if (fs1StreamChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "FS1 Stream channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result FS1 Stream systems will not be created");
            }

            if (fs2StreamChannel == null)
            {
                logger.Log(LogLevel.Fatal,
                           "FS2 Stream channel is null (probably unconfigured) during creation of IntegratedTradingSystemsManger - as a result FS2 Stream systems will not be created");
            }

            _integratedSystemSettingsProvidersManager = new IntegratedSystemSettingsProvidersManager(logger, priceStreamMonitor, riskManager.SymbolTrustworthinessInfoProvider, riskManager);
            _toBReceiver = toBReceiver;

            VenuePoolForMarketOrder venuePoolForMarketOrder =
                new VenuePoolForMarketOrder(priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01),
                    priceStreamMonitor, lgaOrdChannel, riskManager, logger);
            _bankpoolToLGASessionAdapter = new BankpoolToVenueSessionAdapter(orderManagement, venuePoolForMarketOrder);
            _orderRateCheckersHolder = new OrderRateCheckersHolder(logger); 


            _integratedSystemSettingsProvidersManager.NewIntegratedStreamMMSystemSettingsAdded += provider =>
            {

                if (provider.TargetVenueCounterparty == Counterparty.FS1 && fs1StreamChannel != null)
                {
                    StreamingMMSystemBase streamingAsk = new StreamingMMSystemBase(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        fs1StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );

                    StreamingMMSystemBase streamingBid = new StreamingMMSystemBase(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        fs1StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.FS2 && fs2StreamChannel != null)
                {
                    StreamingMMSystemBase streamingAsk = new StreamingMMSystemBase(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        fs2StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );

                    StreamingMMSystemBase streamingBid = new StreamingMMSystemBase(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        fs2StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.FL1 && fl1StreamChannel != null)
                {
                    StreamingMMSystemBase streamingAsk = new StreamingMMSystemBase(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        fl1StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );

                    StreamingMMSystemBase streamingBid = new StreamingMMSystemBase(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        fl1StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );
                }
                //else if (provider.TargetVenueCounterparty == Counterparty.HT3 && ht3StreamChannel != null)
                //{
                //    StreamingMMSystemBase streamingAsk = new StreamingMMSystemBase(
                //        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                //            .GetChangeableAskPriceBook(provider.TargetSymbol),
                //        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                //            .GetChangeableBidPriceBook(provider.TargetSymbol),
                //        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                //        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                //        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                //        ht3StreamChannel,
                //        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                //        orderManagement, riskManager, _tradingGroupsCache, logger,
                //        provider
                //        );

                //    StreamingMMSystemBase streamingBid = new StreamingMMSystemBase(
                //        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                //            .GetChangeableAskPriceBook(provider.TargetSymbol),
                //        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                //            .GetChangeableBidPriceBook(provider.TargetSymbol),
                //        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                //        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                //        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                //        ht3StreamChannel,
                //        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                //        orderManagement, riskManager, _tradingGroupsCache, logger,
                //        provider
                //        );
                //}
                else if (provider.TargetVenueCounterparty == Counterparty.HTA && htaOrdChannel != null &&
                         DealRejectionSupported(htaOrdChannel))
                {
                    HotspotStreamingTakerMMSystem hotspotAskTakerMMSystem = new HotspotStreamingTakerMMSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol,
                        PriceSide.Ask,
                        htaOrdChannel,
                        symbolsInfo,
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider,
                        HotspotCounterparty.HTA,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTA));

                    HotspotStreamingTakerMMSystem hotspotBidTakerMMSystem = new HotspotStreamingTakerMMSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol,
                        PriceSide.Bid,
                        htaOrdChannel,
                        symbolsInfo,
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider,
                        HotspotCounterparty.HTA,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTA));
                }
                else if (provider.TargetVenueCounterparty == Counterparty.HT3 && ht3OrdChannel != null &&
                         DealRejectionSupported(ht3OrdChannel))
                {
                    HotspotStreamingTakerMMSystem hotspotAskTakerMMSystem = new HotspotStreamingTakerMMSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol,
                        PriceSide.Ask,
                        ht3OrdChannel,
                        symbolsInfo,
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider,
                        HotspotCounterparty.HT3,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HT3));

                    HotspotStreamingTakerMMSystem hotspotBidTakerMMSystem = new HotspotStreamingTakerMMSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol,
                        PriceSide.Bid,
                        ht3OrdChannel,
                        symbolsInfo,
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider,
                        HotspotCounterparty.HT3,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HT3));
                }
                else if (provider.TargetVenueCounterparty == Counterparty.H4M && h4mOrdChannel != null &&
                     DealRejectionSupported(ht3OrdChannel))
                {
                    HotspotStreamingTakerMMSystem hotspotAskTakerMMSystem = new HotspotStreamingTakerMMSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol,
                        PriceSide.Ask,
                        h4mOrdChannel,
                        symbolsInfo,
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider,
                        HotspotCounterparty.H4M,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.H4M));

                    HotspotStreamingTakerMMSystem hotspotBidTakerMMSystem = new HotspotStreamingTakerMMSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        provider.TargetSymbol,
                        PriceSide.Bid,
                        h4mOrdChannel,
                        symbolsInfo,
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider,
                        HotspotCounterparty.H4M,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.H4M));
                }
                else
                {
                    logger.Log(LogLevel.Fatal,
                        "Receiving new streaming settings for system {0}, but unable to create such a system (unknown or target session unconfigured or not an LL)",
                        provider.CurrentSettings.TradingSystemId);
                }
            };


            _integratedSystemSettingsProvidersManager.NewIntegratedGliderStreamMMSystemSettingsAdded += provider =>
            {

                if (provider.TargetVenueCounterparty == Counterparty.FS1 && fs1StreamChannel != null)
                {
                    StreamingMMLmaxGliderSystem streamingAsk = new StreamingMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        fs1StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );

                    StreamingMMLmaxGliderSystem streamingBid = new StreamingMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        fs1StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.FS2 && fs2StreamChannel != null)
                {
                    StreamingMMLmaxGliderSystem streamingAsk = new StreamingMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        fs2StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );

                    StreamingMMLmaxGliderSystem streamingBid = new StreamingMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        fs2StreamChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.HTA && htaOrdChannel != null)
                {
                    StreamingTakerMMLmaxGliderSystem streamingAsk = new StreamingTakerMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        htaOrdChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTA)
                        );

                    StreamingTakerMMLmaxGliderSystem streamingBid = new StreamingTakerMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        htaOrdChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTA)
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.HT3 && ht3OrdChannel != null)
                {
                    StreamingTakerMMLmaxGliderSystem streamingAsk = new StreamingTakerMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        ht3OrdChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HT3)
                        );

                    StreamingTakerMMLmaxGliderSystem streamingBid = new StreamingTakerMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        ht3OrdChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HT3)
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.H4M && h4mOrdChannel != null)
                {
                    StreamingTakerMMLmaxGliderSystem streamingAsk = new StreamingTakerMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Ask, provider.TargetVenueCounterparty,
                        h4mOrdChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.H4M)
                        );

                    StreamingTakerMMLmaxGliderSystem streamingBid = new StreamingTakerMMLmaxGliderSystem(
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01)
                            .GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                        priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        provider.TargetSymbol, PriceSide.Bid, provider.TargetVenueCounterparty,
                        h4mOrdChannel,
                        symbolsInfo, priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                        orderManagement, riskManager, _tradingGroupsCache, unicastInfoForwardingHub, logger,
                        provider,
                        _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.H4M)
                        );
                }
                else
                {
                    logger.Log(LogLevel.Fatal,
                        "Receiving new gliding streaming settings for system {0}, but unable to create such a system (unknown or target session unconfigured or not an LL)",
                        provider.CurrentSettings.TradingSystemId);
                }
            };

            _integratedSystemSettingsProvidersManager.NewIntegratedQuotingSystemSettingsAdded += provider =>
                {
                    if (provider.TargetVenueCounterparty == Counterparty.LM3 && lm3OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        BasicLmaxQuotingSystem lm3QuotingSystem = new BasicLmaxQuotingSystem(
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            LmaxCounterparty.LM3,
                            lm3OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.TermCurrency),
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.BaseCurrency),
                            _orderRateCheckersHolder.GetEventRateChecker(LmaxCounterparty.LM3), lmaxTickerProvider);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.LX1 && lx1OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        BasicLmaxQuotingSystem lx1QuotingSystem = new BasicLmaxQuotingSystem(
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            LmaxCounterparty.LX1,
                            lx1OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.TermCurrency),
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.BaseCurrency),
                            _orderRateCheckersHolder.GetEventRateChecker(LmaxCounterparty.LX1), lmaxTickerProvider);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.FC1 && fc1OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        FastMatchQuotingSystem fc1QuotingSystem = new FastMatchQuotingSystem(
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            FXCMCounterparty.FC1,
                            fc1OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.TermCurrency),
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.BaseCurrency),
                            lmaxTickerProvider);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.LGC && lgcOrdChannel != null && lgaOrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        MarginLmaxQuotingSystem lgcQuotingSystem = new MarginLmaxQuotingSystem(
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.FirmpoolBookTopProvider
                                .GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            LmaxCounterparty.LGC,
                            lgcOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            _bankpoolToLGASessionAdapter,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.TermCurrency),
                            priceStreamMonitor.CreateUsdConversionProvider(provider.TargetSymbol.BaseCurrency),
                            lgaOrdChannel,
                            _orderRateCheckersHolder.GetEventRateChecker(LmaxCounterparty.LGC), lmaxTickerProvider);
                    }
                    else
                    {
                        logger.Log(LogLevel.Fatal,
                                   "Receiving new quoting settings for system {0}, but unable to create such a system (unknow or target session unconfigured)",
                                   provider.CurrentSettings.TradingSystemId);
                    }
                };

            _integratedSystemSettingsProvidersManager.NewIntegratedCrossSystemSettingsAdded += provider =>
                {
                    if (provider.TargetVenueCounterparty == Counterparty.L01 && l01OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        LmaxCrossSystem lm1CrossSystem = new LmaxCrossSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.L01).GetAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.L01).GetBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache,
                            l01OrdChannel,
                            symbolsInfo, unicastInfoForwardingHub,
                            logger,
                            provider,
                            LmaxCounterparty.L01);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.LM2 && lm2OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        LmaxCrossSystem lm2CrossSystem = new LmaxCrossSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.LM2).GetAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.LM2).GetBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache,
                            lm2OrdChannel,
                            symbolsInfo, unicastInfoForwardingHub,
                            logger,
                            provider,
                            LmaxCounterparty.LM2);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.FA1 && fxallOrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        FXallCrossSystem fXallCrossSystem = new FXallCrossSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.FA1).GetAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.FA1).GetBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache,
                            fxallOrdChannel,
                            symbolsInfo, unicastInfoForwardingHub,
                            logger,
                            provider);
                    }
                        //disabling HTF cross
                    else if (provider.TargetVenueCounterparty == Counterparty.HTF && htfOrdChannel != null)
                    {
                        logger.Log(LogLevel.Error,
                                   "Receiving new HTF cross settings for system {0}, but unable to create such a system - HTF cross not currently supported",
                                   provider.CurrentSettings.TradingSystemId);
                        ////no need to GC root this, as it is rooted as events subscriber
                        //HotspotCrossSystem hotspotCrossSystem = new HotspotCrossSystem(
                        //    priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        //    priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        //    _toBReceiver,
                        //    provider.TargetSymbol,
                        //    orderManagement,
                        //    riskManager,
                        //    _tradingGroupsCache,
                        //    htfOrdChannel,
                        //    symbolsInfo,
                        //    logger,
                        //    provider,
                        //    HotspotCounterparty.HTF);
                    }
                        //HT3 cannot cross currently
                    //else if (provider.TargetVenueCounterparty == Counterparty.HT3 && htfOrdChannel != null)
                    //{
                    //    //no need to GC root this, as it is rooted as events subscriber
                    //    HotspotCrossSystem hotspot3CrossSystem = new HotspotCrossSystem(
                    //        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                    //        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                    //        _toBReceiver,
                    //        provider.TargetSymbol,
                    //        orderManagement,
                    //        riskManager,
                    //        _tradingGroupsCache,
                    //        ht3OrdChannel,
                    //        symbolsInfo,
                    //        logger,
                    //        provider,
                    //        HotspotCounterparty.HT3);
                    //}
                    else if (provider.TargetVenueCounterparty == Counterparty.FC1 && fc1OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        FXCMCrossSystem fXcmCrossSystem = new FXCMCrossSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.FC1).GetAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.FC1).GetBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache,
                            fc1OrdChannel,
                            symbolsInfo, unicastInfoForwardingHub,
                            logger,
                            provider,
                            FXCMCounterparty.FC1);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.FC2 && fc2OrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        FXCMCrossSystem fXcmCrossSystem = new FXCMCrossSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.FC2).GetAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.GetBookTopProvider(Counterparty.FC2).GetBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache,
                            fc2OrdChannel,
                            symbolsInfo, unicastInfoForwardingHub,
                            logger,
                            provider,
                            FXCMCounterparty.FC2);
                    }
                    else
                    {
                        logger.Log(LogLevel.Fatal,
                                   "Receiving new cross settings for system {0}, but unable to create such a system (unknow or target session unconfigured)",
                                   provider.CurrentSettings.TradingSystemId);
                    }
                };

            _integratedSystemSettingsProvidersManager.NewIntegratedSmartCrossSystemSettingsAdded += provider =>
            {
                if (provider.TargetVenueCounterparty == Counterparty.HTF && htfOrdChannel != null)
                {
                    HotspotSmartCrossSystem htaSmartCrossSystem = new HotspotSmartCrossSystem(
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        new HotspotRemoteToBProvider(provider.TargetSymbol, _toBReceiver, (HotspotCounterparty) provider.TargetVenueCounterparty),
                        provider.TargetSymbol,
                        provider.TargetVenueCounterparty,
                        htfOrdChannel,
                        symbolsInfo,
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.LX1 && lx1OrdChannel != null)
                {
                    LmaxSmartCrossSystem lx1SmartCrossSystem = new LmaxSmartCrossSystem(
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        new InProcessVenueToBProvider(
                            priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableBidPriceBook(provider.TargetSymbol)),
                        provider.TargetSymbol,
                        provider.TargetVenueCounterparty,
                        lx1OrdChannel,
                        symbolsInfo,
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.LM2 && lm2OrdChannel != null)
                {
                    LmaxSmartCrossSystem lx1SmartCrossSystem = new LmaxSmartCrossSystem(
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        new InProcessVenueToBProvider(
                            priceBookStoreProvider.TryGetBookTopProvider(Counterparty.LM2).GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.TryGetBookTopProvider(Counterparty.LM2).GetChangeableBidPriceBook(provider.TargetSymbol)),
                        provider.TargetSymbol,
                        provider.TargetVenueCounterparty,
                        lx1OrdChannel,
                        symbolsInfo,
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider
                        );
                }
                else if (provider.TargetVenueCounterparty == Counterparty.FA1 && fxallOrdChannel != null)
                {
                    FxallSmartCrossSystem fa1SmartCrossSystem = new FxallSmartCrossSystem(
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.TryGetBookTopProvider(Counterparty.L01).GetChangeableBidPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                        priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                        new InProcessVenueToBProvider(
                            priceBookStoreProvider.TryGetBookTopProvider(Counterparty.FA1).GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.TryGetBookTopProvider(Counterparty.FA1).GetChangeableBidPriceBook(provider.TargetSymbol)),
                        provider.TargetSymbol,
                        provider.TargetVenueCounterparty,
                        fxallOrdChannel,
                        symbolsInfo,
                        orderManagement,
                        riskManager,
                        _tradingGroupsCache, unicastInfoForwardingHub,
                        logger,
                        provider
                        );
                }
                else
                {
                    logger.Log(LogLevel.Fatal,
                        "Receiving new Smart cross settings for system {0}, but unable to create such a system (unknow or target session unconfigured)",
                        provider.CurrentSettings.TradingSystemId);
                }
            };

            _integratedSystemSettingsProvidersManager.NewIntegratedMMSystemSettingsAdded += provider =>
                {
                    if (provider.TargetVenueCounterparty == Counterparty.FA1 && fxallOrdChannel != null)
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        FXallMMSystem fxallAskMMSystem = new FXallMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            fxallOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider);

                        FXallMMSystem fxallBidMMSystem = new FXallMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            fxallOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.HTF && htfOrdChannel != null && !DealRejectionSupported(htfOrdChannel))
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        HotspotMMSystem hotspotAskMMSystem = new HotspotMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            htfOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            HotspotCounterparty.HTF,
                            _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTF));

                        HotspotMMSystem hotspotBidMMSystem = new HotspotMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            htfOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            HotspotCounterparty.HTF,
                            _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTF));
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.HTA && htaOrdChannel != null && !DealRejectionSupported(htaOrdChannel))
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        HotspotMMSystem hotspotAskMMSystem = new HotspotMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            htaOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            HotspotCounterparty.HTA,
                            _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTA));

                        HotspotMMSystem hotspotBidMMSystem = new HotspotMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            htaOrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            HotspotCounterparty.HTA,
                            _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HTA));
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.HT3 && ht3OrdChannel != null && !DealRejectionSupported(ht3OrdChannel))
                    {
                        //no need to GC root this, as it is rooted as events subscriber
                        HotspotMMSystem hotspotAskMMSystem = new HotspotMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            ht3OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            HotspotCounterparty.HT3,
                            _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HT3));

                        HotspotMMSystem hotspotBidMMSystem = new HotspotMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            ht3OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            HotspotCounterparty.HT3,
                            _orderRateCheckersHolder.GetEventRateChecker(HotspotCounterparty.HT3));
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.FC2 && fc2OrdChannel != null)
                    {
                        FXCMMMSystem fc2AskMMSystem = new FXCMMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            fc2OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            FXCMCounterparty.FC2);

                        FXCMMMSystem fc2BidMMSystem = new FXCMMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            fc2OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            FXCMCounterparty.FC2);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.FC1 && fc1OrdChannel != null)
                    {
                        FXCMMMSystem fc1AskMMSystem = new FXCMMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            fc1OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            FXCMCounterparty.FC1);

                        FXCMMMSystem fc1BidMMSystem = new FXCMMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            fc1OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            FXCMCounterparty.FC1);
                    }
                    else if (provider.TargetVenueCounterparty == Counterparty.LX1 && lx1OrdChannel != null)
                    {
                        LmaxMMSystem lx1AskMMSystem = new LmaxMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Ask,
                            lx1OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Ask),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            LmaxCounterparty.LX1,
                            _orderRateCheckersHolder.GetEventRateChecker(LmaxCounterparty.LX1));

                        LmaxMMSystem lx1BidMMSystem = new LmaxMMSystem(
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableAskPriceBook(provider.TargetSymbol),
                            priceBookStoreProvider.BankpoolBookTopProvider.GetChangeableBidPriceBook(provider.TargetSymbol),
                            provider.TargetSymbol,
                            PriceSide.Bid,
                            lx1OrdChannel,
                            symbolsInfo,
                            priceStreamMonitor.GetMedianPriceProvider(provider.TargetSymbol, PriceSide.Bid),
                            orderManagement,
                            riskManager,
                            _tradingGroupsCache, unicastInfoForwardingHub,
                            logger,
                            provider,
                            LmaxCounterparty.LX1,
                            _orderRateCheckersHolder.GetEventRateChecker(LmaxCounterparty.LX1));
                    }
                    else
                    {
                        logger.Log(LogLevel.Fatal,
                            "Receiving new MM settings for system {0}, but unable to create such a system (unknown or target session unconfigured or it is LL)",
                            provider.CurrentSettings.TradingSystemId);
                    }
                };
        }

        public void Start()
        {
            //Start receiving events, initial settings will be delivered synchronously
            _integratedSystemSettingsProvidersManager.StartRaisingEvents();
        }


        private class OrderRateCheckersHolder
        {
            private Dictionary<Counterparty, EventsRateCheckerEx> _rateCheckers = new Dictionary<Counterparty, EventsRateCheckerEx>();
            private ILogger _logger;

            public OrderRateCheckersHolder(ILogger logger)
            {
                _logger = logger;
            }

            
            internal EventsRateCheckerEx GetEventRateChecker(LmaxCounterparty lmaxCounterparty)
            {
                return this.GetEventRateCheckerInternal(lmaxCounterparty, 950);
            }

            internal EventsRateCheckerEx GetEventRateChecker(HotspotCounterparty hotspotCounterparty)
            {
                FIXChannel_HOTSettings hotspotSettings = null;

                switch (hotspotCounterparty.ToString())
                {
                    case Counterparty.EnumValues.HTA:
                        hotspotSettings = SessionsSettings.FIXChannel_HTABehavior;
                        break;
                    case Counterparty.EnumValues.HTF:
                        hotspotSettings = SessionsSettings.FIXChannel_HTFBehavior;
                        break;
                    case Counterparty.EnumValues.HT3:
                        hotspotSettings = SessionsSettings.FIXChannel_HT3Behavior;
                        break;
                    case Counterparty.EnumValues.H4T:
                        hotspotSettings = SessionsSettings.FIXChannel_H4TBehavior;
                        break;
                    case Counterparty.EnumValues.H4M:
                        hotspotSettings = SessionsSettings.FIXChannel_H4MBehavior;
                        break;
                }

                return this.GetEventRateCheckerInternal(hotspotCounterparty, hotspotSettings.MaxOrdersPerSecond);
            }

            private EventsRateCheckerEx GetEventRateCheckerInternal(Counterparty counterparty, int ordersPerSec)
            {
                EventsRateCheckerEx rateChecker;
                if (!_rateCheckers.TryGetValue(counterparty, out rateChecker))
                {
                    rateChecker = new EventsRateCheckerEx(TimeSpan.FromSeconds(1), ordersPerSec, _logger, TimeSpan.FromMinutes(1),
                        string.Format("Outgoing orders to {0} session", counterparty));
                    _rateCheckers.Add(counterparty, rateChecker);
                }

                return rateChecker;
            }
        }
    }
}
