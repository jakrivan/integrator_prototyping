﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public interface IVenueToBProvider
    {
        bool TryRetrieveVenueToB(PriceSide side, out decimal price, out decimal size);
    }

    public class InProcessVenueToBProvider : IVenueToBProvider
    {
        private IBookTop<PriceObjectInternal>[] _venuePriceBooks;

        public InProcessVenueToBProvider(
            IBookTop<PriceObjectInternal> venueAskPriceBook,
            IBookTop<PriceObjectInternal> venueBidPriceBook)
        {
            this._venuePriceBooks = new IBookTop<PriceObjectInternal>[] { venueBidPriceBook, venueAskPriceBook };
        }

        public bool TryRetrieveVenueToB(PriceSide side, out decimal price, out decimal size)
        {
            price = 0m;
            size = 0m;
            PriceObjectInternal priceObjectInternal = this._venuePriceBooks[(int)side].MaxNodeInternal;

            if (PriceObjectInternal.IsNullPrice(priceObjectInternal))
            {
                priceObjectInternal.Release();
                return false;
            }

            price = priceObjectInternal.Price;
            size = priceObjectInternal.SizeBaseAbsRemaining;

            priceObjectInternal.Release();

            return true;
        }
    }
}
