﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    class LmaxMMSystem : VenueMMSystemBase
    {
        public LmaxMMSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                             IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                             PriceSide observedBookSide, IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                             IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
                             IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache, 
                             IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                             IIntegratedSystemSettingsProvider<IVenueMMSystemSettings> settingsProvider,
                             LmaxCounterparty lmaxCounterparty, EventsRateCheckerEx outgoingOrdersRateCheck)
            : base(
                askPriceBook, bidPriceBook, symbol, observedBookSide, lmaxCounterparty, destinationOrdSession, symbolsInfo,
                medianPriceProvider, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, false, outgoingOrdersRateCheck)
        { }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(
            decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol,
                TimeInForce.Day, (LmaxCounterparty)_destinationVenueCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(
            string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxReplaceOrder(
                replacedOrderId, replacingOrderRequest as LmaxClientOrderRequestInfo);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as LmaxClientOrderRequestInfo, 
                integratedTradingSystemIdentification);
        }
    }
}
