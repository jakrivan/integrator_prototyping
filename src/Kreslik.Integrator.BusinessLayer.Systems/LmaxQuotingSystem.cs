﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    internal class BasicLmaxQuotingSystem : LmaxQuotingSystem
    {
        public BasicLmaxQuotingSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                 IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                                 Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                 ISymbolsInfo symbolsInfo,
                                 IMedianProvider<decimal> askMedianPriceProvider,
                                 IMedianProvider<decimal> bidMedianPriceProvider,
                                 IOrderManagement orderManagement, IRiskManager riskManager, 
                                 ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, 
                                 ILogger logger,
                                 IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings> settingsProvider,
                                 IUsdConversionProvider termUsdConversionProvider, IUsdConversionProvider baseUsdConversionProvider,
                                 EventsRateCheckerEx outgoingOrdersRateCheck, ITickerProvider lmaxTickerProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                askMedianPriceProvider, bidMedianPriceProvider, orderManagement, riskManager, tradingGroupsCache,
                unicastInfoForwardingHub,
                logger, settingsProvider, termUsdConversionProvider, baseUsdConversionProvider, null, outgoingOrdersRateCheck,
                lmaxTickerProvider)
        {
        }
    }

    internal class MarginLmaxQuotingSystem : LmaxQuotingSystem
    {
        internal MarginLmaxQuotingSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                 IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                                 Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                 ISymbolsInfo symbolsInfo,
                                 IMedianProvider<decimal> askMedianPriceProvider,
                                 IMedianProvider<decimal> bidMedianPriceProvider,
                                 BankpoolToVenueSessionAdapter orderManagement, IRiskManager riskManager,
                                 ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub,
                                 ILogger logger,
                                 IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings> settingsProvider,
                                 IUsdConversionProvider termUsdConversionProvider, IUsdConversionProvider baseUsdConversionProvider,
                                 IUnderlyingSessionState coverSessionToWatch,
                                 EventsRateCheckerEx outgoingOrdersRateCheck, ITickerProvider lmaxTickerProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                askMedianPriceProvider, bidMedianPriceProvider, orderManagement, riskManager, tradingGroupsCache,
                unicastInfoForwardingHub,
                logger, settingsProvider, termUsdConversionProvider, baseUsdConversionProvider, coverSessionToWatch, outgoingOrdersRateCheck, 
                lmaxTickerProvider)
        {
        }
    }


    internal class LmaxQuotingSystem: VenueQuotingSystemBase
    {
        protected LmaxQuotingSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                 IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                                 Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                 ISymbolsInfo symbolsInfo,
                                 IMedianProvider<decimal> askMedianPriceProvider,
                                 IMedianProvider<decimal> bidMedianPriceProvider,
                                 IOrderManagement orderManagement, IRiskManager riskManager, 
                                 ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                 IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings> settingsProvider,
                                 IUsdConversionProvider termUsdConversionProvider, IUsdConversionProvider baseUsdConversionProvider,
                                 IUnderlyingSessionState coverSessionToWatch,
                                 EventsRateCheckerEx outgoingOrdersRateCheck, ITickerProvider lmaxTickerProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                askMedianPriceProvider, bidMedianPriceProvider, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub,
                logger, settingsProvider, termUsdConversionProvider, baseUsdConversionProvider, coverSessionToWatch, outgoingOrdersRateCheck,
                lmaxTickerProvider)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(string replacedOrderId, Contracts.VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxReplaceOrder(
                replacedOrderId, replacingOrderRequest as LmaxClientOrderRequestInfo);
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price,
                                                                               DealDirection dealDirection,
                                                                               Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol,
                TimeInForce.Day, (LmaxCounterparty) _destinationVenueCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price,
            DealDirection dealDirection, Symbol symbol,
            decimal sizeBaseAbs, bool isRiskRemoving)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol,
                TimeInForce.Day, (LmaxCounterparty)_destinationVenueCounterparty, isRiskRemoving);
        }

        protected override VenueClientOrder CreateVenueOrder(string orderIdentity, string clientIdentity, Contracts.VenueClientOrderRequestInfo venueOrderRequest, Contracts.Internal.IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateLmaxClientOrder(orderIdentity, clientIdentity, venueOrderRequest as LmaxClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }
    }

    public class FastMatchQuotingSystem : VenueQuotingSystemBase
    {
        public FastMatchQuotingSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                 IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                                 Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                 ISymbolsInfo symbolsInfo,
                                 IMedianProvider<decimal> askMedianPriceProvider,
                                 IMedianProvider<decimal> bidMedianPriceProvider,
                                 IOrderManagement orderManagement, IRiskManager riskManager, 
                                 ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                 IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings> settingsProvider,
                                 IUsdConversionProvider termUsdConversionProvider, IUsdConversionProvider baseUsdConversionProvider
                                 , ITickerProvider lmaxTickerProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                askMedianPriceProvider, bidMedianPriceProvider, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub,
                logger, settingsProvider, termUsdConversionProvider, baseUsdConversionProvider, null, null, lmaxTickerProvider)
        {
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(string replacedOrderId, Contracts.VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMReplaceOrder(
                replacedOrderId, replacingOrderRequest as FXCMClientOrderRequestInfo);
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price,
                                                                               DealDirection dealDirection,
                                                                               Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMOrder(
                sizeBaseAbs, 0m, price, dealDirection, symbol,
                TimeInForce.Day, (FXCMCounterparty) _destinationVenueCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price,
                                                                               DealDirection dealDirection,
                                                                               Symbol symbol, decimal sizeBaseAbs, bool isRiskRemoving)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMOrder(
                sizeBaseAbs, 0m, price, dealDirection, symbol,
                TimeInForce.Day, (FXCMCounterparty)_destinationVenueCounterparty, isRiskRemoving);
        }

        protected override VenueClientOrder CreateVenueOrder(string orderIdentity, string clientIdentity, Contracts.VenueClientOrderRequestInfo venueOrderRequest, Contracts.Internal.IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateFXCMClientOrder(orderIdentity, clientIdentity, venueOrderRequest as FXCMClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }
    }
}
