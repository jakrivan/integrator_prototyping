﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class FXCMMMSystem : VenueMMSystemBase
    {
        public FXCMMMSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                             IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                             PriceSide observedBookSide, IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                             IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
                             IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
                             IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                             IIntegratedSystemSettingsProvider<IVenueMMSystemSettings> settingsProvider,
                             FXCMCounterparty fxcmCounterparty)
            : base(
                askPriceBook, bidPriceBook, symbol, observedBookSide, fxcmCounterparty, destinationOrdSession, symbolsInfo,
                medianPriceProvider, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, false, null)
        { }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(
            decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMOrder(
                sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol, 
                TimeInForce.Day, (FXCMCounterparty) _destinationVenueCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(
            string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMReplaceOrder(
                replacedOrderId, replacingOrderRequest as FXCMClientOrderRequestInfo);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as FXCMClientOrderRequestInfo, 
                integratedTradingSystemIdentification);
        }
    }
}
