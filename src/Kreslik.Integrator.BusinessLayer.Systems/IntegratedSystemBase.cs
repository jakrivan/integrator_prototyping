﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.RiskManagement;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public interface IInputsProviderForIntegratedSystem<out T> where T : class, IIntegratedSystemSettings
    {
        //pricing
        PriceObjectInternal MaxNodeInternal { get; }
        event Action<PriceObjectInternal> ChangeableBookTopImproved;
        event Action<PriceObjectInternal> ChangeableBookTopDeteriorated;
        event Action<PriceObjectInternal> ChangeableBookTopReplaced;

        //destination session
        event Action OnStarted;
        event Action OnStopped;
        //this might be wrapped - we need just info about running status
        SessionState State { get; }

        //Symbols info
        bool TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal price, out decimal result);
        bool IsSupported(Symbol symbol, TradingTargetType tradingTargetType);
        bool IsSupported(Currency currency);

        //RiskMgmt
        event Action<bool> GoFlatOnlyStateChanged;
        event Action<OrderTransmissionStatusChangedEventArgs> OrderTransmissionChanged;
        bool IsTransmissionDisabled { get; }
        bool IsGoFlatOnlyEnabled { get; }

        //settings
        IIntegratedSystemSettingsProvider<T> IntegratedSystemSettingsProvider { get; }


        //various
        Symbol Symbol { get; }
        Counterparty DestinationVenueCounterparty { get; }
        IntegratedTradingSystemType IntegratedTradingSystemType { get; }
        //observedBookSide == PriceSide.Ask ? "S" : "B"    vs string.empty
        string SystemSubtypeDistinguisher { get; }
        bool EnableVerboseLogging { get; }
    }

    public interface IInputsProviderForIntegratedMMSystem<out T> : IInputsProviderForIntegratedSystem<T> where T : class, IIntegratedSystemSettings
    {
        //MidPriceMedian
        decimal Median { get; }

        PriceSide ObservedBookSide { get; }
    }

    public interface IInputsProviderForIntegratedCrossSystem<out T> : IInputsProviderForIntegratedSystem<T> where T : class, IIntegratedSystemSettings
    {
        //bool TryRetrieveVenueToB(PriceSide side, out decimal price, out decimal size, out PriceObjectInternal priceObjectInternal);
    }

    public interface IInputsProviderForIntegratedQuotingSystem<out T> : IInputsProviderForIntegratedSystem<T> where T : class, IIntegratedSystemSettings
    {
        //MidPriceMedian
        decimal Median { get; }

        //todo: split this into interface methods
        StopLossTrackingModule StopLossTrackingModule { get; }
    }

    public interface IOutputsRouterFromIntegratedSystem
    {
        //order management
        IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway);
        IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo, ITakerUnicastInfoForwarder dispatchGateway);
    }

    public class OutputsRouterFromIntegratedSystem: IOutputsRouterFromIntegratedSystem
    {
        private readonly IOrderManagement _orderManagement;

        public OutputsRouterFromIntegratedSystem(IOrderManagement orderManagement)
        {
            this._orderManagement = orderManagement;
        }

        public IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this._orderManagement.RegisterOrder(clientOrder, dispatchGateway);
        }

        public IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo, ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this._orderManagement.CancelOrder(cancelOrderRequestInfo, dispatchGateway);
        }
    }

    public class InputsProviderForIntegratedCrossSystem<T> : IInputsProviderForIntegratedCrossSystem<T> where T : class, IIntegratedSystemSettings
    {
        private IVenueToBProvider _venueToBProvider;

        //public bool TryRetrieveVenueToB(PriceSide side, out decimal price, out decimal size, out PriceObjectInternal priceObjectInternal)
        //{
        //    return _venueToBProvider.TryRetrieveVenueToB(side, out price, out size, out priceObjectInternal);
        //}

        public PriceObjectInternal MaxNodeInternal
        {
            get { throw new NotImplementedException(); }
        }

        public event Action<PriceObjectInternal> ChangeableBookTopImproved;

        public event Action<PriceObjectInternal> ChangeableBookTopDeteriorated;

        public event Action<PriceObjectInternal> ChangeableBookTopReplaced;

        public event Action OnStarted;

        public event Action OnStopped;

        public SessionState State
        {
            get { throw new NotImplementedException(); }
        }

        public bool TryRoundToSupportedPriceIncrement(Symbol symbol, TradingTargetType tradingTargetType, RoundingKind roundingKind, decimal price, out decimal result)
        {
            throw new NotImplementedException();
        }

        public bool IsSupported(Symbol symbol, TradingTargetType tradingTargetType)
        {
            throw new NotImplementedException();
        }

        public bool IsSupported(Currency currency)
        {
            throw new NotImplementedException();
        }

        public event Action<bool> GoFlatOnlyStateChanged;

        public event Action<OrderTransmissionStatusChangedEventArgs> OrderTransmissionChanged;

        public bool IsTransmissionDisabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsGoFlatOnlyEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public IIntegratedSystemSettingsProvider<T> IntegratedSystemSettingsProvider
        {
            get { throw new NotImplementedException(); }
        }

        public Symbol Symbol
        {
            get { throw new NotImplementedException(); }
        }

        public Counterparty DestinationVenueCounterparty
        {
            get { throw new NotImplementedException(); }
        }

        public IntegratedTradingSystemType IntegratedTradingSystemType
        {
            get { throw new NotImplementedException(); }
        }

        public string SystemSubtypeDistinguisher
        {
            get { throw new NotImplementedException(); }
        }

        public bool EnableVerboseLogging
        {
            get { throw new NotImplementedException(); }
        }
    }

    public abstract class IntegratedSystemBase<T>: ITakerUnicastInfoForwarder where T : class, IIntegratedSystemSettings
    {
        protected ILogger _logger;
        //Those two books are used to get reference prices - systems subscribes to their events
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _askReferencePriceBook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _bidReferencePriceBook;
        protected Symbol _symbol;
        protected Counterparty _destinationVenueCounterparty;

        private const int ENABLED_AND_READY_FOR_SENDING = 1;
        private const int DISABLED_OR_SENDING_NEXT_REQUEST = 0;
        //Not marking as volatile - as we want quick reads on each event, and volatile access only if attempted to trade
        private /*volatile*/ int _requestsState;
        protected int _headOrdersCnt = 0;
        protected int _bankSubOrdersCnt = 0;

        protected IOrderManagement _orderManagement;
        private ITakerUnicastInfoForwarder _unicastInfoForwarder;
        protected IRiskManager _riskManager;
        private ISymbolTrustworthyWatcher _symbolTrustworthyWatcher;
        protected ISymbolsInfo _symbolsInfo;
        private IUnderlyingSessionState _destinationOrdSession;
        protected T _settings;
        private IIntegratedSystemSettingsProvider<T> _settingsProvider;
        protected volatile bool _enabledInSettings;
        protected string _systemFriendlyName;
        protected string _systemIdentity;
        protected IntegratedTradingSystemIdentification _integratedTradingSystemIdentification;
        protected bool _verboseLoggingEnabled;
        protected volatile bool _enabled = false;
        protected DateTime _disabledTime = DateTime.UtcNow;
        protected decimal _orderMinimumSizeIncrement = 1000m;
        private PriceSide? _outgoingPriceSide;


        public IntegratedSystemBase(IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferencePriceBook,
                               IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferencePriceBook,
                               Symbol symbol,
                               Counterparty destinationVenueCounterparty,
                               IUnderlyingSessionState destinationOrdSession,
                               ISymbolsInfo symbolsInfo,
                               IOrderManagement orderManagement,
                               IRiskManager riskManager,
                               ITradingGroupsCache tradingGroupsCache,
                               IUnicastInfoForwardingHub unicastInfoForwardingHub,
                               ILogger logger,
                               IIntegratedSystemSettingsProvider<T> settingsProvider,
                                //MM vs Cross
                               IntegratedTradingSystemType integratedTradingSystemType,
                                //observedBookSide == PriceSide.Ask ? "S" : "B"    vs string.empty
                               string systemSubtypeDistinguisher,
                               bool enableVerboseLogging,
                               PriceSide? outgoingPriceSide
                               
            )
        {
            this._askReferencePriceBook = askReferencePriceBook;
            this._bidReferencePriceBook = bidReferencePriceBook;
            this._symbol = symbol;
            this._destinationVenueCounterparty = destinationVenueCounterparty;
            this._symbolsInfo = symbolsInfo;
            this._orderManagement = orderManagement;
            this._riskManager = riskManager;
            this._symbolTrustworthyWatcher = riskManager.GetSymbolTrustworthyWatcher(symbol);
            this._logger = logger;
            this._settingsProvider = settingsProvider;
            this._settings = settingsProvider.CurrentSettings;
            this._outgoingPriceSide = outgoingPriceSide;

            if (destinationVenueCounterparty == Counterparty.LM2)
                _orderMinimumSizeIncrement = 10000m;

            this._verboseLoggingEnabled = enableVerboseLogging;
            this._systemIdentity = _settings.TradingSystemId.ToString();
            this._integratedTradingSystemIdentification =
                new IntegratedTradingSystemIdentification(_settings.TradingSystemId, integratedTradingSystemType,
                    destinationVenueCounterparty, tradingGroupsCache.GeTradingSystemGroup(_settings.TradingSystemGroupId));
            this._systemFriendlyName = string.Format("{0}{1}{2}_{3}_{4}__{5}", destinationVenueCounterparty,
                integratedTradingSystemType, systemSubtypeDistinguisher, symbol, _settings.TradingSystemId,
                _integratedTradingSystemIdentification.TradingSystemGroup.LastKnownName);
            this._unicastInfoForwarder = unicastInfoForwardingHub.CreateWrappedUnicastInfoForwarder(this);

            this._destinationOrdSession = destinationOrdSession;
            destinationOrdSession.OnStopInitiated += this.OnSessionStoppInitiated;
            destinationOrdSession.OnStarted += this.OnSessionStarted;

            settingsProvider.EnableChanged += this.FlipStateAdapter;
            settingsProvider.SideEnablementChanged += this.FlipSidedStateAdapter;
            settingsProvider.GoFlatOnlyChanged += this.FlipGoFlatOnlyPerSystem;
            settingsProvider.OrdersTransmissionDisabledChanged += this.FlipOrdersTransmissionDisabledPerSystem;
            settingsProvider.ResetRequested += this.Reset;
            settingsProvider.SettingsChanged += this.UpdateSettings;
            settingsProvider.SystemDeleted += this.DeactivateSelf;
            settingsProvider.RegularCheck += RegularCheck;
            riskManager.GoFlatOnlyStateChanged += this.FlipGoFlatOnly;
            riskManager.OrderTransmissionChanged += RiskManagerOnOrderTransmissionChanged;
            _symbolTrustworthyWatcher.TrustworthynessChanged += SymbolTrustworthyWatcherOnTrustworthynessChanged;
            TradingHoursHelper.Instance.MarketRollingOver += OnMarketRollingOver;
            this._enabledInSettings = settingsProvider.Enabled && settingsProvider.IsOutgoingPriceSideEnabled(outgoingPriceSide);
            //Carefull! we cannot call this directly here as derived classes ctors haven't yet
            // run and so the derived classes are not yet fully initialized
            TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(1), () =>
            {
                this.SettingsUpdating(this._settings);
                if (this._enabledInSettings) this.TryRegisterSystem();
                this.EnableIfAllowed();
            });
        }

        protected virtual void RegularCheck(DateTime now)
        {
            if (this._currentPositionBasePol != AtomicSize.ZERO
                && now - _currentPositionBasePol.ZeroLastCrossed > TimeSpan.FromSeconds(5))
            {
                this._logger.Log(LogLevel.Fatal, "{0} found to have nonzero position ({1}), while it last crossed 0 position before {2} (at {3:hh:mm:ss.fffffff} UTC). HardBlocking",
                    this._systemFriendlyName, _currentPositionBasePol, now - _currentPositionBasePol.ZeroLastCrossed, _currentPositionBasePol.ZeroLastCrossed);
                this.HardBlock();
            }
        }

        private void DeactivateSelf(IIntegratedSystemSettingsProvider<T> sender)
        {
            this._logger.Log(LogLevel.Trace, "{0} system got signal to delete itself", this._systemFriendlyName);

            _enabledInSettings = false;

            //Remove all events subscriptions - as it's how we are GC rooted
            sender.EnableChanged -= this.FlipStateAdapter;
            sender.SideEnablementChanged -= this.FlipSidedStateAdapter;
            sender.GoFlatOnlyChanged -= this.FlipGoFlatOnlyPerSystem;
            sender.OrdersTransmissionDisabledChanged -= this.FlipOrdersTransmissionDisabledPerSystem;
            sender.ResetRequested -= this.Reset;
            sender.SettingsChanged -= this.UpdateSettings;
            sender.SystemDeleted -= this.DeactivateSelf;
            sender.RegularCheck -= RegularCheck;
            this._riskManager.GoFlatOnlyStateChanged -= this.FlipGoFlatOnly;
            this._riskManager.OrderTransmissionChanged -= RiskManagerOnOrderTransmissionChanged;
            this._symbolTrustworthyWatcher.TrustworthynessChanged -= SymbolTrustworthyWatcherOnTrustworthynessChanged;
            this._destinationOrdSession.OnStopInitiated -= this.OnSessionStoppInitiated;
            this._destinationOrdSession.OnStarted -= this.OnSessionStarted;
            TradingHoursHelper.Instance.MarketRollingOver -= OnMarketRollingOver;

            //this can still send some cancels - we'll be temporary rooted by OrderManager until we receive final response
            this.HardBlock(false);

            this.AfterDeletionProcedure();
        }

        private void OnMarketRollingOver()
        {
            bool waitNeeded = this._enabled;
            this.OnShouldNotBeActive();
            if(waitNeeded)
                Thread.Sleep(TimeSpan.FromMilliseconds(500));

            this.ResetInternal(true);
            //this.OnShouldNotBeActive();
        }

        private void OnShouldNotBeActive()
        {
            if (this._enabled || InterlockedEx.Read(ref this._requestsState) == ENABLED_AND_READY_FOR_SENDING)
            {
                // it can happen that two different disablement (e.g. disable, symbol trust) flips their conditions and then start invoking events
                // Each of the events will not disable - as it will see state in which it is not the only condition that should lead to disablement (e.g. on GFO, everything else
                //  should be enabled to trigger disablement of system, but if also SysmbolTrust is disabled then nothing happens - as the thought is that SymbolTrust already did disable system - but it might be still waiting to do that)
                this._logger.Log(LogLevel.Error, "Trading system {0} was found not fully disabled during disable period (killsw/rollover/GFO/Symbol trust ...) - so rather explicitly blocking", this._systemFriendlyName);
                this.Disable();
            }
        }

        protected virtual bool IsAllowedToBeEnabled { get { return true; } } 

        private void RiskManagerOnOrderTransmissionChanged(OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs)
        {
            //was the system running or is this the only change missing to make it running? (prevent unnecesary flips)
            if (this._enabledInSettings && !this._settingsProvider.OrdersTransmissionDisabled
                && (this.AllowActivityDuringTradingConstrainingSwitches
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && _destinationOrdSession.IsReadyAndOpen
                && this.IsAllowedToBeEnabled)
            {
                if (orderTransmissionStatusChangedEventArgs.IsOrderTransmissionEnabled)
                    Enable();
                else
                    Disable();
            }

            if(!orderTransmissionStatusChangedEventArgs.IsOrderTransmissionEnabled)
                this.OnShouldNotBeActive();
        }

        private void FlipStateAdapter(bool enable)
        {
            this.FlipState(enable && this._settingsProvider.IsOutgoingPriceSideEnabled(this._outgoingPriceSide));
        }

        private void FlipSidedStateAdapter(bool askEnable, bool bidEnable)
        {
            this.FlipState(this._settingsProvider.Enabled && this._settingsProvider.IsOutgoingPriceSideEnabled(this._outgoingPriceSide));
        }

        private void FlipState(bool enable)
        {
            //do nothing if nothing changed (since two events lead here)
            if(this._enabledInSettings == enable)
                return;

            //We want this order: 
            //  1) Register 
            //  2) Enble 
            //  3) Disable 
            //  4) Unregister

            this._enabledInSettings = enable;

            if(enable)
                this.TryRegisterSystem();

            //was the system running or is this the only change missing to make it running? (prevent unnecesary flips)
            if (!this._settingsProvider.OrdersTransmissionDisabled 
                && (this.AllowActivityDuringTradingConstrainingSwitches
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && !this._riskManager.IsTransmissionDisabled &&
                _destinationOrdSession.IsReadyAndOpen && 
                 this.IsAllowedToBeEnabled)
            {
                if (enable)
                    Enable();
                else
                    Disable();
            }
            else if(!enable)
            {
                this._logger.Log(LogLevel.Info,
                                 "Disabling already inactivated system {0}, so invoking AfterDisableProcedure - if system needs to close positions.",
                                 this._systemFriendlyName);
                this.AfterDisablingProcedure();
            }

            if (!enable)
            {
                this.OnShouldNotBeActive();
                this.UnregisterSystem();
            }
        }

        private void SymbolTrustworthyWatcherOnTrustworthynessChanged(bool symbolTrusted)
        {
            if(this.AllowActivityDuringTradingConstrainingSwitches)
                return;

            //was the system running or is this the only change missing to make it running? (prevent unnecesary flips)
            if (this._enabledInSettings && !this._settingsProvider.OrdersTransmissionDisabled
                && (this.AllowActivityDuringTradingConstrainingSwitches || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled))
                && !this._riskManager.IsTransmissionDisabled &&
                _destinationOrdSession.IsReadyAndOpen && this.IsAllowedToBeEnabled)
            {
                if (symbolTrusted)
                    Enable();
                else
                    Disable();
            }

            if (!symbolTrusted)
                this.OnShouldNotBeActive();
        }

        private void FlipGoFlatOnlyPerSystem(bool goFlatOnly)
        {
            if(AllowActivityDuringTradingConstrainingSwitches)
                return;

            //was the system running or is this the only change missing to make it running? (prevent unnecesary flips)
            if (this._enabledInSettings  && !this._settingsProvider.OrdersTransmissionDisabled && !this._riskManager.IsGoFlatOnlyEnabled
                && !this._riskManager.IsTransmissionDisabled && _destinationOrdSession.IsReadyAndOpen
                && this._symbolTrustworthyWatcher.IsTrustworthy && this.IsAllowedToBeEnabled)
            {
                if (!goFlatOnly)
                    Enable();
                else
                    Disable();
            }

            if(goFlatOnly)
                this.OnShouldNotBeActive();
        }

        private void FlipOrdersTransmissionDisabledPerSystem(bool disableTransmission)
        {
            //was the system running or is this the only change missing to make it running? (prevent unnecesary flips)
            if (this._enabledInSettings
                && (this.AllowActivityDuringTradingConstrainingSwitches
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && !this._riskManager.IsTransmissionDisabled && _destinationOrdSession.IsReadyAndOpen
                && this.IsAllowedToBeEnabled)
            {
                if (!disableTransmission)
                    Enable();
                else
                    Disable();
            }

            if(disableTransmission)
                this.OnShouldNotBeActive();
        }

        private void FlipGoFlatOnly(bool goFlatOnly)
        {
            if(AllowActivityDuringTradingConstrainingSwitches)
                return;

            //was the system running or is this the only change missing to make it running? (prevent unnecesary flips)
            if (this._enabledInSettings && !this._settingsProvider.GoFlatOnly && !this._settingsProvider.OrdersTransmissionDisabled
                && !this._riskManager.IsTransmissionDisabled && _destinationOrdSession.IsReadyAndOpen
                && this._symbolTrustworthyWatcher.IsTrustworthy && this.IsAllowedToBeEnabled)
            {
                if (!goFlatOnly)
                    Enable();
                else
                    Disable();
            }

            if(goFlatOnly)
                this.OnShouldNotBeActive();
        }

        private void OnSessionStarted()
        {
            if (this._enabledInSettings && !this._settingsProvider.OrdersTransmissionDisabled
                && (this.AllowActivityDuringTradingConstrainingSwitches
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && !this._riskManager.IsTransmissionDisabled
                && this.IsAllowedToBeEnabled)
                this.Enable();
        }

        private void OnSessionStoppInitiated()
        {
            if (this._enabledInSettings &&
                !this._settingsProvider.OrdersTransmissionDisabled
                && (this.AllowActivityDuringTradingConstrainingSwitches
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && !this._riskManager.IsTransmissionDisabled
                && this.IsAllowedToBeEnabled
                //prevent actions on multiple stops without starts (case of streaming session with two subsessions)
                && this._enabled)
                    this.Disable();

            this.OnShouldNotBeActive();
        }

        protected void OnSystemAllowedStateChanged(bool allowed)
        {
            if (this._enabledInSettings &&
                !this._settingsProvider.OrdersTransmissionDisabled
                && (this.AllowActivityDuringTradingConstrainingSwitches
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && !this._riskManager.IsTransmissionDisabled
                && _destinationOrdSession.IsReadyAndOpen)
            {
                //prevent actions on multiple stops without starts (case of streaming session with two subsessions)
                if(!allowed && this._enabled)
                    this.Disable();
                if(allowed && !this._enabled)
                    this.Enable();
            }

            if(!allowed)
                this.OnShouldNotBeActive();
        }

        private void UpdateSettings(T newSettings)
        {
            SettingsUpdating(newSettings);
            Interlocked.Exchange(ref this._settings, newSettings);
            if(this._enabled)
                ProcessEventWithRecursionCheck();
        }

        protected bool IsPriceValid(PriceObject priceObject, PriceSide expectedSide)
        {
            if (priceObject.Side != expectedSide)
            {
                this._logger.Log(LogLevel.Fatal, "System {0} receiving price on mismatched side (actual:{1}, expected:{2})",
                    this._systemFriendlyName, priceObject.Side, expectedSide);
                return false;
            }

            return this.IsPriceValid(priceObject);
        }

        protected bool IsPriceValid(PriceObject priceObject)
        {
            if (priceObject.Symbol != this._symbol)
            {
                this._logger.Log(LogLevel.Fatal, "System {0} receiving price on mismatched symbol ({1})",
                    this._systemFriendlyName, priceObject.Symbol);
                return false;
            }

            return true;
        }

        protected bool CheckTargetSymbolSupported(ISymbolsInfo symbolsInfo, Symbol symbol,
                                                TradingTargetType tradingTargetType)
        {
            bool supported = false;

            try
            {
                supported = symbolsInfo.IsSupported(symbol, tradingTargetType);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Error, "Exception during condition checking", e);
            }

            if (!supported)
            {
                this._logger.Log(LogLevel.Fatal, "{0} not enabled for {1}, ignoring the attempt for enabling system (can be retried)", symbol, tradingTargetType);
            }

            if (!symbolsInfo.IsSupported(symbol.BaseCurrency))
            {
                this._logger.Log(LogLevel.Fatal, "{0} currency (in {1}) is not supported by PB, ignoring the attempt for enabling of the system", symbol.BaseCurrency, symbol);
                supported = false;
            }

            if (!symbolsInfo.IsSupported(symbol.TermCurrency))
            {
                this._logger.Log(LogLevel.Fatal, "{0} currency (in {1}) is not supported by PB, ignoring the attempt for enabling of the system", symbol.TermCurrency, symbol);
                supported = false;
            }

            return supported;
        }

        protected abstract void ProcessEvent();
        protected virtual void SettingsUpdating(T newSettings) { }
        protected virtual void AfterDeletionProcedure() { }
        protected abstract void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook);
        protected abstract void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook);
        protected virtual void AfterDisablingProcedure() { }
        protected virtual void DuringEnableProcedure() { }
        protected virtual void TryRegisterSystem() { }
        protected virtual void UnregisterSystem() { }
        protected abstract void OnResetProcedure();
        protected abstract void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo);
        protected abstract DealDirection CurrentHeadOrderPolarity { get; }
        protected virtual bool AllowNonflatPositionDuringEnable { get { return false; } }
        protected virtual bool AllowActivityDuringTradingConstrainingSwitches { get { return false; } }

        protected abstract VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
                                                                               decimal sizeBaseAbs);

        protected abstract VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification);

        private void AfterDisablingProcedureInternal()
        {
            this._logger.Log(LogLevel.Info, "System {0} detected situation when it should be flat and without positions - ensuring this by after disable procedure", this._systemFriendlyName);
            AfterDisablingProcedure();
        }

        protected virtual Counterparty AlternateDestinationCounterparty { get { return Counterparty.NULL; } }

        private void Enable()
        {
            if (!this._currentPositionBasePol.InterlockedIsZero())
            {
                if (this.AllowNonflatPositionDuringEnable)
                {
                    this._logger.Log(LogLevel.Error,
                                     "System {0} allowed attempt to enable self despite nonflat position - Polarized pozition: {1} of {2}.",
                                     this._systemFriendlyName, this._currentPositionBasePol, this._symbol);
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                                     "Attempt to enable system {0} with nonflat position - Polarized pozition: {1} of {2}. Ignoring - must be unblocked first",
                                     this._systemFriendlyName, this._currentPositionBasePol, this._symbol);
                    return;
                }
            }

            this._logger.Log(LogLevel.Trace, "Enabling {0} system", this._systemFriendlyName);

            if (!CheckTargetSymbolSupported(this._symbolsInfo, this._symbol, _destinationVenueCounterparty.TradingTargetType))
                return;
            if (AlternateDestinationCounterparty != Counterparty.NULL && !CheckTargetSymbolSupported(this._symbolsInfo, this._symbol, AlternateDestinationCounterparty.TradingTargetType))
                return;
            if (!CheckTargetSymbolSupported(this._symbolsInfo, this._symbol, TradingTargetType.BankPool))
                return;

            if (this._enabled)
            {
                this._logger.Log(LogLevel.Warn, "Attempt to enable already enabled system {0}. Ignoring", this._systemIdentity);
                return;
            }
            //Prevent double subscriptions
            this.UnsubscribeFromPricingEvents();

            this._enabled = true;
            this._disabledTime = DateTime.MaxValue;
            this._ordersSentAfterDisabled = 0;

            //Subscribe
            SubscribeToBookEvents(_askReferencePriceBook);
            SubscribeToBookEvents(_bidReferencePriceBook);
            this.DuringEnableProcedure();

            ExitSingleAccessRegion();

            ProcessEventWithRecursionCheck();
        }

        private void UnsubscribeFromPricingEvents()
        {
            UnsubscribeFromBookEvents(_askReferencePriceBook);
            UnsubscribeFromBookEvents(_bidReferencePriceBook);
        }

        protected void SwapReferenceBooks(IChangeablePriceBook<PriceObjectInternal, Counterparty> askReferencePriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidReferencePriceBook)
        {
            this.UnsubscribeFromPricingEvents();

            this._askReferencePriceBook = askReferencePriceBook;
            this._bidReferencePriceBook = bidReferencePriceBook;

            if (this._enabled)
            {
                //Subscribe
                SubscribeToBookEvents(_askReferencePriceBook);
                SubscribeToBookEvents(_bidReferencePriceBook);
            }
        }

        private void Disable()
        {
            this.Disable(false);
        }

        public void DisableExternaly()
        {
            this.Disable(true);
        }

        private void Disable(bool propagateToDb)
        {
            this._logger.Log(LogLevel.Trace, "Disabling {0} system", this._systemFriendlyName);

            if(propagateToDb)
                _enabledInSettings = false;

            //unsubscriptions
            this.UnsubscribeFromPricingEvents();

            this._disabledTime = DateTime.UtcNow;
            this._enabled = false;

            //force prevention of sending other orders, however order sending can be already in process
            // (or system can be already in the single access region and attempting to send order later)
            // In case of blocking systems MUST ensure position cancellation after each position update from Venue
            BlockSingleAccessRegion();

            AfterDisablingProcedureInternal();

            if (propagateToDb &&
                !IntegratedSystemsSettingsProvider.Instance.TryDisableSystem(this._settings.TradingSystemId))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Integrator couldn't propagate disabled flag for system {0} - it is still disabled internally, but might not appear so on the web. It should be reset from the web.",
                    this._systemFriendlyName);
            }
        }



        protected bool IsTradingUnexpectedNow
        {
            get { return DateTime.UtcNow.AddSeconds(-1) > this._disabledTime; }
        }

        internal void HardBlock()
        {
            this.HardBlock(true);
        }

        private void HardBlock(bool propagateToDb)
        {
            //we cannot use ordinary check as that can invoke HardBlock - so we would have recursion due to recursion check
            if (!EnterLasteResortRecursionCheckedRegion())
                return;
            this._logger.Log(LogLevel.Error, "Hard blocking system {0}", this._systemFriendlyName);
            this._enabledInSettings = false;
            this.Disable();
            this.UnregisterSystem();
            this._settingsProvider.RegularCheck -= RegularCheck;

            if (!_currentPositionBasePol.InterlockedIsZero())
            {
                this._logger.Log(LogLevel.Error, "{0} is getting into blocked state but it's position(pol) is: {1}. This might not be closed automatically as system is blocked (but closing order might still be pending). Will recheck",
                        this._systemFriendlyName, _currentPositionBasePol);

                Task.Delay(TimeSpan.FromSeconds(1)).ContinueWith(t =>
                {
                    if (_currentPositionBasePol.InterlockedIsZero())
                    {
                        this._logger.Log(LogLevel.Info, "{0} Was able to flat out it's position after hardblocking itself",
                        this._systemFriendlyName, _currentPositionBasePol);
                    }
                    else
                    {
                        string errorMsg = string.Format(
                            "{0} is got into blocked state but it's position(pol) is: {1} even a second after blocked. This won't likely be closed automatically as system is blocked. (closing order might be still pending, but it's unlikely now - second after being blocked)",
                            this._systemFriendlyName, _currentPositionBasePol);
                        this._riskManager.TurnOnGoFlatOnly(errorMsg);
                    }
                });
            }

            if (propagateToDb && !IntegratedSystemsSettingsProvider.Instance.TryHardBlockSystem(this._settings.TradingSystemId))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Integrator couldn't propagate blocked flag for system {0} - it is still blocked internally, but might not appear so on the web. It should be reset from the web.",
                    this._systemFriendlyName);
            }

            this.ExitRecursionCheckedRegion();
        }

        private void EnableIfAllowed()
        {
            //if all flags shows it should have been enabled but it s not (_enabled) -> then enable
            //  (to prevent double enable which can exit single access region)
            if (this._enabledInSettings && !this._settingsProvider.OrdersTransmissionDisabled
                && (this.AllowActivityDuringTradingConstrainingSwitches 
                        || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                && !this._riskManager.IsTransmissionDisabled 
                && _destinationOrdSession.IsReadyAndOpen
                && this.IsAllowedToBeEnabled && !_enabled)
            {
                this.Enable();
            }
        }

        protected void DisableIfNeeded()
        {
            //if all flags shows it should have been enabled but it s not (_enabled) -> then enable
            //  (to prevent double enable which can exit single access region)
            if (!
                (this._enabledInSettings && !this._settingsProvider.OrdersTransmissionDisabled
                 && (this.AllowActivityDuringTradingConstrainingSwitches 
                    || !(this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                 && !this._riskManager.IsTransmissionDisabled
                 && _destinationOrdSession.IsReadyAndOpen
                 && this.IsAllowedToBeEnabled
                 )
                 && _enabled
                )
            {
                this.Disable();
            }
        }


        //Will be used to manualy Reset system after broken order or so
        public void Reset()
        {
            this._logger.Log(LogLevel.Trace, "Resetting {0} system", this._systemFriendlyName);
            this.ResetInternal(true);
        }

        protected bool CanSubmitSoftPrice(StreamingPrice streamingPrice)
        {
            if (this._settingsProvider.OrdersTransmissionDisabled || this._riskManager.IsTransmissionDisabled)
            {
                this._logger.Log(LogLevel.Error, "Cannot sent orders - order transmission is currently disabled");
                return false;
            }

            if (!this._enabled || !this._enabledInSettings)
            {
                this._logger.Log(LogLevel.Fatal, "Attempt to send soft price order while the system is not enabled. {0}",
                    this._systemFriendlyName);
                return false;
            }

            //this stays here (instead of risk mgmt) since we can spare one indexing operation
            if (!this._symbolTrustworthyWatcher.IsTrustworthy)
            {
                this._logger.Log(LogLevel.Error, this._symbol + " is not trusted.");
                return false;
            }

            return this._riskManager.IsTradeRequestLikelyToPass(streamingPrice);
        }

        protected bool CanSubmitOrderRequest(IIntegratorOrderInfo orderInfo)
        {
            if (!this._riskManager.IsTradeRequestLikelyToPass(orderInfo))
            {
                return false;
            }

            if (!this._symbolTrustworthyWatcher.IsTrustworthy)
            {
                return false;
            }

            return true;
        }

        protected IntegratorRequestResult RegisterOrder(IClientOrder clientOrder)
        {
            if (this._settingsProvider.OrdersTransmissionDisabled || this._riskManager.IsTransmissionDisabled)
            {
                return
                    IntegratorRequestResult.CreateFailedResult(
                        "Cannot sent orders - order transmission is currently disabled");
            }


            if (!this._enabled || !this._enabledInSettings)
            {
                if (!clientOrder.IsRiskRemovingOrder)
                {
                    this._logger.Log(LogLevel.Fatal, "Attempt to register risk adding order while the system is not enabled. {0}",
                        clientOrder);
                    return IntegratorRequestResult.CreateFailedResult("System is disabled");
                }

                if (Interlocked.Increment(ref this._ordersSentAfterDisabled) -
                    InterlockedEx.Read(ref this._reissueAttempts) > 5)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "Attempt to register repeated risk removing order while the system is not enabled (at least 5 orders that were not result of reissuing broken order). {0}",
                        clientOrder);
                    return IntegratorRequestResult.CreateFailedResult("System is disabled");
                }
            }

            return this._orderManagement.RegisterOrder(clientOrder, this._unicastInfoForwarder);
        }

        protected void RequestOrderCancel(VenueClientOrder clientOrderToCancel)
        {
            if (clientOrderToCancel == null)
                return;

            CancelOrderRequestInfo cri = new CancelOrderRequestInfo(clientOrderToCancel.ClientOrderIdentity,
                                                                        this._systemIdentity, clientOrderToCancel.Counterparty);
            var result = this.CancelOrder(cri);

            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system failed to cancel order {1}. Reason: {2}. If this happened during disabling and venue session is is running order can be STILL ACTIVE EXTERNALLY. It should be reviewed manually or system should be (enabled and) disabled again to force another attemppt for cancel.",
                                 this._systemFriendlyName, clientOrderToCancel, result.ErrorMessage);
            }
        }

        protected IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo)
        {
            return this._orderManagement.CancelOrder(cancelOrderRequestInfo, this._unicastInfoForwarder);
        }

        protected void CheckUnknownClientOrderUpdate(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {

            //if cancel is already requested and then aggressive cancel is requested in the meantime - it can get answer after the
            // first cancel goes successfuly through (and we cannot reliably use weak reference as _nextOrder is only hard reference to it)
            if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo 
                &&
                //cancel in progress only means that we've issued request - it's not comming from counterparty
                (
                    clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress 
                    ||
                    //order is already unknown
                    (
                        !this._orderManagement.HasVenueOrder(this.AlternateDestinationCounterparty,clientOrderUpdateInfo.ClientOrderIdentity)
                        &&
                        !this._orderManagement.HasVenueOrder(this._destinationVenueCounterparty,clientOrderUpdateInfo.ClientOrderIdentity)
                    )
                )
                )
                this._logger.Log(LogLevel.Info,
                    "{0} system receiving order update for (already) unknown order [{1}]: {2}",
                    this._systemFriendlyName, clientOrderUpdateInfo.ClientOrderIdentity, clientOrderUpdateInfo);
            else
                this._logger.Log(clientOrderUpdateInfo.SizeBaseAbsTotalRemaining > 0 ? LogLevel.Fatal : LogLevel.Error,
                    "{0} system receiving unexpected order update for (already) unknown order [{1}]: {2}, remaining qty: {3}",
                    this._systemFriendlyName, clientOrderUpdateInfo.ClientOrderIdentity, clientOrderUpdateInfo, clientOrderUpdateInfo.SizeBaseAbsTotalRemaining);
        }

        protected void ResetInternal(bool force)
        {
            if (!this._currentPositionBasePol.InterlockedIsZero())
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system position [{1}] is not flat as it was expected. Situation should be reviewed. System will {2} reset its position and will {2} continue trading now (if or once enabled).",
                                 this._systemFriendlyName, _currentPositionBasePol, force ? string.Empty : "NOT");
                if (force)
                {
                    this._currentPositionBasePol.InterlockedZeroOut();
                }
                else
                {
                    this.HardBlock();
                    return;
                }
            }

            Volatile.Write(ref this._bankSubOrdersCnt, 0);
            Volatile.Write(ref this._reissueAttempts, 0);

            OnResetProcedure();

            //if this was external reset request - re enable system if it is enabled
            if (force)
            {
                this._enabledInSettings = this._settingsProvider.Enabled &&
                                          this._settingsProvider.IsOutgoingPriceSideEnabled(this._outgoingPriceSide);
                this.EnableIfAllowed();
            }

            ExitSingleAccessRegionInternal();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected bool TryEnterSingleAccessRegion()
        {
            return (Interlocked.Exchange(ref this._requestsState, DISABLED_OR_SENDING_NEXT_REQUEST) ==
                    ENABLED_AND_READY_FOR_SENDING);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void ExitSingleAccessRegion()
        {
            this.ExitSingleAccessRegionInternal();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ExitSingleAccessRegionInternal()
        {
            //Warning - system could have been in single access region during disabling procedure
            // make sure to reenable only if this wasn't the case

            bool shouldBeFlat = this.ShouldBeFlatWithNoPendingPosition;
            if (shouldBeFlat)
            {
                this._logger.Log(LogLevel.Info,
                    "System {0} trying to exit single access region when positions should be flat.",
                    this._systemFriendlyName);
                this.AfterDisablingProcedure();
            }
            else
            {
                Volatile.Write(ref this._requestsState, ENABLED_AND_READY_FOR_SENDING);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void BlockSingleAccessRegion()
        {
            Volatile.Write(ref this._requestsState, DISABLED_OR_SENDING_NEXT_REQUEST);
        }

        protected bool ShouldBeFlatWithNoPendingPosition
        {
            get
            {
                return !this._enabled || 
                        (!this.AllowActivityDuringTradingConstrainingSwitches
                            && (this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                        ||
                       this._settingsProvider.OrdersTransmissionDisabled
                       || _riskManager.IsTransmissionDisabled
                       || !this.IsAllowedToBeEnabled;
            }
        }

        private void PerformClosingProceduresIfNeeded()
        {
            if (this.ShouldBeFlatWithNoPendingPosition)
            {
                this.AfterDisablingProcedure();
            }
        }

        protected bool IsReadyToProcessEvents()
        {
            //no need to log this
            if (this._settingsProvider.OrdersTransmissionDisabled 
                || _riskManager.IsTransmissionDisabled || 
                (!this.AllowActivityDuringTradingConstrainingSwitches
                    && (this._settingsProvider.GoFlatOnly || _riskManager.IsGoFlatOnlyEnabled || !this._symbolTrustworthyWatcher.IsTrustworthy))
                ||
                !this.IsAllowedToBeEnabled)
            {
                //if we should be flat - make double-sure that we are
                this.AfterDisablingProcedureInternal();
                return false;
            }

            //here we don't read this as volatile - so we might get stale read,
            // but we'll need to access it in Interlocked if attempted to trade anyway
            if (this._requestsState != ENABLED_AND_READY_FOR_SENDING ||
                !_destinationOrdSession.IsReadyAndOpen)
            {
                if(this._verboseLoggingEnabled)
                    this._logger.Log(LogLevel.Trace, this._systemFriendlyName + " ignoring price change - session is not running or other thread is processing change");
                return false;
            }

            return true;
        }


        #region IUnicastInfoForwarder members

        protected AtomicSizeZeroCrossingTracking _currentPositionBasePol;
        protected AtomicSize _totalAcquiredAmountTermPol;

        protected void CancelBankpoolOrder(BankPoolClientOrder clientOrderToCancel)
        {
            if (clientOrderToCancel == null)
                return;

            CancelOrderRequestInfo cri = new CancelOrderRequestInfo(clientOrderToCancel.ClientOrderIdentity,
                                                                        this._systemIdentity, TradingTargetType.BankPool);
            var result = this.CancelOrder(cri);

            if (!result.RequestSucceeded)
            {
                for (int i = 0; i < 5; i++)
                {
                    this._logger.Log(LogLevel.Error,
                                 "{0} system failed to cancel bankpool order {1} (attempt #{2} of 5). Reason: {3}.",
                                 this._systemFriendlyName, clientOrderToCancel, i+1, result.ErrorMessage);
                    Thread.Sleep(10);
                    result = this.CancelOrder(cri);
                    if(result.RequestSucceeded)
                        break;
                }

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "{0} system failed to cancel bankpool order {1} (there were 5 attempts). Reason: {2}. Order can be STILL ACTIVE EXTERNALLY. It should be reviewed manually or kill switch should be used to make sure all orders are explicitly cancelled.",
                        this._systemFriendlyName, clientOrderToCancel, result.ErrorMessage);
                    this.HardBlock();
                }
            }
        }

        protected void SendBankOrder(decimal amountBaseAbs, DealDirection direction, Symbol symbol, TimeSpan maxWaitTimeOnImprovement, string headOrderId)
        {
            this.SendBankOrder(amountBaseAbs, null, direction, true, symbol, maxWaitTimeOnImprovement, OrderType.MarketOnImprovement, headOrderId, null);
        }

        protected void SendLimitTillTimeoutBankOrder(decimal amountBaseAbs, decimal requestedPrice, DealDirection direction, Symbol symbol, TimeSpan limitTimeout, string headOrderId)
        {
            this.SendBankOrder(amountBaseAbs, requestedPrice, direction, true, symbol, limitTimeout, OrderType.LimitTillTimeout, headOrderId, null);
        }

        protected void SendBankOrder(decimal amountBaseAbs, DealDirection direction, Symbol symbol, bool preferLmax, string headOrderId)
        {
            this.SendBankOrder(amountBaseAbs, null, direction, true, symbol, TimeSpan.Zero, preferLmax ? OrderType.MarketPreferLmax : OrderType.Market, headOrderId, null);
        }

        protected void SendBankOrderWithoutDirectionCheck(decimal amountBaseAbs, DealDirection direction, Symbol symbol, bool preferLmax, string headOrderId, GlidingInfoBag gib)
        {
            this.SendBankOrder(amountBaseAbs, null, direction, false, symbol, TimeSpan.Zero, preferLmax ? OrderType.MarketPreferLmax : OrderType.Market, headOrderId, gib);
        }

        protected void SendBankOrderWithHeadOrderDirection(decimal amountBaseAbs, DealDirection direction, Symbol symbol, bool preferLmax, string headOrderId)
        {
            if (direction == this.CurrentHeadOrderPolarity.ToOpositeDirection())
            {
                this._logger.Log(LogLevel.Fatal, "System {0} is trying to send {1} order for {2} {3}, however polarity of last executed head order that was rejected is {4}. NOT sending the order, BLOCKING the system - situation should be reviwed manually",
                    this._systemFriendlyName, direction, amountBaseAbs, symbol, this.CurrentHeadOrderPolarity);
                this.HardBlock();
                return;
            }

            this.SendBankOrder(amountBaseAbs, null, direction, false, symbol, TimeSpan.Zero, preferLmax ? OrderType.MarketPreferLmax : OrderType.Market, headOrderId, null);
        }

        private void SendBankOrder(decimal amountBaseAbs, decimal? requestedPrice, DealDirection direction, bool performDirectionCheck, Symbol symbol, TimeSpan maxWaitTimeBeforeConvertToMkt, OrderType orderType, string headOrderId, GlidingInfoBag gib)
        {
            if (performDirectionCheck && direction == this.CurrentHeadOrderPolarity)
            {
                this._logger.Log(LogLevel.Fatal, "System {0} is trying to send {1} order for {2} {3}, however polarity of last executed nonmatched head order is also {4}. NOT sending the order, BLOCKING the system - situation should be reviwed manually",
                    this._systemFriendlyName, direction, amountBaseAbs, symbol, this.CurrentHeadOrderPolarity);
                this.HardBlock();
            }

            BankPoolClientOrderRequestInfo bankOri;

            if (maxWaitTimeBeforeConvertToMkt <= TimeSpan.Zero &&
                (orderType == OrderType.LimitTillTimeout || orderType == OrderType.MarketOnImprovement))
            {
                orderType = OrderType.Market;
            }

            if (orderType == OrderType.Market && maxWaitTimeBeforeConvertToMkt > TimeSpan.Zero)
            {
                this._logger.Log(LogLevel.Fatal, "{0} requested market bankpool order but timeout ({1}) was specified (intended to be MktOnImprovement or LimitTillTimeout?), timeout ignored",
                    this._systemFriendlyName, maxWaitTimeBeforeConvertToMkt);
            }

            if (requestedPrice.HasValue && orderType != OrderType.LimitTillTimeout && orderType != OrderType.Limit)
            {
                this._logger.Log(LogLevel.Fatal, "{0} requested {1} bankpool order but price ({1}) is specified (intended to be Limit or LimitTillTimeout?), price ignored",
                    this._systemFriendlyName, orderType, requestedPrice);
            }

            switch (orderType)
            {
                case OrderType.Market:
                case OrderType.MarketPreferLmax:
                    bankOri =
                        ClientOrdersBuilder_OnlyForIntegratorUsage.CreateMarket(
                            orderType == OrderType.MarketPreferLmax, amountBaseAbs,
                            direction, symbol, true);
                    break;

                case OrderType.MarketOnImprovement:
                    bankOri = ClientOrdersBuilder_OnlyForIntegratorUsage.CreateMarketOnImprovement(amountBaseAbs,
                        maxWaitTimeBeforeConvertToMkt, direction, symbol, true);
                    break;

                case OrderType.LimitTillTimeout:
                    if (!requestedPrice.HasValue)
                    {
                        this._logger.Log(LogLevel.Fatal,
                            "{0} attempted to create LimitTillTimeout BankPool order (to {1} {2} of {3}), but price is not specified. HardBlocking",
                            this._systemFriendlyName, direction, amountBaseAbs, symbol);
                        this.HardBlock();
                        return;
                    }
                    bankOri = ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLimitTillTimeout(amountBaseAbs,
                        requestedPrice.Value, maxWaitTimeBeforeConvertToMkt, direction, symbol, true);
                    break;

                case OrderType.Limit:
                case OrderType.Quoted:
                case OrderType.Pegged:
                default:
                    this._logger.Log(LogLevel.Fatal,
                        "{0} attempted to create unsupported BankPool order type: {1} (to {2} {3} of {4}). HardBlocking",
                        this._systemFriendlyName, orderType, direction, amountBaseAbs, symbol);
                    this.HardBlock();
                    return;
            }

            //this.AdjustBankOrderRequest(bankOri);

            int currentBankOrdrCnt = Interlocked.Increment(ref this._bankSubOrdersCnt);
            BankPoolClientOrder bankOrder = ClientOrdersBuilder_OnlyForIntegratorUsage.CreateBankPoolClientOrder(
                headOrderId + "_BNK_" + currentBankOrdrCnt, this._systemIdentity,
                bankOri, this._integratedTradingSystemIdentification, gib);

            var result = this.RegisterOrder(bankOrder);
            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system unable to register bank pool order to {1} {2} of {3}. Reason: [{4}]. This will efectively lead to block of this system until manually unblocked. Order will need to be performed manualy in order to flat the position.",
                                 this._systemFriendlyName, bankOri.IntegratorDealDirection, bankOri.SizeBaseAbsInitial, bankOri.Symbol, result.ErrorMessage);
                this.HardBlock();
            }
        }

        protected virtual void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal, AtomicSize currentPositionBasePol)
        {
            //MarketOnImprovement is the second leg
            if (integratorDealInternal.SenderTradingTargetType.GetTradingTargetCategory() == TradingTargetCategory.Venue)
            {
                this.SendBankOrder(integratorDealInternal.FilledAmountBaseAbs, integratorDealInternal.Direction.ToOpositeDirection(), integratorDealInternal.Symbol,
                    this._settings.MaximumWaitTimeOnImprovementTick, integratorDealInternal.ClientOrderIdentity);
            }
        }

        public void OnIntegratorDealInternal(IntegratorDealInternal integratorDealInternal)
        {
            AtomicSize currentPositionBasePol = _currentPositionBasePol.InterlockedAdd(integratorDealInternal.FilledAmountBasePol);
            _totalAcquiredAmountTermPol.InterlockedAdd(integratorDealInternal.AcquiredAmountTermPol);

            this.HandleIntegratorDealInternal(integratorDealInternal, currentPositionBasePol);

            //This can be partiall fill of venue order - in that case we'd still not receive order update
            // So theoretically we can have an unneded position our - make sure it doesn't happen
            this.PerformClosingProceduresIfNeeded();
        }

        public virtual void OnIntegratorUnconfirmedDealInternal(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
        {
            this._logger.Log(LogLevel.Fatal, "{0} system receiving Unconfirmed Deal - this is unexpected. {1}", this._systemFriendlyName, integratorUnconfirmedDealInternal);
            this.HardBlock();

            //(integratorUnconfirmedDealInternal.OrderChangeEventArgsInternal as
            //    Kreslik.Integrator.FIXMessaging.OrderChangeEventArgsEx).AutoRejectableDeal;
        }

        private const int _MAX_RECURSE_DEPTH = 20;
        private const int _MAX_RECURSE_DEPTH_TO_FORCE_RETURN = 25;
        private System.Threading.ThreadLocal<int> _recurseLevel = new ThreadLocal<int>(() => 0);

        private bool EnterRecursionCheckedRegion()
        {
            if (_recurseLevel.Value++ > _MAX_RECURSE_DEPTH)
            {
                this._logger.Log(LogLevel.Fatal, "{0} detected too deep recursion. Breaking it and blocking system, but system state consistency should be reviewed", this._systemFriendlyName);
                if (_recurseLevel.Value < _MAX_RECURSE_DEPTH_TO_FORCE_RETURN)
                {
                    this.HardBlock();
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal, "{0} detected continuing deep recursion - breaking without blockign the system. SYSTEM SHOULD BE BLOCKED MANUALY A.S.A.P!", this._systemFriendlyName);
                    this._riskManager.TurnOnGoFlatOnly("Too deep recursion");
                }
                return false;
            }
            return true;
        }

        private bool EnterLasteResortRecursionCheckedRegion()
        {
            if (_recurseLevel.Value++ > _MAX_RECURSE_DEPTH_TO_FORCE_RETURN)
            {
                this._logger.Log(LogLevel.Fatal, "{0} detected continuing deep recursion - breaking without blockign the system. SYSTEM SHOULD BE BLOCKED MANUALY A.S.A.P!", this._systemFriendlyName);
                return false;
            }
            return true;
        }

        private void ExitRecursionCheckedRegion()
        {
            _recurseLevel.Value--;
        }


        public void OnIntegratorClientOrderUpdate(ClientOrderUpdateInfo clientOrderUpdateInfo, Symbol symbol)
        {
            if (!EnterRecursionCheckedRegion())
                return;

            if(this.IsTradingUnexpectedNow)
                this._logger.Log(LogLevel.Fatal, "System {0} should be disabled from {1:HH:mm:ss.fff}, however it received order update. This is unexpected and should be investigated!. {2}",
                    this._systemIdentity, this._disabledTime, clientOrderUpdateInfo);

            if (clientOrderUpdateInfo is BankPoolClientOrderUpdateInfo)
            {
                this.OnBankPoolOrderUpdate(clientOrderUpdateInfo as BankPoolClientOrderUpdateInfo);
            }

            this.HandleClientOrderUpdateInfo(clientOrderUpdateInfo);


            if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo
                &&
                (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed
                 && clientOrderUpdateInfo.IntegratorRejectionReason.StartsWith("Cancellation rejected by Counterparty") &&
                 clientOrderUpdateInfo.SizeBaseAbsTotalRemaining != 0)
                ||
                clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelNotRequested)
            {
                //WARNING: We cannot perform this for all order updates (as pending/rejection updates on a same thread would cause
                // infinite loop)
                // All final status updates will go through ProcessEvents and will chck status - that will already trigger cancelation
                // From nonfinal statuse we are only concerned about CancelRejected (however not the local one)
                // AND all updates not related to cancellations
                this.PerformClosingProceduresIfNeeded();
            }

            ExitRecursionCheckedRegion();
        }

        protected void ProcessEventWithRecursionCheck()
        {
            if(!EnterRecursionCheckedRegion())
                return;

            this.ProcessEvent();

            ExitRecursionCheckedRegion();
        }

        protected virtual void HandleCounterpartyReject(CounterpartyRejectionInfo counterpartyRejectionInfo) { }

        public void OnCounterpartyRejectedOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            this.HandleCounterpartyReject(counterpartyRejectionInfo);
            //Overkill here - but just to be on a sure side
            this.PerformClosingProceduresIfNeeded();
        }

        private int _ordersSentAfterDisabled;
        protected int _reissueAttempts;
        public void OnCounterpartyIgnoredOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            //Bankpool ignored orders are NOT handeled by the handler for closed bankpool orders
            if (counterpartyOrderIgnoringInfo.SenderTradingTargetType == TradingTargetType.BankPool)
            {
                this.TryReissueBankOrderOnNotFilledAmount(counterpartyOrderIgnoringInfo.IgnoredAmountBaseAbs,
                                                          counterpartyOrderIgnoringInfo.Direction,
                                                          counterpartyOrderIgnoringInfo.ClientOrderIdentity,
                                                          counterpartyOrderIgnoringInfo.GlidingInfoBag);
            }
            else
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system receiving order ignored info for head deal for size [{1}] (base absolute). {2}. BLOCKING system - until explicit reset",
                                 this._systemFriendlyName, counterpartyOrderIgnoringInfo.IgnoredAmountBaseAbs, counterpartyOrderIgnoringInfo);
                this.HardBlock();
            }
        }

        protected bool CheckIsAllowedToReissueFlattingOrder()
        {
            return Interlocked.Increment(ref this._reissueAttempts) <= 10; 
        }

        private readonly TimeSpan[] _reissueDelays = new TimeSpan[]
        {
            TimeSpan.Zero, TimeSpan.Zero, TimeSpan.Zero, TimeSpan.Zero,
            TimeSpan.FromMilliseconds(100), TimeSpan.FromMilliseconds(100), TimeSpan.FromMilliseconds(100),
            TimeSpan.FromSeconds(1)
        };

        //Attempts to redo bankpool order that hasn't been fully filled for some reason
        // Handeled cases: some amount is ignored, rejected or canceled
        // Use cases when this can be triggered:
        //                         - order is ignored
        //                         - order is cancelled - most probably because of flipped kill switch (as it cancells all orders) - replacing order will likely fail
        //                         - order is rejected by BankPool - currently only when it triggers killSwitch - replacing order will likely fail
        private void TryReissueBankOrderOnNotFilledAmount(decimal notFilledAmount, DealDirection direction, string failedOrderIdentity, GlidingInfoBag gib)
        {
            bool reissue = CheckIsAllowedToReissueFlattingOrder();

            this._logger.Log(LogLevel.Fatal, "{0} system receiving final order update or ignore info for bankpool order {1}, but position size(pol) {2} was not filled. {3}etrying to reissue failed amount. Total system position(pol): {4}",
                this._systemFriendlyName, failedOrderIdentity, direction == DealDirection.Buy ? notFilledAmount : -notFilledAmount,  reissue ? "R" : "NOT r", _currentPositionBasePol);

            if (reissue)
            {
                int reissueIdx = _reissueAttempts;
                TimeSpan reissueDelay = reissueIdx >= _reissueDelays.Length
                    ? _reissueDelays[_reissueDelays.Length - 1]
                    : _reissueDelays[reissueIdx];

                //reissue without side check
                if (reissueDelay <= TimeSpan.Zero) 
                    this.SendBankOrder(notFilledAmount, null, direction, false, _symbol, TimeSpan.Zero, OrderType.Market,
                        failedOrderIdentity, gib);
                else
                    TaskEx.ExecAfterDelayWithErrorHandling(reissueDelay, () =>
                                this.SendBankOrder(notFilledAmount, null, direction, false, _symbol, TimeSpan.Zero, OrderType.Market,
                                    failedOrderIdentity, gib));
            }
            else
            {
                this.HardBlock();
            }
        }

        private void OnBankPoolOrderUpdate(BankPoolClientOrderUpdateInfo bankPoolClientOrderUpdateInfo)
        {
            //Ignored orders doesn't go through here - as they do not have nonzero rejected/Canceled size
            if (bankPoolClientOrderUpdateInfo.SizeBaseAbsRejected + bankPoolClientOrderUpdateInfo.SizeBaseAbsCancelled > 0)
            {
                if (!bankPoolClientOrderUpdateInfo.Side.HasValue)
                {
                    this._logger.Log(LogLevel.Fatal, "Detected not filled amount from order update, but side is unknown! Situation need to be rewieved, {0}", bankPoolClientOrderUpdateInfo);
                    return;
                }

                this.TryReissueBankOrderOnNotFilledAmount(bankPoolClientOrderUpdateInfo.SizeBaseAbsRejected + bankPoolClientOrderUpdateInfo.SizeBaseAbsCancelled,
                    bankPoolClientOrderUpdateInfo.Side.Value, bankPoolClientOrderUpdateInfo.ClientOrderIdentity, bankPoolClientOrderUpdateInfo.GlidingInfoBag);
            }
            else if(bankPoolClientOrderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator)
            {
                _reissueAttempts = 0;
            }
        }

        #endregion IUnicastInfoForwarder members



        internal virtual void HandleOrderCurrentPartiallyRejected(VenueClientOrder orderCurrent, VenueClientOrder orderNext) { }
        internal virtual void HandleOrderNextPartiallyRejected(VenueClientOrder orderNext) { }

        protected virtual bool DirectCancelOfOrderCurrentAllowed { get { return false; } }

        private bool ShouldRetryToCancel(VenueClientOrderUpdateInfo venueClientOrderUpdateInfo)
        {
            return 
            venueClientOrderUpdateInfo != null    
            && venueClientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed
            && !venueClientOrderUpdateInfo.HasPendingCancellation
            && venueClientOrderUpdateInfo.SizeBaseAbsTotalRemaining != 0
            && _destinationOrdSession.IsReadyAndOpen;
        }

        internal protected bool HandleClientOrderUpdateInfoHelper(ClientOrderUpdateInfo clientOrderUpdateInfo, CancelReplaceOrdersHolder cancelReplaceOrdersHolder)
        {
            //BEWARE:
            // OpenInIntegrator - only opened in integrator, 
            // ConfirmedByCounterparty - once the order was confirmed by counterparty until it gets to some terminal state


            // New order is allways created as Next, also order is allways moved to next before cancelled (unless disable happend)
            // So current order should only receive internediate fills and final statuses (no OpenInIntegrator, Confirmed etc.)
            //   Exceptions: when system is disabled; when system is disabled; when order was cancelled by lower mechanism (session stop, kill sw, etc)
            // Next order will get intermediate states (Open, Current) - after those it moves to current; but it can get terminal status right away
            // Also it can get confirmed status with initial reject


            bool isOrderInTerminalStatus = clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator
                                           ||
                                           clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator
                                           ||
                                           clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RemovedFromIntegrator
                                           ||
                                           clientOrderUpdateInfo.SizeBaseAbsTotalRemaining <= 0;
            bool shouldReprocessEvents = isOrderInTerminalStatus;

            if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo)
            {
                var orderCurrentLocal = cancelReplaceOrdersHolder.OrderCurrent;
                var orderNextLocal = cancelReplaceOrdersHolder.OrderNext;
                if (orderCurrentLocal != null &&
                    orderCurrentLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
                {
                    //confirmation of order cancel within c/replace (successfull and also failed)
                    // do not touch next - as it will get separate info
                    if (isOrderInTerminalStatus)
                    {
                        //nullify if it's the one that we expect
                        if (InterlockedEx.CheckAndFlipState(ref cancelReplaceOrdersHolder.OrderCurrent, null, orderCurrentLocal))
                        {
                            //If orderNext was already confirmed in the meantime - then lets move it to current
                            // As it can happen that it would never be moved to current (if C/Replace during partial LL fill)
                            if (orderNextLocal != null &&
                                orderNextLocal.OrderStatus != ClientOrderStatus.NewInIntegrator &&
                                orderNextLocal.OrderStatus != ClientOrderStatus.OpenInIntegrator)
                            {
                                //Confirm only when we have no pending cancels, otherwise wait for those
                                if (!orderNextLocal.HasPendingCancellation)
                                {
                                    cancelReplaceOrdersHolder.OrderCurrent = orderNextLocal;
                                    //this is to prevent case where orderNext is originally null, so parallel
                                    // replace attempt can sneak in and populate order next - wo don't want to nulify it
                                    Interlocked.CompareExchange(ref cancelReplaceOrdersHolder.OrderNext, null, orderNextLocal);
                                }
                            }
                        }

                        shouldReprocessEvents = cancelReplaceOrdersHolder.OrderNext == null;
                    }
                    //Current order was created and integrator rejected some initial amount
                    // (partial fill replace) - so this can happen only in ConfirmedByCtp status
                    else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty &&
                             clientOrderUpdateInfo.CancelRequestStatus ==
                             ClientOrderCancelRequestStatus.CancelNotRequested
                             && clientOrderUpdateInfo.SizeBaseAbsRejected != 0)
                    {
                        //rejecting initial size of current order - let's cancel it and than create new order from scratch
                        // double check orderNext - so that we prevent unnecessary check for single access region, but also prevent race
                        this.HandleOrderCurrentPartiallyRejected(orderCurrentLocal, orderNextLocal);
                    }
                    //whatever other intermediate state confirmed by ctp is allowed, but not relevant 
                    else if (
                        //this is when Integrator tries to cancel non-confirmed order and counterparty rejects
                        // the cancel attempt before even confirming the order ('not known') - in that case
                        //  the state is still OpenInIntegrator even though this came from counterparty

                        //Also this is for failed C/replace - then counterparty sends Cancel failed for current order (that is already confirmed)  
                        (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                         ||
                         clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
                        &&
                        clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed)
                    {
                        if (this.ShouldRetryToCancel(clientOrderUpdateInfo as VenueClientOrderUpdateInfo))
                            shouldReprocessEvents = true;
                    }
                    //plain confirm
                    else if(clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                        //this can happen when we are sending C/Replace and receiving open for next (on thread A),
                        // but in the meantime on another thread B we receive Current canceled and Next confirmed,
                        //  B will move Next to current, A will find it in current but will still see the order as just Opened
                        //  and not Confirmed, despite it's already confirmed.
                        // This is OK
                        || clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
                    { }
                    else if ((this.ShouldBeFlatWithNoPendingPosition || this.DirectCancelOfOrderCurrentAllowed)
                            &&
                             clientOrderUpdateInfo.CancelRequestStatus !=
                             ClientOrderCancelRequestStatus.CancelNotRequested)
                    {
                        //after disabling it is allowed that cancellation is performed directly on current order 
                        //  without moving to next
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for OrderCurrent: {1}",
                                         this._systemFriendlyName, clientOrderUpdateInfo);
                    }
                }
                else if (orderNextLocal != null &&
                    orderNextLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
                {
                    //this usually means direct cancel of order next
                    if (isOrderInTerminalStatus)
                    {
                        cancelReplaceOrdersHolder.OrderNext = null;
                    }
                    //We shouldn't receive cancels on non-confirmed orders (as we don' cancel them prior that)
                    else if ((clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                        || clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
                        //We need to rule out update during plain cancel (as this should receive another update in near future)
                        && clientOrderUpdateInfo.CancelRequestStatus != ClientOrderCancelRequestStatus.CancelRequestInProgress)
                    {
                        //this is confirmation of opening of a NewOrderSingle (no replace)
                        //it also handles the failure during submitting plain OrderCancel
                        if (orderCurrentLocal == null)
                        {
                            //rejecting initial size of current order - let's cancel it and than create new order from scratch
                            if (clientOrderUpdateInfo.SizeBaseAbsRejected != 0)
                            {
                                //we don't need single access as it is already order next
                                this.HandleOrderNextPartiallyRejected(orderNextLocal);
                            }
                            else
                            {
                                //Confirm only when we have no pending cancels, otherwise wait for those
                                if (!(clientOrderUpdateInfo as VenueClientOrderUpdateInfo).HasPendingCancellation)
                                {
                                    orderNextLocal = cancelReplaceOrdersHolder.OrderNext;
                                    cancelReplaceOrdersHolder.OrderCurrent = orderNextLocal;
                                    //this is to prevent case where orderNext is originally null, so parallel
                                    // replace attempt can sneak in and populate order next - we don't want to nullify it
                                    Interlocked.CompareExchange(ref cancelReplaceOrdersHolder.OrderNext, null, orderNextLocal);
                                    //we are ready for next order
                                    shouldReprocessEvents = true;
                                }
                                else
                                {
                                    this._logger.Log(LogLevel.Info,
                                        "{0} system receiving confirmation for order that has pending cancelation in the meantime - ignoring. Update info: {1}",
                                        this._systemFriendlyName, clientOrderUpdateInfo);
                                }
                            }
                        }
                        else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty)
                        {
                            //this can be valid for aggressive cancel situations
                            //And this is also valid for LL - while after C/Replace done the original order still waits for all C/Replace responses
                            this._logger.Log(LogLevel.Error,
                                         "{0} system receiving confirmed order update for OrderNext: {1}, while order current is not null: {2}",
                                         this._systemFriendlyName, clientOrderUpdateInfo, orderCurrentLocal);
                        }
                        //here we get OpenInIntegrator for C/replace, but at this point curret order is still occupied,
                        // as we hasn't yet received final confirmation for order current. 
                        // This is valid - as handling OpenInIntegrator is only for a case where there was new order without replace

                        //we failed to cancel the order, but there is no other cancel pending - so we should try to cancel again
                        if (this.ShouldRetryToCancel(clientOrderUpdateInfo as VenueClientOrderUpdateInfo))
                            shouldReprocessEvents = true;
                    }
                    else if (
                        //initial update - just from integrator
                        clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator
                        ||
                        //this also comes just from integrator
                        clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress) { }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for OrderNext: {1}",
                                         this._systemFriendlyName, clientOrderUpdateInfo);
                    }
                }
                else if (isOrderInTerminalStatus)
                {
                    //nothing - this is expectd:
                    // We can send order, than cancel for it, but receive fill and done in the meantime

                    // We can also receive last fill, react with Bank order, which uses all liquidity and removes price from book
                    //  which triggers deterioration which lead to attempt to cancel the current order -> and receaving info about NotActive (CancelRequestFailed)
                    //  And after all those we will receive the finnal NotActive CancelNotRequested correcponding to the fill

                    //Scenario 3 - subsequent updates on multiple Hotspot cancel requests

                    //in all cases - it is redundand to reprocess
                    shouldReprocessEvents = false;
                }
                else
                {
                    this.CheckUnknownClientOrderUpdate(clientOrderUpdateInfo);
                }
            }

            return shouldReprocessEvents;
        }
    }
}
