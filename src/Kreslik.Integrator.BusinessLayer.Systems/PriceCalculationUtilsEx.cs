﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public static class PriceCalculationUtilsEx
    {
        public static bool TryRoundToSupportedPriceIncrement(ISymbolsInfo symbolsInfo, DealDirection integratorDealDirection, bool roundToImprovedPrice, Symbol symbol, TradingTargetType tradingTargetType,
            decimal price, out decimal result)
        {
            bool roundToZero = integratorDealDirection == DealDirection.Buy && roundToImprovedPrice ||
                               integratorDealDirection == DealDirection.Sell && !roundToImprovedPrice;

            return symbolsInfo.TryRoundToSupportedPriceIncrement(symbol, tradingTargetType,
                roundToZero ? RoundingKind.AlwaysToZero : RoundingKind.AlwaysAwayFromZero, price, out result);
        }
    }
}
