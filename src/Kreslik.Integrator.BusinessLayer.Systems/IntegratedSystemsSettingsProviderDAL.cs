﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.DAL
{
    public interface IIntegratedSystemSettingsBag
    {
        Symbol Symbol { get; }
        Counterparty Counterparty { get; }
        int TradingSystemId { get; }
        int TradingSystemGroupId { get; }
        bool Enabled { get; }
        bool AskEnabled { get; }
        bool BidEnabled { get; }
        bool GoFlatOnly { get; }
        bool TransmissionDisabled { get; }
        DateTime? LastResetRequestedUtc { get; }
        bool IsUpdated { get; }
    }

    public abstract class IntegratedSystemSettingsBagBase : IIntegratedSystemSettingsBag
    {
        public IntegratedSystemSettingsBagBase(Symbol symbol, Counterparty counterparty, int tradingSystemId,
            int tradingGroupId, bool enabled, bool bidEnabled, bool askEnabled, bool goFlatOnly, bool transmissionDisabled,
            DateTime? lastResetRequestedUtc, bool isUpdated)
        {
            this.Symbol = symbol;
            this.Counterparty = counterparty;
            this.TradingSystemId = tradingSystemId;
            this.TradingSystemGroupId = tradingGroupId;
            this.Enabled = enabled;
            this.BidEnabled = bidEnabled;
            this.AskEnabled = askEnabled;
            this.GoFlatOnly = goFlatOnly;
            this.TransmissionDisabled = transmissionDisabled;
            this.LastResetRequestedUtc = lastResetRequestedUtc;
            this.IsUpdated = isUpdated;
        }

        public Symbol Symbol { get; private set; }
        public Counterparty Counterparty { get; private set; }
        public int TradingSystemId { get; private set; }
        public int TradingSystemGroupId { get; private set; }
        public bool Enabled { get; private set; }
        public bool AskEnabled { get; private set; }
        public bool BidEnabled { get; private set; }
        public bool GoFlatOnly { get; private set; }
        public bool TransmissionDisabled { get; private set; }
        public DateTime? LastResetRequestedUtc { get; private set; }
        public bool IsUpdated { get; private set; }

        public override string ToString()
        {
            return String.Format(
                    "[{0}-{1} systemId: {2} groupId: {3}] - Enabled: {4}; BidEnabled: {5}, AskEnabled: {6} GoFlatOnly: {7}, TransmissionDisabled: {8}, LastResetRequestedUtc: {9}; IsUpdated: {10}",
                    Counterparty, Symbol, TradingSystemId, TradingSystemGroupId, Enabled, BidEnabled, AskEnabled, GoFlatOnly, TransmissionDisabled, LastResetRequestedUtc, IsUpdated);
        }
    }

    public class VenueGliderStreamMMSystemSettingsBag : VenueStreamMMSystemSettingsBag
    {
        public VenueGliderStreamMMSystemSettingsBag(Symbol symbol, Counterparty counterparty, int tradingSystemId,
            int tradingGroupId, bool enabled, bool bidEnabled, bool askEnabled, bool goFlatOnly,
            bool transmissionDisabled, DateTime? lastResetRequestedUtc, bool isUpdated, decimal maximumSizeToTrade,
            decimal bestPriceImprovementOffsetBasisPoints, decimal maximumDiscountBasisPointsToTrade, TimeSpan kgtLLTime,
            bool preferLLDestinationCheckToLmax, decimal minimumBPGain, decimal minimumBPGrossGainIfZeroGlideFill, bool preferLmaxDuringHedging,
            TimeSpan minimumIntegratorPriceLife, TimeSpan minimumGlidePriceLife, TimeSpan glidingCutoffSpan, decimal minimumSizeToTrade, decimal minimumFillSize,
            int maximumSourceBookTiers, decimal minimumBasisPointsFromKgtPriceToFastCancel, bool referenceLmaxBookOnly, Counterparty glidingCounterparty)
            : base(
                symbol, counterparty, tradingSystemId, tradingGroupId, enabled, bidEnabled, askEnabled, goFlatOnly,
                transmissionDisabled, lastResetRequestedUtc, isUpdated, maximumSizeToTrade,
                bestPriceImprovementOffsetBasisPoints, maximumDiscountBasisPointsToTrade, kgtLLTime,
                preferLLDestinationCheckToLmax, minimumBPGain, preferLmaxDuringHedging,
                minimumIntegratorPriceLife, minimumSizeToTrade, minimumFillSize, maximumSourceBookTiers,
                minimumBasisPointsFromKgtPriceToFastCancel, referenceLmaxBookOnly)
        {
            this.GlidePriceLife = minimumGlidePriceLife;
            this.GlidingCutoffSpan = glidingCutoffSpan;
            this.MinimumBPGrossGainIfZeroGlideFill = minimumBPGrossGainIfZeroGlideFill;
            this.GlidingCounterparty = glidingCounterparty;
        }

        public TimeSpan GlidePriceLife { get; private set; }
        public TimeSpan GlidingCutoffSpan { get; private set; }
        public decimal MinimumBPGrossGainIfZeroGlideFill { get; private set; }
        public Counterparty GlidingCounterparty { get; private set; }

        public override string ToString()
        {
            return
                String.Format(
                    "VenueGliderStreamMMSystemSettingsBag {0} - GlidePriceLife: {1}, GlidingCutoffSpan: {2}, MinimumBPGrossGainIfZeroGlideFill: {3}, GlidingCounterparty: {4}",
                    base.ToString(), GlidePriceLife, GlidingCutoffSpan, MinimumBPGrossGainIfZeroGlideFill, GlidingCounterparty);
        }
    }

    public class VenueStreamMMSystemSettingsBag : IntegratedSystemSettingsBagBase
    {
        public VenueStreamMMSystemSettingsBag(Symbol symbol, Counterparty counterparty, int tradingSystemId, int tradingGroupId, bool enabled, bool bidEnabled, bool askEnabled, bool goFlatOnly, bool transmissionDisabled, DateTime? lastResetRequestedUtc, bool isUpdated,
            decimal maximumSizeToTrade, decimal bestPriceImprovementOffsetBasisPoints, decimal maximumDiscountBasisPointsToTrade, TimeSpan kgtLLTime,
            bool preferLLDestinationCheckToLmax, decimal minimumBPGrossToAccept, bool preferLmaxDuringHedging, TimeSpan minimumIntegratorPriceLife,
            decimal minimumSizeToTrade, decimal minimumFillSize, int maximumSourceBookTiers, decimal minimumBasisPointsFromKgtPriceToFastCancel, bool referenceLmaxBookOnly)
            : base(symbol, counterparty, tradingSystemId, tradingGroupId, enabled, bidEnabled, askEnabled, goFlatOnly, transmissionDisabled, lastResetRequestedUtc, isUpdated)
        {
            this.MaximumSizeToTrade = maximumSizeToTrade;
            this.BestPriceImprovementOffsetBasisPoints = bestPriceImprovementOffsetBasisPoints;
            this.MaximumDiscountBasisPointsToTrade = maximumDiscountBasisPointsToTrade;
            this.KGTLLTime = kgtLLTime;
            this.PreferLLDestinationCheckToLmax = preferLLDestinationCheckToLmax;
            this.MinimumBPGrossToAccept = minimumBPGrossToAccept;
            this.PreferLmaxDuringHedging = preferLmaxDuringHedging;
            this.MinimumIntegratorPriceLife = minimumIntegratorPriceLife <= TimeSpan.Zero ? TimeSpan.MinValue : minimumIntegratorPriceLife;
            this.MinimumSizeToTrade = minimumSizeToTrade;
            this.MinimumFillSize = minimumFillSize;
            this.MaximumSourceBookTiers = maximumSourceBookTiers;
            this.MinimumBasisPointsFromKgtPriceToFastCancel = minimumBasisPointsFromKgtPriceToFastCancel;
            this.ReferenceLmaxBookOnly = referenceLmaxBookOnly;
        }

        public decimal MaximumSizeToTrade { get; private set; }

        public decimal BestPriceImprovementOffsetBasisPoints { get; private set; }

        public decimal MaximumDiscountBasisPointsToTrade { get; private set; }

        public TimeSpan KGTLLTime { get; private set; }

        public bool PreferLLDestinationCheckToLmax { get; private set; }

        public decimal MinimumBPGrossToAccept { get; private set; }

        public bool PreferLmaxDuringHedging { get; private set; }

        public TimeSpan MinimumIntegratorPriceLife { get; private set; }

        public decimal MinimumSizeToTrade { get; private set; }

        public decimal MinimumFillSize { get; private set; }

        public int MaximumSourceBookTiers { get; private set; }
        public decimal MinimumBasisPointsFromKgtPriceToFastCancel { get; private set; }

        public bool ReferenceLmaxBookOnly { get; private set; }

        public override string ToString()
        {
            return
                String.Format(
                    "VenueStreamMMSystemSettingsBag {0} - MaximumSizeToTrade: {1}; MinimumSizeToTrade: {2}; MinimumFillSize: {3}; BestPriceImprovementOffsetBasisPoints: {4}; MaximumDiscountBasisPointsToTrade: {5}; KGTLLTime: {6}; PreferLLDestinationCheckToLmax: {7}; MinimumBPGrossToAccept: {8}; PreferLmaxDuringHedging: {9}; MinimumIntegratorPriceLife: {10}; MaximumSourceBookTiers: {11}; MinimumBasisPointsFromKgtPriceToFastCancel: {12}; ReferenceLmaxBookOnly: {13}",
                    base.ToString(), MaximumSizeToTrade, MinimumSizeToTrade, MinimumFillSize, BestPriceImprovementOffsetBasisPoints, MaximumDiscountBasisPointsToTrade, KGTLLTime, PreferLLDestinationCheckToLmax, MinimumBPGrossToAccept, PreferLmaxDuringHedging, MinimumIntegratorPriceLife, MaximumSourceBookTiers, MinimumBasisPointsFromKgtPriceToFastCancel, ReferenceLmaxBookOnly);
        }
    }

    public class VenueCrossSystemSettingsBag : IntegratedSystemSettingsBagBase
    {
        public VenueCrossSystemSettingsBag(Symbol symbol, Counterparty counterparty, int tradingSystemId, int tradingGroupId, decimal minimumSizeToTrade, decimal maximumSizeToTrade, decimal minimumDiscountBasisPointsToTrade, TimeSpan minimumTimeBetweenSingleCounterpartySignals,
            TimeSpan maximumWaitTimeOnImprovementTick, decimal minimumFillSize, bool enabled, bool goFlatOnly, bool transmissionDisabled, DateTime? lastResetRequestedUtc, bool isUpdated)
            : base(symbol, counterparty, tradingSystemId, tradingGroupId, enabled, true, true, goFlatOnly, transmissionDisabled, lastResetRequestedUtc, isUpdated)
        {
            this.MinimumSizeToTrade = minimumSizeToTrade;
            this.MaximumSizeToTrade = maximumSizeToTrade;
            this.MinimumDiscountBasisPointsToTrade = minimumDiscountBasisPointsToTrade;
            this.MinimumTimeBetweenSingleCounterpartySignals = minimumTimeBetweenSingleCounterpartySignals;
            this.MaximumWaitTimeOnImprovementTick = maximumWaitTimeOnImprovementTick;
            this.MinimumFillSize = minimumFillSize;
        }

        public decimal MinimumSizeToTrade { get; private set; }
        public decimal MaximumSizeToTrade { get; private set; }
        public decimal MinimumDiscountBasisPointsToTrade { get; private set; }
        public TimeSpan MinimumTimeBetweenSingleCounterpartySignals { get; private set; }
        public TimeSpan MaximumWaitTimeOnImprovementTick { get; private set; }
        public decimal MinimumFillSize { get; private set; }

        public override string ToString()
        {
            return
                String.Format(
                    "VenueCrossSystemSettingsBag {0} - MinimumSizeToTrade: {1}; MaximumSizeToTrade: {2}; MinimumFillSize: {3}; MinimumDiscountBasisPointsToTrade: {4}; MinimumTimeBetweenTrades: {5}; MaximumWaitTimeOnImprovementTick: {6}",
                    base.ToString(), MinimumSizeToTrade, MaximumSizeToTrade, MinimumFillSize, MinimumDiscountBasisPointsToTrade, MinimumTimeBetweenSingleCounterpartySignals, MaximumWaitTimeOnImprovementTick);
        }
    }

    public class VenueSmartCrossSystemSettingsBag : VenueCrossSystemSettingsBag
    {
        public VenueSmartCrossSystemSettingsBag(Symbol symbol, Counterparty counterparty, int tradingSystemId,
            int tradingGroupId, decimal minimumSizeToTrade, decimal maximumSizeToTrade,
            decimal minimumDiscountBasisPointsToTrade, TimeSpan minimumTimeBetweenSingleCounterpartySignals,
            TimeSpan maximumWaitTimeOnImprovementTick, decimal minimumFillSize,
            decimal coverDistanceGrossBasisPoints, TimeSpan coverTimeout, decimal minimumTrueGainGrossBp,
            bool enabled, bool goFlatOnly,
            bool transmissionDisabled, DateTime? lastResetRequestedUtc, bool isUpdated)
            : base(
                symbol, counterparty, tradingSystemId, tradingGroupId, minimumSizeToTrade, maximumSizeToTrade,
                minimumDiscountBasisPointsToTrade, minimumTimeBetweenSingleCounterpartySignals,
                maximumWaitTimeOnImprovementTick, minimumFillSize, enabled, goFlatOnly, transmissionDisabled,
                lastResetRequestedUtc, isUpdated)
        {
            this.CoverDistanceGrossBasisPoints = coverDistanceGrossBasisPoints;
            this.CoverTimeout = coverTimeout;
            this.MinimumTrueGainGrossBp = minimumTrueGainGrossBp;
        }

        public decimal CoverDistanceGrossBasisPoints { get; private set; }
        public TimeSpan CoverTimeout { get; private set; }

        public decimal MinimumTrueGainGrossBp { get; private set; }

        public override string ToString()
        {
            return
                String.Format(
                    "VenueSmartCrossSystemSettingsBag {0} - CoverDistanceGrossBasisPoints: {1}; CoverTimeout: {2}",
                    base.ToString(), CoverDistanceGrossBasisPoints, CoverTimeout);
        }
    }

    public class VenueMMSystemSettingsBag : IntegratedSystemSettingsBagBase
    {
        public VenueMMSystemSettingsBag(Symbol symbol, Counterparty counterparty, int tradingSystemId, int tradingGroupId, decimal maximumSizeToTrade, decimal bestPriceImprovementOffsetBasisPoints, decimal maximumDiscountBasisPointsToTrade,
            TimeSpan maximumWaitTimeOnImprovementTick, TimeSpan minimumIntegratorPriceLife, decimal minimumSizeToTrade, decimal minimumFillSize, int maximumSourceBookTiers, decimal minimumBasisPointsFromKgtPriceToFastCancel, bool enabled, bool bidEnabled, bool askEnabled, bool goFlatOnly, bool transmissionDisabled, DateTime? lastResetRequestedUtc, bool isUpdated)
            : base(symbol, counterparty, tradingSystemId, tradingGroupId, enabled, bidEnabled, askEnabled, goFlatOnly, transmissionDisabled, lastResetRequestedUtc, isUpdated)
        {
            this.MaximumSizeToTrade = maximumSizeToTrade;
            this.BestPriceImprovementOffsetBasisPoints = bestPriceImprovementOffsetBasisPoints;
            this.MaximumDiscountBasisPointsToTrade = maximumDiscountBasisPointsToTrade;
            this.MaximumWaitTimeOnImprovementTick = maximumWaitTimeOnImprovementTick;
            this.MinimumIntegratorPriceLife = minimumIntegratorPriceLife <= TimeSpan.Zero ? TimeSpan.MinValue : minimumIntegratorPriceLife;
            this.MinimumSizeToTrade = minimumSizeToTrade;
            this.MinimumFillSize = minimumFillSize;
            this.MaximumSourceBookTiers = maximumSourceBookTiers;
            this.MinimumBasisPointsFromKgtPriceToFastCancel = minimumBasisPointsFromKgtPriceToFastCancel;
        }

        public decimal MaximumSizeToTrade { get; private set; }
        public decimal BestPriceImprovementOffsetBasisPoints { get; private set; }
        public decimal MaximumDiscountBasisPointsToTrade { get; private set; }
        public TimeSpan MaximumWaitTimeOnImprovementTick { get; private set; }
        public TimeSpan MinimumIntegratorPriceLife { get; private set; }
        public decimal MinimumSizeToTrade { get; private set; }
        public decimal MinimumFillSize { get; private set; }
        public int MaximumSourceBookTiers { get; private set; }
        public decimal MinimumBasisPointsFromKgtPriceToFastCancel { get; private set; }

        public override string ToString()
        {
            return
                String.Format(
                    "VenueMMSystemSettingsBag {0} - MaximumSizeToTrade: {1}; MinimumSizeToTrade: {2}; MinimumFillSize: {3}; BestPriceImprovementOffsetBasisPoints: {4}; MaximumDiscountBasisPointsToTrade: {5}; MaximumWaitTimeOnImprovementTick: {6}; MinimumIntegratorPriceLife: {7}; MaximumSourceBookTiers: {8}; MinimumBasisPointsFromKgtPriceToFastCancel: {9}",
                    base.ToString(), MaximumSizeToTrade, MinimumSizeToTrade, MinimumFillSize, BestPriceImprovementOffsetBasisPoints, MaximumDiscountBasisPointsToTrade, MaximumWaitTimeOnImprovementTick, MinimumIntegratorPriceLife, MaximumSourceBookTiers, MinimumBasisPointsFromKgtPriceToFastCancel);
        }
    }

    public class VenueQuotingSystemSettingsBag : IntegratedSystemSettingsBagBase
    {
        public enum QuotingSpreadType
        {
            Fixed,
            Dynamic
        }

        public VenueQuotingSystemSettingsBag(Symbol symbol, Counterparty counterparty, int tradingSystemId, int tradingGroupId, decimal maximumPositionBaseAbs, decimal fixedSpreadBasisPoints, long stopLossNetInUsd,
            QuotingSpreadType spreadType, decimal minDynamicSpreadBp, decimal dynamicMarkupDecimal, bool skewEnabled, decimal skewDecimal,
            TimeSpan inactivityTimeout, decimal liquidationSpreadBasisPoints, TimeSpan lmaxTickerLookbackInterval, long minimumLmaxTickerVolumeUsd,
            long maximumLmaxTickerVolumeUsd, long minimumLmaxTickerAvgDealSizeUsd, long maximumLmaxTickerAvgDealSizeUsd,
            bool enabled, bool goFlatOnly, bool transmissionDisabled, DateTime? lastResetRequestedUtc, bool isUpdated)
            : base(symbol, counterparty, tradingSystemId, tradingGroupId, enabled, true, true, goFlatOnly, transmissionDisabled, lastResetRequestedUtc, isUpdated)
        {
            this.MaximumPositionBaseAbs = maximumPositionBaseAbs;
            this.FixedSpreadBasisPoints = fixedSpreadBasisPoints;
            this.StopLossNetInUsd = stopLossNetInUsd;
            this.SpreadType = spreadType;
            this.MinDynamicSpreadBp = minDynamicSpreadBp;
            this.DynamicMarkupDecimal = dynamicMarkupDecimal;
            this.SkewEnabled = skewEnabled;
            this.SkewDecimal = skewDecimal;
            this.InactivityTimeout = inactivityTimeout;
            this.LiquidationSpreadBasisPoints = liquidationSpreadBasisPoints;
            this.LmaxTickerLookbackInterval = lmaxTickerLookbackInterval;
            this.MinimumLmaxTickerVolumeUsd = minimumLmaxTickerVolumeUsd;
            this.MaximumLmaxTickerVolumeUsd = maximumLmaxTickerVolumeUsd;
            this.MinimumLmaxTickerAvgDealSizeUsd = minimumLmaxTickerAvgDealSizeUsd;
            this.MaximumLmaxTickerAvgDealSizeUsd = maximumLmaxTickerAvgDealSizeUsd;
        }

        public decimal MaximumPositionBaseAbs { get; private set; }
        public long StopLossNetInUsd { get; private set; }
        public QuotingSpreadType SpreadType { get; private set; }
        public decimal FixedSpreadBasisPoints { get; private set; }
        public decimal MinDynamicSpreadBp { get; private set; }
        public decimal DynamicMarkupDecimal { get; private set; }
        public bool SkewEnabled { get; private set; }
        public decimal SkewDecimal { get; private set; }
        public TimeSpan InactivityTimeout { get; private set; }
        public decimal LiquidationSpreadBasisPoints { get; private set; }
        public TimeSpan LmaxTickerLookbackInterval { get; private set; }
        public long MinimumLmaxTickerVolumeUsd { get; private set; }
        public long MaximumLmaxTickerVolumeUsd { get; private set; }
        public long MinimumLmaxTickerAvgDealSizeUsd { get; private set; }
        public long MaximumLmaxTickerAvgDealSizeUsd { get; private set; }



        public override string ToString()
        {
            return
                String.Format(
                    "VenueQuotingSystemSettingsBag {0} - MaximumPositionBaseAbs: {1}; FixedSpreadBasisPoints: {2}; StopLossNetInUsd: {3}; SpreadType: {4}; MinDynamicSpreadBp: {5}; DynamicMarkupDecimal: {6}; SkewEnabled: {7}; SkewDecimal: {8}; InactivityTimeout: {9}, LiquidationSpreadBasisPoints: {10}, LmaxTickerLookbackInterval: {11}, MinimumLmaxTickerVolumeUsd: {12}, MaximumLmaxTickerVolumeUsd: {13}, MinimumLmaxTickerAvgDealSizeUsd: {14}, MaximumLmaxTickerAvgDealSizeUsd: {15}",
                    base.ToString(), MaximumPositionBaseAbs, FixedSpreadBasisPoints, StopLossNetInUsd, SpreadType, MinDynamicSpreadBp, DynamicMarkupDecimal, SkewEnabled, SkewDecimal, InactivityTimeout, LiquidationSpreadBasisPoints, LmaxTickerLookbackInterval, MinimumLmaxTickerVolumeUsd, MaximumLmaxTickerVolumeUsd, MinimumLmaxTickerAvgDealSizeUsd, MaximumLmaxTickerAvgDealSizeUsd);
        }
    }

    public interface IIntegratedSystemsDal
    {
        bool TryHardBlockSystem(int tradingSystemId);
        bool TryDisableSystem(int tradingSystemId);
    }

    public class IntegratedSystemsSettingsProvider : IIntegratedSystemsDal
    {
        private DateTime _lastCrossSettingsUpdateTimeUtc;
        private DateTime _lastSmartCrossSettingsUpdateTimeUtc;
        private DateTime _lastMMSettingsUpdateTimeUtc;
        private DateTime _lastStreamMMSettingsUpdateTimeUtc;
        private DateTime _lastGliderMMSettingsUpdateTimeUtc;
        private DateTime _lastQuotingSettingsUpdateTimeUtc;
        private SafeTimer _pollTimer;

        private IntegratedSystemsSettingsProvider() { }
        private static IntegratedSystemsSettingsProvider _instance = new IntegratedSystemsSettingsProvider();
        public static IntegratedSystemsSettingsProvider Instance { get { return _instance; }}

        public void EnableRaisingEvents()
        {
            if (_pollTimer == null)
            {
                _pollTimer = new SafeTimer(PollChanges, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(3), true)
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.TimerPrecision.UpTo1sec,
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
            }
        }

        public bool TryHardBlockSystem(int tradingSystemId)
        {
            try
            {
                return this.HardBlockSystemInternal(tradingSystemId);
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                    .LogException(LogLevel.Fatal, e, "Exception when attempting to hard block system with id: {0}",
                        tradingSystemId);
                return false;
            }
        }

        public bool TryDisableSystem(int tradingSystemId)
        {
            try
            {
                return this.DisableSystemInternal(tradingSystemId);
            }
            catch (Exception e)
            {
                LogFactory.Instance.GetLogger(null)
                    .LogException(LogLevel.Fatal, e, "Exception when attempting to Disable system with id: {0}",
                        tradingSystemId);
                return false;
            }
        }

        private bool HardBlockSystemInternal(int tradingSystemId)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand blockCommand = new SqlCommand("[dbo].[VenueSystemHardblock_SP]",
                                                                             sqlConnection))
                {
                    blockCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    blockCommand.Parameters.AddWithValue("@TradingSystemId", tradingSystemId);

                    return blockCommand.ExecuteNonQuery() > 0;
                }
            }
        }

        private bool DisableSystemInternal(int tradingSystemId)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand blockCommand = new SqlCommand("[dbo].[VenueSystemDisableEnableOne_SP]",
                                                                             sqlConnection))
                {
                    blockCommand.CommandType = CommandType.StoredProcedure;
                    blockCommand.Parameters.AddWithValue("@TradingSystemId", tradingSystemId);
                    blockCommand.Parameters.AddWithValue("@Enable", false);

                    return blockCommand.ExecuteNonQuery() > 0;
                }
            }
        }

        public event Action<List<VenueCrossSystemSettingsBag>> VenueCrossSystemSettingsUpdated;

        public List<VenueCrossSystemSettingsBag> GetFullVenueCrossSystemSettingsList()
        {
            return this.GetUpdatedVenueCrossSystemSettings(DateTime.MinValue);
        }

        public event Action<List<VenueSmartCrossSystemSettingsBag>> VenueSmartCrossSystemSettingsUpdated;

        public List<VenueSmartCrossSystemSettingsBag> GetFullVenueSmartCrossSystemSettingsList()
        {
            return this.GetUpdatedVenueSmartCrossSystemSettings(DateTime.MinValue);
        }

        public event Action<List<VenueMMSystemSettingsBag>> VenueMMSystemSettingsUpdated;

        public List<VenueMMSystemSettingsBag> GetFullVenueMMSystemSettingsList()
        {
            return this.GetUpdatedVenueMMSystemSettings(DateTime.MinValue);
        }

        public event Action<List<VenueStreamMMSystemSettingsBag>> VenueStreamMMSystemSettingsUpdated;

        public List<VenueStreamMMSystemSettingsBag> GetFullVenueStreamMMSystemSettingsList()
        {
            return this.GetUpdatedVenueStreamMMSystemSettings(DateTime.MinValue);
        }

        public event Action<List<VenueGliderStreamMMSystemSettingsBag>> VenueGliderStreamMMSystemSettingsUpdated;

        public List<VenueGliderStreamMMSystemSettingsBag> GetFullVenueGliderStreamMMSystemSettingsList()
        {
            return this.GetUpdatedVenueGliderStreamMMSystemSettings(DateTime.MinValue);
        }

        public event Action<List<VenueQuotingSystemSettingsBag>> VenueQuotingSystemSettingsUpdated;

        public List<VenueQuotingSystemSettingsBag> GetFullVenueQuotingSystemSettingsList()
        {
            return this.GetUpdatedVenueQuotingSystemSettings(DateTime.MinValue);
        }

        private void PollChanges()
        {
            //DateTime lastCrossSettingsUpdateTimeUtc = this._lastCrossSettingsUpdateTimeUtc;
            //var crossUpdatesList = this.GetUpdatedVenueCrossSystemSettings(lastCrossSettingsUpdateTimeUtc);
            ////prevent unnecesary clearings if there was no change
            //if ((this._lastCrossSettingsUpdateTimeUtc > lastCrossSettingsUpdateTimeUtc ||
            //     crossUpdatesList.Count > 0)
            //    &&
            //    VenueCrossSystemSettingsUpdated != null)
            //{
            //    VenueCrossSystemSettingsUpdated(crossUpdatesList);
            //}

            DateTime lastSmartCrossSettingsUpdateTimeUtc = this._lastSmartCrossSettingsUpdateTimeUtc;
            var smartCrossUpdatesList = this.GetUpdatedVenueSmartCrossSystemSettings(lastSmartCrossSettingsUpdateTimeUtc);
            //prevent unnecesary clearings if there was no change
            if ((this._lastSmartCrossSettingsUpdateTimeUtc > lastSmartCrossSettingsUpdateTimeUtc ||
                 smartCrossUpdatesList.Count > 0)
                &&
                VenueSmartCrossSystemSettingsUpdated != null)
            {
                VenueSmartCrossSystemSettingsUpdated(smartCrossUpdatesList);
            }


            DateTime lastMMSettingsUpdateTimeUtc = this._lastMMSettingsUpdateTimeUtc;
            var mmUpdatesList = this.GetUpdatedVenueMMSystemSettings(lastMMSettingsUpdateTimeUtc);
            //prevent unnecesary clearings if there was no change
            if ((this._lastMMSettingsUpdateTimeUtc > lastMMSettingsUpdateTimeUtc ||
                 mmUpdatesList.Count > 0)
                && VenueMMSystemSettingsUpdated != null)
            {
                VenueMMSystemSettingsUpdated(mmUpdatesList);
            }


            DateTime lastStreamMMSettingsUpdateTimeUtc = this._lastStreamMMSettingsUpdateTimeUtc;
            var streamMMUpdatesList = this.GetUpdatedVenueStreamMMSystemSettings(lastStreamMMSettingsUpdateTimeUtc);
            //prevent unnecesary clearings if there was no change
            if ((this._lastStreamMMSettingsUpdateTimeUtc > lastStreamMMSettingsUpdateTimeUtc ||
                 streamMMUpdatesList.Count > 0)
                && VenueStreamMMSystemSettingsUpdated != null)
            {
                VenueStreamMMSystemSettingsUpdated(streamMMUpdatesList);
            }

            DateTime lastGliderMMSettingsUpdateTimeUtc = this._lastGliderMMSettingsUpdateTimeUtc;
            var gliderMMUpdatesList = this.GetUpdatedVenueGliderStreamMMSystemSettings(lastGliderMMSettingsUpdateTimeUtc);
            //prevent unnecesary clearings if there was no change
            if ((this._lastGliderMMSettingsUpdateTimeUtc > lastGliderMMSettingsUpdateTimeUtc ||
                 gliderMMUpdatesList.Count > 0)
                && VenueGliderStreamMMSystemSettingsUpdated != null)
            {
                VenueGliderStreamMMSystemSettingsUpdated(gliderMMUpdatesList);
            }

            DateTime lastQuotingSettingsUpdateTimeUtc = this._lastQuotingSettingsUpdateTimeUtc;
            var quotingUpdatesList = this.GetUpdatedVenueQuotingSystemSettings(lastQuotingSettingsUpdateTimeUtc);
            //prevent unnecesary clearings if there was no change
            if ((this._lastQuotingSettingsUpdateTimeUtc > lastQuotingSettingsUpdateTimeUtc ||
                 quotingUpdatesList.Count > 0)
                && VenueQuotingSystemSettingsUpdated != null)
            {
                VenueQuotingSystemSettingsUpdated(quotingUpdatesList);
            }
        }

        private List<VenueCrossSystemSettingsBag> GetUpdatedVenueCrossSystemSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getLmaxSystemSettingsCommand = new SqlCommand("[dbo].[GetUpdatedVenueCrossSystemSettings_SP]",
                                                                             sqlConnection))
                {
                    getLmaxSystemSettingsCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    getLmaxSystemSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                        {
                            Direction = ParameterDirection.Output
                        };
                    getLmaxSystemSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<VenueCrossSystemSettingsBag> lmaxSystemSettingsList = new List<VenueCrossSystemSettingsBag>();

                    using (var reader = getLmaxSystemSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            decimal minimumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade"));
                            decimal minimumFillSize = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize"));
                            decimal maximumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade"));
                            int tradingSystemId = reader.GetInt32(reader.GetOrdinal("TradingSystemId"));

                            if (minimumFillSize > minimumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Cross TradingSystem [{0}] minimumFillSize ({1}) > minimumSizeToTrade ({2}) - increasing minimumSizeToTrade",
                                    tradingSystemId, minimumFillSize, minimumSizeToTrade);
                                minimumSizeToTrade = minimumFillSize;
                            }

                            if (minimumSizeToTrade > maximumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Cross TradingSystem [{0}] minimumSizeToTrade ({1}) > maximumSizeToTrade ({2}) - decreasing minimumSizeToTrade",
                                    tradingSystemId, minimumSizeToTrade, maximumSizeToTrade);
                                minimumSizeToTrade = maximumSizeToTrade;
                            }

                            lmaxSystemSettingsList
                                .Add(new VenueCrossSystemSettingsBag(
                                        symbol: (Symbol) reader.GetString(reader.GetOrdinal("Symbol")),
                                        counterparty: (Counterparty) reader.GetString(reader.GetOrdinal("Counterparty")),
                                        tradingSystemId: tradingSystemId,
                                        tradingGroupId: reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId")),
                                        minimumSizeToTrade: minimumSizeToTrade,
                                        maximumSizeToTrade: maximumSizeToTrade,
                                        minimumDiscountBasisPointsToTrade: reader.GetDecimal(reader.GetOrdinal("MinimumDiscountBasisPointsToTrade")),
                                        minimumTimeBetweenSingleCounterpartySignals: TimeSpan.FromMilliseconds((double)(1000m * reader.GetDecimal(reader.GetOrdinal("MinimumTimeBetweenSingleCounterpartySignals_seconds")))),
                                        maximumWaitTimeOnImprovementTick: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("MaximumWaitTimeOnImprovementTick_milliseconds"))),
                                        minimumFillSize: minimumFillSize,
                                        enabled: reader.GetBoolean(reader.GetOrdinal("Enabled")),
                                        goFlatOnly: reader.GetBoolean(reader.GetOrdinal("GoFlatOnly")),
                                        transmissionDisabled: reader.GetBoolean(reader.GetOrdinal("OrderTransmissionDisabled")),
                                        lastResetRequestedUtc: reader.IsDBNull(reader.GetOrdinal("LastResetRequestedUtc")) ? (DateTime?) null : reader.GetDateTime(reader.GetOrdinal("LastResetRequestedUtc")),
                                        isUpdated: !reader.IsDBNull(reader.GetOrdinal("LastUpdatedUtc")) && reader.GetDateTime(reader.GetOrdinal("LastUpdatedUtc")) > lastUpdateTimeUtc)
                                    );
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastCrossSettingsUpdateTimeUtc = (DateTime) val;
                    }

                    return lmaxSystemSettingsList;

                }
            }
        }

        private List<VenueSmartCrossSystemSettingsBag> GetUpdatedVenueSmartCrossSystemSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getSmartCrossSystemSettingsCommand = new SqlCommand("[dbo].[GetUpdatedVenueSmartCrossSystemSettings_SP]",
                                                                             sqlConnection))
                {
                    getSmartCrossSystemSettingsCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    getSmartCrossSystemSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                        {
                            Direction = ParameterDirection.Output
                        };
                    getSmartCrossSystemSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<VenueSmartCrossSystemSettingsBag> smartCrossSystemSettingsList = new List<VenueSmartCrossSystemSettingsBag>();

                    using (var reader = getSmartCrossSystemSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            decimal minimumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade"));
                            decimal minimumFillSize = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize"));
                            decimal maximumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade"));
                            int tradingSystemId = reader.GetInt32(reader.GetOrdinal("TradingSystemId"));

                            if (minimumFillSize > minimumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Cross TradingSystem [{0}] minimumFillSize ({1}) > minimumSizeToTrade ({2}) - increasing minimumSizeToTrade",
                                    tradingSystemId, minimumFillSize, minimumSizeToTrade);
                                minimumSizeToTrade = minimumFillSize;
                            }

                            if (minimumSizeToTrade > maximumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Cross TradingSystem [{0}] minimumSizeToTrade ({1}) > maximumSizeToTrade ({2}) - decreasing minimumSizeToTrade",
                                    tradingSystemId, minimumSizeToTrade, maximumSizeToTrade);
                                minimumSizeToTrade = maximumSizeToTrade;
                            }

                            smartCrossSystemSettingsList
                                .Add(new VenueSmartCrossSystemSettingsBag(
                                        symbol: (Symbol) reader.GetString(reader.GetOrdinal("Symbol")),
                                        counterparty: (Counterparty) reader.GetString(reader.GetOrdinal("Counterparty")),
                                        tradingSystemId: tradingSystemId,
                                        tradingGroupId: reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId")),
                                        minimumSizeToTrade: minimumSizeToTrade,
                                        maximumSizeToTrade: maximumSizeToTrade,
                                        minimumDiscountBasisPointsToTrade: reader.GetDecimal(reader.GetOrdinal("MinimumDiscountBasisPointsToTrade")),
                                        minimumTimeBetweenSingleCounterpartySignals: TimeSpan.FromMilliseconds((double)(1000m * reader.GetDecimal(reader.GetOrdinal("MinimumTimeBetweenSingleCounterpartySignals_seconds")))),
                                        maximumWaitTimeOnImprovementTick: TimeSpan.Zero,
                                        minimumFillSize: minimumFillSize,
                                        coverDistanceGrossBasisPoints: reader.GetDecimal(reader.GetOrdinal("CoverDistanceGrossBasisPoints")),
                                        coverTimeout: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("CoverTimeout_milliseconds"))),
                                        minimumTrueGainGrossBp: reader.GetDecimal(reader.GetOrdinal("MinimumTrueGainGrossBasisPoints")),
                                        enabled: reader.GetBoolean(reader.GetOrdinal("Enabled")),
                                        goFlatOnly: reader.GetBoolean(reader.GetOrdinal("GoFlatOnly")),
                                        transmissionDisabled: reader.GetBoolean(reader.GetOrdinal("OrderTransmissionDisabled")),
                                        lastResetRequestedUtc: reader.IsDBNull(reader.GetOrdinal("LastResetRequestedUtc")) ? (DateTime?) null : reader.GetDateTime(reader.GetOrdinal("LastResetRequestedUtc")),
                                        isUpdated: !reader.IsDBNull(reader.GetOrdinal("LastUpdatedUtc")) && reader.GetDateTime(reader.GetOrdinal("LastUpdatedUtc")) > lastUpdateTimeUtc)
                                    );
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastSmartCrossSettingsUpdateTimeUtc = (DateTime)val;
                    }

                    return smartCrossSystemSettingsList;

                }
            }
        }

        private List<VenueStreamMMSystemSettingsBag> GetUpdatedVenueStreamMMSystemSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getStreamSystemSettingsCommand = new SqlCommand("[dbo].[GetUpdatedVenueStreamSystemSettings_SP]",
                                                                             sqlConnection))
                {
                    getStreamSystemSettingsCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    getStreamSystemSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                    {
                        Direction = ParameterDirection.Output
                    };
                    getStreamSystemSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<VenueStreamMMSystemSettingsBag> streamSystemSettingsList = new List<VenueStreamMMSystemSettingsBag>();

                    using (var reader = getStreamSystemSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            decimal minimumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade"));
                            decimal minimumFillSize = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize"));
                            decimal maximumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade"));
                            int tradingSystemId = reader.GetInt32(reader.GetOrdinal("TradingSystemId"));

                            if (minimumFillSize > minimumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Stream TradingSystem [{0}] minimumFillSize ({1}) > minimumSizeToTrade ({2}) - increasing minimumSizeToTrade",
                                    tradingSystemId, minimumFillSize, minimumSizeToTrade);
                                minimumSizeToTrade = minimumFillSize;
                            }

                            if (minimumSizeToTrade > maximumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Stream TradingSystem [{0}] minimumSizeToTrade ({1}) > maximumSizeToTrade ({2}) - decreasing minimumSizeToTrade",
                                    tradingSystemId, minimumSizeToTrade, maximumSizeToTrade);
                                minimumSizeToTrade = maximumSizeToTrade;
                            }

                            streamSystemSettingsList
                                .Add(new VenueStreamMMSystemSettingsBag(
                                        tradingSystemId: tradingSystemId,
                                        tradingGroupId: reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId")),
                                        symbol: (Symbol)reader.GetString(reader.GetOrdinal("Symbol")),
                                        counterparty: (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty")),
                                        maximumSizeToTrade: maximumSizeToTrade,
                                        bestPriceImprovementOffsetBasisPoints: reader.GetDecimal(reader.GetOrdinal("BestPriceImprovementOffsetBasisPoints")),
                                        maximumDiscountBasisPointsToTrade: reader.GetDecimal(reader.GetOrdinal("MaximumDiscountBasisPointsToTrade")),
                                        kgtLLTime: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("KGTLLTime_milliseconds"))),
                                        preferLLDestinationCheckToLmax: reader.GetBoolean(reader.GetOrdinal("PreferLLDestinationCheckToLmax")),
                                        minimumBPGrossToAccept: reader.GetDecimal(reader.GetOrdinal("MinimumBPGrossToAccept")),
                                        preferLmaxDuringHedging: reader.GetBoolean(reader.GetOrdinal("PreferLmaxDuringHedging")),
                                        minimumIntegratorPriceLife: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("MinimumIntegratorPriceLife_milliseconds"))),
                                        minimumSizeToTrade: minimumSizeToTrade,
                                        minimumFillSize: minimumFillSize,
                                        maximumSourceBookTiers: reader.GetInt32(reader.GetOrdinal("MaximumSourceBookTiers")),
                                        minimumBasisPointsFromKgtPriceToFastCancel: reader.GetDecimal(reader.GetOrdinal("MinimumBasisPointsFromKgtPriceToFastCancel")),
                                        referenceLmaxBookOnly: reader.GetBoolean(reader.GetOrdinal("ReferenceLmaxBookOnly")),
                                        enabled: reader.GetBoolean(reader.GetOrdinal("Enabled")),
                                        bidEnabled: reader.GetBoolean(reader.GetOrdinal("BidEnabled")),
                                        askEnabled: reader.GetBoolean(reader.GetOrdinal("AskEnabled")),
                                        goFlatOnly: reader.GetBoolean(reader.GetOrdinal("GoFlatOnly")),
                                        transmissionDisabled: reader.GetBoolean(reader.GetOrdinal("OrderTransmissionDisabled")),
                                        lastResetRequestedUtc: reader.IsDBNull(reader.GetOrdinal("LastResetRequestedUtc")) ? (DateTime?)null : reader.GetDateTime(reader.GetOrdinal("LastResetRequestedUtc")),
                                        isUpdated: !reader.IsDBNull(reader.GetOrdinal("LastUpdatedUtc")) && reader.GetDateTime(reader.GetOrdinal("LastUpdatedUtc")) > lastUpdateTimeUtc)
                                    );
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastStreamMMSettingsUpdateTimeUtc = (DateTime)val;
                    }

                    return streamSystemSettingsList;

                }
            }
        }

        private List<VenueGliderStreamMMSystemSettingsBag> GetUpdatedVenueGliderStreamMMSystemSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getGliderSystemSettingsCommand = new SqlCommand("[dbo].[GetUpdatedVenueGliderSystemSettings_SP]",
                                                                             sqlConnection))
                {
                    getGliderSystemSettingsCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    getGliderSystemSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                    {
                        Direction = ParameterDirection.Output
                    };
                    getGliderSystemSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<VenueGliderStreamMMSystemSettingsBag> gliderSystemSettingsList = new List<VenueGliderStreamMMSystemSettingsBag>();

                    using (var reader = getGliderSystemSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            decimal minimumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade"));
                            decimal minimumFillSize = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize"));
                            decimal maximumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade"));
                            int tradingSystemId = reader.GetInt32(reader.GetOrdinal("TradingSystemId"));

                            if (minimumFillSize > minimumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Stream TradingSystem [{0}] minimumFillSize ({1}) > minimumSizeToTrade ({2}) - increasing minimumSizeToTrade",
                                    tradingSystemId, minimumFillSize, minimumSizeToTrade);
                                minimumSizeToTrade = minimumFillSize;
                            }

                            if (minimumSizeToTrade > maximumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "Stream TradingSystem [{0}] minimumSizeToTrade ({1}) > maximumSizeToTrade ({2}) - decreasing minimumSizeToTrade",
                                    tradingSystemId, minimumSizeToTrade, maximumSizeToTrade);
                                minimumSizeToTrade = maximumSizeToTrade;
                            }

                            gliderSystemSettingsList
                                .Add(new VenueGliderStreamMMSystemSettingsBag(
                                        tradingSystemId: tradingSystemId,
                                        tradingGroupId: reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId")),
                                        symbol: (Symbol)reader.GetString(reader.GetOrdinal("Symbol")),
                                        counterparty: (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty")),
                                        maximumSizeToTrade: maximumSizeToTrade,
                                        bestPriceImprovementOffsetBasisPoints: reader.GetDecimal(reader.GetOrdinal("BestPriceImprovementOffsetBasisPoints")),
                                        maximumDiscountBasisPointsToTrade: reader.GetDecimal(reader.GetOrdinal("MaximumDiscountBasisPointsToTrade")),
                                        kgtLLTime: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("KGTLLTime_milliseconds"))),
                                        preferLLDestinationCheckToLmax: reader.GetBoolean(reader.GetOrdinal("PreferLLDestinationCheckToLmax")),
                                        minimumBPGain: reader.GetDecimal(reader.GetOrdinal("MinimumBPGrossGain")),
                                        minimumBPGrossGainIfZeroGlideFill: reader.GetDecimal(reader.GetOrdinal("MinimumBPGrossGainIfZeroGlideFill")),
                                        preferLmaxDuringHedging: reader.GetBoolean(reader.GetOrdinal("PreferLmaxDuringHedging")),
                                        minimumIntegratorPriceLife: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("MinimumIntegratorPriceLife_milliseconds"))),
                                        minimumGlidePriceLife: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("MinimumIntegratorGlidePriceLife_milliseconds"))),
                                        glidingCutoffSpan: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("GlidingCutoffSpan_milliseconds"))),
                                        minimumSizeToTrade: minimumSizeToTrade,
                                        minimumFillSize: minimumFillSize,
                                        maximumSourceBookTiers: reader.GetInt32(reader.GetOrdinal("MaximumSourceBookTiers")),
                                        minimumBasisPointsFromKgtPriceToFastCancel: reader.GetDecimal(reader.GetOrdinal("MinimumBasisPointsFromKgtPriceToFastCancel")),
                                        referenceLmaxBookOnly: reader.GetBoolean(reader.GetOrdinal("ReferenceLmaxBookOnly")),
                                        glidingCounterparty: (Counterparty)reader.GetString(reader.GetOrdinal("GlidingCounterparty")),
                                        enabled: reader.GetBoolean(reader.GetOrdinal("Enabled")),
                                        bidEnabled: reader.GetBoolean(reader.GetOrdinal("BidEnabled")),
                                        askEnabled: reader.GetBoolean(reader.GetOrdinal("AskEnabled")),
                                        goFlatOnly: reader.GetBoolean(reader.GetOrdinal("GoFlatOnly")),
                                        transmissionDisabled: reader.GetBoolean(reader.GetOrdinal("OrderTransmissionDisabled")),
                                        lastResetRequestedUtc: reader.IsDBNull(reader.GetOrdinal("LastResetRequestedUtc")) ? (DateTime?)null : reader.GetDateTime(reader.GetOrdinal("LastResetRequestedUtc")),
                                        isUpdated: !reader.IsDBNull(reader.GetOrdinal("LastUpdatedUtc")) && reader.GetDateTime(reader.GetOrdinal("LastUpdatedUtc")) > lastUpdateTimeUtc)
                                    );
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastGliderMMSettingsUpdateTimeUtc = (DateTime)val;
                    }

                    return gliderSystemSettingsList;

                }
            }
        }

        private List<VenueMMSystemSettingsBag> GetUpdatedVenueMMSystemSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getLmaxSystemSettingsCommand = new SqlCommand("[dbo].[GetUpdatedVenueMMSystemSettings_SP]",
                                                                             sqlConnection))
                {
                    getLmaxSystemSettingsCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    getLmaxSystemSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                    {
                        Direction = ParameterDirection.Output
                    };
                    getLmaxSystemSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<VenueMMSystemSettingsBag> mmSystemSettingsList = new List<VenueMMSystemSettingsBag>();

                    using (var reader = getLmaxSystemSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            decimal minimumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade"));
                            decimal minimumFillSize = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize"));
                            decimal maximumSizeToTrade = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade"));
                            int tradingSystemId = reader.GetInt32(reader.GetOrdinal("TradingSystemId"));

                            if (minimumFillSize > minimumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "MM TradingSystem [{0}] minimumFillSize ({1}) > minimumSizeToTrade ({2}) - increasing minimumSizeToTrade",
                                    tradingSystemId, minimumFillSize, minimumSizeToTrade);
                                minimumSizeToTrade = minimumFillSize;
                            }

                            if (minimumSizeToTrade > maximumSizeToTrade)
                            {
                                LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "MM TradingSystem [{0}] minimumSizeToTrade ({1}) > maximumSizeToTrade ({2}) - decreasing minimumSizeToTrade",
                                    tradingSystemId, minimumSizeToTrade, maximumSizeToTrade);
                                minimumSizeToTrade = maximumSizeToTrade;
                            }

                            mmSystemSettingsList
                                .Add(new VenueMMSystemSettingsBag(
                                        symbol: (Symbol)reader.GetString(reader.GetOrdinal("Symbol")),
                                        counterparty: (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty")),
                                        tradingSystemId: tradingSystemId,
                                        tradingGroupId: reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId")),
                                        maximumSizeToTrade: maximumSizeToTrade,
                                        bestPriceImprovementOffsetBasisPoints: reader.GetDecimal(reader.GetOrdinal("BestPriceImprovementOffsetBasisPoints")),
                                        maximumDiscountBasisPointsToTrade: reader.GetDecimal(reader.GetOrdinal("MaximumDiscountBasisPointsToTrade")),
                                        maximumWaitTimeOnImprovementTick: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("MaximumWaitTimeOnImprovementTick_milliseconds"))),
                                        minimumIntegratorPriceLife: TimeSpan.FromMilliseconds(reader.GetInt32(reader.GetOrdinal("MinimumIntegratorPriceLife_milliseconds"))),
                                        minimumSizeToTrade: minimumSizeToTrade,
                                        minimumFillSize: minimumFillSize,
                                        maximumSourceBookTiers: reader.GetInt32(reader.GetOrdinal("MaximumSourceBookTiers")),
                                        minimumBasisPointsFromKgtPriceToFastCancel: reader.GetDecimal(reader.GetOrdinal("MinimumBasisPointsFromKgtPriceToFastCancel")),
                                        enabled: reader.GetBoolean(reader.GetOrdinal("Enabled")),
                                        bidEnabled: reader.GetBoolean(reader.GetOrdinal("BidEnabled")),
                                        askEnabled: reader.GetBoolean(reader.GetOrdinal("AskEnabled")),
                                        goFlatOnly: reader.GetBoolean(reader.GetOrdinal("GoFlatOnly")),
                                        transmissionDisabled: reader.GetBoolean(reader.GetOrdinal("OrderTransmissionDisabled")),
                                        lastResetRequestedUtc: reader.IsDBNull(reader.GetOrdinal("LastResetRequestedUtc")) ? (DateTime?)null : reader.GetDateTime(reader.GetOrdinal("LastResetRequestedUtc")),
                                        isUpdated: !reader.IsDBNull(reader.GetOrdinal("LastUpdatedUtc")) && reader.GetDateTime(reader.GetOrdinal("LastUpdatedUtc")) > lastUpdateTimeUtc)
                                    );
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastMMSettingsUpdateTimeUtc = (DateTime)val;
                    }

                    return mmSystemSettingsList;

                }
            }
        }

        private List<VenueQuotingSystemSettingsBag> GetUpdatedVenueQuotingSystemSettings(DateTime lastUpdateTimeUtc)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(DALBehavior.Sections.IntegratorConnection.ConnectionString))
            {
                sqlConnection.Open();
                using (
                    SqlCommand getQuotingSystemSettingsCommand = new SqlCommand("[dbo].[GetUpdatedVenueQuotingSystemSettings_SP]",
                                                                             sqlConnection))
                {
                    getQuotingSystemSettingsCommand.CommandType = CommandType.StoredProcedure;
                    //getLmaxSystemSettingsCommand.Parameters.AddWithValue("@LastUpdateTimeUtc", this._lastUpdateTimeUtc);
                    getQuotingSystemSettingsCommand.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value =
                        lastUpdateTimeUtc;

                    SqlParameter lastUpdateTimeOutParam = new SqlParameter("@LastUpdateTimeServerSideUtc",
                                                                           SqlDbType.DateTime2)
                    {
                        Direction = ParameterDirection.Output
                    };
                    getQuotingSystemSettingsCommand.Parameters.Add(lastUpdateTimeOutParam);

                    List<VenueQuotingSystemSettingsBag> quotingSystemSettingsList = new List<VenueQuotingSystemSettingsBag>();

                    using (var reader = getQuotingSystemSettingsCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            quotingSystemSettingsList
                                .Add(new VenueQuotingSystemSettingsBag(
                                        symbol: (Symbol)reader.GetString(reader.GetOrdinal("Symbol")),
                                        counterparty: (Counterparty)reader.GetString(reader.GetOrdinal("Counterparty")),
                                        tradingSystemId: reader.GetInt32(reader.GetOrdinal("TradingSystemId")),
                                        tradingGroupId: reader.GetInt32(reader.GetOrdinal("TradingSystemGroupId")),
                                        maximumPositionBaseAbs: reader.GetDecimal(reader.GetOrdinal("MaximumPositionBaseAbs")),
                                        fixedSpreadBasisPoints: reader.GetDecimal(reader.GetOrdinal("FixedSpreadBasisPoints")),
                                        stopLossNetInUsd: reader.GetInt64(reader.GetOrdinal("StopLossNetInUsd")),
                                        spreadType: (VenueQuotingSystemSettingsBag.QuotingSpreadType) Enum.Parse(typeof(VenueQuotingSystemSettingsBag.QuotingSpreadType), reader.GetString(reader.GetOrdinal("SpreadType"))),
                                        minDynamicSpreadBp: reader.GetDecimal(reader.GetOrdinal("MinimumDynamicSpreadBp")),
                                        dynamicMarkupDecimal: reader.GetDecimal(reader.GetOrdinal("DynamicMarkupDecimal")),
                                        skewEnabled: reader.GetBoolean(reader.GetOrdinal("EnableSkew")),
                                        skewDecimal: reader.GetDecimal(reader.GetOrdinal("SkewDecimal")),
                                        inactivityTimeout: TimeSpan.FromSeconds(reader.GetInt32(reader.GetOrdinal("InactivityTimeoutSec"))),
                                        liquidationSpreadBasisPoints: reader.GetDecimal(reader.GetOrdinal("LiquidationSpreadBp")),
                                        lmaxTickerLookbackInterval: TimeSpan.FromSeconds(reader.GetInt32(reader.GetOrdinal("LmaxTickerLookbackSec"))),
                                        minimumLmaxTickerVolumeUsd: reader.GetInt64(reader.GetOrdinal("MinimumLmaxTickerVolumeUsd")),
                                        maximumLmaxTickerVolumeUsd: reader.GetInt64(reader.GetOrdinal("MaximumLmaxTickerVolumeUsd")),
                                        minimumLmaxTickerAvgDealSizeUsd: reader.GetInt64(reader.GetOrdinal("MinimumLmaxTickerAvgDealSizeUsd")),
                                        maximumLmaxTickerAvgDealSizeUsd: reader.GetInt64(reader.GetOrdinal("MaximumLmaxTickerAvgDealSizeUsd")),
                                        enabled: reader.GetBoolean(reader.GetOrdinal("Enabled")),
                                        goFlatOnly: reader.GetBoolean(reader.GetOrdinal("GoFlatOnly")),
                                        transmissionDisabled: reader.GetBoolean(reader.GetOrdinal("OrderTransmissionDisabled")),
                                        lastResetRequestedUtc: reader.IsDBNull(reader.GetOrdinal("LastResetRequestedUtc")) ? (DateTime?)null : reader.GetDateTime(reader.GetOrdinal("LastResetRequestedUtc")),
                                        isUpdated: !reader.IsDBNull(reader.GetOrdinal("LastUpdatedUtc")) && reader.GetDateTime(reader.GetOrdinal("LastUpdatedUtc")) > lastUpdateTimeUtc)
                                    );
                        }
                    }

                    var val = lastUpdateTimeOutParam.Value;

                    if (val != null && val != DBNull.Value)
                    {
                        _lastQuotingSettingsUpdateTimeUtc = (DateTime)val;
                    }

                    return quotingSystemSettingsList;

                }
            }
        }
    }
}
