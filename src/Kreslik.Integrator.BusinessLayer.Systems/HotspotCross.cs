﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class HotspotRemoteToBProvider: IVenueToBProvider
    {
        private ToBReceiver _toBReceiver;

        private ToBReceiver.ToBReceiverIndex[] _toBReceiverIndexes = new ToBReceiver.ToBReceiverIndex[2];

        public HotspotRemoteToBProvider(Symbol symbol, ToBReceiver toBReceiver, HotspotCounterparty hotspotCounterparty)
        {
            this._toBReceiver = toBReceiver;

            this._toBReceiverIndexes[(int)PriceSide.Bid] = toBReceiver.CreateToBReceiverIndex(symbol, PriceSide.Bid, hotspotCounterparty);
            this._toBReceiverIndexes[(int)PriceSide.Ask] = toBReceiver.CreateToBReceiverIndex(symbol, PriceSide.Ask, hotspotCounterparty);
        }

        public bool TryRetrieveVenueToB(PriceSide side, out decimal price, out decimal size)
        {
            this._toBReceiver.ReceivePrice(true, this._toBReceiverIndexes[(int) side], out price, out size);

            //zeros are returned if receiving failed, or if the retrieved price is null price
            return price != 0m && size != 0m;
        }
    }

    internal class HotspotCrossSystem: VenueCrossSystemBase
    {
        public HotspotCrossSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                  IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook,
                                  ToBReceiver toBReceiver, Symbol symbol, IOrderManagement orderManagement,
                                  IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
                                  IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                                  IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                  IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings> settingsProvider, HotspotCounterparty hotspotCounterparty)
            : base(
                askPriceBook, bidPriceBook,
                new HotspotRemoteToBProvider(symbol, toBReceiver, hotspotCounterparty),
                symbol, hotspotCounterparty, destinationOrdSession, symbolsInfo, orderManagement, riskManager, 
                tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        { }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
                                                                               decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotOrder(
                sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol,
                TimeInForce.ImmediateOrKill, (HotspotCounterparty) this._destinationVenueCounterparty);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateHotspotClientOrder(orderIdentity, clientIdentity, venueOrderRequest as HotspotClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }

        private readonly Counterparty[] _counterpartiesWhiteList = new Counterparty[] { Counterparty.CZB, Counterparty.FC1, Counterparty.GLS, Counterparty.JPM };
        protected override Counterparty[] CounterpartiesWhiteList { get { return _counterpartiesWhiteList; } }
    }
}
