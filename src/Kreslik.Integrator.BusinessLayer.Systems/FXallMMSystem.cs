﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class FXallMMSystem: VenueMMSystemBase
    {
        public FXallMMSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                             IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                             PriceSide observedBookSide, IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                             IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
                             IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
                             IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                             IIntegratedSystemSettingsProvider<IVenueMMSystemSettings> settingsProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, observedBookSide, Counterparty.FA1, destinationOrdSession, symbolsInfo,
                medianPriceProvider, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, false, null)
        { }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXallOrder(
                sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol, TimeInForce.Day);
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXallReplaceOrder(
                replacedOrderId, replacingOrderRequest as FXallClientOrderRequestInfo);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXallClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as FXallClientOrderRequestInfo, integratedTradingSystemIdentification);
        }
    }
}
