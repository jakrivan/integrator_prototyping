﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class HotspotStreamingTakerMMSystem : StreamingTakerMMSystemBase<IVenueStreamMMSystemSettings>
    {
        public HotspotStreamingTakerMMSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook, Symbol symbol,
            PriceSide observedBookSide, IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
            IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
            IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
            IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<IVenueStreamMMSystemSettings> settingsProvider,
            HotspotCounterparty hotspotCounterparty, EventsRateCheckerEx outgoingOrdersRateCheck)
            : base(
                lmaxAskPriceBook, lmaxBidPriceBook, bankpoolAskPricebook, bankpoolBidPricebook, symbol, observedBookSide,
                hotspotCounterparty, destinationOrdSession, symbolsInfo,
                medianPriceProvider, orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, true,
                outgoingOrdersRateCheck)
        { }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol, decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotOrder
                (sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol, TimeInForce.Day, (HotspotCounterparty)_destinationVenueCounterparty);
        }

        protected override VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotReplaceOrder(
                replacedOrderId, replacingOrderRequest as HotspotClientOrderRequestInfo);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateHotspotClientOrder(
                orderIdentity, clientIdentity, venueOrderRequest as HotspotClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }
    }
}
