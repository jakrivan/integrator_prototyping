﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    internal class FXCMCrossSystem : VenueCrossSystemBase
    {
        public FXCMCrossSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook,
                                IBookTop<PriceObjectInternal> venueAskPriceBook, IBookTop<PriceObjectInternal> venueBidPriceBook,
                                Symbol symbol, IOrderManagement orderManagement, IRiskManager riskManager,
                                ITradingGroupsCache tradingGroupsCache,
                                IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                                IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings> settingsProvider,
                                FXCMCounterparty fxcmCounterparty)
            : base(
                askPriceBook, bidPriceBook,
                new InProcessVenueToBProvider(venueAskPriceBook, venueBidPriceBook),
                symbol, fxcmCounterparty, destinationOrdSession, symbolsInfo, orderManagement, riskManager,
                tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        { }


        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
                                                                               decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXCMOrder(
                sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol,
                TimeInForce.ImmediateOrKill, (FXCMCounterparty) _destinationVenueCounterparty);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateFXCMClientOrder(orderIdentity, clientIdentity, venueOrderRequest as FXCMClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }

        private readonly Counterparty[] _counterpartiesWhiteList = new Counterparty[] { Counterparty.CZB, Counterparty.FA1, Counterparty.GLS, Counterparty.JPM };
        protected override Counterparty[] CounterpartiesWhiteList { get { return _counterpartiesWhiteList; } }
    }
}
