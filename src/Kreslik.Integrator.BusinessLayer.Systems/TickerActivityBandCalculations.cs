﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public static class LmaxTickerActivityMonitorConsts
    {
        public const int MaximumRequiredStatsGranularitySeconds = 5;
        public const int MaximumRequiredLookbackSeconds = 900;

        public static readonly TimeSpan MaximumRequiredStatsGranularityDuration =
            TimeSpan.FromSeconds(MaximumRequiredStatsGranularitySeconds);
    }

    internal struct ActivityBandInfo
    {
        public int DealsCnt { get; private set; }
        public decimal TotalVolumeBaseAbs { get; private set; }
        public decimal PaidVolumeBaseAbs { get; private set; }

        public void AddDeal(VenueDealObjectInternal deal)
        {
            this.DealsCnt++;
            this.TotalVolumeBaseAbs += deal.DealSizeBaseAbs;
            if (deal.TradeSide == TradeSide.BuyPaid)
            {
                this.PaidVolumeBaseAbs += deal.DealSizeBaseAbs;
            }
        }

        public void ZeroOut()
        {
            this.DealsCnt = 0;
            this.TotalVolumeBaseAbs = 0;
            this.PaidVolumeBaseAbs = 0;
        }

        public static ActivityBandInfo operator +(ActivityBandInfo a, ActivityBandInfo b)
        {
            ActivityBandInfo sum = new ActivityBandInfo();
            sum.DealsCnt = a.DealsCnt + b.DealsCnt;
            sum.TotalVolumeBaseAbs = a.TotalVolumeBaseAbs + b.TotalVolumeBaseAbs;
            sum.PaidVolumeBaseAbs = a.PaidVolumeBaseAbs + b.PaidVolumeBaseAbs;
            return sum;
        }

        public override string ToString()
        {
            return string.Format("[DealsCnt: {0}; VolBaseAbs: {1}; PaidBolBaseAbs: {2}]", this.DealsCnt, this.TotalVolumeBaseAbs, this.PaidVolumeBaseAbs);
        }
    }

    public class LmaxTickerActivityMonitorWrapper
    {
        private TimeSpan _observedStatsInterval;

        public LmaxTickerActivityMonitorWrapper(LmaxTickerActivityMonitor activityMonitor, TimeSpan observedStatsInterval)
        {
            this.SetObservedStatsInterval(observedStatsInterval);
            this.ActivityMonitorOnStatsRecalculating(activityMonitor);
            activityMonitor.StatsRecalculating += ActivityMonitorOnStatsRecalculating;
        }

        public IActivityBandStats CurrentStats { get; private set; }

        public event Action<IActivityBandStats> NewActivityStats;

        private void ActivityMonitorOnStatsRecalculating(LmaxTickerActivityMonitor lmaxTickerActivityMonitor)
        {
            this.CurrentStats = lmaxTickerActivityMonitor.GetHistoricActivityInfo(this._observedStatsInterval);
            if (NewActivityStats != null)
                NewActivityStats(this.CurrentStats);
        }

        public void SetObservedStatsInterval(TimeSpan observedStatsInterval)
        {
            int totalSeconds = (int) observedStatsInterval.TotalSeconds;
            if(totalSeconds > LmaxTickerActivityMonitorConsts.MaximumRequiredLookbackSeconds)
                throw new ArgumentException(
                    string.Format("Unexpectedly high duration for LmaxTickerActivityMonitor ([{0}])", observedStatsInterval));

            if (totalSeconds%LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds != 0)
                throw new ArgumentException(
                    string.Format(
                        "Unsupported granularity requested for LmaxTickerActivityMonitor (requested: [{0}] supported granularity: {1})",
                        observedStatsInterval,
                        LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds));

            this._observedStatsInterval = observedStatsInterval;
        }
    }

    public class LmaxTickerActivityMonitor
    {
        private readonly ActivityBandInfo[] _activityBandChunks;
        private int _currentActivityBandChunkIdx = 0;
        private readonly TimeSpan _activityBandChunkLength;
        private ISafeTimer _statsRecalculationTimer;

        private IUsdConversionProvider _usdConversionProvider;
        private ulong _totalChunksCollected = 0;
        private Symbol _observeSymbol;


        private static Dictionary<Symbol, LmaxTickerActivityMonitor> _instancesDic = new Dictionary<Symbol, LmaxTickerActivityMonitor>();

        public static LmaxTickerActivityMonitor GetNewLmaxTickerActivityMonitor(ITickerProvider lmaxTickerProvider, ISafeTimerFactory safeTimerFactory, IUsdConversionProvider usdConversionProvider,
            Symbol observeSymbol)
        {
            LmaxTickerActivityMonitor monitor;
            if (!_instancesDic.TryGetValue(observeSymbol, out monitor))
            {
                monitor = new LmaxTickerActivityMonitor(lmaxTickerProvider, safeTimerFactory, usdConversionProvider, observeSymbol);
                _instancesDic.Add(observeSymbol, monitor);
            }

            return monitor;
        }

        public static void TestOnly__ClearInstanceCache()
        {
            _instancesDic.Clear();
        }


        public event Action<LmaxTickerActivityMonitor> StatsRecalculating;

        private LmaxTickerActivityMonitor(ITickerProvider lmaxTickerProvider, ISafeTimerFactory safeTimerFactory, IUsdConversionProvider usdConversionProvider,
            Symbol observeSymbol)
        {
            lmaxTickerProvider.NewVenueDeal += LmaxTickerProviderOnNewVenueDeal;
            this._usdConversionProvider = usdConversionProvider;
            _activityBandChunks =
                new ActivityBandInfo[
                    LmaxTickerActivityMonitorConsts.MaximumRequiredLookbackSeconds /
                    LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds];
            this._observeSymbol = observeSymbol;
            _activityBandChunkLength =
                TimeSpan.FromSeconds(LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds);
            _statsRecalculationTimer = safeTimerFactory.CreateSafeTimer(OnChunkIntervalEnd, _activityBandChunkLength, _activityBandChunkLength, true);
        }

        private void LmaxTickerProviderOnNewVenueDeal(VenueDealObjectInternal venueDealObjectInternal)
        {
            if (venueDealObjectInternal.Symbol != _observeSymbol)
                return;

            this._activityBandChunks[this._currentActivityBandChunkIdx].AddDeal(venueDealObjectInternal);
        }

        private void OnChunkIntervalEnd()
        {
            _currentActivityBandChunkIdx = GetWrappedIndex(_currentActivityBandChunkIdx + 1);
            _totalChunksCollected++;
            this._activityBandChunks[_currentActivityBandChunkIdx].ZeroOut();

            if (StatsRecalculating != null)
                StatsRecalculating(this);
        }

        private int GetWrappedIndex(int rawIndexValue)
        {
            while (rawIndexValue < 0)
            {
                rawIndexValue += _activityBandChunks.Length;
            }

            while (rawIndexValue >= _activityBandChunks.Length)
            {
                rawIndexValue -= _activityBandChunks.Length;
            }

            return rawIndexValue;
        }

        internal IActivityBandStats GetHistoricActivityInfo(TimeSpan activityDuration)
        {
            int chunksNumToInspect = ((int)activityDuration.TotalSeconds) /
                                     LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds;

            bool hasEnoughData = this._totalChunksCollected >= (ulong)chunksNumToInspect;

            ActivityBandInfo sum = new ActivityBandInfo();

            int firstIndexToInspect = this.GetWrappedIndex(_currentActivityBandChunkIdx - chunksNumToInspect);
            for (int idx = 0; idx < chunksNumToInspect; idx++)
            {
                sum += _activityBandChunks[this.GetWrappedIndex(firstIndexToInspect + idx)];
            }

            decimal totalVolUsd = sum.TotalVolumeBaseAbs*this._usdConversionProvider.GetCurrentUsdConversionMultiplier();
            decimal paidVolumeRatio = sum.TotalVolumeBaseAbs == 0m ? 0m : sum.PaidVolumeBaseAbs / sum.TotalVolumeBaseAbs;

            return new ActivityBandStats(totalVolUsd, sum.DealsCnt > 0 ? totalVolUsd / sum.DealsCnt : 0m, paidVolumeRatio, hasEnoughData);
        }

        private struct ActivityBandStats : IActivityBandStats
        {
            public decimal TotalVolumUsd { get; private set; }
            public decimal AvgDealSizeUsd { get; private set; }
            public decimal PaidVolumeRatio { get; private set; }
            public bool HadEnoughData { get; private set; }

            internal ActivityBandStats(decimal totalVolumeUsd, decimal avgDealSizeUsd, decimal paidVolRatio, bool hadEnoughData)
                :this()
            {
                this.TotalVolumUsd = totalVolumeUsd;
                this.AvgDealSizeUsd = avgDealSizeUsd;
                this.PaidVolumeRatio = paidVolRatio;
                this.HadEnoughData = hadEnoughData;
            }

            public override string ToString()
            {
                return string.Format("ActivityBandStats: [TotalVolumUsd: {0}, AvgDealSizeUsd: {1}, PaidVolumeRatio: {2} HadEnoughData: {3}]",
                    TotalVolumUsd, AvgDealSizeUsd, PaidVolumeRatio, HadEnoughData);
            }

            public bool Equals(IActivityBandStats activityBandStats)
            {
                return
                    this.TotalVolumUsd == activityBandStats.TotalVolumUsd &&
                    this.AvgDealSizeUsd == activityBandStats.AvgDealSizeUsd &&
                    this.PaidVolumeRatio == activityBandStats.PaidVolumeRatio &&
                    this.HadEnoughData == activityBandStats.HadEnoughData;
            }
        }
    }




    //public class LmaxTickerActivityStatsProvider
    //{
    //    private readonly ActivityBandInfo[] _activityBandChunks;
    //    private int _currentActivityBandChunkIdx = 0;
    //    private readonly TimeSpan _activityBandChunkLength;
    //    private ISafeTimer _statsRecalculationTimer;

    //    private readonly ActivityBandInfo[] _overflowingUnroundedActivityBand = new ActivityBandInfo[2];
    //    private int _currentOverflowingUnroundedActivityBandIdx = 0;
    //    private TimeSpan _activityBandOverflowingChunkLength;
    //    private DateTime _currentStandardChunkEndTime;
    //    //private LmaxTickerActivityMonitor _activityMonitor;
    //    private IUsdConversionProvider _usdConversionProvider;
    //    private TimeSpan _lookbackDuration;
    //    private ulong _totalChunksCollected = 0;
    //    private Symbol _observeSymbol;


    //    public event Action<IActivityBandStats> NewActivityStats;
    //    private ActivityBandStats _currentStats;
    //    public IActivityBandStats CurrentStats { get { return _currentStats; } }

    //    public LmaxTickerActivityStatsProvider(ITickerProvider lmaxTickerProvider, ISafeTimerFactory safeTimerFactory, IUsdConversionProvider usdConversionProvider,
    //        Symbol observeSymbol)
    //    {
    //        this._observeSymbol = observeSymbol;
    //        lmaxTickerProvider.NewVenueDeal += LmaxTickerProviderOnNewVenueDeal;
    //        _activityBandChunks =
    //            new ActivityBandInfo[
    //                LmaxTickerActivityMonitorConsts.MaximumRequiredLookbackSeconds /
    //                LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds];
    //        _activityBandChunkLength = LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularityDuration;
    //        _statsRecalculationTimer = safeTimerFactory.CreateSafeTimer(OnChunkIntervalEnd, _activityBandChunkLength, true);

    //        _currentStandardChunkEndTime = HighResolutionDateTime.UtcNow +
    //                                       LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularityDuration;
    //        this._usdConversionProvider = usdConversionProvider;
    //        this._currentStats = new ActivityBandStats();
    //    }

    //    public void SetLookBackDuration(TimeSpan lookBackDuration)
    //    {
    //        if (lookBackDuration.TotalSeconds > LmaxTickerActivityMonitorConsts.MaximumRequiredLookbackSeconds)
    //            throw new ArgumentException(
    //                string.Format("Too long lookBack duration requested from LmaxTickerActivityStatsProvider ([{0}])",
    //                    lookBackDuration));

    //        _lookbackDuration = lookBackDuration;

    //        _activityBandOverflowingChunkLength =
    //            TimeSpan.FromSeconds(((int) lookBackDuration.TotalSeconds)%
    //                                 LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds);
    //        _overflowingUnroundedActivityBand[0].ZeroOut();
    //        _overflowingUnroundedActivityBand[1].ZeroOut();
    //        TimeSpan due = (_currentStandardChunkEndTime + _activityBandOverflowingChunkLength) -
    //                       this.UtcNowInternal;
    //        //long warm-up time can take longer then refresh granularity
    //        while (due <= TimeSpan.Zero)
    //            due += LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularityDuration;
    //        _statsRecalculationTimer.Change(due,
    //            LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularityDuration);
    //    }

    //    private void LmaxTickerProviderOnNewVenueDeal(VenueDealObjectInternal venueDealObjectInternal)
    //    {
    //        if(venueDealObjectInternal.Symbol != _observeSymbol)
    //            return;

    //        if (_activityBandOverflowingChunkLength != TimeSpan.Zero &&
    //            venueDealObjectInternal.IntegratorReceivedTimeUtc >= this._currentStandardChunkEndTime)
    //        {
    //            StatndartChunkFlipProcedure();
    //        }

    //        this._activityBandChunks[this._currentActivityBandChunkIdx].AddDeal(venueDealObjectInternal);
    //        this._overflowingUnroundedActivityBand[this._currentOverflowingUnroundedActivityBandIdx].AddDeal(venueDealObjectInternal);
    //    }

    //    protected virtual DateTime UtcNowInternal
    //    {
    //        get { return HighResolutionDateTime.UtcNow; }
    //    }

    //    private object _chunksFlippingLocker = new object();
    //    private void StatndartChunkFlipProcedure()
    //    {
    //        lock (_chunksFlippingLocker)
    //        {
    //            if (_currentStandardChunkEndTime < this.UtcNowInternal)
    //            {
    //                _currentStandardChunkEndTime += LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularityDuration;
    //                _currentActivityBandChunkIdx = GetWrappedIndex(_currentActivityBandChunkIdx + 1);
    //                this._activityBandChunks[_currentActivityBandChunkIdx].ZeroOut();
    //                //untill now we collected stats but the overflow chunk was over (e.g. seconds 6 to 10)
    //                _overflowingUnroundedActivityBand[_currentOverflowingUnroundedActivityBandIdx].ZeroOut();
    //                _totalChunksCollected++;
    //            }
    //        }
    //    }

    //    private void OnChunkIntervalEnd()
    //    {
    //        StatndartChunkFlipProcedure();


    //        int previousIdx = _currentOverflowingUnroundedActivityBandIdx;
    //        _currentOverflowingUnroundedActivityBandIdx = (_currentOverflowingUnroundedActivityBandIdx + 1) %
    //                                                      _overflowingUnroundedActivityBand.Length;
    //        _overflowingUnroundedActivityBand[_currentOverflowingUnroundedActivityBandIdx].ZeroOut();


    //        bool hadEnoughData;
    //        ActivityBandInfo totalInfo = _overflowingUnroundedActivityBand[previousIdx] +
    //                                     this.GetHistoricActivityInfo(_lookbackDuration, out hadEnoughData);

    //        decimal usdConversionMultiplier = this._usdConversionProvider.GetCurrentUsdConversionMultiplier();
    //        decimal totalVolumeUsd = totalInfo.TotalVolumeBaseAbs * usdConversionMultiplier;
    //        decimal avgDealUsd = totalInfo.DealsCnt > 0 ? (totalVolumeUsd / (decimal)totalInfo.DealsCnt) : 0m;
    //        this._currentStats.SetActivityStats(totalVolumeUsd, avgDealUsd, hadEnoughData);

    //        if (NewActivityStats != null)
    //        {
    //            this.NewActivityStats(_currentStats);
    //        }
    //    }

    //    private int GetWrappedIndex(int rawIndexValue)
    //    {
    //        while (rawIndexValue < 0)
    //        {
    //            rawIndexValue += _activityBandChunks.Length;
    //        }

    //        while (rawIndexValue >= _activityBandChunks.Length)
    //        {
    //            rawIndexValue -= _activityBandChunks.Length;
    //        }

    //        return rawIndexValue;
    //    }

    //    private ActivityBandInfo GetHistoricActivityInfo(TimeSpan activityDuration, out bool hasEnoughData)
    //    {
    //        int chunksNumToInspect = ((int)activityDuration.TotalSeconds) /
    //                                 LmaxTickerActivityMonitorConsts.MaximumRequiredStatsGranularitySeconds;
    //        if (chunksNumToInspect > _activityBandChunks.Length)
    //            throw new ArgumentException(
    //                string.Format("Unexpectedly high duration for LmaxTickerActivityMonitor ([{0}])", activityDuration));

    //        ActivityBandInfo sum = new ActivityBandInfo();
    //        for (int idx = 1; idx <= chunksNumToInspect; idx++)
    //        {
    //            sum += _activityBandChunks[this.GetWrappedIndex(_currentActivityBandChunkIdx - idx)];
    //        }

    //        hasEnoughData = this._totalChunksCollected >= (ulong)chunksNumToInspect;
    //        return sum;
    //    }

    //    private struct ActivityBandStats : IActivityBandStats
    //    {
    //        public decimal TotalVolumUsd { get; private set; }
    //        public decimal AvgDealSizeUsd { get; private set; }
    //        public bool HadEnoughData { get; private set; }

    //        internal void SetActivityStats(decimal totalVolumeUsd, decimal avgDealSizeUsd, bool hadEnoughData)
    //        {
    //            this.TotalVolumUsd = totalVolumeUsd;
    //            this.AvgDealSizeUsd = avgDealSizeUsd;
    //            this.HadEnoughData = hadEnoughData;
    //        }

    //        public override string ToString()
    //        {
    //            return string.Format("ActivityBandStats: [TotalVolumUsd: {0}, AvgDealSizeUsd: {1}, HadEnoughData: {2}]",
    //                TotalVolumUsd, AvgDealSizeUsd, HadEnoughData);
    //        }
    //    }
    //}

    
}
