﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public abstract class VenueMMSystemBase : IntegratedSystemBase<IVenueMMSystemSettings>
    {
        private PriceSide _observedBookSide;
        private IMedianProvider<decimal> _medianPriceProvider;
        private bool _repetitiveCancelRequiredDuringSweepDefence;
        private static readonly TimeSpan _MIN_WAIT_TIME_AFTER_SWEEP_DEFENSE = TimeSpan.FromMilliseconds(300);
        private EventsRateCheckerEx _outgoingOrdersRateCheck;
        private SafeTimer _recheckPositionTimer;

        protected VenueMMSystemBase(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                     IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                                     PriceSide observedBookSide,
                                     Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                     ISymbolsInfo symbolsInfo, IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
                                     IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache, IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                     IIntegratedSystemSettingsProvider<IVenueMMSystemSettings> settingsProvider, bool repetitiveCancelRequiredDuringSweepDefence,
                                     EventsRateCheckerEx outgoingOrdersRateCheck)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, IntegratedTradingSystemType.MM,
                observedBookSide == PriceSide.Ask ? "S" : "B", false, observedBookSide)
        {
            this._observedBookSide = observedBookSide;
            this._medianPriceProvider = medianPriceProvider;
            this._repetitiveCancelRequiredDuringSweepDefence = repetitiveCancelRequiredDuringSweepDefence;
            this._outgoingOrdersRateCheck = outgoingOrdersRateCheck;
            this._recheckPositionTimer = new SafeTimer(ProcessEventWithRecursionCheck);
        }

        protected override void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopDeteriorated -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopReplaced -= ProcessEventTobAdapter;
        }

        protected override void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved += ProcessEventTobAdapter;

            //MM system side quotes only after changes on it's side of book (the other side is for check of negative spread)
            if (this._observedBookSide == PriceSide.Ask ? priceBook == _askReferencePriceBook : priceBook == _bidReferencePriceBook)
            {
                priceBook.ChangeableBookTopDeteriorated += ProcessEventTobAdapter;
                priceBook.ChangeableBookTopReplaced += ProcessEventTobAdapter;  
            }
        }

        protected override void AfterDisablingProcedure()
        {
            //Now - we know that no new order can be created
            // cancel the existing ones (even if we potentially double cancel)

            //Cancel the next first, as it can 'disappear' under hands and move to current
            if (IsReadyForNextAggressiveCancel())
                this.RequestOrderCancelDuringSweepDefence(this._cancelReplaceOrdersHolder.OrderNext);
            this.RequestOrderCancel(this._cancelReplaceOrdersHolder.OrderCurrent);
        }

        protected override void OnResetProcedure()
        {
            if (_cancelReplaceOrdersHolder.OrderNext != null)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system has non-null pending order [{1}]. Resetting it, but situation should be reviewed manually.",
                                 this._systemFriendlyName, _cancelReplaceOrdersHolder.OrderNext);
                this.RequestOrderCancel(this._cancelReplaceOrdersHolder.OrderNext);
                _cancelReplaceOrdersHolder.OrderNext = null;
            }

            if (_cancelReplaceOrdersHolder.OrderCurrent != null)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system has non-null pending order [{1}]. Resetting it, but situation should be reviewed manually.",
                                 this._systemFriendlyName, _cancelReplaceOrdersHolder.OrderCurrent);
                this.RequestOrderCancel(this._cancelReplaceOrdersHolder.OrderCurrent);
                _cancelReplaceOrdersHolder.OrderCurrent = null;
            }
        }

        protected override DealDirection CurrentHeadOrderPolarity { get { return this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(); }}


        private bool MatchesSpreadCriteria(PriceObjectInternal askPrice, PriceObjectInternal bidPrice)
        {
            if (PriceObjectInternal.IsNullPrice(askPrice) || PriceObjectInternal.IsNullPrice(bidPrice))
                return false;

            if (bidPrice.Price - askPrice.Price > _settings.MaximumDiscountDecimalToTrade)
                return false;

            if (!this.IsPriceValid(askPrice, PriceSide.Ask) || !this.IsPriceValid(bidPrice, PriceSide.Bid))
                return false;

            var orderCurrentlocal = _cancelReplaceOrdersHolder.OrderCurrent;
            if (orderCurrentlocal != null)
            {
                decimal lastKgtPrice = orderCurrentlocal.VenueClientOrderRequestInfo.RequestedPrice.Value;

                decimal askDifference = lastKgtPrice - askPrice.Price;
                decimal bidDifference = lastKgtPrice - bidPrice.Price;

                //ToB price crossed last KGT price
                if (
                    this._observedBookSide == PriceSide.Ask &&
                    (-askDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel || -bidDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel)
                    ||
                    this._observedBookSide == PriceSide.Bid &&
                    (askDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel || bidDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel)
                    )
                {
                    _sweepDefenceActiveTillUtc = HighResolutionDateTime.UtcNow + _MIN_WAIT_TIME_AFTER_SWEEP_DEFENSE;
                    return false;
                }
            }

            if (_sweepDefenceActiveTillUtc > HighResolutionDateTime.UtcNow)
            {
                return false;
            }

            return true;
        }

        private DateTime _sweepDefenceActiveTillUtc;

        //private volatile VenueClientOrder _orderCurrent;
        //private volatile VenueClientOrder _orderNext;
        private readonly CancelReplaceOrdersHolder _cancelReplaceOrdersHolder = new CancelReplaceOrdersHolder();

        private void ProcessEventTobAdapter(PriceObjectInternal priceObject)
        {
            //We do not need recursion check here as this event is invoked only directly by market data event
            // subsequent recursions (after issuing order or cancel) goes through ProcessEvent()

            if (!this.IsReadyToProcessEvents())
                return;
            if (!IsReadyForNextAggressiveCancel())
                return;

            PriceObjectInternal askToB = priceObject.Side == PriceSide.Ask ? priceObject : _askReferencePriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = priceObject.Side == PriceSide.Ask ? _bidReferencePriceBook.MaxNodeInternal : priceObject;
            PriceObjectInternal priceToRelease = priceObject.Side == PriceSide.Ask ? bidToB : askToB;

            try
            {
                ProcessEventInternal(askToB, bidToB);
            }
            finally
            {
                priceToRelease.Release();
            }
        }

        protected override void SettingsUpdating(IVenueMMSystemSettings newSettings)
        {
            if (newSettings.MinimumFillSize > newSettings.MinimumSizeToTrade ||
                newSettings.MinimumSizeToTrade > newSettings.MaximumSizeToTrade)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with sizes (MinFill: {1}, MinSize: {2}, MaxSize: {3}) with incorrect order (should be MinFil<=MinSize<=MaxSize) - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumFillSize, newSettings.MinimumSizeToTrade,
                    newSettings.MaximumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(newSettings.MinimumFillSize,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MinimumFillSize: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumFillSize);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderSizeWithinSizeAndIncrementLimit(newSettings.MinimumSizeToTrade,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MinimumSizeToTrade: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderSizeWithinSizeAndIncrementLimit(newSettings.MaximumSizeToTrade,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MaximumSizeToTrade: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MaximumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (newSettings.MaximumSourceBookTiers > 8)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MaximumSourceBookTiers: {1} greater than 8 - Hard blocking system",
                    this._systemFriendlyName, newSettings.MaximumSourceBookTiers);
                this.HardBlock();
                return;
            }
        }

        protected override void ProcessEvent()
        {
            if (!this.IsReadyToProcessEvents())
                return;

            //frontload at least some check
            if (!IsReadyForNextAggressiveCancel())
            {
                this._logger.Log(LogLevel.Trace, this._systemFriendlyName + " Ignoring trigger as it is not ready even for aggressive cancel");
            }

            PriceObjectInternal askToB = _askReferencePriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = _bidReferencePriceBook.MaxNodeInternal;

            try
            {
                ProcessEventInternal(askToB, bidToB);
            }
            finally
            {
                askToB.Release();
                bidToB.Release();
            }
        }

        private void TryCancelPositionOnCheckFailed()
        {
            if (TryEnterSingleAccessRegion())
            {
                OnExitPostionAggresiveRequested();

                ExitSingleAccessRegion();
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName +
                    " not canceling after spread or size or decay criteria missed - not ready for sending (second check)");
            }
            this._logger.Log(LogLevel.Trace,
                this._systemFriendlyName +
                " ignoring price change (or cancelling) - spread or size or sweep criteria missed");
        }

        private bool BookProbingAllowed { get { return this._settings.MaximumSourceBookTiers > 1; } }

        private void ProcessEventInternal(PriceObjectInternal askToB, PriceObjectInternal bidToB)
        {
            PriceObjectInternal observedSidePrice = this._observedBookSide == PriceSide.Ask ? askToB : bidToB;

            if (
                //not enaugh liquidity on a book
                    (observedSidePrice.SizeBaseAbsRemaining < _settings.MinimumSizeToTrade && !this.BookProbingAllowed)
                //spread misssed
                    || !MatchesSpreadCriteria(askToB, bidToB)
                    )
            {
                this.TryCancelPositionOnCheckFailed();
                return;
            }

            //we might got here only because we were ready for aggressive cancel but not for the markup
            if (!IsReadyForNextMarkup())
                return;

            decimal referencePrice = observedSidePrice.Price;
            decimal sizeBaseAbs = Math.Min(observedSidePrice.SizeBaseAbsRemaining, _settings.MaximumSizeToTrade);
            bool tobSizeInsufficient = sizeBaseAbs != _settings.MaximumSizeToTrade;
            if (tobSizeInsufficient)
            {
                if (BookProbingAllowed)
                {
                    sizeBaseAbs = _settings.MaximumSizeToTrade;
                    referencePrice =
                        (this._observedBookSide == PriceSide.Ask
                            ? this._askReferencePriceBook
                            : this._bidReferencePriceBook)
                            .GetWeightedPriceForSize(ref sizeBaseAbs, this._settings.MaximumSourceBookTiers);
                }

                sizeBaseAbs = SymbolsInfoUtils.RoundToIncrement(RoundingKind.AlwaysToZero, _orderMinimumSizeIncrement, sizeBaseAbs);

                if (sizeBaseAbs < _settings.MinimumSizeToTrade || referencePrice == 0m)
                {
                    this.TryCancelPositionOnCheckFailed();
                    return;
                }

                //This is redundant, but extremely dangerous if not met - therefore double checking
                if (sizeBaseAbs > _settings.MaximumSizeToTrade)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "{0} trading system generated size request of [{1}] - cutting it to max size [{2}]",
                        this._systemFriendlyName, sizeBaseAbs, _settings.MaximumSizeToTrade);
                    sizeBaseAbs = _settings.MaximumSizeToTrade;
                }
            }

            decimal requestedPrice;
            RoundingKind improvingRoundingKind = this._observedBookSide == PriceSide.Ask
                ? RoundingKind.AlwaysAwayFromZero
                : RoundingKind.AlwaysToZero;

            decimal offset = _settings.BestPriceImprovementOffsetDecimal;

            decimal requestedPriceUnrounded = this._observedBookSide == PriceSide.Ask
                ? referencePrice + offset
                : referencePrice - offset;

            if (!this._symbolsInfo.TryRoundToSupportedPriceIncrement(
                this._symbol, _destinationVenueCounterparty.TradingTargetType, improvingRoundingKind,
                requestedPriceUnrounded,
                out requestedPrice))
            {
                this._logger.Log(LogLevel.Error,
                    this._systemFriendlyName + " - Submitting Venue leg failed due to unability to roundprice");
                return;
            }

            this._logger.Log(LogLevel.Trace,
                "Venue leg of {4} will have price {0} - from unrounded value of {1} (Book ref price {2} and offset {3}) And size {5} (ToB size {6}sufficient, book probing {7}allowed)",
                requestedPrice, requestedPriceUnrounded, referencePrice, offset, this._systemFriendlyName, sizeBaseAbs,
                tobSizeInsufficient ? "IN" : string.Empty, BookProbingAllowed ? string.Empty : "NOT ");

            if (TryEnterSingleAccessRegion())
            {
                //
                // BEGIN OF SINGLE ACCESS CONSTRAINED REGION
                //

                VenueClientOrder orderCurrentlocal = this._cancelReplaceOrdersHolder.OrderCurrent;

                bool readyForNextMarkup = IsReadyForNextMarkup();
                bool priceChanged = orderCurrentlocal == null || orderCurrentlocal.VenueClientOrderRequestInfo.RequestedPrice != requestedPrice;
                bool ttlExpired = orderCurrentlocal == null || _settings.MinimumIntegratorPriceLife == TimeSpan.MinValue ||
                                  orderCurrentlocal.CreatedUtc + _settings.MinimumIntegratorPriceLife <
                                  HighResolutionDateTime.UtcNow;


                if (readyForNextMarkup && priceChanged)
                {
                    if (ttlExpired)
                    {
                        VenueClientOrderRequestInfo venueOri = this.CreateVenueOrderRequest(
                                requestedPrice, this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(),
                                this._symbol, sizeBaseAbs);

                        if (this.CanSubmitOrderRequest(venueOri)
                            &&
                            (_outgoingOrdersRateCheck == null ||
                            _outgoingOrdersRateCheck.AddNextEventAndCheckIsAllowed()))
                        {
                            //double check the current order, as it could have been null (and next non-null), then we was swapped before next check
                            // next became null and current non-null (success c/replace result)
                            orderCurrentlocal = this._cancelReplaceOrdersHolder.OrderCurrent;
                            if (orderCurrentlocal != null)
                            {
                                if (
                                    //attempt to perform replace on partially filled/rejected order however to size lower than already removed 
                                    (orderCurrentlocal.SizeBaseAbsInitial - orderCurrentlocal.SizeBaseAbsActive) >=
                                    sizeBaseAbs
                                    ||
                                    //attempt to perform replace on partially filled/rejected order to greater size - this would lead to initial cancel
                                    (sizeBaseAbs >= orderCurrentlocal.SizeBaseAbsInitial &&
                                     orderCurrentlocal.SizeBaseAbsInitial != orderCurrentlocal.SizeBaseAbsActive)
                                    )
                                {
                                    this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                                    this._cancelReplaceOrdersHolder.OrderCurrent = null;
                                    this.RequestOrderCancel(orderCurrentlocal);
                                    ExitSingleAccessRegion();
                                    this._logger.Log(LogLevel.Info,
                                        "New size is lower then remaining - so canceling order instead of C/Replace");
                                    return;
                                }

                                venueOri = this.CreateVenueReplaceOrderRequest(orderCurrentlocal.ClientOrderIdentity,
                                    venueOri);
                            }

                            int currentOrderCnt = Interlocked.Increment(ref _headOrdersCnt);

                            this._cancelReplaceOrdersHolder.OrderNext =
                                this.CreateVenueOrder(this._systemFriendlyName + "_HEAD_" + currentOrderCnt,
                                    this._systemIdentity, venueOri,
                                    this._integratedTradingSystemIdentification);

                            var result = this.RegisterOrder(this._cancelReplaceOrdersHolder.OrderNext);
                            if (!result.RequestSucceeded)
                            {
                                this._logger.Log(LogLevel.Error, "Submitting Venue leg of {0} failed: {1}",
                                    this._systemFriendlyName,
                                    result.ErrorMessage);
                                this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                                //Since we potentially couldn't replace, request cancel
                                this.RequestOrderCancel(orderCurrentlocal);
                            }
                        }
                        else
                        {
                            this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                            this._cancelReplaceOrdersHolder.OrderCurrent = null;
                            this.RequestOrderCancel(orderCurrentlocal);
                        }
                    }
                    else
                    {
                        //scheduled only if there was price change that we missed to reflect due to our TTL
                        TimeSpan due = orderCurrentlocal.CreatedUtc + _settings.MinimumIntegratorPriceLife +
                                       TimeSpan.FromMilliseconds(1) - HighResolutionDateTime.UtcNow;
                        this._recheckPositionTimer.Change(TimeSpanEx.Max(due, TimeSpan.FromMilliseconds(1)), Timeout.InfiniteTimeSpan);
                    }
                }
                else if (this._verboseLoggingEnabled)
                {
                    this._logger.Log(LogLevel.Trace,
                        this._systemFriendlyName +
                        " ignoring price change - not flat, or next order pending, or no price change");
                }

                ExitSingleAccessRegion();
                //
                // END OF SINGLE ACCESS CONSTRAINED REGION
                //
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName + " ignoring price change - not ready for sending (second check)");
            }
        }

        protected abstract VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(
            string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest);

        //private void OnExitPostionRequested()
        //{
        //    VenueClientOrder orderCurrentlocal = this._orderCurrent;
        //    VenueClientOrder orderNextlocal = this._orderNext;

        //    if (orderCurrentlocal != null && orderNextlocal == null)
        //    {
        //        //this is needed to mark cancelaton in progress and prevent concurrent attempts to Cancel or Cancel/Replace
        //        //but perform makring first
        //        //order next should not change outside of exclusive access region - no need to interlocked
        //        this._orderNext = orderCurrentlocal;
        //        //However ordercurrent CAN change in parallel; nullify if it's the one expected
        //        Interlocked.CompareExchange(ref this._orderCurrent, null, orderCurrentlocal);

        //        this.RequestOrderCancel(orderCurrentlocal);
        //    }
        //}

        private void OnExitPostionAggresiveRequested()
        {
            VenueClientOrder orderCurrentlocal = this._cancelReplaceOrdersHolder.OrderCurrent;
            VenueClientOrder orderNextlocal = this._cancelReplaceOrdersHolder.OrderNext;

            if (orderNextlocal != null)
            {
                if (IsReadyForNextAggressiveCancel())
                {
                    this.RequestOrderCancelDuringSweepDefence(orderNextlocal);

                    //orderNext already populated - so no need to mark operation is in progress (as it already is)

                    //do not attempt to cancel order current - as cancel is already in progress
                    //this.RequestOrderCancel(orderCurrentlocal);

                }
                else
                {
                    this._logger.Log(LogLevel.Info,
                        "{0} not processing aggressive cancel request as it should have been already sent",
                        this._systemFriendlyName);
                }
            }
            else if (orderCurrentlocal != null)
            {
                //this is needed to mark cancelaton in progress and prevent concurrent attempts to Cancel/Replace
                //but perform makring first
                //order next should not change outside of exclusive access region - no need to interlocked
                this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                //However ordercurrent CAN change in parallel; nullify if it's the one expected
                Interlocked.CompareExchange(ref this._cancelReplaceOrdersHolder.OrderCurrent, null, orderCurrentlocal);

                this.RequestOrderCancelDuringSweepDefence(orderCurrentlocal);
            }
        }

        private void RequestOrderCancelDuringSweepDefence(VenueClientOrder orderToCancel)
        {
            if(orderToCancel == null)
                return;

            //marking order as being aggressively cancelled - so that we prevent concurrent attempts
            //  (writing nonvolatile date first to use follwing mem barier)
            this._lastAggressiveCancelRequested = HighResolutionDateTime.UtcNow;
            this._lastAggressiveCancelledOder.SetTarget(orderToCancel);

            this.RequestOrderCancel(orderToCancel);

            if (_repetitiveCancelRequiredDuringSweepDefence)
            {
                //this blocks thread for 4 ms - would be better to temp return it to pool.
                Task.Factory.StartNew(() =>
                {
                    //up to additional 4 async requests (1st one was already sent immediately synchronously)
                    for (int i = 0; i < 4; i++)
                    {
                        Thread.Sleep(TimeSpan.FromMilliseconds(1));
                        //_orderNext would be nullified if one of cancels was processed successfully in meantime
                        if (orderToCancel == this._cancelReplaceOrdersHolder.OrderNext)
                            //We do not have no synchronisation here already, however we are not changing any state variable
                            this.RequestOrderCancel(orderToCancel);
                        else
                            break;
                    }
                });
            }
        }




        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsReadyForNextMarkup()
        {
            return _cancelReplaceOrdersHolder.OrderNext == null && _currentPositionBasePol.InterlockedIsZero();
        }

        private volatile WeakReference<VenueClientOrder> _lastAggressiveCancelledOder = new WeakReference<VenueClientOrder>(null);
        //not volatile, but we don not need up to tick freshness
        private DateTime _lastAggressiveCancelRequested;

        private static readonly TimeSpan _unconfirmedAggressiveCancelTimeout =
            SessionsSettings.OrderFlowSessionBehavior.UnconfirmedOrderTimeout.Add(TimeSpan.FromSeconds(1));

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsReadyForNextAggressiveCancel()
        {
            VenueClientOrder orderNextLocal = this._cancelReplaceOrdersHolder.OrderNext;
            //_orderNext was not yet aggressively cancelled.
            bool nextOrderInAggressiveCancel = IsGivenOrderInAggressiveCancel(orderNextLocal);
            if (nextOrderInAggressiveCancel && (HighResolutionDateTime.UtcNow - _lastAggressiveCancelRequested > _unconfirmedAggressiveCancelTimeout))
            {
                this._logger.Log(LogLevel.Fatal, "{0} requested aggressive cancel of order {1} on {2:dd-HH:mm:ss.fffffff}, but haven't received success or all reject replies until now, reissuing the attempt",
                    this._systemFriendlyName, orderNextLocal, _lastAggressiveCancelRequested);
                return true;
            }

            return !nextOrderInAggressiveCancel;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsGivenOrderInAggressiveCancel(VenueClientOrder order)
        {
            VenueClientOrder lastAggressiveCancelledOrderLocal;
            return
                order != null &&
                this._lastAggressiveCancelledOder.TryGetTarget(out lastAggressiveCancelledOrderLocal) &&
                lastAggressiveCancelledOrderLocal == order;
        }

        internal override void HandleOrderCurrentPartiallyRejected(VenueClientOrder orderCurrent,
            VenueClientOrder orderNext)
        {
            //rejecting initial size of current order - let's cancel it and than create new order from scratch
            // double check orderNext - so that we prevent unnecessary check for single access region, but also prevent race
            if (orderNext == null && TryEnterSingleAccessRegion() && this._cancelReplaceOrdersHolder.OrderNext == null)
            {
                this.RequestOrderCancel(orderCurrent);
                //this is needed to mark cancellation in progress and prevent concurrent attempts to Cancel or Cancel/Replace
                this._cancelReplaceOrdersHolder.OrderNext = this._cancelReplaceOrdersHolder.OrderCurrent;
                this._cancelReplaceOrdersHolder.OrderCurrent = null;

                ExitSingleAccessRegion();
            }
        }

        internal override void HandleOrderNextPartiallyRejected(VenueClientOrder orderNext)
        {
            this.RequestOrderCancel(orderNext);
        }

        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            bool shouldReprocessEvents = this.HandleClientOrderUpdateInfoHelper(clientOrderUpdateInfo, this._cancelReplaceOrdersHolder);

            if (shouldReprocessEvents)
            {
                this.ProcessEventWithRecursionCheck();
            }

            ////BEWARE:
            //// OpenInIntegrator - only opened in integrator, 
            //// ConfirmedByCounterparty - once the order was confirmed by counterparty until it gets to some terminal state


            //// New order is allways created as Next, also order is allways moved to next before cancelled (unless disable happend)
            //// So current order should only receive internediate fills and final statuses (no OpenInIntegrator, Confirmed etc.)
            ////   Exceptions: when system is disabled; when system is disabled; when order was cancelled by lower mechanism (session stop, kill sw, etc)
            //// Next order will get intermediate states (Open, Current) - after those it moves to current; but it can get terminal status right away
            //// Also it can get confirmed status with initial reject


            //bool isOrderInTerminalStatus = clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator
            //                               ||
            //                               clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator
            //                               ||
            //                               clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RemovedFromIntegrator;
            //bool shouldReprocessEvents = isOrderInTerminalStatus;

            //if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo)
            //{
            //    var orderCurrentLocal = this._orderCurrent;
            //    var orderNextLocal = this._orderNext;
            //    if (orderCurrentLocal != null &&
            //        orderCurrentLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
            //    {
            //        //confirmation of order cancel within c/replace (successfull and also failed)
            //        // do not touch next - as it will get separate info
            //        if (isOrderInTerminalStatus)
            //        {
            //            //nullify if it's the one that we expect
            //            Interlocked.CompareExchange(ref this._orderCurrent, null, orderCurrentLocal);
            //            shouldReprocessEvents = this._orderNext == null;
            //        }
            //        //Current order was created and integrator rejected some initial amount
            //        // (partial fill replace) - so this can happen only in ConfirmedByCtp status
            //        else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty &&
            //                 clientOrderUpdateInfo.CancelRequestStatus ==
            //                 ClientOrderCancelRequestStatus.CancelNotRequested
            //                 && clientOrderUpdateInfo.SizeBaseAbsRejected != 0)
            //        {
            //            //rejecting initial size of current order - let's cancel it and than create new order from scratch
            //            // double check orderNext - so that we prevent unnecessary check for single access region, but also prevent race
            //            if (orderNextLocal == null && TryEnterSingleAccessRegion() && _orderNext == null)
            //            {
            //                this.RequestOrderCancel(orderCurrentLocal);
            //                //this is needed to mark cancellation in progress and prevent concurrent attempts to Cancel or Cancel/Replace
            //                this._orderNext = this._orderCurrent;
            //                this._orderCurrent = null;

            //                ExitSingleAccessRegion();
            //            }
            //        }
            //        //whatever other intermediate state confirmed by ctp is allowed, but not relevant 
            //        else if (
            //            //plain confirm
            //            clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty

            //            ||

            //            //this is when Integrator tries to cancel non-confirmed order and counterparty rejects
            //            // the cancel attempt before even confirming the order ('not known') - in that case
            //            //  the state is still OpenInIntegrator even though this came from counterparty

            //            //Also this is for failed C/replace - then counterparty sends Cancel failed for current order (that is already confirmed)  
            //            (
            //                (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
            //                    ||
            //                clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
            //                &&
            //                clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed)
            //            )
            //        { }
            //        else if (this.ShouldBeFlatWithNoPendingPosition &&
            //                 clientOrderUpdateInfo.CancelRequestStatus !=
            //                 ClientOrderCancelRequestStatus.CancelNotRequested)
            //        {
            //            //after disabling it is allowed that cancellation is performed directly on current order 
            //            //  without moving to next
            //        }
            //        else
            //        {
            //            this._logger.Log(LogLevel.Fatal,
            //                             "{0} system receiving unexpected order update for OrderCurrent: {1}",
            //                             this._systemFriendlyName, clientOrderUpdateInfo);
            //        }
            //    }
            //    else if (orderNextLocal != null &&
            //        orderNextLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
            //    {
            //        //this usually means direct cancel of order next
            //        if (isOrderInTerminalStatus)
            //        {
            //            this._orderNext = null;
            //        }
            //        //We shouldn't receive cancels on non-confirmed orders (as we don' cancel them prior that)
            //        else if ((clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
            //            || clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
            //            //We need to rule out update during plain cancel (as this should receive another update in near future)
            //            && clientOrderUpdateInfo.CancelRequestStatus != ClientOrderCancelRequestStatus.CancelRequestInProgress)
            //        {
            //            //this is confirmation of opening of a NewOrderSingle (no replace)
            //            //it also handles the failure during submitting plain OrderCancel
            //            if (orderCurrentLocal == null)
            //            {
            //                //rejecting initial size of current order - let's cancel it and than create new order from scratch
            //                if (clientOrderUpdateInfo.SizeBaseAbsRejected != 0)
            //                {
            //                    //we don't need single access as it is already order next
            //                    this.RequestOrderCancel(orderNextLocal);
            //                }
            //                else
            //                {
            //                    //Confirm only when we have no pending cancels, otherwise wait for those
            //                    if (!(clientOrderUpdateInfo as VenueClientOrderUpdateInfo).HasPendingCancellation)
            //                    {
            //                        orderNextLocal = this._orderNext;
            //                        this._orderCurrent = orderNextLocal;
            //                        //this is to prevent case where orderNext is originally null, so paralel
            //                        // replace attempt can sneak in and populate order next - wo don't want to nulify it
            //                        Interlocked.CompareExchange(ref this._orderNext, null, orderNextLocal);
            //                        //we are ready for next order
            //                        shouldReprocessEvents = true;
            //                    }
            //                    else
            //                    {
            //                        this._logger.Log(LogLevel.Info,
            //                            "{0} system receiving confirmation for order that has pending cancelation in the meantime - ignoring. Update info: {1}",
            //                            this._systemFriendlyName, clientOrderUpdateInfo);
            //                    }
            //                }
            //            }
            //            else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty)
            //            {
            //                //this can be valid for aggressive cancel situations
            //                this._logger.Log(LogLevel.Error,
            //                             "{0} system receiving confirmed order update for OrderNext: {1}, while order current is not null: {2}",
            //                             this._systemFriendlyName, clientOrderUpdateInfo, orderCurrentLocal);
            //            }
            //            //here we get OpenInIntegrator for C/replace, but at this point curret order is still occupied,
            //            // as we hasn't yet received final confirmation for order current. 
            //            // This is valid - as handling OpenInIntegrator is only for a case where there was new order without replace
            //        }
            //        else if (
            //            //initial update - just from integrator
            //            clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator
            //            ||
            //            //this also comes just from integrator
            //            clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress) { }
            //        else
            //        {
            //            this._logger.Log(LogLevel.Fatal,
            //                             "{0} system receiving unexpected order update for OrderNext: {1}",
            //                             this._systemFriendlyName, clientOrderUpdateInfo);
            //        }
            //    }
            //    else if (isOrderInTerminalStatus)
            //    {
            //        //nothing - this is expectd:
            //        // We can send order, than cancel for it, but receive fill and done in the meantime

            //        // We can also receive last fill, react with Bank order, which uses all liquidity and removes price from book
            //        //  which triggers deterioration which lead to attempt to cancel the current order -> and receaving info about NotActive (CancelRequestFailed)
            //        //  And after all those we will receive the finnal NotActive CancelNotRequested correcponding to the fill

            //        //Scenario 3 - subsequent updates on multiple Hotspot cancel requests
            //    }
            //    else
            //    {
            //        this.CheckUnknownClientOrderUpdate(clientOrderUpdateInfo);
            //    }
            //}

            //if (shouldReprocessEvents)
            //{
            //    this.ProcessEventWithRecursionCheck();
            //}
        }
    }

}
