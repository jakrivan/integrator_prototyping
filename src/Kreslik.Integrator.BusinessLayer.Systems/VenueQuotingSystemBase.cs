﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public static class QuotingSpreadProvidingModuleManager
    {
        private static QuotingSpreadProvidingModule[] modules = new QuotingSpreadProvidingModule[Symbol.ValuesCount];

        public static QuotingSpreadProvidingModule GetQuotingSpreadProvidingModule(Symbol symbol,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                         IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook)
        {
            QuotingSpreadProvidingModule module = modules[(int) symbol];

            if (module == null)
            {
                module = new QuotingSpreadProvidingModule(askPriceBook, bidPriceBook);
                modules[(int) symbol] = module;
            }

            return module;
        }
    }

    public interface IQuotingSpreadProvidingModule
    {
        void RegisterSystem();
        void UnregisterSystem();

        AtomicDecimal SpreadToUse { get; }
    }

    public class QuotingSpreadProvidingModule : IQuotingSpreadProvidingModule
    {
        private const int _SPREAD_MEAN_HISTORY_ITEMS = 32;
        private const int _SPREAD_MEAN_HISTORY_MS = 100;
        private static readonly TimeSpan _DEFENSIVE_PERIOD = TimeSpan.FromMilliseconds(500);

        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _askPriceBook;
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _bidPriceBook;
        private readonly MeanTrackerTimeWindowedUnsynchronized _spreadMeanTrackersUnsynchronized =
            new MeanTrackerTimeWindowedUnsynchronized(_SPREAD_MEAN_HISTORY_ITEMS,
                TimeSpan.FromMilliseconds(_SPREAD_MEAN_HISTORY_MS));

        private DateTime _defensiveSpreadEndUtc = DateTime.MinValue;

        public QuotingSpreadProvidingModule(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                         IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook)
        {
            this._askPriceBook = askPriceBook;
            this._bidPriceBook = bidPriceBook;
        }

        private AtomicDecimal _currAsk, _currBid, _prevAsk, _prevBid;
        private AtomicDecimal _bidMove, _askMove;
        private AtomicDecimal _spreadToUse;

        private void SubscribeToBook()
        {
            this.UnsubscribeFromBook();

            this.InitializeBookTopValues();

            this._askPriceBook.ChangeableBookTopImproved += OnPriceChange;
            this._askPriceBook.ChangeableBookTopDeteriorated += OnPriceChange;
            this._bidPriceBook.ChangeableBookTopImproved += OnPriceChange;
            this._bidPriceBook.ChangeableBookTopDeteriorated += OnPriceChange;
        }

        private void UnsubscribeFromBook()
        {
            this._askPriceBook.ChangeableBookTopImproved -= OnPriceChange;
            this._askPriceBook.ChangeableBookTopDeteriorated -= OnPriceChange;
            this._bidPriceBook.ChangeableBookTopImproved -= OnPriceChange;
            this._bidPriceBook.ChangeableBookTopDeteriorated -= OnPriceChange;
        }

        private void InitializeBookTopValues()
        {
            PriceObjectInternal askToB = _askPriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = _bidPriceBook.MaxNodeInternal;

            if (PriceObjectInternal.IsNullPrice(askToB) && PriceObjectInternal.IsNullPrice(bidToB))
            {
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal,
                        "Experienced both sides of book null for {0} - smoothed spread might be too wide initially as a result",
                        askToB.Symbol);
                return;
            }

            if (!PriceObjectInternal.IsNullPrice(askToB))
            {
                _currAsk = askToB.ConvertedPrice;
                _prevAsk = askToB.ConvertedPrice;
                _currBid = askToB.ConvertedPrice;
                _prevBid = askToB.ConvertedPrice;
            }
            else
            {
                _currAsk = bidToB.ConvertedPrice;
                _prevAsk = bidToB.ConvertedPrice;
            }

            if (!PriceObjectInternal.IsNullPrice(bidToB))
            {
                _currBid = bidToB.ConvertedPrice;
                _prevBid = bidToB.ConvertedPrice;
            }

            bidToB.Release();
            askToB.Release();
        }


        private void OnPriceChange(PriceObjectInternal priceObjectInternal)
        {
            if (PriceObjectInternal.IsNullPrice(priceObjectInternal))
            {
                _spreadMeanTrackersUnsynchronized.MarkNullItem(priceObjectInternal.IntegratorReceivedTimeUtc);
                return;
            }

            if (priceObjectInternal.Side == PriceSide.Ask)
            {
                _prevAsk = _currAsk;
                _currAsk = priceObjectInternal.ConvertedPrice;
                _askMove = AtomicDecimal.Difference(_prevAsk, _currAsk);
            }
            else
            {
                _prevBid = _currBid;
                _currBid = priceObjectInternal.ConvertedPrice;
                _bidMove = AtomicDecimal.Difference(_prevBid, _currBid);
            }

            AtomicDecimal currentSpread = _currAsk > _currBid
                ? AtomicDecimal.Difference(_currAsk, _currBid)
                : AtomicDecimal.Difference(_currAsk, _currBid).MultiplyByPowerOf2(1);
            AtomicDecimal trueSpread = AtomicDecimal.Max(currentSpread, _bidMove, _askMove);
            _spreadMeanTrackersUnsynchronized.InsertNext(trueSpread, priceObjectInternal.IntegratorReceivedTimeUtc);

            if (trueSpread > currentSpread)
            {
                _defensiveSpreadEndUtc = HighResolutionDateTime.UtcNow + _DEFENSIVE_PERIOD;
            }

            AtomicDecimal spreadMean = _spreadMeanTrackersUnsynchronized.Mean;
            if (spreadMean == AtomicDecimal.Zero)
            {
                //We want to explicitly signal that spreads are missing
                _spreadToUse = AtomicDecimal.Zero;
            }
            else if (priceObjectInternal.IntegratorReceivedTimeUtc > _defensiveSpreadEndUtc)
            {
                _spreadToUse = spreadMean;
            }
            else
            {
                _spreadToUse = AtomicDecimal.Max(_spreadToUse, trueSpread, spreadMean);
            }
        }

        private int _refCount;
        public void RegisterSystem()
        {
            if (Interlocked.Increment(ref this._refCount) == 1)
            {
                this.SubscribeToBook();
            }
        }

        public void UnregisterSystem()
        {
            if (Interlocked.Decrement(ref this._refCount) == 0)
            {
                this.UnsubscribeFromBook();
            }
        }

        public AtomicDecimal SpreadToUse
        {
            get
            {
                if (_refCount <= 0)
                {
                    LogFactory.Instance.GetLogger(null)
                        .Log(LogLevel.Fatal,
                            "Attempt to use spread from QuotingSpreadProvidingModule while no system is registered and so module is not collecting data. System will likely default to minimum spread. This should be investigated");
                }

                return _spreadToUse;
            }
        }
    }

    public abstract class VenueQuotingSystemBase : IntegratedSystemBase<IVenueQuotingSystemSettings>
    {
        private IMedianProvider<decimal> _medianPriceProvider;
        private StopLossTrackingModule _stopLossTrackingModule;
        private long _maximumOrderSizeBaseAbs;
        private IUnderlyingSessionState _coverSessionToWatch;
        private EventsRateCheckerEx _outgoingOrdersRateCheck;
        private DateTime _lastDealReceived = DateTime.MinValue;
        private SafeTimer _inventoryLiquidationTimer;
        private bool _isInRestrictedClosingOnlyMode = false;
        private LmaxTickerActivityMonitorWrapper _lmaxTickerActivityStatsProvider;
        private int _enableCount;
        private IQuotingSpreadProvidingModule _spreadMeanTracker;
        private IQuotingStatsPersistorProxy _quotingStatsPersistor;

        protected VenueQuotingSystemBase(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                         IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook, Symbol symbol,
                                         Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                         ISymbolsInfo symbolsInfo,
                                         IMedianProvider<decimal> askMedianPriceProvider,
                                         IMedianProvider<decimal> bidMedianPriceProvider,
                                         IOrderManagement orderManagement,
                                         IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
                                         IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                         IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings> settingsProvider,
                                         IUsdConversionProvider termUsdConversionProvider, IUsdConversionProvider baseUsdConversionProvider,
                                         IUnderlyingSessionState coverSessionToWatch, EventsRateCheckerEx outgoingOrdersRateCheck,
                                         ITickerProvider lmaxTickerProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, IntegratedTradingSystemType.Quoting,
                string.Empty, false, null)
        {
            StopLossTrackingModule sl = new StopLossTrackingModule(
                symbol,
                askPriceBook,
                bidPriceBook,
                askMedianPriceProvider,
                bidMedianPriceProvider,
                symbolsInfo,
                termUsdConversionProvider,
                unicastInfoForwardingHub,
                logger,
                orderManagement,
                settingsProvider,
                this._systemFriendlyName);

            this._outgoingOrdersRateCheck = outgoingOrdersRateCheck;
            this._medianPriceProvider = askMedianPriceProvider;
            this._stopLossTrackingModule = sl;
            this._maximumOrderSizeBaseAbs = (long)(2 * settingsProvider.CurrentSettings.MaximumPositionBaseAbs);
            this._logger.Log(LogLevel.Info, "{0} maximum order size limit: {1}", this._systemFriendlyName, this._maximumOrderSizeBaseAbs);
            sl.RegisterSystem(this);
            this._coverSessionToWatch = coverSessionToWatch;
            if (this._coverSessionToWatch != null)
                //this will efectively lead to hardblock if system enabled
                this._coverSessionToWatch.OnStopInitiated += this.DuringEnableProcedure;
            _inventoryLiquidationTimer = new SafeTimer(TryEnterInactivityRestrictedClosingOnlyMode);
            _lmaxTickerActivityStatsProvider =
                new LmaxTickerActivityMonitorWrapper(
                    LmaxTickerActivityMonitor.GetNewLmaxTickerActivityMonitor(lmaxTickerProvider,
                        ProductionSafeTimerFactory.Instance, baseUsdConversionProvider, symbol),
                    settingsProvider.CurrentSettings.LmaxTickerLookbackInterval);

            _lmaxTickerActivityStatsProvider.NewActivityStats += LmaxTickerActivityStatsProviderOnNewActivityStats;
            this._spreadMeanTracker = QuotingSpreadProvidingModuleManager.GetQuotingSpreadProvidingModule(symbol, askPriceBook, bidPriceBook);
            this._quotingStatsPersistor =
                QuotingStatsPersistor.Instance.CreateQuotingStatsPersistorProxy(
                    settingsProvider.CurrentSettings.TradingSystemId);
        }

        protected override void DuringEnableProcedure()
        {
            if (this._enabled && this._coverSessionToWatch != null && !this._coverSessionToWatch.IsReadyAndOpen)
            {
                this._logger.Log(LogLevel.Fatal, "{0} system is enabled or being enabled, but session for emergency covering ({1}) is not running. Blocking the system to prevent un-closable position in emergency situation",
                    this._systemFriendlyName, this._coverSessionToWatch.Counterparty);
                this.HardBlock();
                return;
            }

            if (this._enabled)
            {
                _stopLossTrackingModule.Enable();
                Interlocked.Exchange(ref this._enableCount, 1);
                _spreadMeanTracker.RegisterSystem();
                this._disabledAndFullyExitedFromVenueEvent.Reset();
                Volatile.Write(ref this._reissueAttempts, 0);
                this._quotingStatsPersistor.SetShouldUploadStats(true);
                this.TryEnterRestrictedClosingOnlyMode(true, true);
                _isLmaxOnAskToB = false;
            }
        }

        //WARNING: Not this way!
        // as if we would only prevent the enablement, we would need to also handle events when enablement is already allowed (an enable if needed)
        // on top of that - simple enable/disable is not enough for quoting system - it would quietly rest with open position
        //protected override bool IsAllowedToBeEnabled
        //{
        //    get { return this._coverSessionToWatch == null || this._coverSessionToWatch.IsReadyAndOpen; }
        //}

        protected abstract VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(
            string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest);

        //this is for SL to correctly link closing order to original system
        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification
        {
            get { return this._integratedTradingSystemIdentification; }
        }

        protected override void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved += ProcessEventTobAdapter;
            priceBook.ChangeableBookTopDeteriorated += ProcessEventTobAdapter;

            //for lmax we want also just updates on book top ask - so that we don't miss the bidd only change that was skipped
            if(priceBook == this._askReferencePriceBook)
                priceBook.ChangeableBookTopReplaced += ProcessEventTobAdapterLmaxAsk;
        }

        protected override void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopDeteriorated -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopReplaced -= ProcessEventTobAdapterLmaxAsk;
        }

        private void ProcessEventTobAdapterLmaxAsk(PriceObjectInternal priceObject)
        {
            if(priceObject.Counterparty.TradingTargetType == TradingTargetType.LMAX)
                this.ProcessEventTobAdapter(priceObject);
        }

        private bool _isLmaxOnAskToB = false;
        private void ProcessEventTobAdapter(PriceObjectInternal priceObject)
        {
            //Workaround for all bids and the all asks
            //LMAX specific: lmax sends all bids and then all asks in one quote; we want to evaluate only once for current quote

            bool isLmax = priceObject.Counterparty.TradingTargetType == TradingTargetType.LMAX;

            if (priceObject.Side == PriceSide.Bid)
            {
                //if this is lmax bid and lmax is already on ask top, then we know that there will be other price in same quote price
                // that will be ask, and it will somehow change the ask side, so eventually we will reprocess for the same message.
                //BUT: we need to process also bokk top replaces for lmax asks
                if (isLmax && _isLmaxOnAskToB)
                    return;
            }
            else
            {
                _isLmaxOnAskToB = isLmax;
            }

            this.ProcessEventWithRecursionCheck();
        }

        protected override void RegularCheck(DateTime now)
        {
            //This is it - not meaningfull to perform position checking
        }

        protected override void AfterDisablingProcedure()
        {
            //Now - we know that no new order can be created
            // cancel the existing ones (even if we potentially double cancel)
            this.SetFullyExitedEventIfNeeded();

            this.RequestOrderCancel(this._buyOrderInfo.OrderCurrent);
            this.RequestOrderCancel(this._sellOrderInfo.OrderCurrent);
            this.RequestOrderCancel(this._buyOrderInfo.OrderNext);
            this.RequestOrderCancel(this._sellOrderInfo.OrderNext);


            //We want to close on closing session if disabled or position set explicitly to zero (but not in restriction mode)
            if (!this._enabledInSettings || this._settings.IsPositionExplicitlySetToZero)
            {
                if (!_stopLossTrackingModule.TradingAllowed)
                {
                    this._logger.Log(LogLevel.Warn,
                        "System {0} detected situation when it should attempt to flatten positions, but stoploss disabled the system. Position (basePol): {1} of {2}",
                        this._systemFriendlyName, this._currentPositionBasePol, this._symbol);
                }
                else if (InterlockedEx.CheckAndFlipState(ref this._reissueAttempts, 1, 0))
                {
                    this.InititeExplicitClosingOfPostion("PositionClosingOrder_" + this._systemFriendlyName);
                }
                else
                {
                    this._logger.Log(LogLevel.Warn, "System {0} detected situation when it should attempt to flatten positions, but flattening should have been already initiated. Position (basePol): {1} of {2}",
                        this._systemFriendlyName, this._currentPositionBasePol, this._symbol);
                }
            }

            this._quotingStatsPersistor.SetShouldUploadStats(this._enabledInSettings);

            if (!this._enabled)
            {
                _stopLossTrackingModule.Disable();
                if(Interlocked.Decrement(ref this._enableCount) == 0)
                    _spreadMeanTracker.UnregisterSystem();
            }
        }

        private void EnsuresOrderNull(VenueClientOrder clientOrderToNullify)
        {
            if (clientOrderToNullify != null)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system has non-null pending order [{1}]. Resetting it, but situation should be reviewed manually.",
                                 this._systemFriendlyName, clientOrderToNullify);
                this.RequestOrderCancel(clientOrderToNullify);
            }
        }

        protected override void OnResetProcedure()
        {
            this._stopLossTrackingModule.Reset();

            this.EnsuresOrderNull(this._buyOrderInfo.OrderCurrent);
            this.EnsuresOrderNull(this._sellOrderInfo.OrderCurrent);
            this.EnsuresOrderNull(this._buyOrderInfo.OrderNext);
            this.EnsuresOrderNull(this._sellOrderInfo.OrderNext);

            this._buyOrderInfo.Reset_Shared();
            this._sellOrderInfo.Reset_Shared();
        }

        private void CancelExternalOrders(bool isCalledFromSingleAccessRegion)
        {
            this.RequestOrderCancel(this._buyOrderInfo.OrderCurrent);
            this.RequestOrderCancel(this._sellOrderInfo.OrderCurrent);
            //The Next order should be altered only from constrained region
            // Otherwise it can happen that next is being created in parallel attempted to cancel - but business layer does not know it yet
            //  so it will reject cancel and system will remove the order as already not known, but after that the registration finishes successfully (thread #1)
            //  but system already refuses to track that order
            if (isCalledFromSingleAccessRegion || TryEnterSingleAccessRegion())
            {
                this.RequestOrderCancel(this._buyOrderInfo.OrderNext);
                this.RequestOrderCancel(this._sellOrderInfo.OrderNext);

                ExitSingleAccessRegion();
            }
        }

        private const int _SIZE_FOR_WEIGHTED_PRICE = 500000;

        protected override void ProcessEvent()
        {
            if (!this.IsReadyToProcessEvents())
                return;

            if (!this._stopLossTrackingModule.TradingAllowed)
            {
                this._logger.Log(LogLevel.Trace, "{0} ignoring the event as stop loss module is not allowing trading",
                    this._systemFriendlyName);
                return;
            }

            if (_isInRestrictedClosingOnlyMode && this.IsFullyExitedFromVenue && this._currentPositionBasePol.InterlockedIsZero())
                return;

            decimal askToBOriginal = _askReferencePriceBook.GetWeightedPriceForExactSize(_SIZE_FOR_WEIGHTED_PRICE, int.MaxValue);
            decimal bidToBOriginal = _bidReferencePriceBook.GetWeightedPriceForExactSize(_SIZE_FOR_WEIGHTED_PRICE, int.MaxValue);
            decimal askToB = askToBOriginal;
            decimal bidToB = bidToBOriginal;

            if (askToB <= 0 || bidToB <= 0)
            {
                if (askToB > 0)
                    bidToB = askToB -
                             2 * (_settings.SpreadType == VenueQuotingSystemSettingsBag.QuotingSpreadType.Fixed
                                 ? _settings.FixedSpreadDecimalHalf
                                 : _settings.MinDynamicSpreadDecimalHalf);
                else if (bidToB > 0)
                    askToB = bidToB +
                             2 * (_settings.SpreadType == VenueQuotingSystemSettingsBag.QuotingSpreadType.Fixed
                                 ? _settings.FixedSpreadDecimalHalf
                                 : _settings.MinDynamicSpreadDecimalHalf);
                else
                {
                    this._logger.Log(LogLevel.Info,
                        "{0} system is detecting vanishing of BOTH sides of ToB, initiated exiting positions. However flattening bank order will not be sent. Current position (base polarized): {1} of {2}",
                        this._systemFriendlyName, this._currentPositionBasePol, this._symbol);
                    this.CancelExternalOrders(false);
                    return;
                }
            }


            decimal pivotPrice;
            pivotPrice = (askToB + bidToB) / 2m;

            //if (askToB >= bidToB)
            //    pivotPrice = (askToB + bidToB) / 2m;
            //else
            //    pivotPrice = (_askReferencePriceBook.MaxNodeUpdatedUtc > _bidReferencePriceBook.MaxNodeUpdatedUtc
            //        ? askToB
            //        : bidToB);

            decimal offset;

            if (_settings.SpreadType == VenueQuotingSystemSettingsBag.QuotingSpreadType.Fixed)
                offset = _settings.FixedSpreadDecimalHalf;
            else
            {
                AtomicDecimal spreadBase = _spreadMeanTracker.SpreadToUse;
                if (spreadBase == AtomicDecimal.Zero)
                {
                    this._logger.Log(LogLevel.Info,
                        "{0} system is detecting zero spread base (probably by vanishing of book side). Exiting external orders, not flattening",
                        this._systemFriendlyName);
                    this.CancelExternalOrders(false);
                    return;
                }

                offset = spreadBase.DivideByPowerOf2(1).ToDecimal() + _settings.DynamicMarkupDecimal;

                if (offset < _settings.MinDynamicSpreadDecimalHalf)
                    offset = _settings.MinDynamicSpreadDecimalHalf;
            }

            //this not only ensures exclusive access, it also prevents unwanted recursion
            if (TryEnterSingleAccessRegion())
            {
                //
                // BEGIN OF SINGLE ACCESS CONSTRAINED REGION
                //

                decimal currentPositionBasePolLocal = this._currentPositionBasePol.InterlockedToDecimal();


                decimal skewToAdd = (_settings.SkewEnabled &&
                                     Math.Abs(currentPositionBasePolLocal) > _settings.MaximumPositionBaseAbs / 2)
                    ? (currentPositionBasePolLocal > 0
                        ? Decimal.Negate(_settings.SkewDecimal)
                        : _settings.SkewDecimal)
                    : 0m;

                decimal requestedBuyPrice;
                decimal requestedSellPrice;
                decimal requestedBuyPriceUnrounded;
                decimal requestedSellPriceUnrounded;

                requestedBuyPriceUnrounded = pivotPrice - offset + skewToAdd;
                requestedSellPriceUnrounded = pivotPrice + offset + skewToAdd;

                if (!this._symbolsInfo.TryRoundToSupportedPriceIncrement(
                    this._symbol, _destinationVenueCounterparty.TradingTargetType, RoundingKind.AlwaysToZero,
                    requestedBuyPriceUnrounded,
                    out requestedBuyPrice))
                {
                    this._logger.Log(LogLevel.Error,
                        this._systemFriendlyName + " - Submitting Venue leg failed due to unability to round buy price");
                    return;
                }

                if (!this._symbolsInfo.TryRoundToSupportedPriceIncrement(
                    this._symbol, _destinationVenueCounterparty.TradingTargetType, RoundingKind.AlwaysAwayFromZero,
                    requestedSellPriceUnrounded,
                    out requestedSellPrice))
                {
                    this._logger.Log(LogLevel.Error,
                        this._systemFriendlyName + " - Submitting Venue leg failed due to unability to round sell price");
                    return;
                }

                //Read those first - they can change to non-nulls ONLY inside single access region
                // so here we are guaranteed that they will reamin null (they can change to nulls later on
                //  (but we need to snapshot them first - to prevent operating on null current orders from snapshot
                //   despite they changed to non-null after takeing snapshot. This cannot happen if we frontload
                //   ordernext check)
                var orderNextBuySnapshot = _buyOrderInfo.OrderNext;
                var orderNextSellSnapshot = _sellOrderInfo.OrderNext;

                //First take snapshot of current orders status, only after tht take the position
                // as we can operate with newer position, but not with newer orders
                SidedOrderCurrentInfoSnapshot buySidesnapshot = this._buyOrderInfo.TakeOrderCurrentSnapshot();
                SidedOrderCurrentInfoSnapshot sellSidesnapshot = this._sellOrderInfo.TakeOrderCurrentSnapshot();

                if (Math.Abs(currentPositionBasePolLocal) > _settings.MaximumPositionBaseAbs)
                {
                    this._logger.Log(
                        Math.Abs(currentPositionBasePolLocal) > this._maximumOrderSizeBaseAbs / 2
                            ? LogLevel.Fatal
                            : LogLevel.Info,
                        "{0} detecting position ({1}) out of allowed bounds ({2}). If the allowed size wasn't lowered recently than situation should be promptly investigated. (system will restrict external order sizes to mitigate risk)",
                        this._systemFriendlyName, this._currentPositionBasePol, _settings.MaximumPositionBaseAbs);
                }

                decimal newBuySize = _settings.MaximumPositionBaseAbs - currentPositionBasePolLocal;
                decimal newSellSize = _settings.MaximumPositionBaseAbs + currentPositionBasePolLocal;

                this.CheckRequestedOrderSize(ref newBuySize);
                this.CheckRequestedOrderSize(ref newSellSize);

                this._logger.Log(LogLevel.Trace,
                    "{0} generated buy/sell prices {1}/{2} from unrounded values {3}/{4} and fair price {5} (ask: {6} bid: {7}) and offset {8} ({9}) and skew {10}. Requesting buy/sell size: {11}/{12}",
                    this._systemFriendlyName, requestedBuyPrice, requestedSellPrice,
                    requestedBuyPriceUnrounded, requestedSellPriceUnrounded, pivotPrice,
                    askToBOriginal, bidToBOriginal,
                    offset, _settings.SpreadType, skewToAdd, newBuySize, newSellSize);

                if (newBuySize <= 0 && newSellSize <= 0)
                    this._quotingStatsPersistor.ClearCurrentSpread();
                else
                    this._quotingStatsPersistor.UpdateCurrentSpread(requestedSellPrice - requestedBuyPrice);

                if (requestedBuyPrice >= requestedSellPrice && newBuySize > 0 && newSellSize > 0)
                {
                    this._logger.Log(LogLevel.Fatal, "{0} generated 'crossed' prices (buy: {1}, sell: {2}). Ignoring and cancelling existing orders of this system (Trace info: DynamicMarkupDecimal: {3}, MinDynamicSpreadDecimalHalf: {4})",
                        this._systemFriendlyName, requestedBuyPrice, requestedSellPrice, _settings.DynamicMarkupDecimal, _settings.MinDynamicSpreadDecimalHalf);
                    this.CancelExternalOrders(true);
                    ExitSingleAccessRegion();
                    return;
                }

                this.PerformTargetVenueStateUpdate(DealDirection.Buy, newBuySize, requestedBuyPrice,
                    this._buyOrderInfo,
                    buySidesnapshot, orderNextBuySnapshot);
                this.PerformTargetVenueStateUpdate(DealDirection.Sell, newSellSize, requestedSellPrice,
                    this._sellOrderInfo, sellSidesnapshot, orderNextSellSnapshot);

                ExitSingleAccessRegion();
                //
                // END OF SINGLE ACCESS CONSTRAINED REGION
                //
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName +
                    " ignoring price change or other event - not ready for sending (second check)");
            }
        }

        private void PerformTargetVenueStateUpdate(DealDirection direction, decimal newSize, decimal newPrice,
                                                   SidedOrderInfo sidedOrderInfo, SidedOrderCurrentInfoSnapshot snapshot, VenueClientOrder orderNextSnapshot)
        {
            //thisis the only check - order next cannot change to non-null outside of single access region
            if (orderNextSnapshot != null)
            {
                this._logger.Log(LogLevel.Trace,
                                 "{0} ignoring event - next {1} order pending",
                                 this._systemFriendlyName, direction);
                return;
            }


            var orderCurrent = snapshot.OrderCurrent;
            bool isOrderCurrentPartiallyFilled = snapshot.IsOrderPartiallyFilled;

            if (newSize <= 0 && orderCurrent != null)
            {
                //mark the cancellation first (co cancel replies ar allways on nextorder)
                orderCurrent = sidedOrderInfo.MarkOrderCurrentCancelling_ExclusiveAccess();
                //handles nulls, this is in fact already order next
                this.RequestOrderCancel(orderCurrent);
                if (orderCurrent != null)
                    this._logger.Log(LogLevel.Info,
                                     "{0} Detecting <=0 size change request for current order - so cancelling it. Order: {1}",
                                     this._systemFriendlyName, orderCurrent);
                return;
            }

            if (newSize <= 0
                ||
                //requesting identical order - no need to change
                (orderCurrent != null && orderCurrent.OrderRequestInfo.RequestedPrice == newPrice &&
                 (orderCurrent.OrderRequestInfo.SizeBaseAbsInitial == newSize ||
                //or reqesting order with smaller size, but original order is partially filled 
                //  (so we likely request just the decremented size by the partiall fill, 
                //     unless other side partiall fill didn't happen also (however smaller than this side))
                //       This is small likelyhood and we would still need even the pending partiall fill
                  orderCurrent.OrderRequestInfo.SizeBaseAbsInitial > newSize && isOrderCurrentPartiallyFilled)))
            {
                this._logger.Log(LogLevel.Trace,
                                 "{0} ignoring event - next {1} no price and size change",
                                 this._systemFriendlyName, direction);
                return;
            }

            //We would need to top off the size of current order, or live with it being partially rejected right after creation
            // as part is filled
            if (isOrderCurrentPartiallyFilled && orderCurrent != null &&
                newSize > orderCurrent.OrderRequestInfo.SizeBaseAbsInitial)
            {
                //mark the cancellation first (co cancel replies ar allways on nextorder)
                orderCurrent = sidedOrderInfo.MarkOrderCurrentCancelling_ExclusiveAccess();
                //this is in fact already order next
                this.RequestOrderCancel(orderCurrent);
                return;
            }

            if (_outgoingOrdersRateCheck != null && !_outgoingOrdersRateCheck.AddNextEventAndCheckIsAllowed())
            {
                //mark the cancellation first (co cancel replies ar allways on nextorder)
                orderCurrent = sidedOrderInfo.MarkOrderCurrentCancelling_ExclusiveAccess();
                //this is in fact already order next
                this.RequestOrderCancel(orderCurrent);
                return;
            }

            //order was partially filled, so we want to request original size in order to get all remaining fills
            if (orderCurrent != null && newSize < orderCurrent.OrderRequestInfo.SizeBaseAbsInitial &&
                isOrderCurrentPartiallyFilled)
            {
                newSize = orderCurrent.OrderRequestInfo.SizeBaseAbsInitial;
            }

            bool isRiskRemoving = this._isInRestrictedClosingOnlyMode
                                  && newSize <= Math.Abs(this._currentPositionBasePol.ToDecimal())
                                  &&
                                  (this._currentPositionBasePol.InterlockedIsPositive()
                                      ? direction == DealDirection.Sell
                                      : direction == DealDirection.Buy);

            VenueClientOrderRequestInfo venueOri = this.CreateVenueOrderRequest(
                        newPrice, direction, this._symbol, newSize, isRiskRemoving);


            if (orderCurrent != null)
            {
                venueOri = this.CreateVenueReplaceOrderRequest(orderCurrent.ClientOrderIdentity, venueOri);
            }


            int currentOrderCnt = sidedOrderInfo.GetNextOrderCnt();
            VenueClientOrder nextOrder = this.CreateVenueOrder(this._systemFriendlyName + "_" + direction + currentOrderCnt,
                                                    this._systemIdentity, venueOri,
                                                    this._integratedTradingSystemIdentification);

            sidedOrderInfo.PopulateNextOrder_ExclusiveAccess(nextOrder);

            var result = this.RegisterOrder(nextOrder);
            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Error, "Submitting {0} leg of {1} failed: {2}",
                                 direction, this._systemFriendlyName, result.ErrorMessage);
                sidedOrderInfo.NullifyNextOrder_Shared();
            }
        }

        protected abstract VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
                                                                               decimal sizeBaseAbs, bool isRiskRemoving);

        private void TryEnterInactivityRestrictedClosingOnlyMode()
        {
            this.TryEnterRestrictedClosingOnlyMode(true, false);
        }

        private void TryEnterRestrictedClosingOnlyMode(bool inactivityCaused, bool ltBandCaused)
        {
            if (inactivityCaused)
                this._quotingStatsPersistor.UpdateInactivityRestrictionState(true);
            if (ltBandCaused)
                this._quotingStatsPersistor.UpdateLtBandRestrictionState(true);

            if (_isInRestrictedClosingOnlyMode)
                return;

            _isInRestrictedClosingOnlyMode = true;
            //to prevent race condition where we entered restricted mode just after condition lapsed
            this.TryExitRestrictedClosingOnlyModeIfNeeded();
            if (_isInRestrictedClosingOnlyMode)
            {
                this._logger.Log(LogLevel.Info, "{0} entering restricted closing only mode (setting position size to 0)", this._systemFriendlyName);

                _settings.SetRestrictedMode();
                //if system was in GFO and got into restricted mode - it must attempt to enable self in order to be able to exit
                this.OnSystemAllowedStateChanged(true);

                //immediately update our positions
                this.ProcessEventWithRecursionCheck();
            }
        }

        private void TryExitRestrictedClosingOnlyModeIfNeeded()
        {
            if (_isInRestrictedClosingOnlyMode && this._currentPositionBasePol.InterlockedIsZero())
            {
                this._quotingStatsPersistor.UpdateInactivityRestrictionState(false);
                this.DisableIfNeeded();

                //We cannot apply condition for last deal - it is enough that we are in zero position
                // The problem is that other restriction condition could take longer than inactivity timeout and than we would be blocked forever
                //_lastDealReceived > HighResolutionDateTime.UtcNow.Subtract(_settings.InactivityTimeout)
                if (
                    this.IsWithinSafetyTickerActivityBounds(this._lmaxTickerActivityStatsProvider.CurrentStats)
                    )
                {
                    this._quotingStatsPersistor.UpdateLtBandRestrictionState(false);
                    this._settings.RemoveRestrictedMode();
                    this.UpdateMaximumOrderSizeLimit(_settings.MaximumPositionBaseAbs);

                    _isInRestrictedClosingOnlyMode = false;
                    this._logger.Log(LogLevel.Info, "{0} exiting restricted closing only mode", this._systemFriendlyName);
                }

            }
        }

        protected override void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal, AtomicSize currentPositionBasePol)
        {
            this._stopLossTrackingModule.HandleIntegratorDealInternal(integratorDealInternal);

            this._logger.Log(Math.Abs(currentPositionBasePol.ToDecimal()) > this._settings.MaximumPositionBaseAbs ? LogLevel.Warn : LogLevel.Info, "{0} current polarized position: {1} of {2} (max possition from settings: {3})",
                this._systemFriendlyName, currentPositionBasePol, this._symbol, this._settings.MaximumPositionBaseAbs);

            _lastDealReceived = HighResolutionDateTime.UtcNow;
            _inventoryLiquidationTimer.Change(
                currentPositionBasePol == AtomicSize.ZERO ? Timeout.InfiniteTimeSpan : _settings.InactivityTimeout,
                Timeout.InfiniteTimeSpan);

            this.TryExitRestrictedClosingOnlyModeIfNeeded();

            if (!integratorDealInternal.IsTerminalFill && integratorDealInternal.SenderTradingTargetType != TradingTargetType.BankPool)
            {
                this._logger.Log(LogLevel.Info, "{0} Detecting non-terminal partiall fill for {1} - marking and invoking new decision making",
                    this._systemFriendlyName, integratorDealInternal.ClientOrderIdentity);

                this._buyOrderInfo.MarkOrderPartiallyFilledIfMatches_Shared(integratorDealInternal.ClientOrderIdentity);
                this._sellOrderInfo.MarkOrderPartiallyFilledIfMatches_Shared(integratorDealInternal.ClientOrderIdentity);

                this.ProcessEventWithRecursionCheck();
            }
        }

        protected override bool AllowNonflatPositionDuringEnable
        {
            //if this system is enabled then it can have nonflat position during disable times
            // (goflatonly, stoploss, killsw, etc)
            get { return this._enabledInSettings; }
        }

        protected override bool AllowActivityDuringTradingConstrainingSwitches { get { return this._isInRestrictedClosingOnlyMode && !this._currentPositionBasePol.InterlockedIsZero(); } }



        private readonly ManualResetEvent _disabledAndFullyExitedFromVenueEvent = new ManualResetEvent(true);
        public WaitHandle IsDisabledAndExitedFromVenue
        {
            get { return _disabledAndFullyExitedFromVenueEvent; }
        }

        private void SetFullyExitedEventIfNeeded()
        {
            if (IsFullyExitedFromVenue)
            {
                this._logger.Log(LogLevel.Info, "{0} system is fully exited from target venue and position is {1}.",
                        this._systemFriendlyName, this._currentPositionBasePol.InterlockedToDecimal());
                this._quotingStatsPersistor.ClearCurrentSpread();

                if (!this._enabled)
                {
                    this._logger.Log(LogLevel.Info, "{0} system is fully exited from target venue and disabled. So raising event - closing flattening bankpool order can be issued after this point",
                        this._systemFriendlyName);
                    this._disabledAndFullyExitedFromVenueEvent.Set();
                }

                if (!this._enabledInSettings)
                {
                    this.CheckPositionOnceAfterDelay();
                }
            }
        }

        private int _checkingPosition = 0;
        private void CheckPositionOnceAfterDelay()
        {
            if (Interlocked.Exchange(ref this._checkingPosition, 1) == 0)
            {
                TaskEx.ExecAfterDelayWithErrorHandling(TimeSpan.FromSeconds(5), () =>
                {
                    if (!_currentPositionBasePol.InterlockedIsZero() && !this._enabledInSettings)
                    {
                        string reason =
                            string.Format(
                                "{0} system is fully exited from target venue (and disabled) but position is {1} (even 5 seconds after the disable). Situation should be reviewed",
                                this._systemFriendlyName, _currentPositionBasePol);
                        //this._logger.Log(LogLevel.Fatal, reason);
                        this._riskManager.TurnOnGoFlatOnly(reason);
                    }

                    Interlocked.Exchange(ref this._checkingPosition, 0);
                });
            }
        }

        private bool IsFullyExitedFromVenue
        {
            get
            {
                return this._buyOrderInfo.OrderCurrent == null && this._buyOrderInfo.OrderNext == null
                       && this._sellOrderInfo.OrderCurrent == null && this._sellOrderInfo.OrderNext == null;
            }
        }

        public decimal ResetPositionQuiet()
        {
            return this._currentPositionBasePol.InterlockedZeroOutReturningPrevious();
        }

        protected override void AfterDeletionProcedure()
        {
            //Systems should have been already disabled - but just to be on a sure side
            AfterDisablingProcedure();
            this._stopLossTrackingModule.UnRegisterSystem(this);
        }

        private void CheckRequestedOrderSize(ref decimal newSize)
        {
            if (newSize > _maximumOrderSizeBaseAbs)
            {
                this._logger.Log(LogLevel.Fatal, "{0} detecting requested size ({1}) out of allowed bounds ({2}, max postion size in settings: {3}) (Position: {4}), adjusting the outgoing orders sizes like if the position wouldn't cross the bounds",
                    this._systemFriendlyName, newSize, _maximumOrderSizeBaseAbs, _settings.MaximumPositionBaseAbs, this._currentPositionBasePol);
                newSize = _maximumOrderSizeBaseAbs;
            }
        }

        private void UpdateMaximumOrderSizeLimit(decimal maximumPositionBaseAbs)
        {
            //using interlocked to 'push' the change to all caches
            //We can hav max position on one side and then need to flip it to max position on other side - therefore adding the maxes
            //Interlocked.Exchange(ref this._maximumOrderSizeBaseAbs,
            //                     (long) (oldSettings.MaximumPositionBaseAbs + newSettings.MaximumPositionBaseAbs));
            //however this is easier as we don't have to update the allowed max after the position changes 
            // (as max1 + max2 will change to 2*max2 after the position is balanced and if 2*max2 > max1 + max2  then we'd have false alarms)
            long prevVal = Interlocked.Exchange(ref this._maximumOrderSizeBaseAbs,
                                 (long)Math.Max(this._maximumOrderSizeBaseAbs, maximumPositionBaseAbs * 2));

            this._logger.Log(LogLevel.Info, "{0} Adjusting the maximum order size limit from {1} to {2}",
                             this._systemFriendlyName, prevVal, this._maximumOrderSizeBaseAbs);
        }

        protected override void SettingsUpdating(IVenueQuotingSystemSettings newSettings)
        {
            this.UpdateMaximumOrderSizeLimit(this._settings.MaximumPositionBaseAbs);

            if (newSettings.InactivityTimeout < TimeSpan.FromSeconds(1))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with too small inactivity interval ({1}) - Hard blocking system",
                    this._systemFriendlyName, newSettings.InactivityTimeout);
                this.HardBlock();
                return;
            }

            if (newSettings.LmaxTickerLookbackInterval < TimeSpan.FromSeconds(10) ||
                newSettings.LmaxTickerLookbackInterval > TimeSpan.FromSeconds(900))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with invalid LmaxTicker LookBack interval ({1}) - Hard blocking system",
                    this._systemFriendlyName, newSettings.InactivityTimeout);
                this.HardBlock();
                return;
            }

            _lmaxTickerActivityStatsProvider.SetObservedStatsInterval(newSettings.LmaxTickerLookbackInterval);
            this.TryEnterRestrictedClosingOnlyMode(true, true);
        }


        private bool IsWithinSafetyTickerActivityBounds(IActivityBandStats activityBandStats)
        {
            return activityBandStats.TotalVolumUsd < this._settings.MaximumLmaxTickerVolumeUsd
                   &&
                   activityBandStats.HadEnoughData
                   &&
                   activityBandStats.TotalVolumUsd > this._settings.MinimumLmaxTickerVolumeUsd
                   &&
                   activityBandStats.AvgDealSizeUsd > this._settings.MinimumLmaxTickerAvgDealSizeUsd
                   &&
                   activityBandStats.AvgDealSizeUsd < this._settings.MaximumLmaxTickerAvgDealSizeUsd;

        }

        private void LmaxTickerActivityStatsProviderOnNewActivityStats(IActivityBandStats activityBandStats)
        {
            bool isLtBandRestriction = !this.IsWithinSafetyTickerActivityBounds(activityBandStats);
            if (this._enabled)
            {
                if (isLtBandRestriction)
                {
                    this._logger.Log(LogLevel.Info, "{0} Activity stats: {1} has stepped outside of some configured bounds - flipping to constrained mode",
                        this._systemFriendlyName, activityBandStats);
                    TryEnterRestrictedClosingOnlyMode(false, true);
                }
                else
                {
                    this.TryExitRestrictedClosingOnlyModeIfNeeded();
                }
            }

            if (this._enabledInSettings)
                this._quotingStatsPersistor.UpdateLTStats(activityBandStats, isLtBandRestriction);
            this._quotingStatsPersistor.SetShouldUploadStats(this._enabledInSettings);
        }

        protected override DealDirection CurrentHeadOrderPolarity
        {
            get { return this._currentPositionBasePol.InterlockedIsPositive() ? DealDirection.Buy : DealDirection.Sell; }
        }

        private void InititeExplicitClosingOfPostion(string closingOrderName)
        {
            TaskEx.StartNew(() =>
            {
                if (!this._disabledAndFullyExitedFromVenueEvent.WaitOne(TimeSpan.FromSeconds(3)))
                {
                    var pos = this._currentPositionBasePol.InterlockedToDecimal();
                    this._logger.Log(LogLevel.Fatal, "{0} system failed to wait for all orders to exit from venue (for 3 seconds). Position(pol): {1}. {2}Sending the closing order - but position should be reviewed",
                        this._systemFriendlyName, pos, pos == 0 ? "NOT " : string.Empty);
                }

                if (!this._currentPositionBasePol.InterlockedIsZero())
                {
                    decimal currentPositionBasePolLocal = this._currentPositionBasePol.InterlockedToDecimal();

                    this.SendBankOrder(Math.Abs(currentPositionBasePolLocal),
                                       currentPositionBasePolLocal > 0 ? DealDirection.Sell : DealDirection.Buy,
                                       this._symbol, true, closingOrderName);
                }
            });
        }

        private readonly SidedOrderInfo _buyOrderInfo = new SidedOrderInfo();
        private readonly SidedOrderInfo _sellOrderInfo = new SidedOrderInfo();

        //be carefull on using synchronization here as some updates come recurzively on the same thread registering the order
        // on the other hand - all updates from venue should arrive on a single thread
        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            bool isOrderInTerminalStatus = clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator
                                           ||
                                           clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator
                                           ||
                                           clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RemovedFromIntegrator;
            //do not reprocess events if next order is not null - do this check upfront to prevent prices calculation
            bool shouldReprocessEvents = isOrderInTerminalStatus;

            if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo)
            {
                var orderCurrentBuyLocal = this._buyOrderInfo.OrderCurrent;
                var orderNextBuyLocal = this._buyOrderInfo.OrderNext;
                var orderCurrentSellLocal = this._sellOrderInfo.OrderCurrent;
                var orderNextSellLocal = this._sellOrderInfo.OrderNext;
                if (orderCurrentBuyLocal != null &&
                    orderCurrentBuyLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
                {
                    //confirmation of order cancel within c/replace
                    // do not touch next - as it will get separate info
                    if (isOrderInTerminalStatus)
                    {
                        //nullify if it's the one that we expect
                        this._buyOrderInfo.OrderCurrentFinalized_Shared(orderCurrentBuyLocal);
                        shouldReprocessEvents = this._buyOrderInfo.OrderNext == null;
                    }
                    else if ((this.ShouldBeFlatWithNoPendingPosition &&
                             clientOrderUpdateInfo.CancelRequestStatus !=
                             ClientOrderCancelRequestStatus.CancelNotRequested)
                        ||
                        (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed))
                    {
                        //after disabling it is allowed that cancellation is performed directly on current order 
                        //  without moving to next

                        //also C/Replace might fail and then current order receives cancel failed - this is expected
                    }
                    //plain confirm
                    else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                        ||
                        //Temp colution - C/Replace can be sent and receive update that it was sent (open, next)
                        //   but it was performed in the meantime - and so moved to current already
                        clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
                    { }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for OrderCurrentBuy: {1}",
                                         this._systemFriendlyName, clientOrderUpdateInfo);
                    }
                }
                else if (orderCurrentSellLocal != null &&
                    orderCurrentSellLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
                {

                    //confirmation of order cancel within c/replace
                    // do not touch next - as it will get separate info
                    if (isOrderInTerminalStatus)
                    {
                        //nullify if it's the one that we expect
                        this._sellOrderInfo.OrderCurrentFinalized_Shared(orderCurrentSellLocal);
                        shouldReprocessEvents = this._sellOrderInfo.OrderNext == null;
                    }
                    else if ((this.ShouldBeFlatWithNoPendingPosition &&
                             clientOrderUpdateInfo.CancelRequestStatus !=
                             ClientOrderCancelRequestStatus.CancelNotRequested)
                        ||
                        (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed))
                    {
                        //after disabling it is allowed that cancellation is performed directly on current order 
                        //  without moving to next

                        //also C/Replace might fail and then current order receives cancel failed - this is expected
                    }
                    //plain confirm
                    else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                        ||
                        //Temp colution - C/Replace can be sent and receive update that it was sent (open, next)
                        //   but it was performed in the meantime - and so moved to current already
                        clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator)
                    { }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for OrderCurrentSell: {1}",
                                         this._systemFriendlyName, clientOrderUpdateInfo);
                    }
                }
                else if (orderNextBuyLocal != null &&
                    orderNextBuyLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
                {
                    if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                        //We need to rule out update during plain cancel (as this should receive another update in near future)
                        && clientOrderUpdateInfo.CancelRequestStatus != ClientOrderCancelRequestStatus.CancelRequestInProgress)
                    {
                        //this is confirmation of opening of a NewOrderSingle (no replace)
                        //it also handles the failure during submitting plain OrderCancel
                        if (orderCurrentBuyLocal == null)
                        {
                            //here we also hapen to get partiall reject - but we don't care, it is handled separately
                            // (before sending next order)
                            this._buyOrderInfo.OrderNextConfirmed_Shared();
                        }
                        else
                        {
                            this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving confirmed order update for OrderNextBuy: {1}, while order current is not null: {2}",
                                         this._systemFriendlyName, clientOrderUpdateInfo, orderCurrentBuyLocal);
                        }
                    }
                    else if (
                        //initial update - just from integrator
                        clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator
                        ||
                        //this also comes just from integrator
                        clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress) { }
                    else if (isOrderInTerminalStatus)
                    {
                        this._buyOrderInfo.NullifyNextOrder_Shared();
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for OrderNextBuy: {1}",
                                         this._systemFriendlyName, clientOrderUpdateInfo);
                    }
                }
                else if (orderNextSellLocal != null &&
                    orderNextSellLocal.ClientOrderIdentity == clientOrderUpdateInfo.ClientOrderIdentity)
                {
                    if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.ConfirmedByCounterparty
                        //We need to rule out update during plain cancel (as this should receive another update in near future)
                        && clientOrderUpdateInfo.CancelRequestStatus != ClientOrderCancelRequestStatus.CancelRequestInProgress)
                    {
                        //this is confirmation of opening of a NewOrderSingle (no replace)
                        //it also handles the failure during submitting plain OrderCancel
                        if (orderCurrentSellLocal == null)
                        {
                            //here we also hapen to get partiall reject - but we don't care, it is handled separately
                            // (before sending next order)
                            this._sellOrderInfo.OrderNextConfirmed_Shared();
                            shouldReprocessEvents = true;
                        }
                        else
                        {
                            this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving confirmed order update for OrderNextSell: {1}, while order current is not null: {2}",
                                         this._systemFriendlyName, clientOrderUpdateInfo, orderCurrentSellLocal);
                        }
                    }
                    else if (
                        //initial update - just from integrator
                        clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator
                        ||
                        //this also comes just from integrator
                        clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestInProgress) { }
                    else if (isOrderInTerminalStatus)
                    {
                        this._sellOrderInfo.NullifyNextOrder_Shared();
                    }
                    else
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for OrderNextSell: {1}",
                                         this._systemFriendlyName, clientOrderUpdateInfo);
                    }
                }
                else if (isOrderInTerminalStatus)
                {
                    //nothing - this is expectd:
                    // We can send order, than cancel for it, but receive fill and done in the meantime

                    // We can also receive last fill, react with Bank order, which uses all liquidity and removes price from book
                    //  which triggers deterioration which lead to attempt to cancel the current order -> and receaving info about NotActive (CancelRequestFailed)
                    //  And after all those we will receive the finnal NotActive CancelNotRequested correcponding to the fill
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                                         "{0} system receiving unexpected order update for (already) unknown order [{1}]: {2}",
                                         this._systemFriendlyName, clientOrderUpdateInfo.ClientOrderIdentity, clientOrderUpdateInfo);
                }
            }

            if (isOrderInTerminalStatus)
            {
                this.SetFullyExitedEventIfNeeded();
            }

            //finalization of current order should not trigger reprocess, however confirmation of opened next->current order should
            // (both are cases of c/replace confirm)
            // so checking just terminal status is insuficcient
            if (shouldReprocessEvents)
            {
                this.ProcessEventWithRecursionCheck();
            }
        }

        private class SidedOrderCurrentInfoSnapshot
        {
            public VenueClientOrder OrderCurrent;
            public bool IsOrderPartiallyFilled;

            public SidedOrderCurrentInfoSnapshot Update(VenueClientOrder orderCurrent, bool isOrderPartiallyFilled)
            {
                this.OrderCurrent = orderCurrent;
                this.IsOrderPartiallyFilled = isOrderPartiallyFilled;
                return this;
            }
        }

        private class SidedOrderInfo
        {
            private volatile VenueClientOrder _orderNext;
            private volatile VenueClientOrder _orderCurrent;
            private volatile bool _isOrderPartiallyFilled;
            private int _ordersCnt;
            private readonly SidedOrderCurrentInfoSnapshot _sidedOrderCurrentInfoSnapshot = new SidedOrderCurrentInfoSnapshot();

            public VenueClientOrder OrderNext
            {
                get { return this._orderNext; }
            }

            public VenueClientOrder OrderCurrent
            {
                get { return this._orderCurrent; }
            }

            public SidedOrderCurrentInfoSnapshot TakeOrderCurrentSnapshot()
            {
                return this._sidedOrderCurrentInfoSnapshot.Update(this._orderCurrent, this._isOrderPartiallyFilled);
            }

            public int GetNextOrderCnt()
            {
                return Interlocked.Increment(ref _ordersCnt);
            }

            public void MarkOrderPartiallyFilledIfMatches_Shared(string clientOrderIdentity)
            {
                var orderCurrentLocal = this._orderCurrent;
                if (orderCurrentLocal != null && orderCurrentLocal.ClientOrderIdentity == clientOrderIdentity)
                    this._isOrderPartiallyFilled = true;
            }

            public void OrderNextConfirmed_Shared()
            {
                var orderNextLocal = this._orderNext;

                this._orderCurrent = orderNextLocal;
                //if both - curr and next - are populated, then only curr can receive partiall fills (next is not active until curr confirmed done)
                // but we don't want to publish clearing of flag before updating order - so that old order is not read without flag
                this._isOrderPartiallyFilled = false;
                //this is to prevent case where orderNext is originally null, so paralel
                // replace attempt can sneak in and populate order next - wo don't want to nulify it
                Interlocked.CompareExchange(ref this._orderNext, null, orderNextLocal);
            }

            public void OrderCurrentFinalized_Shared(VenueClientOrder orderCurrentLocal)
            {
                //nullify if it's the one that we expect
                Interlocked.CompareExchange(ref this._orderCurrent, null, orderCurrentLocal);
            }

            public void NullifyNextOrder_Shared()
            {
                //if only next order is populated (means current was marked as cancel)
                // then nobody could create current order in meantime before nullifying next (so we need to clear flag first, then order)
                if (this._orderCurrent == null)
                    this._isOrderPartiallyFilled = false;

                this._orderNext = null;
            }

            //public void NullifyCurrentOrder_Shared()
            //{
            //    this._orderCurrent = null;
            //}

            public VenueClientOrder MarkOrderCurrentCancelling_ExclusiveAccess()
            {
                VenueClientOrder orderCurrentlocal = this._orderCurrent;

                //if order current is null, then we are fine proceeding as inside here next must be null already
                // and it cannot be changed outside of exclusive access region

                //this is needed to mark cancelaton in progress and prevent concurrent attempts to Cancel or Cancel/Replace
                // also nobody can assign to next order outside of exclusive access - so no need for interlocked
                this._orderNext = orderCurrentlocal;
                //nullify if it's the one expected (order current CAN change in parallel)
                Interlocked.CompareExchange(ref this._orderCurrent, null, orderCurrentlocal);

                return orderCurrentlocal;
            }

            public void PopulateNextOrder_ExclusiveAccess(VenueClientOrder newNextOrder)
            {
                this._orderNext = newNextOrder;
            }

            public void Reset_Shared()
            {
                this._orderCurrent = null;
                this._orderNext = null;
                this._isOrderPartiallyFilled = false;
            }
        }
    }
}
