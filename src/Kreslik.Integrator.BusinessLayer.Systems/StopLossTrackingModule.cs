﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class StopLossTrackingModule : ITakerUnicastInfoForwarder
    {
        // Rationale behind PnL calcullation triggers:
        // PnL changes when:
        //     - there is a new deal
        //     - on each sided price if sided position is opened
        //     - if sided position is not opened, then with changes of median USD conversion - but this is marginal change

        private Symbol _symbol;
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _askPriceBook;
        private IChangeablePriceBook<PriceObjectInternal, Counterparty> _bidPriceBook;
        private IMedianProvider<decimal> _askMedianPriceProvider;
        private IMedianProvider<decimal> _bidMedianPriceProvider;
        private ISymbolsInfo _symbolsInfo;
        private IUsdConversionProvider _termCurrencyUsdConversionProvider;
        private ILogger _logger;
        private IOrderManagement _orderManagement;
        private ITakerUnicastInfoForwarder _unicastInfoForwarder;

        private AtomicSize _currentPositionBasePol;
        private AtomicSize _totalAcquiredAmountTermPol;
        private AtomicSize _totalCommissionsMultByMillion;
        private AtomicSize _totalCommissions;
        private long _stopLossLimit;
        private volatile bool _stopLossExceeded;
        private string _systemFriendlyName;

        public StopLossTrackingModule(
            Symbol symbol,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook,
            IMedianProvider<decimal> askMedianPriceProvider,
            IMedianProvider<decimal> bidMedianPriceProvider,
            ISymbolsInfo symbolsInfo,
            IUsdConversionProvider termCurrencyUsdConversionProvider,
            IUnicastInfoForwardingHub unicastInfoForwardingHub,
            ILogger logger,
            IOrderManagement orderManagement,
            IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings> settingsProvider,
            string systemFriendlyName)
        {
            this._symbol = symbol;
            this._askPriceBook = askPriceBook;
            this._bidPriceBook = bidPriceBook;
            this._askMedianPriceProvider = askMedianPriceProvider;
            this._bidMedianPriceProvider = bidMedianPriceProvider;
            this._symbolsInfo = symbolsInfo;
            this._termCurrencyUsdConversionProvider = termCurrencyUsdConversionProvider;
            this._logger = logger;
            this._orderManagement = orderManagement;
            this._systemFriendlyName = systemFriendlyName;
            this._unicastInfoForwarder = unicastInfoForwardingHub.CreateWrappedUnicastInfoForwarder(this);

            this._stopLossLimit = settingsProvider.CurrentSettings.StopLossNetInUsd;
            settingsProvider.SettingsChanged += settings =>
            {
                _stopLossLimit = settings.StopLossNetInUsd;
                this.CalculatePnlNet(false, null);
            };
        }

        private List<VenueQuotingSystemBase> _systems = new List<VenueQuotingSystemBase>();
        public void RegisterSystem(VenueQuotingSystemBase system)
        {
            _systems.Add(system);
        }

        public void UnRegisterSystem(VenueQuotingSystemBase system)
        {
            _systems.Remove(system);
        }

        //public event Action<long> StopLossLimitExceeded;
        //public event Action PositionClosed;

        public void Reset()
        {
            //reset all positions
            _totalAcquiredAmountTermPol.InterlockedZeroOut();
            _totalCommissionsMultByMillion.InterlockedZeroOut();
            _totalCommissions.InterlockedZeroOut();
            _stopLossExceeded = false;
            Volatile.Write(ref _activeBankOrders, 0);

            if (!_currentPositionBasePol.InterlockedIsZero())
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} StopLoss module position [{1}] is not flat as it was expected. Situation should be reviewed. Reseting it to 0 now.",
                                 this._systemFriendlyName, _currentPositionBasePol);
                this._currentPositionBasePol.InterlockedZeroOut();
            }
        }

        public bool TradingAllowed
        {
            get
            {
                return !this._stopLossExceeded;
            }
        }

        public void Enable()
        {
            this._askPriceBook.ChangeableBookTopImproved -= ProcessAskChange;
            this._askPriceBook.ChangeableBookTopDeteriorated -= ProcessAskChange;
            this._askPriceBook.ChangeableBookTopImproved += ProcessAskChange;
            this._askPriceBook.ChangeableBookTopDeteriorated += ProcessAskChange;

            this._bidPriceBook.ChangeableBookTopImproved -= ProcessBidChange;
            this._bidPriceBook.ChangeableBookTopDeteriorated -= ProcessBidChange;
            this._bidPriceBook.ChangeableBookTopImproved += ProcessBidChange;
            this._bidPriceBook.ChangeableBookTopDeteriorated += ProcessBidChange;

            Interlocked.Exchange(ref this._processingStopLossCondition, 0);
            this._stopLossExceeded = false;
            this.CalculatePnlNet(false, null);
        }

        public void Disable()
        {
            this._askPriceBook.ChangeableBookTopImproved -= ProcessAskChange;
            this._askPriceBook.ChangeableBookTopDeteriorated -= ProcessAskChange;
            this._bidPriceBook.ChangeableBookTopImproved -= ProcessBidChange;
            this._bidPriceBook.ChangeableBookTopDeteriorated -= ProcessBidChange;
        }

        private void ProcessAskChange(PriceObjectInternal priceObject)
        {
            if (_currentPositionBasePol < AtomicSize.ZERO && !PriceObjectInternal.IsNullPrice(priceObject) && priceObject.Side == PriceSide.Ask)
            {
                this.CalculatePnlNet(priceObject.Price, true, DealDirection.Buy);
            }
        }

        private void ProcessBidChange(PriceObjectInternal priceObject)
        {
            if (_currentPositionBasePol > AtomicSize.ZERO && !PriceObjectInternal.IsNullPrice(priceObject) && priceObject.Side == PriceSide.Bid)
            {
                this.CalculatePnlNet(priceObject.Price, true, DealDirection.Sell);
            }
        }

        private void CalculatePnlNet(bool justPriceChange, DealDirection? integratorDealDirectionForClosingPosition)
        {
            decimal medianPrice = _currentPositionBasePol > AtomicSize.ZERO
                ? _bidMedianPriceProvider.Median
                : _askMedianPriceProvider.Median;

            this.CalculatePnlNet(medianPrice, justPriceChange, integratorDealDirectionForClosingPosition);
        }

        private int _processingStopLossCondition = 0;
        private void HandleStopLossExceeded(decimal pnlNet)
        {
            if (Interlocked.Exchange(ref this._processingStopLossCondition, 1) == 0)
            {
                this._stopLossExceeded = true;
                this._logger.Log(LogLevel.Error, "{0} stop loss limit ({1}) exceeded: {2}", this._systemFriendlyName,
                                 this._stopLossLimit, pnlNet);

                TaskEx.StartNew(()
                                      =>
                {
                    //WARNING!: only disable now - so that system does not try to flatten in parallel.
                    // hardblock after SL is done
                    foreach (VenueQuotingSystemBase venueQuotingSystemBase in _systems)
                    {
                        venueQuotingSystemBase.DisableExternaly();
                    }

                    if (!WaitHandle.WaitAll(_systems.Select(sys => sys.IsDisabledAndExitedFromVenue).ToArray(),
                                            TimeSpan.FromSeconds(3)))
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} StopLoss module failed to wait for all registerd systems to fully disable and exit from venues (for 3 seconds). Sending the closing order anyway - but position should be reviewed",
                                         this._systemFriendlyName);
                    }

                    var identification =
                        _systems.Select(s => s.IntegratedTradingSystemIdentification).FirstOrDefault();
                    decimal sizeToFlatten = this._currentPositionBasePol.ToDecimal();
                    bool orderSent = sizeToFlatten != 0 && this.SendClosingBankOrder(sizeToFlatten, identification);

                    decimal cummulativeSizeOfSystems = 0;
                    foreach (VenueQuotingSystemBase venueQuotingSystemBase in _systems)
                    {
                        cummulativeSizeOfSystems += venueQuotingSystemBase.ResetPositionQuiet();
                    }

                    if (sizeToFlatten != cummulativeSizeOfSystems)
                    {
                        this._logger.Log(LogLevel.Fatal,
                                         "{0} StopLoss module experienced mismatch in closing sizes. It requested closing for {1}, however systems seem to have cumulative position of {2}. Position should be reviewed",
                                         _systemFriendlyName, sizeToFlatten, cummulativeSizeOfSystems);
                    }


                    this._logger.Log(LogLevel.Info,
                                     "{0} SL exiting the SL procedure. Waiting with blocking till order is done",
                                     _systemFriendlyName);

                    if (!orderSent)
                    {
                        ExitStopLossExceededProcedure();
                    }
                });
            }
        }

        private void ExitStopLossExceededProcedure()
        {
            if (!this._currentPositionBasePol.InterlockedIsZero())
            {
                this._logger.Log(LogLevel.Fatal, "{0} StopLoss module was attempting to perform StopLoss, however after it finished the position (pol) is {1}. Situation and position needs to be reviewed. StopLoss module need to be unblocked explicitly",
                    this._systemFriendlyName, this._currentPositionBasePol);
            }
            else
            {
                Interlocked.Exchange(ref this._processingStopLossCondition, 0);
                this._logger.Log(LogLevel.Info, "{0} Unblocking the SL procedure successfully done.", this._systemFriendlyName);
            }
        }

        private decimal _borderStopLossPrice;
        private void CalculatePnlNet(decimal closingPositionPrice, bool justPriceChange, DealDirection? integratorDealDirectionForClosingPosition)
        {
            bool recalculate = !justPriceChange || !integratorDealDirectionForClosingPosition.HasValue ||
                               PriceCalculationUtils.IsWorseOrEqualPrice(closingPositionPrice, _borderStopLossPrice,
                                   integratorDealDirectionForClosingPosition.Value);

            if (recalculate)
            {
                decimal pnlNetUsd =
                (
                    //term ccy position
                this._totalAcquiredAmountTermPol.ToDecimal() +
                    // + cost of flattening opened position - cost of commisions
                    //Comissions should correctly use baseCurrencyMidPrceUsdMultiplier - however this way (converting to current term price and then to USD)
                    // is one multiplication cheaper and still give us reasonable estimate of PnLNet
                (_currentPositionBasePol - this._totalCommissions).ToDecimal() *
                closingPositionPrice
                ) * this._termCurrencyUsdConversionProvider.GetCurrentUsdConversionMultiplier();

                if (_currentPositionBasePol - this._totalCommissions != AtomicSize.ZERO)
                    _borderStopLossPrice = ((-this._stopLossLimit)/
                                            this._termCurrencyUsdConversionProvider.GetCurrentUsdConversionMultiplier() -
                                            this._totalAcquiredAmountTermPol.ToDecimal())/
                                           (_currentPositionBasePol - this._totalCommissions).ToDecimal();

                this._logger.Log(LogLevel.Trace, "{0} Current PnlNetUsd: {1}, Current border stop loss price: {2}",
                    _systemFriendlyName, pnlNetUsd, _borderStopLossPrice);

                if (-pnlNetUsd >= this._stopLossLimit)
                {
                    HandleStopLossExceeded(pnlNetUsd);
                }
                else
                {
                    this._stopLossExceeded = false;
                }
            }
        }

        public void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal)
        {
            _currentPositionBasePol.InterlockedAdd(integratorDealInternal.FilledAmountBasePol);
            _totalAcquiredAmountTermPol.InterlockedAdd(integratorDealInternal.AcquiredAmountTermPol);

            //Make sure that multiple concurrent updates still adds up => Interlocked => long
            // make sure that long doesn't cut the decimal places => MlutByMillion calculation;
            decimal dealCommisionMultByMillion = (this._symbolsInfo.GetCommisionPricePerMillion(integratorDealInternal.Counterparty) *
                                 integratorDealInternal.FilledAmountBaseAbs);
            this._totalCommissionsMultByMillion.InterlockedAdd(dealCommisionMultByMillion);
            //this can potentially get on older value during concurrent updates, but difference is marginal and
            // we can avoid heavy weight lock
            this._totalCommissions = AtomicSize.FromDecimal(_totalCommissionsMultByMillion.ToDecimal() / 1000000.0m);

            this.CalculatePnlNet(false, null);
        }

        private bool SendClosingBankOrder(decimal sizeBasePol, IntegratedTradingSystemIdentification identification)
        {
            return this.SendClosingBankOrder(Math.Abs(sizeBasePol),
                                             sizeBasePol > 0 ? DealDirection.Sell : DealDirection.Buy, identification);
        }

        private bool SendClosingBankOrder(decimal sizeBaseAbs, DealDirection dealDirection, IntegratedTradingSystemIdentification identification)
        {
            if (sizeBaseAbs == 0)
                return false;

            BankPoolClientOrderRequestInfo bankOri = ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateMarket(true, sizeBaseAbs, dealDirection, _symbol, true);

            BankPoolClientOrder bankOrder = ClientOrdersBuilder_OnlyForIntegratorUsage.CreateBankPoolClientOrder(
                string.Format("SLClosing_{0}_{1:HHmmssfffffff}", _symbol.ShortName, HighResolutionDateTime.UtcNow), "SLClosing",
                bankOri, identification, null);

            Interlocked.Increment(ref this._activeBankOrders);
            var result = this._orderManagement.RegisterOrder(bankOrder, this._unicastInfoForwarder);
            if (!result.RequestSucceeded)
            {
                Interlocked.Decrement(ref this._activeBankOrders);
                this._logger.Log(LogLevel.Fatal,
                                 "{0} SL module unable to register bank pool order to {1} {2} of {3}. Reason: [{4}]. Order will need to be performed manualy in order to flat the position.",
                                 this._systemFriendlyName, bankOri.IntegratorDealDirection, bankOri.SizeBaseAbsInitial, bankOri.Symbol, result.ErrorMessage);
            }

            return result.RequestSucceeded;
        }

        #region IUnicastInfoForwarder members

        public void OnIntegratorDealInternal(IntegratorDealInternal integratorDealInternal)
        {
            this.HandleIntegratorDealInternal(integratorDealInternal);
        }

        public void OnIntegratorUnconfirmedDealInternal(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
        {
            this._logger.Log(LogLevel.Fatal, "{0} SL module receiving Unconfirmed Deal - this is unexpected. {1}", this._systemFriendlyName, integratorUnconfirmedDealInternal);
        }

        public void OnIntegratorClientOrderUpdate(ClientOrderUpdateInfo clientOrderUpdateInfo, Symbol symbol)
        {
            if (clientOrderUpdateInfo.SizeBaseAbsRejected + clientOrderUpdateInfo.SizeBaseAbsCancelled > 0)
            {
                if (!clientOrderUpdateInfo.Side.HasValue)
                {
                    this._logger.Log(LogLevel.Fatal, "{1} SL Detected not filled amount from order update, but side is unknown! Situation need to be rewieved, {1}", _systemFriendlyName, clientOrderUpdateInfo);
                    return;
                }

                this.TryReissueBankOrderOnNotFilledAmount(clientOrderUpdateInfo.SizeBaseAbsRejected + clientOrderUpdateInfo.SizeBaseAbsCancelled,
                    clientOrderUpdateInfo.Side.Value, clientOrderUpdateInfo.ClientOrderIdentity, null);
            }
            else if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator
                     ||
                     clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator
                     ||
                     clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RemovedFromIntegrator)
            {
                if (Interlocked.Decrement(ref this._activeBankOrders) == 0)
                {
                    this.ExitStopLossExceededProcedure();
                }
            }
        }

        public void OnCounterpartyRejectedOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            //this is it
        }

        public void OnCounterpartyIgnoredOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            //Bankpool ignored orders are NOT handeled by the handler for closed bankpool orders
            this.TryReissueBankOrderOnNotFilledAmount(counterpartyOrderIgnoringInfo.IgnoredAmountBaseAbs,
                                                      counterpartyOrderIgnoringInfo.Direction,
                                                      counterpartyOrderIgnoringInfo.ClientOrderIdentity,
                                                      counterpartyOrderIgnoringInfo.IntegratedTradingSystemIdentification);
        }

        private int _reissueAttempts;
        private int _activeBankOrders;
        //Attempts to redo bankpool order that hasn't been fully filled for some reason
        // Handeled cases: some amount is ignored, rejected or canceled
        // Use cases when this can be triggered:
        //                         - order is ignored
        //                         - order is cancelled - most probably because of flipped kill switch (as it cancells all orders) - replacing order will likely fail
        //                         - order is rejected by BankPool - currently only when it triggers killSwitch - replacing order will likely fail
        private void TryReissueBankOrderOnNotFilledAmount(decimal notFilledAmount, DealDirection direction,
                                                          string failedOrderIdentity,
                                                          IntegratedTradingSystemIdentification identification)
        {
            bool reissue = Interlocked.Increment(ref this._reissueAttempts) <= 3;

            this._logger.Log(LogLevel.Fatal,
                             "{0} SL receiving final order update or ignore info for bankpool order {1}, but position size(pol) {2} was not filled. {3}etrying to reissue failed amount. Total system position(pol): {4}",
                             this._systemFriendlyName, failedOrderIdentity,
                             direction == DealDirection.Buy ? notFilledAmount : -notFilledAmount,
                             reissue ? "R" : "NOT r", _currentPositionBasePol);

            if (reissue)
            {
                reissue = this.SendClosingBankOrder(notFilledAmount, direction, identification);
            }

            if (!reissue)
            {
                ExitStopLossExceededProcedure();
            }
        }

        #endregion IUnicastInfoForwarder members
    }
}
