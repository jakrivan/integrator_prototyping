﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    internal class FXallCrossSystem: VenueCrossSystemBase
    {
        public FXallCrossSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook,
                                IBookTop<PriceObjectInternal> venueAskPriceBook, IBookTop<PriceObjectInternal> venueBidPriceBook,
                                Symbol symbol, IOrderManagement orderManagement, IRiskManager riskManager,
                                ITradingGroupsCache tradingGroupsCache,
                                IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                                IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings> settingsProvider)
            : base(
                askPriceBook, bidPriceBook,
                new InProcessVenueToBProvider(venueAskPriceBook, venueBidPriceBook),
                symbol, Counterparty.FA1, destinationOrdSession, symbolsInfo, orderManagement, riskManager,
                tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        { }


        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
                                                                               decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateFXallOrder(
                sizeBaseAbs, _settings.MinimumFillSize, price, dealDirection, symbol,
                TimeInForce.ImmediateOrKill);
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateFXallClientOrder(orderIdentity, clientIdentity, venueOrderRequest as FXallClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }

        private readonly Counterparty[] _counterpartiesWhiteList = new Counterparty[]
        {Counterparty.CZB, Counterparty.FC1, Counterparty.GLS, Counterparty.JPM};
        protected override Counterparty[] CounterpartiesWhiteList { get { return _counterpartiesWhiteList; } }
    }
}
