﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public abstract class VenueCrossSystemBase : IntegratedSystemBase<IVenueCrossSystemSettings>
    {
        private IVenueToBProvider _venueToBProvider;
        private DateTime[] _nextTradeNotAllowedBefore =
            Enumerable.Repeat(DateTime.MinValue, Counterparty.ValuesCount).ToArray();
        protected VenueCrossSystemBase(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                        IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook,
                                        IVenueToBProvider venueToBProvider, Symbol symbol,
                                        Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
                                        ISymbolsInfo symbolsInfo, IOrderManagement orderManagement,
                                        IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
                                        IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                        IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings> settingsProvider)
            : base(
                askPriceBook, bidPriceBook, symbol, destinationVenueCounterparty, destinationOrdSession, symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider, IntegratedTradingSystemType.Cross, string.Empty, false, null)
        {
            this._venueToBProvider = venueToBProvider;
        }

        protected override void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            PricingIndirectorsCache.Instance.GetMember(priceBook)
                .DeregisterFromIndirectPricingEvent(this._destinationVenueCounterparty, OnNewPriceAvailable);
        }

        protected override void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            PricingIndirectorsCache.Instance.GetMember(priceBook)
                .RegisterForIndirectPricingEvent(this._destinationVenueCounterparty, OnNewPriceAvailable);
        }

        protected override void OnResetProcedure()
        {
            Volatile.Write(ref this._venueLegIsFullyDone, false);
            Volatile.Write(ref this._bankOrdersFullyDone, 0);
        }

        protected override void ProcessEvent()
        { }

        protected abstract Counterparty[] CounterpartiesWhiteList { get; }

        protected virtual void AdjustBankOrderRequest(BankPoolClientOrderRequestInfo bankOri)
        {
            bankOri.CounterpartiesWhitelist = this.CounterpartiesWhiteList;
        }

        //private static readonly Counterparty[] _forbiddenInitiators = new Counterparty[]
        //{Counterparty.BNP, Counterparty.MGS, Counterparty.RBS, Counterparty.SOC, Counterparty.UBS};

        private void OnNewPriceAvailable(PriceObjectInternal priceObject)
        {
            //Front load this check - as majority of prices are disallowed now and this is not volatile/interlocked check
            //  -- so it is friendlier to other threads also
            //allow only crosses where Bank was the first initiator and when it has available size
            if (PriceObjectInternal.IsNullPrice(priceObject) ||
                //TEMP: only LMAX and MGS can trigger crosses
                //(priceObject.Counterparty.TradingTargetType != TradingTargetType.LMAX
                //&& priceObject.Counterparty != Counterparty.MGS
                //) ||
                //priceObject.Counterparty == this._destinationVenueCounterparty ||
                !CounterpartiesWhiteList.Contains(priceObject.Counterparty) ||
                ////TEMP: do not allow SOC and co. to skew the decision making
                //_forbiddenInitiators.Contains(priceObject.Counterparty) ||
                priceObject.SizeBaseAbsRemainingToClaim < _settings.MinimumSizeToTrade ||
                !this.IsPriceValid(priceObject))
                return;

            if (!this.IsReadyToProcessEvents())
                return;
    
            //cheaper then extracting Hotspot ToB (for others it might be switched but unsignificant)
            DateTime now = HighResolutionDateTime.UtcNow;
            if (this._nextTradeNotAllowedBefore[(int)priceObject.Counterparty] > now)
                return;

            decimal venueSize;
            decimal venuePrice;
            //PriceObjectInternal venuePriceObjectInternal;

            if (!this._venueToBProvider.TryRetrieveVenueToB(priceObject.Side.ToOppositeSide(), out venuePrice, out venueSize/*, out venuePriceObjectInternal*/))
                return;

            if (venueSize < _settings.MinimumSizeToTrade)
                return;

            decimal askPrice = priceObject.Side == PriceSide.Ask ? priceObject.Price : venuePrice;
            decimal bidPrice = priceObject.Side == PriceSide.Ask ? venuePrice : priceObject.Price;

            if (askPrice > bidPrice)
                return;

            if (bidPrice - askPrice < _settings.MinimumDecimalDiscountToTrade)
                return;

            decimal sizeBaseAbs = Math.Min(venueSize,
                Math.Min(priceObject.SizeBaseAbsRemainingToClaim, _settings.MaximumSizeToTrade));

            //potentially we claim little more here, but we want to avoid costly cost of double rounding
            sizeBaseAbs = priceObject.ClaimSizeBaseAbs(_settings.MinimumSizeToTrade, sizeBaseAbs);

            sizeBaseAbs = SymbolsInfoUtils.RoundToIncrement(RoundingKind.AlwaysToZero, _orderMinimumSizeIncrement, sizeBaseAbs);

            if (sizeBaseAbs < _settings.MinimumSizeToTrade)
                return;

            //This is rendundant, but extremely dangerous if not met - therefore double checking
            if (sizeBaseAbs > _settings.MaximumSizeToTrade)
            {
                this._logger.Log(LogLevel.Fatal, "{0} trading system generated size request of [{1}] - cutting it to max size [{2}]",
                    this._systemFriendlyName, sizeBaseAbs, _settings.MaximumSizeToTrade);
                sizeBaseAbs = _settings.MaximumSizeToTrade;
            }

            //Careful remote Venue price providers doesn't provide price objects
            //if (venuePriceObjectInternal != null && !venuePriceObjectInternal.SubstractSizeBaseAbs(sizeBaseAbs))
            //    return;

            if (TryEnterSingleAccessRegion())
            {
                //Prices from the same counterparty arrives on the same thread so it cannot happen that 2 signals
                // passes the previous check on 2 parallel threads - therefor following check is not needed.
                //if (this._nextTradeNotAllowedBefore[(int) priceObject.Counterparty] > now)
                //{
                //    ExitSingleAccessRegion();
                //    return;
                //}
                this._nextTradeNotAllowedBefore[(int) priceObject.Counterparty] = 
                    now + _settings.MinimumTimeBetweenSingleCounterpartySignals;
                this._logger.Log(LogLevel.Trace, "Discount {0} situation encountered", this._systemFriendlyName);
                this.PerformCross(venuePrice, sizeBaseAbs, priceObject);
            }
        }

        protected override DealDirection CurrentHeadOrderPolarity { get { return this._currentHeadOrderPolrity; } }

        private void PerformCross(decimal venuePrice, decimal venueSizeBaseAbs, PriceObjectInternal bankCrossingPrice)
        {
            this._currentHeadOrderPolrity = bankCrossingPrice.Side.ToOppositeSide().ToOutgoingDealDirection();
            VenueClientOrderRequestInfo venueOri = this.CreateVenueOrderRequest(
                venuePrice, this._currentHeadOrderPolrity, bankCrossingPrice.Symbol,
                venueSizeBaseAbs);

            int currentOrderCnt = Interlocked.Increment(ref _headOrdersCnt);
            var venueClientOrder = this.CreateVenueOrder(this._systemFriendlyName + "_HEAD_" + currentOrderCnt,
                                            this._systemIdentity, venueOri, this._integratedTradingSystemIdentification);

            var result = this.RegisterOrder(venueClientOrder);
            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Error, "Submitting Venue leg of {0} failed: {1}", this._systemFriendlyName, result.ErrorMessage);
                ExitSingleAccessRegion();
            }
        }


        private bool _venueLegIsFullyDone;
        private int _bankOrdersFullyDone;
        private DealDirection _currentHeadOrderPolrity;

        private bool IsCorssFullyDone()
        {
            return _venueLegIsFullyDone && _bankOrdersFullyDone >= _bankSubOrdersCnt;
        }

        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            if (clientOrderUpdateInfo.SizeBaseAbsTotalRemaining == 0m)
            {
                if (clientOrderUpdateInfo is VenueClientOrderUpdateInfo)
                    Volatile.Write(ref this._venueLegIsFullyDone, true);

                if (clientOrderUpdateInfo is BankPoolClientOrderUpdateInfo)
                    Interlocked.Increment(ref this._bankOrdersFullyDone);

                if (this.IsCorssFullyDone())
                {
                    this._logger.Log(LogLevel.Info, "Closed next ({0}) {1} cross (with {2} bank suborders), total acquired term amount polarized: {3}",
                             this._headOrdersCnt, this._systemFriendlyName, this._bankSubOrdersCnt, this._totalAcquiredAmountTermPol);

                    this.ResetInternal(false);
                }
            }
        }
    }
}
