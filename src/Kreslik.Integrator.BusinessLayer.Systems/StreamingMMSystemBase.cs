﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class StreamingMMSystemBase<T> : IntegratedSystemBase<T>, IStreamingPricesProducer where T : class, IVenueStreamMMSystemSettings
    {
        private static readonly TimeSpan _MIN_WAIT_TIME_AFTER_SWEEP_DEFENSE = TimeSpan.FromMilliseconds(300);
        private SafeTimer _recheckPositionTimer;

        public StreamingMMSystemBase(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook, 
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook,
            Symbol symbol, PriceSide observedBookSide, Counterparty destinationVenueCounterparty,
            IStreamingChannel destinationStreamingChannel, ISymbolsInfo symbolsInfo,
            IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement, 
            IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
            IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<T> settingsProvider)
            : this(
                lmaxAskPriceBook, lmaxBidPriceBook, bankpoolAskPricebook, bankpoolBidPricebook,
                symbol, observedBookSide, destinationVenueCounterparty,
                destinationStreamingChannel, symbolsInfo,
                medianPriceProvider, orderManagement,
                riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger,
                settingsProvider, IntegratedTradingSystemType.Stream)
        { }

        protected StreamingMMSystemBase(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook,
            Symbol symbol, PriceSide observedBookSide, Counterparty destinationVenueCounterparty,
            IStreamingChannel destinationStreamingChannel, ISymbolsInfo symbolsInfo,
            IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
            IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
            IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<T> settingsProvider, IntegratedTradingSystemType systemType)
            : base(
                bankpoolAskPricebook, bankpoolBidPricebook, symbol, destinationVenueCounterparty,
                destinationStreamingChannel, symbolsInfo, orderManagement, riskManager, tradingGroupsCache, 
                unicastInfoForwardingHub, logger,
                settingsProvider, systemType, observedBookSide == PriceSide.Ask ? "S" : "B", false, observedBookSide)
        {
            this._bankpoolAskPricebook = bankpoolAskPricebook;
            this._bankpoolBidPricebook = bankpoolBidPricebook;
            this._lmaxAskPricebook = lmaxAskPriceBook;
            this._lmaxBidPricebook = lmaxBidPriceBook;
            this._destinationStreamingChannel = destinationStreamingChannel;
            this._observedBookSide = observedBookSide;
            this._medianPriceProvider = medianPriceProvider;
            this._recheckPositionTimer = new SafeTimer(ProcessEventWithRecursionCheck);

            this._destinationStreamingChannelProxy = destinationStreamingChannel.GetStreamingChannelProxy(this);
            //verify that settings are OK
            //Not needed here - already in base
            //this.SettingsUpdating(this._settings);
        }

        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _bankpoolAskPricebook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _bankpoolBidPricebook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _lmaxAskPricebook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _lmaxBidPricebook;
        protected IStreamingChannel _destinationStreamingChannel;
        private IStreamingChannelProxy _destinationStreamingChannelProxy;
        protected PriceSide _observedBookSide;
        private IMedianProvider<decimal> _medianPriceProvider;
        //TODO: remove
        private bool _pricingSuspended;
        private decimal _lastSentPrice;
        private DateTime _lastSentPriceSentUtc;
        private SingleThreadedEventsRateChecker _submissionErroRateChecker =
            new SingleThreadedEventsRateChecker(TimeSpan.FromSeconds(10), 100);
        private StreamingPrice _streamingPrice = new StreamingPrice();

        protected override void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopDeteriorated -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopReplaced -= ProcessEventTobAdapter;
        }

        protected override void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved += ProcessEventTobAdapter;

            //MM system side quotes only after changes on it's side of book (the other side is for check of negative spread)
            if (this._observedBookSide == PriceSide.Ask ? priceBook == _askReferencePriceBook : priceBook == _bidReferencePriceBook)
            {
                priceBook.ChangeableBookTopDeteriorated += ProcessEventTobAdapter;
                priceBook.ChangeableBookTopReplaced += ProcessEventTobAdapter;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        //TODO: this will hold the streaming rate condition
        private bool IsReadyForNextMarkup()
        {
            return _currentPositionBasePol.InterlockedIsZero() && !this._pricingSuspended;
        }

        private void SuspendPricing()
        {
            this._lastSentPrice = 0;
            this._pricingSuspended = true;
            this._destinationStreamingChannelProxy.SuspendPricing();
        }

        private void ResumePricing()
        {
            if (this._pricingSuspended)
            {
                this._pricingSuspended = false;
                this._destinationStreamingChannelProxy.ResumePricing();
            }
        }

        //Single access regions ensure that we send either price or cancel - and that order doesn't get switched
        private void CancelLastStreamedPrice()
        {
            if (_lastSentPrice != 0)
            {
                _lastSentPrice = 0;
                this._destinationStreamingChannelProxy.CancelLastPriceIfRegistered();
            }
        }

        protected override void OnResetProcedure()
        {
            CancelLastStreamedPrice();
            if (this._availableSizeToTradeBaseAbs.ToDecimal() != this._settings.MaximumSizeToTrade)
            {
                this._logger.Log(LogLevel.Fatal, "System {0} has internal availble position to trade set to {1} while in settings its {2}. Reseting it based on settings, however this is unexpected.",
                    this._systemFriendlyName, this._availableSizeToTradeBaseAbs.ToDecimal(), this._settings.MaximumSizeToTrade);
                this._availableSizeToTradeBaseAbs = AtomicSize.FromDecimal(_settings.MaximumSizeToTrade);
            }
        }

        protected override void AfterDisablingProcedure()
        {
            this.SuspendPricing();
        }

        protected override void DuringEnableProcedure()
        {
            this.ResumePricing();
        }

        protected override bool IsAllowedToBeEnabled { get { return this._destinationStreamingChannelProxy.IsAllowedToStream; } }


        private int _registrationsCnt = 0;
        protected override void TryRegisterSystem()
        {
            this._logger.Log(LogLevel.Info, "Registering system {0}", this._systemFriendlyName);

            if (InterlockedEx.CheckAndFlipState(ref this._registrationsCnt, 1, 0))
            {
                if (!this._destinationStreamingChannelProxy.RegisterStreamingPricesProducer())
                {
                    this._logger.Log(LogLevel.Fatal, "System {0} couldn't register itself (unable to add additional streaming layer) - hardblocking it",
                        this._systemFriendlyName);
                    this.HardBlock();
                }
            }
            else
            {
                this._logger.Log(LogLevel.Warn, "{0} attempted to be double registered - ignoring", this._systemFriendlyName);
            }
            
        }

        protected override void UnregisterSystem()
        {
            if (InterlockedEx.CheckAndFlipState(ref this._registrationsCnt, 0, 1))
            {
                this._destinationStreamingChannelProxy.UnregisterStreamingPricesProducer();
            }
        }

        public void StartStreaming()
        {
            this.OnSystemAllowedStateChanged(true);
        }

        public void StopStreaming()
        {
            this._logger.Log(LogLevel.Info, "{0}, destination unsubscribed from updates", this._systemFriendlyName);
            this.CancelLastStreamedPrice();

            this.OnSystemAllowedStateChanged(false);
        }

        protected override void SettingsUpdating(T newSettings)
        {
            if (newSettings.ReferenceLmaxBookOnly)
                this.SwapReferenceBooks(_lmaxAskPricebook, _lmaxBidPricebook);
            else
                this.SwapReferenceBooks(_bankpoolAskPricebook, _bankpoolBidPricebook);

            this._availableSizeToTradeBaseAbs =
                AtomicSize.FromDecimal(newSettings.MaximumSizeToTrade)
                    .InterlockedAdd(-Math.Abs(_currentPositionBasePol.ToDecimal()));
            this._minimumSizeToTradeAtomicBaseAbs = AtomicSize.FromDecimal(newSettings.MaximumSizeToTrade);

            if (!this._destinationStreamingChannel.IsOrderHoldTimeWithinLimits(newSettings.KGTLLTime))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with LL time higher then destination session auto-reject interval - Hard blocking system",
                    this._systemFriendlyName);
                this.HardBlock();
                return;
            }

            if (newSettings.MinimumFillSize > newSettings.MinimumSizeToTrade ||
                newSettings.MinimumSizeToTrade > newSettings.MaximumSizeToTrade)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with sizes (MinFill: {1}, MinSize: {2}, MaxSize: {3}) with incorrect order (should be MinFil<=MinSize<=MaxSize) - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumFillSize, newSettings.MinimumSizeToTrade,
                    newSettings.MaximumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(newSettings.MinimumFillSize,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MinimumFillSize: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumFillSize);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderSizeWithinSizeAndIncrementLimit(newSettings.MinimumSizeToTrade,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MinimumSizeToTrade: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderSizeWithinSizeAndIncrementLimit(newSettings.MaximumSizeToTrade,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MaximumSizeToTrade: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MaximumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (newSettings.MaximumSourceBookTiers > 8)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MaximumSourceBookTiers: {1} greater than 8 - Hard blocking system",
                    this._systemFriendlyName, newSettings.MaximumSourceBookTiers);
                this.HardBlock();
                return;
            }
        }

        private CancelingReason MatchesSpreadCriteria_ToBCheckOnly(PriceObjectInternal askPrice, PriceObjectInternal bidPrice)
        {
            if (PriceObjectInternal.IsNullPrice(askPrice) || PriceObjectInternal.IsNullPrice(bidPrice))
                return CancelingReason.PricesNull;

            if (bidPrice.Price - askPrice.Price > _settings.MaximumDiscountDecimalToTrade)
                return CancelingReason.MaxDiscountExceeded;

            if (!this.IsPriceValid(askPrice, PriceSide.Ask) || !this.IsPriceValid(bidPrice, PriceSide.Bid))
                return CancelingReason.PricesNull;

            if (_sweepDefenceActiveTillUtc > HighResolutionDateTime.UtcNow)
            {
                return CancelingReason.SweepDefenceTimeout;
            }

            return CancelingReason.OK;
        }

        private CancelingReason MatchesSpreadCriteria(PriceObjectInternal askPrice, PriceObjectInternal bidPrice)
        {
            CancelingReason reason = this.MatchesSpreadCriteria_ToBCheckOnly(askPrice, bidPrice);
            if (reason != CancelingReason.OK)
                return reason;

            if (_lastSentPrice != 0)
            {
                decimal askDifference = _lastSentPrice - askPrice.Price;
                decimal bidDifference = _lastSentPrice - bidPrice.Price;

                //ToB price crossed last KGT price
                if (
                    this._observedBookSide == PriceSide.Ask &&
                    (-askDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel || -bidDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel)
                    ||
                    this._observedBookSide == PriceSide.Bid &&
                    (askDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel || bidDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel)
                    )
                {
                    _sweepDefenceActiveTillUtc = HighResolutionDateTime.UtcNow + _MIN_WAIT_TIME_AFTER_SWEEP_DEFENSE;
                    return CancelingReason.SweepDefenecePriceCrossed;
                }
            }

            return CancelingReason.OK;
        }

        private DateTime _sweepDefenceActiveTillUtc;

        private void ProcessEventTobAdapter(PriceObjectInternal priceObject)
        {
            //We do not need recursion check here as this event is invoked only directly by market data event
            // subsequent recursions (after issuing order or cancel) goes through ProcessEvent()

            if (!this.IsReadyToProcessEvents())
                return;
            if (!IsReadyForNextMarkup())
                return;

            PriceObjectInternal askToB = priceObject.Side == PriceSide.Ask ? priceObject : _askReferencePriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = priceObject.Side == PriceSide.Ask ? _bidReferencePriceBook.MaxNodeInternal : priceObject;
            PriceObjectInternal priceToRelease = priceObject.Side == PriceSide.Ask ? bidToB : askToB;

            try
            {
                ProcessEventInternal(askToB, bidToB);
            }
            finally
            {
                priceToRelease.Release();
            }
        }

        protected override void ProcessEvent()
        {
            if (!this.IsReadyToProcessEvents())
                return;

            //Frontload this check as because of Venue RT time, this will be often blocked
            if (!IsReadyForNextMarkup())
            {
                this._logger.Log(LogLevel.Trace, this._systemFriendlyName + " ignoring price change - not ready for sending (not flat or waiting for venue response)");
                return;
            }

            PriceObjectInternal askToB = _askReferencePriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = _bidReferencePriceBook.MaxNodeInternal;

            try
            {
                ProcessEventInternal(askToB, bidToB);
            }
            finally
            {
                askToB.Release();
                bidToB.Release();
            }
        }

        private void TryCancelPositionOnCheckFailed(CancelingReason reason)
        {
            if (TryEnterSingleAccessRegion())
            {
                CancelLastStreamedPrice();

                ExitSingleAccessRegion();
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName +
                    " not canceling after spread or size or decay criteria missed - not ready for sending (second check)");
            }
            this._logger.Log(LogLevel.Trace,
                this._systemFriendlyName +
                " ignoring price change (or canceling) - " + reason);
        }

        private bool BookProbingAllowed { get { return this._settings.MaximumSourceBookTiers > 1; } }

        private enum CancelingReason
        {
            OK,
            NotEnaughAvailableSize,
            UnavailableBookSizeAndProbingNotAllowed,
            UnavailableBookSizeAfterProbing,
            MaxDiscountExceeded,
            SweepDefenecePriceCrossed,
            SweepDefenceTimeout,
            PricesNull
        }

        private void ProcessEventInternal(PriceObjectInternal askToB, PriceObjectInternal bidToB)
        {
            PriceObjectInternal observedSidePrice = this._observedBookSide == PriceSide.Ask ? askToB : bidToB;

            //not enough available size
            if (_availableSizeToTradeBaseAbs < this._minimumSizeToTradeAtomicBaseAbs)
            {
                this.TryCancelPositionOnCheckFailed(CancelingReason.NotEnaughAvailableSize);
                return;
            }

            //not enough liquidity on a book
            if (observedSidePrice.SizeBaseAbsRemaining < _settings.MinimumSizeToTrade && !this.BookProbingAllowed)
            {
                this.TryCancelPositionOnCheckFailed(CancelingReason.UnavailableBookSizeAndProbingNotAllowed);
                return;
            }

            //spread missed
            CancelingReason reason = MatchesSpreadCriteria(askToB, bidToB);
            if (reason != CancelingReason.OK)
            {
                this.TryCancelPositionOnCheckFailed(reason);
                return;
            }

            decimal referencePrice = observedSidePrice.Price;
            decimal availableSizeToTrade = _availableSizeToTradeBaseAbs.ToDecimal();
            decimal sizeBaseAbs = Math.Min(observedSidePrice.SizeBaseAbsRemaining, availableSizeToTrade);
            bool tobSizeInsufficient = sizeBaseAbs != availableSizeToTrade;
            if (availableSizeToTrade >= _settings.MinimumSizeToTrade)
            {
                if (tobSizeInsufficient)
                {
                    if (this.BookProbingAllowed)
                    {
                        sizeBaseAbs = availableSizeToTrade;
                        referencePrice =
                            (this._observedBookSide == PriceSide.Ask
                                ? this._askReferencePriceBook
                                : this._bidReferencePriceBook)
                                .GetWeightedPriceForSize(ref sizeBaseAbs, this._settings.MaximumSourceBookTiers);
                    }

                    sizeBaseAbs = SymbolsInfoUtils.RoundToIncrement(RoundingKind.AlwaysToZero,
                        _orderMinimumSizeIncrement, sizeBaseAbs);

                    //This is redundant, but extremely dangerous if not met - therefore double checking
                    if (sizeBaseAbs > availableSizeToTrade)
                    {
                        this._logger.Log(LogLevel.Fatal,
                            "{0} trading system generated size request of [{1}] - cutting it to max size [{2}]",
                            this._systemFriendlyName, sizeBaseAbs, _settings.MaximumSizeToTrade);
                        sizeBaseAbs = availableSizeToTrade;
                    }
                }
            }

            if (sizeBaseAbs < _settings.MinimumSizeToTrade || referencePrice == 0m)
            {
                this.TryCancelPositionOnCheckFailed(CancelingReason.UnavailableBookSizeAfterProbing);
                return;
            }

            decimal requestedPrice;
            RoundingKind improvingRoundingKind = this._observedBookSide == PriceSide.Ask
                ? RoundingKind.AlwaysAwayFromZero
                : RoundingKind.AlwaysToZero;

            decimal offset = _settings.BestPriceImprovementOffsetDecimal;

            decimal requestedPriceUnrounded = this._observedBookSide == PriceSide.Ask
                ? referencePrice + offset
                : referencePrice - offset;

            if (!this._symbolsInfo.TryRoundToSupportedPriceIncrement(
                this._symbol, _destinationVenueCounterparty.TradingTargetType, improvingRoundingKind,
                requestedPriceUnrounded,
                out requestedPrice))
            {
                this._logger.Log(LogLevel.Error,
                    this._systemFriendlyName + " - Submitting Venue leg failed due to unability to roundprice");
                return;
            }

            this._logger.Log(LogLevel.Trace,
                "Venue leg of {4} will have price {0} - from unrounded value of {1} (ToB price {2} and offset {3}) And size {5} (ToB size {6}sufficient, book probing {7}allowed)",
                requestedPrice, requestedPriceUnrounded, referencePrice, offset, this._systemFriendlyName, sizeBaseAbs,
                tobSizeInsufficient ? "IN" : string.Empty, this.BookProbingAllowed ? string.Empty : "NOT ");

            if (TryEnterSingleAccessRegion())
            {
                //
                // BEGIN OF SINGLE ACCESS CONSTRAINED REGION
                //

                //Check this again as condition might changed before we got to constrained access region
                if (IsReadyForNextMarkup()
                    &&
                    _lastSentPrice != requestedPrice)
                {
                    if ((_settings.MinimumIntegratorPriceLife == TimeSpan.MinValue ||
                         _lastSentPriceSentUtc + _settings.MinimumIntegratorPriceLife < HighResolutionDateTime.UtcNow))
                    {

                        bool succeeded = this._destinationStreamingChannelProxy.
                            OverrideStreamingPrice(this._streamingPrice, this._symbol,
                                this._destinationVenueCounterparty,
                                this._observedBookSide, sizeBaseAbs, _settings.MinimumFillSize, requestedPrice);
                        succeeded &= this.CanSubmitSoftPrice(this._streamingPrice);

                        SubmissionResult result = SubmissionResult.Failed_InternalError;
                        if (succeeded)
                        {
                            result = _destinationStreamingChannel.SendPrice(this._streamingPrice);
                            succeeded = result == SubmissionResult.Success;
                            if (!succeeded)
                                this._logger.Log(LogLevel.Error, "{0} - Submitting of soft price failed: {1}",
                                    this._systemFriendlyName, result);
                        }

                        if (succeeded)
                        {
                            _lastSentPrice = requestedPrice;
                            _lastSentPriceSentUtc = HighResolutionDateTime.UtcNow;
                        }
                            //exceeding freq not considered an important error
                        else if (result != SubmissionResult.Failed_TooHighFrequency)
                        {
                            if (!this._submissionErroRateChecker.AddNextEventAndCheckIsAllowed())
                            {
                                this._logger.Log(LogLevel.Fatal,
                                    "System [{0}] exceeded allowed rate of soft prices submission errors (max {1} errors per {2}) - however continuing in operation",
                                    this._systemFriendlyName,
                                    this._submissionErroRateChecker.MaximumAllowedEventsPerInterval,
                                    this._submissionErroRateChecker.TimeIntervalToCheck);
                                this._submissionErroRateChecker.Reset();
                                this.CancelLastStreamedPrice();
                                //this.HardBlock();
                            }
                            else
                            {
                                //since we couldn't replace our price - at least cancel it
                                this.CancelLastStreamedPrice();
                            }
                        }
                    }
                    else
                    {
                        //scheduled only if there was price change that we missed to reflect due to our TTL
                        TimeSpan due = _lastSentPriceSentUtc + _settings.MinimumIntegratorPriceLife +
                                       TimeSpan.FromMilliseconds(1) - HighResolutionDateTime.UtcNow;
                        this._recheckPositionTimer.Change(TimeSpanEx.Max(due, TimeSpan.FromMilliseconds(1)), Timeout.InfiniteTimeSpan);
                    }
                }
                else if (this._verboseLoggingEnabled)
                {
                    this._logger.Log(LogLevel.Trace,
                        this._systemFriendlyName +
                        " ignoring price change - not flat, or no price change (or destiantion not subscribed) or TTL miss");
                }

                ExitSingleAccessRegion();
                //
                // END OF SINGLE ACCESS CONSTRAINED REGION
                //
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName + " ignoring price change - not ready for sending (second check)");
            }
        }

        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            //nothing needed
            //TODO: bank orders checks
        }

        protected override DealDirection CurrentHeadOrderPolarity { get { return this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(); } }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
            decimal sizeBaseAbs)
        {
            //this should not be invoked at all
            throw new NotImplementedException();
        }

        protected override VenueClientOrder CreateVenueOrder(string orderIdentity, string clientIdentity,
            VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            //this should not be invoked at all
            throw new NotImplementedException();
        }

        #region IStreamingPricesProducer

        public Symbol Symbol
        {
            get { return this._symbol; }
        }

        public DealDirection? WillProduceOrdersOnSide { get { return this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(); } }

        public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification
        {
            get { return this._integratedTradingSystemIdentification; }
        }

        private void ProcessUnconfirmedDealAfterDelay(RejectableMMDeal deal)
        {
            decimal referencePriceForSize = 0m;
            if (this._settings.PreferLLDestinationCheckToLmax)
            {
                referencePriceForSize = (this._observedBookSide == PriceSide.Ask
                    ? this._lmaxAskPricebook
                    : this._lmaxBidPricebook).GetWeightedPriceForExactSize(deal.CounterpartyRequestedSizeBaseAbs,
                        this._settings.MaximumSourceBookTiers);
            }

            if (referencePriceForSize == 0m)
            {
                referencePriceForSize = (this._observedBookSide == PriceSide.Ask
                    ? this._bankpoolAskPricebook
                    : this._bankpoolBidPricebook).GetWeightedPriceForExactSize(deal.CounterpartyRequestedSizeBaseAbs,
                        this._settings.MaximumSourceBookTiers);
            }

            if (referencePriceForSize == 0m)
            {
                this._logger.Log(LogLevel.Error, "{0} Rejecting deal as reference weighted price is not available", this._systemFriendlyName);
                this._destinationStreamingChannel.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.SystemFailedToProcess);
                AddAvailableSizeBack(deal.CounterpartyRequestedSizeBaseAbs);
                return;
            }

            decimal decimalDifferenceGross =
                (this._observedBookSide == PriceSide.Ask
                    ? (deal.CounterpartyRequestedPrice - referencePriceForSize)
                    : (referencePriceForSize - deal.CounterpartyRequestedPrice));

            bool accept = decimalDifferenceGross >= this._settings.MinimumDecimalDifferenceGrossToAccept;

            if (accept)
            {
                PriceObjectInternal askToB = _askReferencePriceBook.MaxNodeInternal;
                PriceObjectInternal bidToB = _bidReferencePriceBook.MaxNodeInternal;

                try
                {
                    accept = this.MatchesSpreadCriteria_ToBCheckOnly(askToB, bidToB) == CancelingReason.OK;
                }
                finally
                {
                    askToB.Release();
                    bidToB.Release();
                }

                if (!accept)
                    this._logger.Log(LogLevel.Info, "{0} Not Accepting {1} since ToB spread didn't pass criteria", this._systemFriendlyName, deal);
            }

            this._logger.Log(LogLevel.Info, "{0} {1}Accepting deal - DecimalDifferenceGross: {2} (from ref weighted price: {3} and deal price: {4}) vs min value in settings: {5}",
                this._systemFriendlyName, accept ? string.Empty : " NOT", decimalDifferenceGross, referencePriceForSize, deal.CounterpartyRequestedPrice, this._settings.MinimumDecimalDifferenceGrossToAccept);

            if (accept)
            {
                if (this._destinationStreamingChannel.AcceptDeal(deal))
                {
                    //position keeping
                    _currentPositionBasePol.InterlockedAdd(deal.IntegratorDealDirection == DealDirection.Buy ? deal.IntegratorFilledAmountBaseAbs : -deal.IntegratorFilledAmountBaseAbs);
                    _totalAcquiredAmountTermPol.InterlockedAdd(deal.IntegratorDealDirection == DealDirection.Buy ? -deal.IntegratorFilledAmountBaseAbs * deal.CounterpartyRequestedPrice : deal.IntegratorFilledAmountBaseAbs * deal.CounterpartyRequestedPrice);

                    //closing procedures
                    this.SendBankOrder(deal.IntegratorFilledAmountBaseAbs,
                        deal.IntegratorDealDirection.ToOpositeDirection(),
                        deal.Symbol, this._settings.PreferLmaxDuringHedging,
                        this._systemFriendlyName + "_HEAD_" + Interlocked.Increment(ref this._headOrdersCnt));//);
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                        "System {0} attempted to accept deal, but underlying layer rejected it. Details are in log file.",
                        this._systemFriendlyName);
                    AddAvailableSizeBack(deal.CounterpartyRequestedSizeBaseAbs);
                }
            }
            else
            {
                this._destinationStreamingChannel.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed);
                AddAvailableSizeBack(deal.CounterpartyRequestedSizeBaseAbs);
            } 
        }

        private AtomicSize _availableSizeToTradeBaseAbs;
        private AtomicSize _minimumSizeToTradeAtomicBaseAbs;

        protected void AddAvailableSizeBack(decimal sizeBaseAbs)
        {
            bool wasPositive = _availableSizeToTradeBaseAbs.InterlockedIsPositive();

            AtomicSize availableSizeAfterUpdate = this._availableSizeToTradeBaseAbs.InterlockedAdd(sizeBaseAbs);
            this._logger.Log(LogLevel.Info, "{0} available size to trade: {1}", this._systemFriendlyName, availableSizeAfterUpdate);
            if (availableSizeAfterUpdate > AtomicSize.ZERO && !wasPositive)
            {
                this.ResumePricing();
                Thread.MemoryBarrier();
                this.ProcessEventWithRecursionCheck();
            }
        }

        protected override void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal,
            AtomicSize currentPositionBasePol)
        {
            this.AddAvailableSizeBack(integratorDealInternal.FilledAmountBaseAbs);
        }

        private static readonly TimeSpan _maxAcceptableOrderClockSkew = TimeSpan.FromSeconds(3);
        public bool HandleNewUnconfirmedDeal(RejectableMMDeal deal, out RejectableMMDeal.DealRejectionReason rejectionReason)
        {
            if (deal.Symbol != this.Symbol)
            {
                rejectionReason = RejectableMMDeal.DealRejectionReason.IncomingOrderMismatch;
                this._logger.Log(LogLevel.Fatal,
                    "System {0} received order on unexpected symbol {1}! Order was autorejected. {2}",
                    this._systemFriendlyName, deal.Symbol, deal);
                return false;
            }

            if ((deal.CounterpartySentTimeUtc - deal.IntegratorReceivedTimeUtc).Duration() >
                _maxAcceptableOrderClockSkew)
            {
                rejectionReason = RejectableMMDeal.DealRejectionReason.IncomingOrderMismatch;
                this._logger.Log(LogLevel.Fatal,
                    "System {0} received order with reciving clock skew {1}! Order was autorejected. {2}",
                    this._systemFriendlyName, (deal.CounterpartySentTimeUtc - deal.IntegratorReceivedTimeUtc).Duration(), deal);
                return false;
            }

            rejectionReason = RejectableMMDeal.DealRejectionReason.SystemFailedToProcess;
            if (this.IsTradingUnexpectedNow)
            {
                this._logger.Log(LogLevel.Fatal,
                    "System {0} should be disabled from {1:HH:mm:ss.fff}, however it received client order. This is unexpected! Order was autorejected. {2}",
                    this._systemFriendlyName, this._disabledTime, deal);
                return false;
            }

            AtomicSize availableSizeToTrade =
                _availableSizeToTradeBaseAbs.InterlockedAdd(-deal.CounterpartyRequestedSizeBaseAbs);
            if (availableSizeToTrade < AtomicSize.ZERO)
            {
                //in future - partial fill
                this._destinationStreamingChannel.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.SystemUnavailableSizeToTrade);
                this._logger.Log(LogLevel.Error, "{0} rejecting {1} - UnavailableSizeToTrade (deal requested {2}, after subtract: {3})",
                    this._systemFriendlyName, deal.CounterpartyClientOrderIdentifier, deal.CounterpartyRequestedSizeBaseAbs, availableSizeToTrade);
                AddAvailableSizeBack(deal.CounterpartyRequestedSizeBaseAbs);
                return true;
            }
            else
            {
                if(!this.HandleUnconfirmedDealAfterAllChecksPassed(deal))
                    return false;
            }

            if (_availableSizeToTradeBaseAbs == AtomicSize.ZERO)
            {
                this.SuspendPricing();
            }
            else
            {
                this.ProcessEventWithRecursionCheck();
            }

            return true;
        }

        protected virtual bool HandleUnconfirmedDealAfterAllChecksPassed(RejectableMMDeal deal)
        {
            DateTime now = HighResolutionDateTime.UtcNow;
            DateTime systemDecisioningTime = DateTimeEx.Min(now + _settings.KGTLLTime,
                deal.MaxProcessingCutoffTime - Constansts.MinimumSpanBetweenLLandWatchdog);

            if (now >= systemDecisioningTime - HighResolutionDateTime.ResolutionThreshold)
            {
                ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(
                    () => ProcessUnconfirmedDealAfterDelay(deal), TimerTaskFlagUtils.WorkItemPriority.Highest));
            }
            else
            {
                HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(
                    () => ProcessUnconfirmedDealAfterDelay(deal),
                    systemDecisioningTime, TimerTaskFlagUtils.WorkItemPriority.Highest,
                    TimerTaskFlagUtils.TimerPrecision.Highest, false, false);
            }

            return true;
        }

        public bool TryHandleCounterpartyRejectedExecutedDeal(Symbol symbol, decimal sizeBasePol)
        {
            if (
                sizeBasePol == 0 ||
                (sizeBasePol < 0 && this._observedBookSide != PriceSide.Ask) ||
                (sizeBasePol > 0 && this._observedBookSide != PriceSide.Bid))
            {
                this._logger.Log(LogLevel.Fatal, "System {0} observing and sending {1}s is receiving counterparty rejected deal for POLARIZED: {2} {3}. Polarization is mismatched - NOT PROCESSING FURTHER. Postition needs to be handled manually",
                    this._systemFriendlyName, this._observedBookSide, sizeBasePol, symbol);
                this.HardBlock();
                return false;
            }

            //remove from position keepening
            if (_availableSizeToTradeBaseAbs.InterlockedAdd(-Math.Abs(sizeBasePol)) <= AtomicSize.ZERO)
            {
                this.SuspendPricing();
            }

            //send bank order
            this.SendBankOrderWithHeadOrderDirection(Math.Abs(sizeBasePol),
                this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(),
                symbol, this._settings.PreferLmaxDuringHedging,
                this._systemFriendlyName + "_HEAD_" + Interlocked.Increment(ref this._headOrdersCnt));

            //hard block
            this.HardBlock();

            return true;
        }

        #endregion IStreamingPricesProducer
    }


}
