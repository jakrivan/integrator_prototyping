﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public abstract class StreamingTakerMMSystemBase<T> : IntegratedSystemBase<T> where T : class, IVenueStreamMMSystemSettings
    {
        protected PriceSide _observedBookSide;
        private IMedianProvider<decimal> _medianPriceProvider;
        private bool _repetitiveCancelRequiredDuringSweepDefence;
        private static readonly TimeSpan _MIN_WAIT_TIME_AFTER_SWEEP_DEFENSE = TimeSpan.FromMilliseconds(300);
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _bankpoolAskPricebook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _bankpoolBidPricebook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _lmaxAskPricebook;
        protected IChangeablePriceBook<PriceObjectInternal, Counterparty> _lmaxBidPricebook;
        private EventsRateCheckerEx _outgoingOrdersRateCheck;
        private SafeTimer _recheckPositionTimer;

        protected StreamingTakerMMSystemBase(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook, Symbol symbol,
            PriceSide observedBookSide,
            Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
            ISymbolsInfo symbolsInfo, IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
            IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
            IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<T> settingsProvider,
            bool repetitiveCancelRequiredDuringSweepDefence, 
            EventsRateCheckerEx outgoingOrdersRateCheck,
            IntegratedTradingSystemType integratedTradingSystemType)
            : base(
                //TODO this might need to be configurable going forward (reference against bankpool vs against LMAX)
                bankpoolAskPricebook, bankpoolBidPricebook, symbol, destinationVenueCounterparty, destinationOrdSession,
                symbolsInfo,
                orderManagement, riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider,
                integratedTradingSystemType,
                observedBookSide == PriceSide.Ask ? "S" : "B", false, observedBookSide)
        {
            this._bankpoolAskPricebook = bankpoolAskPricebook;
            this._bankpoolBidPricebook = bankpoolBidPricebook;
            this._lmaxAskPricebook = lmaxAskPriceBook;
            this._lmaxBidPricebook = lmaxBidPriceBook;
            this._observedBookSide = observedBookSide;
            this._medianPriceProvider = medianPriceProvider;
            this._repetitiveCancelRequiredDuringSweepDefence = repetitiveCancelRequiredDuringSweepDefence;
            this._venueOrderFlowSession = destinationOrdSession as VenueOrderFlowSession;
            this._outgoingOrdersRateCheck = outgoingOrdersRateCheck;
            this._recheckPositionTimer = new SafeTimer(ProcessEventWithRecursionCheck);

            //verify that settings are OK
            this.SettingsUpdating(this._settings);
        }

        protected StreamingTakerMMSystemBase(
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxAskPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> lmaxBidPriceBook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolAskPricebook,
            IChangeablePriceBook<PriceObjectInternal, Counterparty> bankpoolBidPricebook, Symbol symbol,
            PriceSide observedBookSide,
            Counterparty destinationVenueCounterparty, IOrderFlowSession destinationOrdSession,
            ISymbolsInfo symbolsInfo, IMedianProvider<decimal> medianPriceProvider, IOrderManagement orderManagement,
            IRiskManager riskManager, ITradingGroupsCache tradingGroupsCache,
            IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
            IIntegratedSystemSettingsProvider<T> settingsProvider,
            bool repetitiveCancelRequiredDuringSweepDefence,
            EventsRateCheckerEx outgoingOrdersRateCheck)
            : this(
                lmaxAskPriceBook,
            lmaxBidPriceBook,
            bankpoolAskPricebook,
            bankpoolBidPricebook, symbol,
            observedBookSide,
            destinationVenueCounterparty, destinationOrdSession,
            symbolsInfo, medianPriceProvider, orderManagement,
            riskManager, tradingGroupsCache, unicastInfoForwardingHub, logger,
            settingsProvider,
            repetitiveCancelRequiredDuringSweepDefence, outgoingOrdersRateCheck,
            IntegratedTradingSystemType.Stream)
        {}

        protected override void UnsubscribeFromBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopDeteriorated -= ProcessEventTobAdapter;
            priceBook.ChangeableBookTopReplaced -= ProcessEventTobAdapter;
        }

        protected override void SubscribeToBookEvents(IChangeablePriceBook<PriceObjectInternal, Counterparty> priceBook)
        {
            priceBook.ChangeableBookTopImproved += ProcessEventTobAdapter;

            //MM system side quotes only after changes on it's side of book (the other side is for check of negative spread)
            if (this._observedBookSide == PriceSide.Ask ? priceBook == _askReferencePriceBook : priceBook == _bidReferencePriceBook)
            {
                priceBook.ChangeableBookTopDeteriorated += ProcessEventTobAdapter;
                priceBook.ChangeableBookTopReplaced += ProcessEventTobAdapter;
            }
        }

        protected override void AfterDisablingProcedure()
        {
            //Now - we know that no new order can be created
            // cancel the existing ones (even if we potentially double cancel)

            //Cancel the next first, as it can 'disappear' under hands and move to current
            if(IsReadyForNextAggressiveCancel())
                this.RequestOrderCancelDuringSweepDefence(this._cancelReplaceOrdersHolder.OrderNext);
            this.RequestOrderCancel(this._cancelReplaceOrdersHolder.OrderCurrent);
            _llEvaluationInProgress = false;
        }

        protected override void OnResetProcedure()
        {
            if (_cancelReplaceOrdersHolder.OrderNext != null)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system has non-null pending order [{1}]. Resetting it, but situation should be reviewed manually.",
                                 this._systemFriendlyName, _cancelReplaceOrdersHolder.OrderNext);
                this.RequestOrderCancel(this._cancelReplaceOrdersHolder.OrderNext);
                _cancelReplaceOrdersHolder.OrderNext = null;
            }

            if (_cancelReplaceOrdersHolder.OrderCurrent != null)
            {
                this._logger.Log(LogLevel.Fatal,
                                 "{0} system has non-null pending order [{1}]. Resetting it, but situation should be reviewed manually.",
                                 this._systemFriendlyName, _cancelReplaceOrdersHolder.OrderCurrent);
                this.RequestOrderCancel(this._cancelReplaceOrdersHolder.OrderCurrent);
                _cancelReplaceOrdersHolder.OrderCurrent = null;
            }

            _llEvaluationInProgress = false;
        }

        protected override DealDirection CurrentHeadOrderPolarity { get { return this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(); } }


        private bool MatchesSpreadCriteria_ToBCheckOnly(PriceObjectInternal askPrice, PriceObjectInternal bidPrice)
        {
            if (PriceObjectInternal.IsNullPrice(askPrice) || PriceObjectInternal.IsNullPrice(bidPrice))
                return false;

            if (bidPrice.Price - askPrice.Price > _settings.MaximumDiscountDecimalToTrade)
                return false;

            if (!this.IsPriceValid(askPrice, PriceSide.Ask) || !this.IsPriceValid(bidPrice, PriceSide.Bid))
                return false;

            if (_sweepDefenceActiveTillUtc > HighResolutionDateTime.UtcNow)
            {
                return false;
            }

            return true;
        }

        private bool MatchesSpreadCriteria(PriceObjectInternal askPrice, PriceObjectInternal bidPrice)
        {
            if (!this.MatchesSpreadCriteria_ToBCheckOnly(askPrice, bidPrice))
                return false;

            var orderCurrentlocal = _cancelReplaceOrdersHolder.OrderCurrent;
            if (orderCurrentlocal != null)
            {
                decimal lastKgtPrice = orderCurrentlocal.VenueClientOrderRequestInfo.RequestedPrice.Value;

                decimal askDifference = lastKgtPrice - askPrice.Price;
                decimal bidDifference = lastKgtPrice - bidPrice.Price;

                //ToB price crossed last KGT price
                if (
                    this._observedBookSide == PriceSide.Ask &&
                    (-askDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel || -bidDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel)
                    ||
                    this._observedBookSide == PriceSide.Bid &&
                    (askDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel || bidDifference >= _settings.MinimumDecimalDifferenceFromKgtPriceToFastCancel)
                    )
                {
                    _sweepDefenceActiveTillUtc = HighResolutionDateTime.UtcNow + _MIN_WAIT_TIME_AFTER_SWEEP_DEFENSE;
                    return false;
                }
            }

            return true;
        }

        private DateTime _sweepDefenceActiveTillUtc;

        //private volatile VenueClientOrder _orderCurrent;
        //private volatile VenueClientOrder _orderNext;
        private readonly CancelReplaceOrdersHolder _cancelReplaceOrdersHolder = new CancelReplaceOrdersHolder();

        private void ProcessEventTobAdapter(PriceObjectInternal priceObject)
        {
            //We do not need recursion check here as this event is invoked only directly by market data event
            // subsequent recursions (after issuing order or cancel) goes through ProcessEvent()

            if (!this.IsReadyToProcessEvents())
                return;
            if (!IsReadyForNextAggressiveCancel())
                return;

            PriceObjectInternal askToB = priceObject.Side == PriceSide.Ask ? priceObject : _askReferencePriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = priceObject.Side == PriceSide.Ask ? _bidReferencePriceBook.MaxNodeInternal : priceObject;
            PriceObjectInternal priceToRelease = priceObject.Side == PriceSide.Ask ? bidToB : askToB;

            try
            {
                ProcessEventInternal(askToB, bidToB);
            }
            finally
            {
                priceToRelease.Release();
            }
        }

        protected override void SettingsUpdating(T newSettings)
        {
            if (newSettings.ReferenceLmaxBookOnly)
                this.SwapReferenceBooks(_lmaxAskPricebook, _lmaxBidPricebook);
            else
                this.SwapReferenceBooks(_bankpoolAskPricebook, _bankpoolBidPricebook);

            if (!this._venueOrderFlowSession.IsOrderHoldTimeWithinLimits(newSettings.KGTLLTime))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with LL time higher then destination session auto-reject interval - Hard blocking system",
                    this._systemFriendlyName);
                this.HardBlock();
                return;
            }

            if (newSettings.MinimumFillSize > newSettings.MinimumSizeToTrade ||
                newSettings.MinimumSizeToTrade > newSettings.MaximumSizeToTrade)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with sizes (MinFill: {1}, MinSize: {2}, MaxSize: {3}) with incorrect order (should be MinFil<=MinSize<=MaxSize) - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumFillSize, newSettings.MinimumSizeToTrade,
                    newSettings.MaximumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderMinSizeZeroOrWithinSizeAndIncrementLimit(newSettings.MinimumFillSize,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MinimumFillSize: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumFillSize);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderSizeWithinSizeAndIncrementLimit(newSettings.MinimumSizeToTrade,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MinimumSizeToTrade: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MinimumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (
                !this._symbolsInfo.IsOrderSizeWithinSizeAndIncrementLimit(newSettings.MaximumSizeToTrade,
                    this._symbol, this._destinationVenueCounterparty.TradingTargetType))
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MaximumSizeToTrade: {1} that doesn't meet size and increment limits from backend - Hard blocking system",
                    this._systemFriendlyName, newSettings.MaximumSizeToTrade);
                this.HardBlock();
                return;
            }

            if (newSettings.MaximumSourceBookTiers > 8)
            {
                this._logger.Log(LogLevel.Fatal,
                    "Attempt to configure system {0} with MaximumSourceBookTiers: {1} greater than 8 - Hard blocking system",
                    this._systemFriendlyName, newSettings.MaximumSourceBookTiers);
                this.HardBlock();
                return;
            }
        }

        protected override void ProcessEvent()
        {
            if (!this.IsReadyToProcessEvents())
                return;

            //frontload at least some check
            if (!IsReadyForNextAggressiveCancel())
            {
                this._logger.Log(LogLevel.Trace, this._systemFriendlyName + " Ignoring trigger as it is not ready even for aggressive cancel");
            }

            PriceObjectInternal askToB = _askReferencePriceBook.MaxNodeInternal;
            PriceObjectInternal bidToB = _bidReferencePriceBook.MaxNodeInternal;

            try
            {
                ProcessEventInternal(askToB, bidToB);
            }
            finally
            {
                askToB.Release();
                bidToB.Release();
            }
        }

        private void TryCancelPositionOnCheckFailed()
        {
            if (TryEnterSingleAccessRegion())
            {
                OnExitPostionAggresiveRequested();

                ExitSingleAccessRegion();
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName +
                    " not canceling after spread or size or decay criteria missed - not ready for sending (second check)");
            }
            this._logger.Log(LogLevel.Trace,
                this._systemFriendlyName +
                " ignoring price change (or cancelling) - spread or size or sweep criteria missed");
        }

        private bool BookProbingAllowed { get { return this._settings.MaximumSourceBookTiers > 1; } }

        private void ProcessEventInternal(PriceObjectInternal askToB, PriceObjectInternal bidToB)
        {
            PriceObjectInternal observedSidePrice = this._observedBookSide == PriceSide.Ask ? askToB : bidToB;

            if (
                //not enaugh liquidity on a book
                    (observedSidePrice.SizeBaseAbsRemaining < _settings.MinimumSizeToTrade && !this.BookProbingAllowed)
                //spread misssed
                    || !MatchesSpreadCriteria(askToB, bidToB)
                    )
            {
                this.TryCancelPositionOnCheckFailed();
                return;
            }

            //we might got here only because we were ready for aggressive cancel but not for the markup
            if (!IsReadyForNextMarkup() || _llEvaluationInProgress)
                return;

            decimal referencePrice = observedSidePrice.Price;
            decimal sizeBaseAbs = Math.Min(observedSidePrice.SizeBaseAbsRemaining, _settings.MaximumSizeToTrade);
            bool tobSizeInsufficient = sizeBaseAbs != _settings.MaximumSizeToTrade;
            if (tobSizeInsufficient)
            {
                if (BookProbingAllowed)
                {
                    sizeBaseAbs = _settings.MaximumSizeToTrade;
                    referencePrice =
                        (this._observedBookSide == PriceSide.Ask
                            ? this._askReferencePriceBook
                            : this._bidReferencePriceBook)
                            .GetWeightedPriceForSize(ref sizeBaseAbs, this._settings.MaximumSourceBookTiers);
                }

                sizeBaseAbs = SymbolsInfoUtils.RoundToIncrement(RoundingKind.AlwaysToZero, _orderMinimumSizeIncrement, sizeBaseAbs);

                if (sizeBaseAbs < _settings.MinimumSizeToTrade || referencePrice == 0m)
                {
                    this.TryCancelPositionOnCheckFailed();
                    return;
                }

                //This is redundant, but extremely dangerous if not met - therefore double checking
                if (sizeBaseAbs > _settings.MaximumSizeToTrade)
                {
                    this._logger.Log(LogLevel.Fatal,
                        "{0} trading system generated size request of [{1}] - cutting it to max size [{2}]",
                        this._systemFriendlyName, sizeBaseAbs, _settings.MaximumSizeToTrade);
                    sizeBaseAbs = _settings.MaximumSizeToTrade;
                }
            }

            decimal requestedPrice;
            RoundingKind improvingRoundingKind = this._observedBookSide == PriceSide.Ask
                ? RoundingKind.AlwaysAwayFromZero
                : RoundingKind.AlwaysToZero;

            decimal offset = _settings.BestPriceImprovementOffsetDecimal;

            decimal requestedPriceUnrounded = this._observedBookSide == PriceSide.Ask
                ? referencePrice + offset
                : referencePrice - offset;

            if (!this._symbolsInfo.TryRoundToSupportedPriceIncrement(
                this._symbol, _destinationVenueCounterparty.TradingTargetType, improvingRoundingKind,
                requestedPriceUnrounded,
                out requestedPrice))
            {
                this._logger.Log(LogLevel.Error,
                    this._systemFriendlyName + " - Submitting Venue leg failed due to unability to roundprice");
                return;
            }

            this._logger.Log(LogLevel.Trace,
                "Venue leg of {4} will have price {0} - from unrounded value of {1} (Book ref price {2} and offset {3}) And size {5} (ToB size {6}sufficient, book probing {7}allowed)",
                requestedPrice, requestedPriceUnrounded, referencePrice, offset, this._systemFriendlyName, sizeBaseAbs,
                tobSizeInsufficient ? "IN" : string.Empty, BookProbingAllowed ? string.Empty : "NOT ");

            if (TryEnterSingleAccessRegion())
            {
                //
                // BEGIN OF SINGLE ACCESS CONSTRAINED REGION
                //

                VenueClientOrder orderCurrentlocal = this._cancelReplaceOrdersHolder.OrderCurrent;

                bool readyForNextMarkup = IsReadyForNextMarkup();
                bool priceChanged = orderCurrentlocal == null || orderCurrentlocal.VenueClientOrderRequestInfo.RequestedPrice != requestedPrice;
                bool ttlExpired = orderCurrentlocal == null || _settings.MinimumIntegratorPriceLife == TimeSpan.MinValue ||
                                  orderCurrentlocal.CreatedUtc + _settings.MinimumIntegratorPriceLife <
                                  HighResolutionDateTime.UtcNow;


                if (readyForNextMarkup && priceChanged)
                {
                    if (ttlExpired)
                    {
                        VenueClientOrderRequestInfo venueOri = this.CreateVenueOrderRequest(
                                requestedPrice, this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(),
                                this._symbol, sizeBaseAbs);

                        if (this.CanSubmitOrderRequest(venueOri)
                            &&
                            //check the rate as second - so that it is applied for actually outgoing orders
                            (_outgoingOrdersRateCheck == null ||
                            _outgoingOrdersRateCheck.AddNextEventAndCheckIsAllowed())
                            )
                        {
                            //double check the current order, as it could have been null (and next non-null), then we was swapped before next check
                            // next became null and current non-null (success c/replace result)
                            orderCurrentlocal = this._cancelReplaceOrdersHolder.OrderCurrent;
                            if (orderCurrentlocal != null)
                            {
                                if (
                                    //attempt to perform replace on partially filled/rejected order however to size lower than already removed 
                                    (orderCurrentlocal.SizeBaseAbsInitial - orderCurrentlocal.SizeBaseAbsActive) >=
                                    sizeBaseAbs
                                    ||
                                    //attempt to perform replace on partially filled/rejected order to greater size - this would lead to initial cancel
                                    (sizeBaseAbs >= orderCurrentlocal.SizeBaseAbsInitial &&
                                     orderCurrentlocal.SizeBaseAbsInitial != orderCurrentlocal.SizeBaseAbsActive)
                                    )
                                {
                                    this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                                    this._cancelReplaceOrdersHolder.OrderCurrent = null;
                                    this.RequestOrderCancel(orderCurrentlocal);
                                    ExitSingleAccessRegion();
                                    this._logger.Log(LogLevel.Info,
                                        "New size is lower then remaining - so canceling order instead of C/Replace");
                                    return;
                                }

                                venueOri = this.CreateVenueReplaceOrderRequest(orderCurrentlocal.ClientOrderIdentity,
                                    venueOri);
                            }

                            int currentOrderCnt = Interlocked.Increment(ref _headOrdersCnt);

                            this._cancelReplaceOrdersHolder.OrderNext =
                                this.CreateVenueOrder(this._systemFriendlyName + "_HEAD_" + currentOrderCnt,
                                    this._systemIdentity, venueOri,
                                    this._integratedTradingSystemIdentification);

                            var result = this.RegisterOrder(this._cancelReplaceOrdersHolder.OrderNext);
                            if (!result.RequestSucceeded)
                            {
                                this._logger.Log(LogLevel.Error, "Submitting Venue leg of {0} failed: {1}",
                                    this._systemFriendlyName,
                                    result.ErrorMessage);
                                this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                                //Since we potentially couldn't replace, request cancel
                                this.RequestOrderCancel(orderCurrentlocal);
                            }
                        }
                        else
                        {
                            this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                            this._cancelReplaceOrdersHolder.OrderCurrent = null;
                            this.RequestOrderCancel(orderCurrentlocal);
                        }
                    }
                    else
                    {
                        //scheduled only if there was price change that we missed to reflect due to our TTL
                        TimeSpan due = orderCurrentlocal.CreatedUtc + _settings.MinimumIntegratorPriceLife +
                                       TimeSpan.FromMilliseconds(1) - HighResolutionDateTime.UtcNow;
                        this._recheckPositionTimer.Change(TimeSpanEx.Max(due, TimeSpan.FromMilliseconds(1)), Timeout.InfiniteTimeSpan);
                    }
                }
                else if (this._verboseLoggingEnabled)
                {
                    this._logger.Log(LogLevel.Trace,
                        this._systemFriendlyName +
                        " ignoring price change - not flat, or next order pending, or no price change");
                }

                ExitSingleAccessRegion();
                //
                // END OF SINGLE ACCESS CONSTRAINED REGION
                //
            }
            else
            {
                this._logger.Log(LogLevel.Trace,
                    this._systemFriendlyName + " ignoring price change - not ready for sending (second check)");
            }
        }

        protected abstract VenueClientOrderRequestInfo CreateVenueReplaceOrderRequest(
            string replacedOrderId, VenueClientOrderRequestInfo replacingOrderRequest);

        //private void OnExitPostionRequested()
        //{
        //    VenueClientOrder orderCurrentlocal = this._orderCurrent;
        //    VenueClientOrder orderNextlocal = this._orderNext;

        //    if (orderCurrentlocal != null && orderNextlocal == null)
        //    {
        //        //this is needed to mark cancelaton in progress and prevent concurrent attempts to Cancel or Cancel/Replace
        //        //but perform makring first
        //        //order next should not change outside of exclusive access region - no need to interlocked
        //        this._orderNext = orderCurrentlocal;
        //        //However ordercurrent CAN change in parallel; nullify if it's the one expected
        //        Interlocked.CompareExchange(ref this._orderCurrent, null, orderCurrentlocal);

        //        this.RequestOrderCancel(orderCurrentlocal);
        //    }
        //}

        private void OnExitPostionAggresiveRequested()
        {
            VenueClientOrder orderCurrentlocal = this._cancelReplaceOrdersHolder.OrderCurrent;
            VenueClientOrder orderNextlocal = this._cancelReplaceOrdersHolder.OrderNext;

            if (orderNextlocal != null)
            {
                if (IsReadyForNextAggressiveCancel())
                {
                    this.RequestOrderCancelDuringSweepDefence(orderNextlocal);

                    //orderNext already populated - so no need to mark operation is in progress (as it already is)

                    //do not attempt to cancel order current - as cancel is already in progress
                    //this.RequestOrderCancel(orderCurrentlocal);

                }
                else
                {
                    this._logger.Log(LogLevel.Info,
                        "{0} not processing aggressive cancel request as it should have been already sent",
                        this._systemFriendlyName);
                }
            }
            else if (orderCurrentlocal != null)
            {
                //this is needed to mark cancelaton in progress and prevent concurrent attempts to Cancel/Replace
                //but perform makring first
                //order next should not change outside of exclusive access region - no need to interlocked
                this._cancelReplaceOrdersHolder.OrderNext = orderCurrentlocal;
                //However ordercurrent CAN change in parallel; nullify if it's the one expected
                Interlocked.CompareExchange(ref this._cancelReplaceOrdersHolder.OrderCurrent, null, orderCurrentlocal);

                this.RequestOrderCancelDuringSweepDefence(orderCurrentlocal);
            }
        }

        private void RequestOrderCancelDuringSweepDefence(VenueClientOrder orderToCancel)
        {
            if (orderToCancel == null)
                return;

            //marking order as being aggressively cancelled - so that we prevent concurrent attempts
            //  (writing nonvolatile date first to use follwing mem barier)
            this._lastAggressiveCancelRequested = HighResolutionDateTime.UtcNow;
            this._lastAggressiveCancelledOder.SetTarget(orderToCancel);

            this.RequestOrderCancel(orderToCancel);

            if (_repetitiveCancelRequiredDuringSweepDefence)
            {
                //this blocks thread for 4 ms - would be better to temp return it to pool.
                Task.Factory.StartNew(() =>
                {
                    //up to additional 4 async requests (1st one was already sent immediately synchronously)
                    for (int i = 0; i < 4; i++)
                    {
                        Thread.Sleep(TimeSpan.FromMilliseconds(1));
                        //_orderNext would be nullified if one of cancels was processed successfully in meantime
                        if (orderToCancel == this._cancelReplaceOrdersHolder.OrderNext)
                            //We do not have no synchronisation here already, however we are not changing any state variable
                            this.RequestOrderCancel(orderToCancel);
                        else
                            break;
                    }
                });
            }
        }




        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsReadyForNextMarkup()
        {
            return _cancelReplaceOrdersHolder.OrderNext == null && _currentPositionBasePol.InterlockedIsZero();
        }

        private volatile WeakReference<VenueClientOrder> _lastAggressiveCancelledOder = new WeakReference<VenueClientOrder>(null);
        //not volatile, but we don not need up to tick freshness
        private DateTime _lastAggressiveCancelRequested;

        private static readonly TimeSpan _unconfirmedAggressiveCancelTimeout =
            SessionsSettings.OrderFlowSessionBehavior.UnconfirmedOrderTimeout.Add(TimeSpan.FromSeconds(1));

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsReadyForNextAggressiveCancel()
        {
            VenueClientOrder orderNextLocal = this._cancelReplaceOrdersHolder.OrderNext;
            //_orderNext was not yet aggressively cancelled.
            bool nextOrderInAggressiveCancel = IsGivenOrderInAggressiveCancel(orderNextLocal);
            if (nextOrderInAggressiveCancel && (HighResolutionDateTime.UtcNow - _lastAggressiveCancelRequested > _unconfirmedAggressiveCancelTimeout))
            {
                this._logger.Log(LogLevel.Fatal, "{0} requested aggressive cancel of order {1} on {2:dd-HH:mm:ss.fffffff}, but haven't received success or all reject replies until now, reissuing the attempt",
                    this._systemFriendlyName, orderNextLocal, _lastAggressiveCancelRequested);
                return true;
            }

            return !nextOrderInAggressiveCancel;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsGivenOrderInAggressiveCancel(VenueClientOrder order)
        {
            VenueClientOrder lastAggressiveCancelledOrderLocal;
            return
                order != null &&
                this._lastAggressiveCancelledOder.TryGetTarget(out lastAggressiveCancelledOrderLocal) &&
                lastAggressiveCancelledOrderLocal == order;
        }

        protected override void HandleClientOrderUpdateInfo(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            bool shouldReprocessEvents = this.HandleClientOrderUpdateInfoHelper(clientOrderUpdateInfo, this._cancelReplaceOrdersHolder);

            if (shouldReprocessEvents)
            {
                this.ProcessEventWithRecursionCheck();
            }
        }

        private VenueOrderFlowSession _venueOrderFlowSession;
        protected void RejectSoftDeal(AutoRejectableDeal rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason reason)
        {
            _venueOrderFlowSession.TryRejectDeal(rejectableDeal, reason);
        }

        protected bool AcceptSoftDeal(AutoRejectableDeal rejectableDeal)
        {
            return _venueOrderFlowSession.TryAcceptDeal(rejectableDeal);
        }

        private bool _llEvaluationInProgress;

        private void SetLLEvaluationInProgress()
        {
            this._llEvaluationInProgress = true;
            this.TryCancelPositionOnCheckFailed();
        }

        protected void SetLLEvaluationDone()
        {
            this._llEvaluationInProgress = false;
            this.ProcessEvent();
        }

        private static readonly TimeSpan _maxAcceptableOrderClockSkew = TimeSpan.FromSeconds(3);
        public override void OnIntegratorUnconfirmedDealInternal(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
        {
            AutoRejectableDeal rejectableDeal;

            OrderChangeEventArgsEx args = (integratorUnconfirmedDealInternal.OrderChangeEventArgsInternal as
                Kreslik.Integrator.FIXMessaging.OrderChangeEventArgsEx);
            if (args == null)
            {
                this._logger.Log(LogLevel.Fatal, "{0} Receiving unconfirmed deal that cannot be internally transformed to actionable object. Same type of systems should be disabled. {1}",
                    this._systemFriendlyName, integratorUnconfirmedDealInternal);
                this.HardBlock();
                return;
            }


            rejectableDeal = args.AutoRejectableDeal;

            if (rejectableDeal.Symbol != this._symbol)
            {
                this._logger.Log(LogLevel.Fatal,
                    "System {0} received order on unexpected symbol {1}! Order was autorejected. {2}",
                    this._systemFriendlyName, rejectableDeal.Symbol, rejectableDeal);
                this.RejectSoftDeal(rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason.IncomingOrderMismatch);
                return;
            }

            if ((rejectableDeal.CounterpartySentTimeUtc - rejectableDeal.IntegratorReceivedTimeUtc).Duration() >
                _maxAcceptableOrderClockSkew)
            {
                this._logger.Log(LogLevel.Fatal,
                    "System {0} received order with receiving clock skew {1}! Order was autorejected. {2}",
                    this._systemFriendlyName, (rejectableDeal.CounterpartySentTimeUtc - rejectableDeal.IntegratorReceivedTimeUtc).Duration(), rejectableDeal);
                this.RejectSoftDeal(rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason.IncomingOrderMismatch);
                return;
            }

            if (this.IsTradingUnexpectedNow)
            {
                this._logger.Log(LogLevel.Fatal,
                    "System {0} should be disabled from {1:HH:mm:ss.fff}, however it received client order. This is unexpected! Order was autorejected. {2}",
                    this._systemFriendlyName, this._disabledTime, rejectableDeal);
                this.RejectSoftDeal(rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason.SystemFailedToProcess);
                return;
            }

            //no sizes check

            this.SetLLEvaluationInProgress();

            if (!this.HandleUnconfirmedDealAfterAllChecksPassed(rejectableDeal))
            {
                this.RejectSoftDeal(rejectableDeal, AutoRejectableDeal.RejectableTakerDealRejectionReason.SystemFailedToProcess);
                this.SetLLEvaluationDone();
                return;
            }

            //Testing code only! This is to force partial fills LL reject
            //this.CancelOrder(new CancelOrderRequestInfo(integratorUnconfirmedDealInternal.ClientOrderIdentity,
            //    integratorUnconfirmedDealInternal.ClientIdentity, integratorUnconfirmedDealInternal.Counterparty));

            //no reprocessing of events
        }

        protected virtual bool HandleUnconfirmedDealAfterAllChecksPassed(AutoRejectableDeal deal)
        {
            DateTime now = HighResolutionDateTime.UtcNow;
            DateTime systemDecisioningTime = DateTimeEx.Min(now + _settings.KGTLLTime,
                deal.MaxProcessingCutoffTime - Constansts.MinimumSpanBetweenLLandWatchdog);

            if (now >= systemDecisioningTime - HighResolutionDateTime.ResolutionThreshold)
            {
                ThreadPoolEx.Instance.QueueWorkItem(new WorkItem(
                    () => ProcessUnconfirmedDealAfterDelay(deal), TimerTaskFlagUtils.WorkItemPriority.Highest));
            }
            else
            {
                HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(
                    () => ProcessUnconfirmedDealAfterDelay(deal),
                    systemDecisioningTime, TimerTaskFlagUtils.WorkItemPriority.Highest,
                    TimerTaskFlagUtils.TimerPrecision.Highest, false, false);
            }

            return true;
        }

        private void ProcessUnconfirmedDealAfterDelay(AutoRejectableDeal deal)
        {
            decimal referencePriceForSize = 0m;
            if (this._settings.PreferLLDestinationCheckToLmax)
            {
                referencePriceForSize = (this._observedBookSide == PriceSide.Ask
                    ? this._lmaxAskPricebook
                    : this._lmaxBidPricebook).GetWeightedPriceForExactSize(deal.SizeBaseAbsInitial,
                        this._settings.MaximumSourceBookTiers);
            }

            if (referencePriceForSize == 0m)
            {
                referencePriceForSize = (this._observedBookSide == PriceSide.Ask
                    ? this._bankpoolAskPricebook
                    : this._bankpoolBidPricebook).GetWeightedPriceForExactSize(deal.SizeBaseAbsInitial,
                        this._settings.MaximumSourceBookTiers);
            }

            if (referencePriceForSize == 0m)
            {
                this._logger.Log(LogLevel.Error, "{0} Rejecting deal as reference weighted price is not available", this._systemFriendlyName);
                this.RejectSoftDeal(deal, AutoRejectableDeal.RejectableTakerDealRejectionReason.SystemFailedToProcess);
                return;
            }

            decimal decimalDifferenceGross =
                (this._observedBookSide == PriceSide.Ask
                    ? (deal.RequestedPrice - referencePriceForSize)
                    : (referencePriceForSize - deal.RequestedPrice));

            bool accept = decimalDifferenceGross >= this._settings.MinimumDecimalDifferenceGrossToAccept;

            if (accept)
            {
                PriceObjectInternal askToB = _askReferencePriceBook.MaxNodeInternal;
                PriceObjectInternal bidToB = _bidReferencePriceBook.MaxNodeInternal;

                try
                {
                    accept = this.MatchesSpreadCriteria_ToBCheckOnly(askToB, bidToB);
                }
                finally
                {
                    askToB.Release();
                    bidToB.Release();
                }

                if(!accept)
                    this._logger.Log(LogLevel.Info, "{0} Not Accepting {1} since ToB spread didn't pass criteria", this._systemFriendlyName, deal);
            }

            this._logger.Log(LogLevel.Info, "{0} {1}Accepting deal - DecimalDifferenceGross: {2} (from ref weighted price: {3} and deal price: {4}) vs min value in settings: {5}",
                this._systemFriendlyName, accept ? string.Empty : " NOT", decimalDifferenceGross, referencePriceForSize, deal.RequestedPrice, this._settings.MinimumDecimalDifferenceGrossToAccept);

            if (accept)
            {
                //no positions keeping

                if (this.AcceptSoftDeal(deal))
                {
                    //closing procedures
                    this.SendBankOrder(deal.SizeBaseAbsInitial, deal.IntegratorDealDirection.ToOpositeDirection(), deal.Symbol,
                        this._settings.PreferLmaxDuringHedging, deal.IntegratorOrderIdentity + "_BNK_" + Interlocked.Increment(ref this._headOrdersCnt));
                }
                else
                {
                    this._logger.Log(LogLevel.Fatal,
                        "System {0} attempted to accept deal, but underlying layer rejected it. Details are in log file.",
                        this._systemFriendlyName);
                }
            }
            else
            {
                this.RejectSoftDeal(deal, AutoRejectableDeal.RejectableTakerDealRejectionReason.DealMissed);
                var orderCurrentLocal = this._cancelReplaceOrdersHolder.OrderCurrent;
                //prevent unnecessary Cancel or cancel/Replace requests as current will be autocancelled
                if (orderCurrentLocal != null && deal.IntegratorOrderIdentity == orderCurrentLocal.ClientOrderIdentity)
                {
                    Interlocked.CompareExchange(ref this._cancelReplaceOrdersHolder.OrderCurrent, null, orderCurrentLocal);
                }
            }

            this.SetLLEvaluationDone();
        }

        protected override void HandleCounterpartyReject(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            //DK
            if (counterpartyRejectionInfo.RejectionType == RejectionType.DealRejectByCounterpartyForced)
            {
                if (counterpartyRejectionInfo.Symbol != this._symbol)
                {
                    this._logger.Log(LogLevel.Fatal, "System {0} for {1} receiving counterparty rejected deal for: {2}. Symbol is mismatched - NOT PROCESSING FURTHER. Postition needs to be handled manually. {3}",
                        this._systemFriendlyName, this._symbol, counterpartyRejectionInfo.Symbol, counterpartyRejectionInfo);
                    this.HardBlock();
                    return;
                }

                if (
                counterpartyRejectionInfo.RejectedAmountBasePol == 0 ||
                (counterpartyRejectionInfo.RejectedAmountBasePol < 0 && this._observedBookSide != PriceSide.Ask) ||
                (counterpartyRejectionInfo.RejectedAmountBasePol > 0 && this._observedBookSide != PriceSide.Bid))
                {
                    this._logger.Log(LogLevel.Fatal, "System {0} observing and sending {1}s is receiving counterparty rejected deal for POLARIZED: {2} {3}. Polarization is mismatched - NOT PROCESSING FURTHER. Postition needs to be handled manually. {4}",
                        this._systemFriendlyName, this._observedBookSide, counterpartyRejectionInfo.RejectedAmountBasePol, counterpartyRejectionInfo.Symbol, counterpartyRejectionInfo);
                    this.HardBlock();
                    return;
                }

                //send bank order
                this.SendBankOrderWithHeadOrderDirection(Math.Abs(counterpartyRejectionInfo.RejectedAmountBasePol),
                    this._observedBookSide.ToOutgoingDealDirection().ToOpositeDirection(),
                    counterpartyRejectionInfo.Symbol, this._settings.PreferLmaxDuringHedging,
                    this._systemFriendlyName + "_ECNRejectFlatting_" + Interlocked.Increment(ref this._headOrdersCnt));

                this._logger.Log(LogLevel.Error,
                    "{0} send order to flat up ECNForce reject. HardBlocking itself now. {1}",
                    this._systemFriendlyName, counterpartyRejectionInfo);

                //hard block
                this.HardBlock();
            }
        }

        protected override void HandleIntegratorDealInternal(IntegratorDealInternal integratorDealInternal,
            AtomicSize currentPositionBasePol)
        {
            //This is it
            //This MUST BE OVERRIDEN HERE!!! Otherwise the system would try to cover something already covered
        }
    }
}
