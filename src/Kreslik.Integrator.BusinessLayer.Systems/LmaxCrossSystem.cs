﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.LowLatencyUtils;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public class LmaxCrossSystem : VenueCrossSystemBase
    {
        public LmaxCrossSystem(IChangeablePriceBook<PriceObjectInternal, Counterparty> askPriceBook,
                                IChangeablePriceBook<PriceObjectInternal, Counterparty> bidPriceBook,
                                IBookTop<PriceObjectInternal> venueAskPriceBook, IBookTop<PriceObjectInternal> venueBidPriceBook,
                                Symbol symbol, IOrderManagement orderManagement, IRiskManager riskManager,
                                ITradingGroupsCache tradingGroupsCache,
                                IOrderFlowSession destinationOrdSession, ISymbolsInfo symbolsInfo,
                                IUnicastInfoForwardingHub unicastInfoForwardingHub, ILogger logger,
                                IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings> settingsProvider,
                                LmaxCounterparty lmaxCounterparty)
            : base(
                askPriceBook, bidPriceBook,
                new InProcessVenueToBProvider(venueAskPriceBook, venueBidPriceBook),
                symbol, lmaxCounterparty, destinationOrdSession, symbolsInfo, orderManagement, riskManager,
                tradingGroupsCache, unicastInfoForwardingHub, logger, settingsProvider)
        { }

        protected override VenueClientOrderRequestInfo CreateVenueOrderRequest(decimal price, DealDirection dealDirection, Symbol symbol,
                                                                               decimal sizeBaseAbs)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage.CreateLmaxOrder(
                sizeBaseAbs, price, dealDirection, symbol,
                TimeInForce.ImmediateOrKill, (LmaxCounterparty)_destinationVenueCounterparty);
            
        }

        protected override VenueClientOrder CreateVenueOrder(
            string orderIdentity, string clientIdentity, VenueClientOrderRequestInfo venueOrderRequest,
            IntegratedTradingSystemIdentification integratedTradingSystemIdentification)
        {
            return ClientOrdersBuilder_OnlyForIntegratorUsage
                .CreateLmaxClientOrder(orderIdentity, clientIdentity, venueOrderRequest as LmaxClientOrderRequestInfo,
                integratedTradingSystemIdentification);
        }

        private readonly Counterparty[] _counterpartiesWhiteList = new Counterparty[] { Counterparty.CZB, Counterparty.FC1, Counterparty.GLS, Counterparty.JPM };
        protected override Counterparty[] CounterpartiesWhiteList { get { return _counterpartiesWhiteList; } }
    }
}
