﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    public interface IIntegratedSystemSettings
    {
        bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag);
        void UpdateBpConversions(decimal midPriceRate);
        void Initialize(IIntegratedSystemSettingsBag integratedSystemSettingsBag);
        TimeSpan MaximumWaitTimeOnImprovementTick { get; }
        int TradingSystemId { get; }
        int TradingSystemGroupId { get; }
    }

    //public interface IIntegratedSettingsContainer
    //{
    //    bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag);
    //}

    public interface IIntegratedSystemSettingsProvider<out T> where T : IIntegratedSystemSettings
    {
        T CurrentSettings { get; }
        Counterparty TargetVenueCounterparty { get; }
        Symbol TargetSymbol { get; }
        event Action<T> SettingsChanged;
        bool Enabled { get; }
        event Action<bool> EnableChanged;
        bool IsOutgoingPriceSideEnabled(PriceSide? side);
        event Action<bool, bool> SideEnablementChanged;
        bool GoFlatOnly { get; }
        event Action<bool> GoFlatOnlyChanged;
        bool OrdersTransmissionDisabled { get; }
        event Action<bool> OrdersTransmissionDisabledChanged;
        event Action ResetRequested;
        event Action<IIntegratedSystemSettingsProvider<T>> SystemDeleted;
        event Action<DateTime> RegularCheck;
    }

    public interface IVenueMMSystemSettings : IIntegratedSystemSettings
    {
        decimal MaximumSizeToTrade { get; }
        decimal BestPriceImprovementOffsetDecimal { get; }
        decimal MaximumDiscountDecimalToTrade { get; }
        //TimeSpan MaximumWaitTimeOnImprovementTick { get; }
        TimeSpan MinimumIntegratorPriceLife { get; }
        decimal MinimumSizeToTrade { get; }
        decimal MinimumFillSize { get; }
        int MaximumSourceBookTiers { get; }
        decimal MinimumDecimalDifferenceFromKgtPriceToFastCancel { get; }
    }

    public interface IVenueQuotingSystemSettings : IIntegratedSystemSettings
    {
        decimal MaximumPositionBaseAbs { get; }
        decimal FixedSpreadDecimalHalf { get; }
        long StopLossNetInUsd { get; }
        VenueQuotingSystemSettingsBag.QuotingSpreadType SpreadType { get; }
        decimal MinDynamicSpreadDecimalHalf { get; }
        decimal DynamicMarkupDecimal { get; }
        bool SkewEnabled { get; }
        decimal SkewDecimal { get; }
        TimeSpan InactivityTimeout { get; }
        decimal LiquidationSpreadDecimalHalf { get; }
        TimeSpan LmaxTickerLookbackInterval { get; }
        long MinimumLmaxTickerVolumeUsd { get; }
        long MaximumLmaxTickerVolumeUsd { get; }
        long MinimumLmaxTickerAvgDealSizeUsd { get; }
        long MaximumLmaxTickerAvgDealSizeUsd { get; }
        void SetRestrictedMode();
        void RemoveRestrictedMode();
        bool IsPositionExplicitlySetToZero { get; }
    }

    public interface IVenueCrossSystemSettings : IIntegratedSystemSettings
    {
        decimal MinimumSizeToTrade { get; }
        decimal MaximumSizeToTrade { get; }
        decimal MinimumDecimalDiscountToTrade { get; }
        TimeSpan MinimumTimeBetweenSingleCounterpartySignals { get; }
        //TimeSpan MaximumWaitTimeOnImprovementTick { get; }
        decimal MinimumFillSize { get; }
    }

    public interface IVenueSmartCrossSystemSettings : IVenueCrossSystemSettings
    {
        decimal CoverDistanceGrossDecimal { get; }
        decimal MinimumTrueGainGrossDecimal { get; }
        TimeSpan CoverTimeout { get; }
    }

    public interface IVenueStreamMMSystemSettings : IIntegratedSystemSettings
    {
        decimal MaximumSizeToTrade { get; }
        decimal BestPriceImprovementOffsetDecimal { get; }
        decimal MaximumDiscountDecimalToTrade { get; }
        TimeSpan KGTLLTime { get; }
        bool PreferLLDestinationCheckToLmax { get; }
        decimal MinimumDecimalDifferenceGrossToAccept { get; }
        bool PreferLmaxDuringHedging { get; }
        TimeSpan MinimumIntegratorPriceLife { get; }
        decimal MinimumSizeToTrade { get; }
        decimal MinimumFillSize { get; }
        int MaximumSourceBookTiers { get; }
        decimal MinimumDecimalDifferenceFromKgtPriceToFastCancel { get; }
        bool ReferenceLmaxBookOnly { get; }
    }

    public interface IVenueGliderStreamMMSystemSettings : IVenueStreamMMSystemSettings
    {
        decimal MinimumDecimalDifferenceGrossGain { get; }
        decimal MinimumDecimalGrossGainIfZeroGlideFill { get; }
        TimeSpan GlidePriceLife { get; }
        TimeSpan GlidingCutoffSpan { get; }
        Counterparty GlidingCounterparty { get; }
    }

    //public interface IVenueCrossSystemSettingsProvider 
    //    : IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings>
    //{ }


    public class IntegratedSystemSettingsProvidersManager
    {
        private ILogger _logger;
        private IPriceStreamMonitor _priceStreamMonitor;
        private ISymbolTrustworthinessInfoProvider _symbolTrustworthinessInfoProvider;
        private IRiskManager _riskManager;
        private SafeTimer _bpConversionsUpdateTimer;
        //We need to GC root the event publisher, otherwise it would be collected
        private IntegratedSystemsSettingsProvider _dalSettingsProvider;

        private List<IntegratedSystemSettingsProvider<VenueMMSystemSettings>> _innerMMSystemSettings =
            new List<IntegratedSystemSettingsProvider<VenueMMSystemSettings>>();

        private List<IntegratedSystemSettingsProvider<VenueStreamMMSystemSettings>> _innerStreamMMSystemSettings =
            new List<IntegratedSystemSettingsProvider<VenueStreamMMSystemSettings>>();

        private List<IntegratedSystemSettingsProvider<VenueGliderStreamMMSystemSettings>> _innerGliderStreamMMSystemSettings =
            new List<IntegratedSystemSettingsProvider<VenueGliderStreamMMSystemSettings>>();

        private List<IntegratedSystemSettingsProvider<VenueCrossSystemSettings>> _innerCrossSystemSettings =
            new List<IntegratedSystemSettingsProvider<VenueCrossSystemSettings>>();

        private List<IntegratedSystemSettingsProvider<VenueSmartCrossSystemSettings>> _innerSmartCrossSystemSettings =
            new List<IntegratedSystemSettingsProvider<VenueSmartCrossSystemSettings>>();

        private List<IntegratedSystemSettingsProvider<VenueQuotingSystemSettings>> _innerQuotingSystemSettings =
            new List<IntegratedSystemSettingsProvider<VenueQuotingSystemSettings>>();


        public IntegratedSystemSettingsProvidersManager(ILogger logger, IPriceStreamMonitor priceStreamMonitor,
            ISymbolTrustworthinessInfoProvider symbolTrustworthinessInfoProvider, IRiskManager riskManager)
        {
            this._logger = logger;
            this._priceStreamMonitor = priceStreamMonitor;
            this._symbolTrustworthinessInfoProvider = symbolTrustworthinessInfoProvider;
            this._riskManager = riskManager;
        }

        public void StartRaisingEvents()
        {
            _dalSettingsProvider = IntegratedSystemsSettingsProvider.Instance;
            //this.ProcessCrossUpdates(_dalSettingsProvider.GetFullVenueCrossSystemSettingsList());
            this.ProcessSmartCrossUpdates(_dalSettingsProvider.GetFullVenueSmartCrossSystemSettingsList());
            this.ProcessMMUpdates(_dalSettingsProvider.GetFullVenueMMSystemSettingsList());
            this.ProcessStreamMMUpdates(_dalSettingsProvider.GetFullVenueStreamMMSystemSettingsList());
            this.ProcessGliderStreamMMUpdates(_dalSettingsProvider.GetFullVenueGliderStreamMMSystemSettingsList());
            this.ProcessQuotingUpdates(_dalSettingsProvider.GetFullVenueQuotingSystemSettingsList());
            //_dalSettingsProvider.VenueCrossSystemSettingsUpdated += this.ProcessCrossUpdates;
            _dalSettingsProvider.VenueSmartCrossSystemSettingsUpdated += this.ProcessSmartCrossUpdates;
            _dalSettingsProvider.VenueMMSystemSettingsUpdated += this.ProcessMMUpdates;
            _dalSettingsProvider.VenueStreamMMSystemSettingsUpdated += this.ProcessStreamMMUpdates;
            _dalSettingsProvider.VenueGliderStreamMMSystemSettingsUpdated += this.ProcessGliderStreamMMUpdates;
            _dalSettingsProvider.VenueQuotingSystemSettingsUpdated += this.ProcessQuotingUpdates;
            _dalSettingsProvider.EnableRaisingEvents();
            _bpConversionsUpdateTimer = new SafeTimer(PerformPeriodicUpdateOfConversionRates, TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(100))
                {
                    RequiredTimerPrecision = TimerTaskFlagUtils.GetTimerPrecisionForDelay(TimeSpan.FromSeconds(100)),
                    TaskExecutionPriority = TimerTaskFlagUtils.WorkItemPriority.Lowest
                };
        }

        public event Action<IIntegratedSystemSettingsProvider<IVenueCrossSystemSettings>> NewIntegratedCrossSystemSettingsAdded;
        public event Action<IIntegratedSystemSettingsProvider<IVenueSmartCrossSystemSettings>> NewIntegratedSmartCrossSystemSettingsAdded;
        public event Action<IIntegratedSystemSettingsProvider<IVenueMMSystemSettings>> NewIntegratedMMSystemSettingsAdded;
        public event Action<IIntegratedSystemSettingsProvider<IVenueStreamMMSystemSettings>> NewIntegratedStreamMMSystemSettingsAdded;
        public event Action<IIntegratedSystemSettingsProvider<IVenueGliderStreamMMSystemSettings>> NewIntegratedGliderStreamMMSystemSettingsAdded;
        public event Action<IIntegratedSystemSettingsProvider<IVenueQuotingSystemSettings>> NewIntegratedQuotingSystemSettingsAdded;

        private void ProcessCrossUpdates(List<VenueCrossSystemSettingsBag> updatedSettingsBagList)
        {
            this.ProcessUpdates<VenueCrossSystemSettingsBag, VenueCrossSystemSettings>(updatedSettingsBagList, _innerCrossSystemSettings, NewIntegratedCrossSystemSettingsAdded);
        }

        private void ProcessSmartCrossUpdates(List<VenueSmartCrossSystemSettingsBag> updatedSettingsBagList)
        {
            this.ProcessUpdates<VenueSmartCrossSystemSettingsBag, VenueSmartCrossSystemSettings>(updatedSettingsBagList, _innerSmartCrossSystemSettings, NewIntegratedSmartCrossSystemSettingsAdded);
        }

        private void ProcessMMUpdates(List<VenueMMSystemSettingsBag> updatedSettingsBagList)
        {
            this.ProcessUpdates<VenueMMSystemSettingsBag, VenueMMSystemSettings>(updatedSettingsBagList, _innerMMSystemSettings, NewIntegratedMMSystemSettingsAdded);
        }

        private void ProcessStreamMMUpdates(List<VenueStreamMMSystemSettingsBag> updatedSettingsBagList)
        {
            this.ProcessUpdates<VenueStreamMMSystemSettingsBag, VenueStreamMMSystemSettings>(updatedSettingsBagList, _innerStreamMMSystemSettings, NewIntegratedStreamMMSystemSettingsAdded);
        }

        private void ProcessGliderStreamMMUpdates(List<VenueGliderStreamMMSystemSettingsBag> updatedSettingsBagList)
        {
            this.ProcessUpdates<VenueGliderStreamMMSystemSettingsBag, VenueGliderStreamMMSystemSettings>(updatedSettingsBagList, _innerGliderStreamMMSystemSettings, NewIntegratedGliderStreamMMSystemSettingsAdded);
        }

        private void ProcessQuotingUpdates(List<VenueQuotingSystemSettingsBag> updatedSettingsBagList)
        {
            this.ProcessUpdates<VenueQuotingSystemSettingsBag, VenueQuotingSystemSettings>(updatedSettingsBagList, _innerQuotingSystemSettings, NewIntegratedQuotingSystemSettingsAdded);
        }

        private decimal GetMidPriceRate(Symbol symbol)
        {
            decimal midPrice;
            this.TryGetMidPriceRate(symbol, out midPrice);
            return midPrice;
        }

        private bool TryGetMidPriceRate(Symbol symbol, out decimal midPrice)
        {
            decimal askMedian = this._priceStreamMonitor.GetMedianPriceProvider(symbol, PriceSide.Ask).Median;
            decimal bidMedian = this._priceStreamMonitor.GetMedianPriceProvider(symbol, PriceSide.Bid).Median;
            midPrice = (askMedian + bidMedian)/2m;

            if (midPrice == 0m &&
                this._symbolTrustworthinessInfoProvider.SymbolTrustworthinessInfo[(int) symbol].IsTrusted)
            {
                this._logger.Log(LogLevel.Fatal, "Zero midprice when updating Bp recalcualtions; Symbol: {0}, askMedian: {1}, bidMedian: {2}",
                    symbol, askMedian, bidMedian);
            }

            return midPrice != 0m;
        }

        private void PerformPeriodicUpdateOfConversionRates()
        {
            DateTime now = DateTime.Now;
            decimal midPrice;

            foreach (var integratedSystemSettingsProvider in _innerCrossSystemSettings)
            {
                if (this.TryGetMidPriceRate(integratedSystemSettingsProvider.TargetSymbol, out midPrice))
                    integratedSystemSettingsProvider.UpdateBpConversions(midPrice);
                integratedSystemSettingsProvider.PerformRegularCheck(now);
            }

            foreach (var integratedSystemSettingsProvider in _innerSmartCrossSystemSettings)
            {
                if (this.TryGetMidPriceRate(integratedSystemSettingsProvider.TargetSymbol, out midPrice))
                    integratedSystemSettingsProvider.UpdateBpConversions(midPrice);
                integratedSystemSettingsProvider.PerformRegularCheck(now);
            }

            foreach (var integratedSystemSettingsProvider in _innerMMSystemSettings)
            {
                if (this.TryGetMidPriceRate(integratedSystemSettingsProvider.TargetSymbol, out midPrice))
                    integratedSystemSettingsProvider.UpdateBpConversions(midPrice);
                integratedSystemSettingsProvider.PerformRegularCheck(now);
            }

            foreach (var integratedSystemSettingsProvider in _innerStreamMMSystemSettings)
            {
                if (this.TryGetMidPriceRate(integratedSystemSettingsProvider.TargetSymbol, out midPrice))
                    integratedSystemSettingsProvider.UpdateBpConversions(midPrice);
                integratedSystemSettingsProvider.PerformRegularCheck(now);
            }

            foreach (var integratedSystemSettingsProvider in _innerGliderStreamMMSystemSettings)
            {
                if (this.TryGetMidPriceRate(integratedSystemSettingsProvider.TargetSymbol, out midPrice))
                    integratedSystemSettingsProvider.UpdateBpConversions(midPrice);
                integratedSystemSettingsProvider.PerformRegularCheck(now);
            }

            foreach (var integratedSystemSettingsProvider in _innerQuotingSystemSettings)
            {
                if (this.TryGetMidPriceRate(integratedSystemSettingsProvider.TargetSymbol, out midPrice))
                    integratedSystemSettingsProvider.UpdateBpConversions(midPrice);
                integratedSystemSettingsProvider.PerformRegularCheck(now);
            }
        }

        private void ProcessUpdates<TDbSettings, TInnerSettings>(List<TDbSettings> updatedSettingsBagList,
                                                                 List<IntegratedSystemSettingsProvider<TInnerSettings>>
                                                                     innerSettingsList,
                                                                 Action
                                                                     <IntegratedSystemSettingsProvider<TInnerSettings>>
                                                                     newSettingAddedHandler)
            where TDbSettings : IIntegratedSystemSettingsBag
            where TInnerSettings : IIntegratedSystemSettings, new()
        {
            if (updatedSettingsBagList == null)
                return;

            List<IntegratedSystemSettingsProvider<TInnerSettings>> deletedInternalSettings;

            //first process deletes;
            lock (innerSettingsList)
            {
                deletedInternalSettings = innerSettingsList.Where(
                    innerSetting =>
                    updatedSettingsBagList.All(
                        dbSettings => dbSettings.TradingSystemId != innerSetting.CurrentSettings.TradingSystemId))
                                                           .ToList();

                if(innerSettingsList.RemoveAll(x => deletedInternalSettings.Contains(x)) > 0)
                    this._logger.Log(LogLevel.Trace, "Removed IntegratedSystem settings with ids: {0}",
                                     string.Join(", ",
                                                 deletedInternalSettings.Select(s => s.CurrentSettings.TradingSystemId)));
            }

            foreach (
                IntegratedSystemSettingsProvider<TInnerSettings> integratedSystemSettingsProvider in
                    deletedInternalSettings)
            {
                integratedSystemSettingsProvider.AnnounceSettingsDeleted();
            }

            //then adds
            List<TDbSettings> addedDbSettings;
            List<IntegratedSystemSettingsProvider<TInnerSettings>> addedInternalSettings;

            lock (innerSettingsList)
            {
                addedDbSettings =
                    updatedSettingsBagList.Where(
                        dbSettings =>
                        innerSettingsList.All(
                            innerSettings => innerSettings.CurrentSettings.TradingSystemId != dbSettings.TradingSystemId))
                                           .ToList();

                addedInternalSettings =
                    addedDbSettings.Select(
                        dbSetting => new IntegratedSystemSettingsProvider<TInnerSettings>(dbSetting, this.GetMidPriceRate(dbSetting.Symbol), this._riskManager))
                        .ToList();

                innerSettingsList.AddRange(addedInternalSettings);

                if (addedDbSettings.Count > 0)
                    this._logger.Log(LogLevel.Trace, "Added IntegratedSystem settings bags: {0}",
                                     string.Join(Environment.NewLine, addedDbSettings));
            }

            foreach (
                IntegratedSystemSettingsProvider<TInnerSettings> integratedSystemSettingsProvider in
                    addedInternalSettings)
            {
                if (newSettingAddedHandler != null)
                    newSettingAddedHandler(integratedSystemSettingsProvider);
            }


            //then updates
            foreach (
                TDbSettings venueSystemSettingsBag in
                    updatedSettingsBagList.Where(
                        dbSetting => !addedDbSettings.Contains(dbSetting) && dbSetting.IsUpdated))
            {
                this._logger.Log(LogLevel.Trace, "Updated VenueCrossSystem settings bag: {0}", venueSystemSettingsBag);

                IntegratedSystemSettingsProvider<TInnerSettings> innerSettingToUpdate;

                lock (innerSettingsList)
                {
                    innerSettingToUpdate =
                        innerSettingsList.FirstOrDefault(
                            set => set.CurrentSettings.TradingSystemId == venueSystemSettingsBag.TradingSystemId);
                }

                if (innerSettingToUpdate != null)
                    innerSettingToUpdate.UpdateInternalState(venueSystemSettingsBag, this.GetMidPriceRate(venueSystemSettingsBag.Symbol), this._riskManager);
            }
        }


        ///////////////////////////////////////////


        private abstract class IntegratedSystemSettingsBase : IIntegratedSystemSettings
        {
            public abstract bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag);
            public abstract void UpdateBpConversions(decimal midPriceRate);

            public void Initialize(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                this.TradingSystemId = integratedSystemSettingsBag.TradingSystemId;
                this.TradingSystemGroupId = integratedSystemSettingsBag.TradingSystemGroupId;
                this.UpdateInternalState(integratedSystemSettingsBag);
            }

            public TimeSpan MaximumWaitTimeOnImprovementTick { get; protected set; }
            public int TradingSystemId { get; protected set; }

            public int TradingSystemGroupId { get; private set; }

            protected decimal BpToDecimal(decimal bpValue, decimal medianPrice)
            {
                return (bpValue * medianPrice / 10000m).Normalize();
            }
        }

        private class VenueStreamMMSystemSettings : IntegratedSystemSettingsBase, IVenueStreamMMSystemSettings
        {
            public new TimeSpan MaximumWaitTimeOnImprovementTick { get { return TimeSpan.Zero; } }
            public decimal MaximumSizeToTrade { get; private set; }

            private decimal BestPriceImprovementOffsetBasisPoints { get; set; }
            public decimal BestPriceImprovementOffsetDecimal { get; private set; }

            private decimal MaximumDiscountBasisPointsToTrade { get; set; }
            public decimal MaximumDiscountDecimalToTrade { get; private set; }

            public TimeSpan KGTLLTime { get; private set; }

            public bool PreferLLDestinationCheckToLmax { get; private set; }

            private decimal MinimumBPGrossToAccept { get; set; }
            public decimal MinimumDecimalDifferenceGrossToAccept { get; private set; }

            public bool PreferLmaxDuringHedging { get; private set; }

            public TimeSpan MinimumIntegratorPriceLife { get; private set; }

            public decimal MinimumSizeToTrade { get; private set; }

            public decimal MinimumFillSize { get; private set; }
            public int MaximumSourceBookTiers { get; private set; }
            private decimal MinimumBasisPointsFromKgtPriceToFastCancel { get; set; }
            public decimal MinimumDecimalDifferenceFromKgtPriceToFastCancel { get; private set; }

            public bool ReferenceLmaxBookOnly { get; private set; }

            public override bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                if (integratedSystemSettingsBag.TradingSystemId != this.TradingSystemId)
                    throw new Exception(string.Format("Trading ids mismatch: expected {0} actual {1}",
                        this.TradingSystemId, integratedSystemSettingsBag.TradingSystemId));

                VenueStreamMMSystemSettingsBag venueStreamMMSystemSettingsBag =
                    integratedSystemSettingsBag as VenueStreamMMSystemSettingsBag;

                bool settingsChanged = false;

                if (this.MaximumSizeToTrade != venueStreamMMSystemSettingsBag.MaximumSizeToTrade)
                {
                    this.MaximumSizeToTrade = venueStreamMMSystemSettingsBag.MaximumSizeToTrade;
                    settingsChanged = true;
                }

                if (this.BestPriceImprovementOffsetBasisPoints != venueStreamMMSystemSettingsBag.BestPriceImprovementOffsetBasisPoints)
                {
                    this.BestPriceImprovementOffsetBasisPoints = venueStreamMMSystemSettingsBag.BestPriceImprovementOffsetBasisPoints;
                    settingsChanged = true;
                }

                if (this.MaximumDiscountBasisPointsToTrade !=
                    venueStreamMMSystemSettingsBag.MaximumDiscountBasisPointsToTrade)
                {
                    this.MaximumDiscountBasisPointsToTrade =
                        venueStreamMMSystemSettingsBag.MaximumDiscountBasisPointsToTrade;
                    settingsChanged = true;
                }

                if (this.KGTLLTime != venueStreamMMSystemSettingsBag.KGTLLTime)
                {
                    this.KGTLLTime = venueStreamMMSystemSettingsBag.KGTLLTime;
                    settingsChanged = true;
                }

                if (this.PreferLLDestinationCheckToLmax != venueStreamMMSystemSettingsBag.PreferLLDestinationCheckToLmax)
                {
                    this.PreferLLDestinationCheckToLmax = venueStreamMMSystemSettingsBag.PreferLLDestinationCheckToLmax;
                    settingsChanged = true;
                }

                if (this.MinimumBPGrossToAccept != venueStreamMMSystemSettingsBag.MinimumBPGrossToAccept)
                {
                    this.MinimumBPGrossToAccept = venueStreamMMSystemSettingsBag.MinimumBPGrossToAccept;
                    settingsChanged = true;
                }

                if (this.PreferLmaxDuringHedging != venueStreamMMSystemSettingsBag.PreferLmaxDuringHedging)
                {
                    this.PreferLmaxDuringHedging = venueStreamMMSystemSettingsBag.PreferLmaxDuringHedging;
                    settingsChanged = true;
                }

                if (this.MinimumIntegratorPriceLife != venueStreamMMSystemSettingsBag.MinimumIntegratorPriceLife)
                {
                    this.MinimumIntegratorPriceLife = venueStreamMMSystemSettingsBag.MinimumIntegratorPriceLife;
                    settingsChanged = true;
                }

                if (this.MinimumSizeToTrade != venueStreamMMSystemSettingsBag.MinimumSizeToTrade)
                {
                    this.MinimumSizeToTrade = venueStreamMMSystemSettingsBag.MinimumSizeToTrade;
                    settingsChanged = true;
                }

                if (this.MinimumFillSize != venueStreamMMSystemSettingsBag.MinimumFillSize)
                {
                    this.MinimumFillSize = venueStreamMMSystemSettingsBag.MinimumFillSize;
                    settingsChanged = true;
                }

                if (this.MaximumSourceBookTiers != venueStreamMMSystemSettingsBag.MaximumSourceBookTiers)
                {
                    this.MaximumSourceBookTiers = venueStreamMMSystemSettingsBag.MaximumSourceBookTiers;
                    settingsChanged = true;
                }

                if (this.MinimumBasisPointsFromKgtPriceToFastCancel != venueStreamMMSystemSettingsBag.MinimumBasisPointsFromKgtPriceToFastCancel)
                {
                    this.MinimumBasisPointsFromKgtPriceToFastCancel = venueStreamMMSystemSettingsBag.MinimumBasisPointsFromKgtPriceToFastCancel;
                    settingsChanged = true;
                }

                if (this.ReferenceLmaxBookOnly != venueStreamMMSystemSettingsBag.ReferenceLmaxBookOnly)
                {
                    this.ReferenceLmaxBookOnly = venueStreamMMSystemSettingsBag.ReferenceLmaxBookOnly;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            public override void UpdateBpConversions(decimal midPriceRate)
            {
                this.BestPriceImprovementOffsetDecimal = this.BpToDecimal(this.BestPriceImprovementOffsetBasisPoints,
                    midPriceRate);
                this.MaximumDiscountDecimalToTrade = this.BpToDecimal(this.MaximumDiscountBasisPointsToTrade,
                    midPriceRate);
                this.MinimumDecimalDifferenceGrossToAccept = this.BpToDecimal(this.MinimumBPGrossToAccept,
                    midPriceRate);
                this.MinimumDecimalDifferenceFromKgtPriceToFastCancel = this.BpToDecimal(this.MinimumBasisPointsFromKgtPriceToFastCancel,
                    midPriceRate);
            }
        }

        private class VenueGliderStreamMMSystemSettings : VenueStreamMMSystemSettings, IVenueGliderStreamMMSystemSettings
        {
            public decimal MinimumDecimalDifferenceGrossGain
            {
                get { return this.MinimumDecimalDifferenceGrossToAccept; }
            }

            public decimal MinimumDecimalGrossGainIfZeroGlideFill { get; private set; }

            private decimal MinimumBPGrossGainIfZeroGlideFill { get; set; }

            public TimeSpan GlidePriceLife { get; private set; }

            public TimeSpan GlidingCutoffSpan { get; private set; }

            public Counterparty GlidingCounterparty { get; private set; }

            public override bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                bool settingsChanged = base.UpdateInternalState(integratedSystemSettingsBag);

                VenueGliderStreamMMSystemSettingsBag venueGliderStreamMMSystemSettingsBag =
                    integratedSystemSettingsBag as VenueGliderStreamMMSystemSettingsBag;

                if (this.GlidePriceLife != venueGliderStreamMMSystemSettingsBag.GlidePriceLife)
                {
                    this.GlidePriceLife = venueGliderStreamMMSystemSettingsBag.GlidePriceLife;
                    settingsChanged = true;
                }

                if (this.GlidingCutoffSpan != venueGliderStreamMMSystemSettingsBag.GlidingCutoffSpan)
                {
                    this.GlidingCutoffSpan = venueGliderStreamMMSystemSettingsBag.GlidingCutoffSpan;
                    settingsChanged = true;
                }

                if (this.MinimumBPGrossGainIfZeroGlideFill != venueGliderStreamMMSystemSettingsBag.MinimumBPGrossGainIfZeroGlideFill)
                {
                    this.MinimumBPGrossGainIfZeroGlideFill = venueGliderStreamMMSystemSettingsBag.MinimumBPGrossGainIfZeroGlideFill;
                    settingsChanged = true;
                }

                if (this.GlidingCounterparty != venueGliderStreamMMSystemSettingsBag.GlidingCounterparty)
                {
                    this.GlidingCounterparty = venueGliderStreamMMSystemSettingsBag.GlidingCounterparty;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            public override void UpdateBpConversions(decimal midPriceRate)
            {
                base.UpdateBpConversions(midPriceRate);

                this.MinimumDecimalGrossGainIfZeroGlideFill = this.BpToDecimal(this.MinimumBPGrossGainIfZeroGlideFill,
                    midPriceRate);
            }
        }

        private class VenueCrossSystemSettings : IntegratedSystemSettingsBase, IVenueCrossSystemSettings
        {
            public decimal MinimumSizeToTrade { get; private set; }
            public decimal MaximumSizeToTrade { get; private set; }
            private decimal MinimumDiscountBasisPointsToTrade { get; set; }
            public decimal MinimumDecimalDiscountToTrade { get; private set; }

            public TimeSpan MinimumTimeBetweenSingleCounterpartySignals { get; private set; }
            public decimal MinimumFillSize { get; private set; }

            public override bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                if (integratedSystemSettingsBag.TradingSystemId != this.TradingSystemId)
                    throw new Exception(string.Format("Trading ids mismatch: expected {0} actual {1}",
                                                      this.TradingSystemId, integratedSystemSettingsBag.TradingSystemId));

                VenueCrossSystemSettingsBag venueCrossSystemSettingsBag =
                    integratedSystemSettingsBag as VenueCrossSystemSettingsBag; 

                bool settingsChanged = false;

                if (this.MinimumSizeToTrade != venueCrossSystemSettingsBag.MinimumSizeToTrade)
                {
                    this.MinimumSizeToTrade = venueCrossSystemSettingsBag.MinimumSizeToTrade;
                    settingsChanged = true;
                }

                if (this.MaximumSizeToTrade != venueCrossSystemSettingsBag.MaximumSizeToTrade)
                {
                    this.MaximumSizeToTrade = venueCrossSystemSettingsBag.MaximumSizeToTrade;
                    settingsChanged = true;
                }

                if (this.MinimumDiscountBasisPointsToTrade != venueCrossSystemSettingsBag.MinimumDiscountBasisPointsToTrade)
                {
                    this.MinimumDiscountBasisPointsToTrade = venueCrossSystemSettingsBag.MinimumDiscountBasisPointsToTrade;
                    settingsChanged = true;
                }

                if (this.MinimumTimeBetweenSingleCounterpartySignals != venueCrossSystemSettingsBag.MinimumTimeBetweenSingleCounterpartySignals)
                {
                    this.MinimumTimeBetweenSingleCounterpartySignals = venueCrossSystemSettingsBag.MinimumTimeBetweenSingleCounterpartySignals;
                    settingsChanged = true;
                }

                if (this.MaximumWaitTimeOnImprovementTick != venueCrossSystemSettingsBag.MaximumWaitTimeOnImprovementTick)
                {
                    this.MaximumWaitTimeOnImprovementTick = venueCrossSystemSettingsBag.MaximumWaitTimeOnImprovementTick;
                    settingsChanged = true;
                }

                if (this.MinimumFillSize != venueCrossSystemSettingsBag.MinimumFillSize)
                {
                    this.MinimumFillSize = venueCrossSystemSettingsBag.MinimumFillSize;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            public override void UpdateBpConversions(decimal midPriceRate)
            {
                this.MinimumDecimalDiscountToTrade = this.BpToDecimal(this.MinimumDiscountBasisPointsToTrade,
                    midPriceRate);
            }
        }

        private class VenueSmartCrossSystemSettings : VenueCrossSystemSettings, IVenueSmartCrossSystemSettings
        {
            public decimal CoverDistanceGrossDecimal { get; private set; }

            public TimeSpan CoverTimeout { get; private set; }

            private decimal CoverDistanceGrossDecimalBp { get; set; }

            public decimal MinimumTrueGainGrossDecimal { get; private set; }
            private decimal MinimumTrueGainGrossBp { get; set; }


            public override bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                bool settingsChanged = base.UpdateInternalState(integratedSystemSettingsBag);

                VenueSmartCrossSystemSettingsBag venueSmartCrossSystemSettingsBag =
                    integratedSystemSettingsBag as VenueSmartCrossSystemSettingsBag;

                if (this.CoverDistanceGrossDecimalBp != venueSmartCrossSystemSettingsBag.CoverDistanceGrossBasisPoints)
                {
                    this.CoverDistanceGrossDecimalBp = venueSmartCrossSystemSettingsBag.CoverDistanceGrossBasisPoints;
                    settingsChanged = true;
                }

                if (this.CoverTimeout != venueSmartCrossSystemSettingsBag.CoverTimeout)
                {
                    this.CoverTimeout = venueSmartCrossSystemSettingsBag.CoverTimeout;
                    settingsChanged = true;
                }

                if (this.MinimumTrueGainGrossBp != venueSmartCrossSystemSettingsBag.MinimumTrueGainGrossBp)
                {
                    this.MinimumTrueGainGrossBp = venueSmartCrossSystemSettingsBag.MinimumTrueGainGrossBp;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            public override void UpdateBpConversions(decimal midPriceRate)
            {
                base.UpdateBpConversions(midPriceRate);

                this.CoverDistanceGrossDecimal = this.BpToDecimal(this.CoverDistanceGrossDecimalBp,
                    midPriceRate);
                this.MinimumTrueGainGrossDecimal = this.BpToDecimal(this.MinimumTrueGainGrossBp,
                    midPriceRate);
            }
        }

        private class VenueMMSystemSettings : IntegratedSystemSettingsBase, IVenueMMSystemSettings
        {
            public decimal MaximumSizeToTrade { get; private set; }
            private decimal BestPriceImprovementOffsetBasisPoints { get; set; }
            public decimal BestPriceImprovementOffsetDecimal { get; private set; }
            private decimal MaximumDiscountBasisPointsToTrade { get; set; }
            public decimal MaximumDiscountDecimalToTrade { get; private set; }
            public TimeSpan MinimumIntegratorPriceLife { get; private set; }
            public decimal MinimumSizeToTrade { get; private set; }
            public decimal MinimumFillSize { get; private set; }
            public int MaximumSourceBookTiers { get; private set; }
            private decimal MinimumBasisPointsFromKgtPriceToFastCancel { get; set; }
            public decimal MinimumDecimalDifferenceFromKgtPriceToFastCancel { get; private set; }

            public override bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                if (integratedSystemSettingsBag.TradingSystemId != this.TradingSystemId)
                    throw new Exception(string.Format("Trading ids mismatch: expected {0} actual {1}",
                                                      this.TradingSystemId, integratedSystemSettingsBag.TradingSystemId));

                VenueMMSystemSettingsBag venueMmSystemSettingsBag =
                    integratedSystemSettingsBag as VenueMMSystemSettingsBag;

                bool settingsChanged = false;

                if (this.MaximumSizeToTrade != venueMmSystemSettingsBag.MaximumSizeToTrade)
                {
                    this.MaximumSizeToTrade = venueMmSystemSettingsBag.MaximumSizeToTrade;
                    settingsChanged = true;
                }

                if (this.BestPriceImprovementOffsetBasisPoints != venueMmSystemSettingsBag.BestPriceImprovementOffsetBasisPoints)
                {
                    this.BestPriceImprovementOffsetBasisPoints = venueMmSystemSettingsBag.BestPriceImprovementOffsetBasisPoints;
                    settingsChanged = true;
                }

                if (this.MaximumDiscountBasisPointsToTrade != venueMmSystemSettingsBag.MaximumDiscountBasisPointsToTrade)
                {
                    this.MaximumDiscountBasisPointsToTrade = venueMmSystemSettingsBag.MaximumDiscountBasisPointsToTrade;
                    settingsChanged = true;
                }

                if (this.MinimumIntegratorPriceLife != venueMmSystemSettingsBag.MinimumIntegratorPriceLife)
                {
                    this.MinimumIntegratorPriceLife = venueMmSystemSettingsBag.MinimumIntegratorPriceLife;
                    settingsChanged = true;
                }

                if (this.MaximumWaitTimeOnImprovementTick != venueMmSystemSettingsBag.MaximumWaitTimeOnImprovementTick)
                {
                    this.MaximumWaitTimeOnImprovementTick = venueMmSystemSettingsBag.MaximumWaitTimeOnImprovementTick;
                    settingsChanged = true;
                }

                if (this.MinimumSizeToTrade != venueMmSystemSettingsBag.MinimumSizeToTrade)
                {
                    this.MinimumSizeToTrade = venueMmSystemSettingsBag.MinimumSizeToTrade;
                    settingsChanged = true;
                }

                if (this.MinimumFillSize != venueMmSystemSettingsBag.MinimumFillSize)
                {
                    this.MinimumFillSize = venueMmSystemSettingsBag.MinimumFillSize;
                    settingsChanged = true;
                }

                if (this.MaximumSourceBookTiers != venueMmSystemSettingsBag.MaximumSourceBookTiers)
                {
                    this.MaximumSourceBookTiers = venueMmSystemSettingsBag.MaximumSourceBookTiers;
                    settingsChanged = true;
                }

                if (this.MinimumBasisPointsFromKgtPriceToFastCancel != venueMmSystemSettingsBag.MinimumBasisPointsFromKgtPriceToFastCancel)
                {
                    this.MinimumBasisPointsFromKgtPriceToFastCancel = venueMmSystemSettingsBag.MinimumBasisPointsFromKgtPriceToFastCancel;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            public override void UpdateBpConversions(decimal midPriceRate)
            {
                this.BestPriceImprovementOffsetDecimal = this.BpToDecimal(this.BestPriceImprovementOffsetBasisPoints,
                    midPriceRate);
                this.MaximumDiscountDecimalToTrade = this.BpToDecimal(this.MaximumDiscountBasisPointsToTrade,
                    midPriceRate);
                this.MinimumDecimalDifferenceFromKgtPriceToFastCancel = this.BpToDecimal(this.MinimumBasisPointsFromKgtPriceToFastCancel,
                    midPriceRate);
            }
        }

        private class VenueQuotingSystemSettings: IntegratedSystemSettingsBase, IVenueQuotingSystemSettings
        {
            public decimal MaximumPositionBaseAbs { get; private set; }
            private decimal FixedSpreadBasisPoints { get; set; }
            public decimal FixedSpreadDecimalHalf { get; private set; }
            public long StopLossNetInUsd { get; private set; }
            public VenueQuotingSystemSettingsBag.QuotingSpreadType SpreadType { get; private set; }
            private decimal MinDynamicSpreadBp { get; set; }
            public decimal MinDynamicSpreadDecimalHalf { get; private set; }
            public decimal DynamicMarkupDecimal { get; private set; }
            public bool SkewEnabled { get; private set; }
            public decimal SkewDecimal { get; private set; }
            public TimeSpan InactivityTimeout { get; private set; }
            private decimal LiquidationSpreadBasisPoints { get; set; }
            public decimal LiquidationSpreadDecimalHalf { get; private set; }
            public TimeSpan LmaxTickerLookbackInterval { get; private set; }
            public long MinimumLmaxTickerVolumeUsd { get; private set; }
            public long MaximumLmaxTickerVolumeUsd { get; private set; }
            public long MinimumLmaxTickerAvgDealSizeUsd { get; private set; }
            public long MaximumLmaxTickerAvgDealSizeUsd { get; private set; }

            public VenueQuotingSystemSettings()
            {
                this.MaximumWaitTimeOnImprovementTick = TimeSpan.Zero;
            }

            private VenueQuotingSystemSettings _restrictedModeBackupSetting;
            private bool _isInRestrictedMode = false;

            private void CopyContentFromBackup(VenueQuotingSystemSettings backupSettings)
            {
                this.TradingSystemId = backupSettings.TradingSystemId;
                this.MaximumPositionBaseAbs = backupSettings.MaximumPositionBaseAbs;
                this.FixedSpreadBasisPoints = backupSettings.FixedSpreadBasisPoints;
                this.FixedSpreadDecimalHalf = backupSettings.FixedSpreadDecimalHalf;
                this.StopLossNetInUsd = backupSettings.StopLossNetInUsd;
                this.SpreadType = backupSettings.SpreadType;
                this.MinDynamicSpreadBp = backupSettings.MinDynamicSpreadBp;
                this.MinDynamicSpreadDecimalHalf = backupSettings.MinDynamicSpreadDecimalHalf;
                this.DynamicMarkupDecimal = backupSettings.DynamicMarkupDecimal;
                this.SkewEnabled = backupSettings.SkewEnabled;
                this.SkewDecimal = backupSettings.SkewDecimal;
                this.InactivityTimeout = backupSettings.InactivityTimeout;
                this.LiquidationSpreadBasisPoints = backupSettings.LiquidationSpreadBasisPoints;
                this.LiquidationSpreadDecimalHalf = backupSettings.LiquidationSpreadDecimalHalf;
                this.LmaxTickerLookbackInterval = backupSettings.LmaxTickerLookbackInterval;
                this.MinimumLmaxTickerVolumeUsd = backupSettings.MinimumLmaxTickerVolumeUsd;
                this.MaximumLmaxTickerVolumeUsd = backupSettings.MaximumLmaxTickerVolumeUsd;
                this.MinimumLmaxTickerAvgDealSizeUsd = backupSettings.MinimumLmaxTickerAvgDealSizeUsd;
                this.MaximumLmaxTickerAvgDealSizeUsd = backupSettings.MaximumLmaxTickerAvgDealSizeUsd;
            }

            public bool IsPositionExplicitlySetToZero
            {
                get
                {
                    return _isInRestrictedMode
                        ? _restrictedModeBackupSetting.MaximumPositionBaseAbs == 0
                        : this.MaximumPositionBaseAbs == 0;
                }
            }

            public void SetRestrictedMode()
            {
                //prevent repeated attempts - as otherwise we'd overwrite our backup settings
                if(_isInRestrictedMode)
                    return;

                if (_restrictedModeBackupSetting == null)
                    _restrictedModeBackupSetting = new VenueQuotingSystemSettings();
                this._restrictedModeBackupSetting.CopyContentFromBackup(this);
                this._isInRestrictedMode = true;

                this.MaximumPositionBaseAbs = 0m;
                this.FixedSpreadDecimalHalf = this.LiquidationSpreadDecimalHalf;
                this.SpreadType = VenueQuotingSystemSettingsBag.QuotingSpreadType.Fixed;
                this.SkewEnabled = false;
            }

            public void RemoveRestrictedMode()
            {
                //ignore false attempts - not to overwritte current settings
                if(!_isInRestrictedMode)
                    return;

                this.CopyContentFromBackup(_restrictedModeBackupSetting);
                this._isInRestrictedMode = false;
            }

            public override bool UpdateInternalState(IIntegratedSystemSettingsBag integratedSystemSettingsBag)
            {
                if (integratedSystemSettingsBag.TradingSystemId != this.TradingSystemId)
                    throw new Exception(string.Format("Trading ids mismatch: expected {0} actual {1}",
                                                      this.TradingSystemId, integratedSystemSettingsBag.TradingSystemId));

                VenueQuotingSystemSettingsBag venueQuotingSystemSettingsBag =
                    integratedSystemSettingsBag as VenueQuotingSystemSettingsBag;

                bool settingsChanged = false;

                settingsChanged = UpdateInternalStateDuringAnyStatus(venueQuotingSystemSettingsBag);

                if (_isInRestrictedMode)
                {
                    //those changes are allowed
                    this._restrictedModeBackupSetting.UpdateInternalStateDuringAnyStatus(venueQuotingSystemSettingsBag);
                    //those are not allowed during restriction
                    if (this._restrictedModeBackupSetting.UpdateInternalStateDuringUnrestrictedStatus(venueQuotingSystemSettingsBag))
                    {
                        LogFactory.Instance.GetLogger(null)
                            .Log(LogLevel.Warn,
                                "Attempt to alter settings for trading system {0} while it is in restricted closing only mode (due to market conditions). Only restriction conditions will be updated, other settings changes will be reflected only after restriction mode is elapsed",
                                this.TradingSystemId);
                    }

                    return settingsChanged;
                }

                settingsChanged &= this.UpdateInternalStateDuringUnrestrictedStatus(venueQuotingSystemSettingsBag);

                return settingsChanged;
            }

            private bool UpdateInternalStateDuringAnyStatus(VenueQuotingSystemSettingsBag venueQuotingSystemSettingsBag)
            {
                bool settingsChanged = false;


                if (this.InactivityTimeout != venueQuotingSystemSettingsBag.InactivityTimeout)
                {
                    this.InactivityTimeout = venueQuotingSystemSettingsBag.InactivityTimeout;
                    settingsChanged = true;
                }

                if (this.LiquidationSpreadBasisPoints != venueQuotingSystemSettingsBag.LiquidationSpreadBasisPoints)
                {
                    this.LiquidationSpreadBasisPoints = venueQuotingSystemSettingsBag.LiquidationSpreadBasisPoints;
                    settingsChanged = true;
                }

                if (this.LmaxTickerLookbackInterval != venueQuotingSystemSettingsBag.LmaxTickerLookbackInterval)
                {
                    this.LmaxTickerLookbackInterval = venueQuotingSystemSettingsBag.LmaxTickerLookbackInterval;
                    settingsChanged = true;
                }

                if (this.MinimumLmaxTickerVolumeUsd != venueQuotingSystemSettingsBag.MinimumLmaxTickerVolumeUsd)
                {
                    this.MinimumLmaxTickerVolumeUsd = venueQuotingSystemSettingsBag.MinimumLmaxTickerVolumeUsd;
                    settingsChanged = true;
                }

                if (this.MaximumLmaxTickerVolumeUsd != venueQuotingSystemSettingsBag.MaximumLmaxTickerVolumeUsd)
                {
                    this.MaximumLmaxTickerVolumeUsd = venueQuotingSystemSettingsBag.MaximumLmaxTickerVolumeUsd;
                    settingsChanged = true;
                }

                if (this.MinimumLmaxTickerAvgDealSizeUsd != venueQuotingSystemSettingsBag.MinimumLmaxTickerAvgDealSizeUsd)
                {
                    this.MinimumLmaxTickerAvgDealSizeUsd = venueQuotingSystemSettingsBag.MinimumLmaxTickerAvgDealSizeUsd;
                    settingsChanged = true;
                }

                if (this.MaximumLmaxTickerAvgDealSizeUsd != venueQuotingSystemSettingsBag.MaximumLmaxTickerAvgDealSizeUsd)
                {
                    this.MaximumLmaxTickerAvgDealSizeUsd = venueQuotingSystemSettingsBag.MaximumLmaxTickerAvgDealSizeUsd;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            private bool UpdateInternalStateDuringUnrestrictedStatus(VenueQuotingSystemSettingsBag venueQuotingSystemSettingsBag)
            {
                bool settingsChanged = false;

                if (this.MaximumPositionBaseAbs != venueQuotingSystemSettingsBag.MaximumPositionBaseAbs)
                {
                    this.MaximumPositionBaseAbs = venueQuotingSystemSettingsBag.MaximumPositionBaseAbs;
                    settingsChanged = true;
                }

                if (this.FixedSpreadBasisPoints != venueQuotingSystemSettingsBag.FixedSpreadBasisPoints)
                {
                    this.FixedSpreadBasisPoints = venueQuotingSystemSettingsBag.FixedSpreadBasisPoints;
                    settingsChanged = true;
                }

                if (this.StopLossNetInUsd != venueQuotingSystemSettingsBag.StopLossNetInUsd)
                {
                    this.StopLossNetInUsd = venueQuotingSystemSettingsBag.StopLossNetInUsd;
                    settingsChanged = true;
                }

                if (this.SpreadType != venueQuotingSystemSettingsBag.SpreadType)
                {
                    this.SpreadType = venueQuotingSystemSettingsBag.SpreadType;
                    settingsChanged = true;
                }

                if (this.MinDynamicSpreadBp != venueQuotingSystemSettingsBag.MinDynamicSpreadBp)
                {
                    this.MinDynamicSpreadBp = venueQuotingSystemSettingsBag.MinDynamicSpreadBp;
                    settingsChanged = true;
                }

                if (this.DynamicMarkupDecimal != venueQuotingSystemSettingsBag.DynamicMarkupDecimal)
                {
                    this.DynamicMarkupDecimal = venueQuotingSystemSettingsBag.DynamicMarkupDecimal;
                    settingsChanged = true;
                }

                if (this.SkewEnabled != venueQuotingSystemSettingsBag.SkewEnabled)
                {
                    this.SkewEnabled = venueQuotingSystemSettingsBag.SkewEnabled;
                    settingsChanged = true;
                }

                if (this.SkewDecimal != venueQuotingSystemSettingsBag.SkewDecimal)
                {
                    this.SkewDecimal = venueQuotingSystemSettingsBag.SkewDecimal;
                    settingsChanged = true;
                }

                return settingsChanged;
            }

            public override void UpdateBpConversions(decimal midPriceRate)
            {
                if (_isInRestrictedMode)
                {
                    //In restriction mode keep updating the liquidation spread (store it into fixed spread)
                    this.FixedSpreadDecimalHalf = this.BpToDecimal(this.LiquidationSpreadBasisPoints / 2m, midPriceRate);
                }
                else
                {
                    this.FixedSpreadDecimalHalf = this.BpToDecimal(this.FixedSpreadBasisPoints / 2m, midPriceRate);
                }

                decimal newMinDynamicSpreadDecimalHalf = this.BpToDecimal(this.MinDynamicSpreadBp / 2m, midPriceRate);
                if (newMinDynamicSpreadDecimalHalf > 0m)
                    this.MinDynamicSpreadDecimalHalf = newMinDynamicSpreadDecimalHalf;
                else if(this.SpreadType == VenueQuotingSystemSettingsBag.QuotingSpreadType.Dynamic)
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "{0} attempt to set MinDynamicSpreadDecimalHalf to zero - calculated from BP value: {1} and midPriceRate: {2}",
                        this.TradingSystemId, this.MinDynamicSpreadBp, midPriceRate);
                this.LiquidationSpreadDecimalHalf = this.BpToDecimal(this.LiquidationSpreadBasisPoints/2m, midPriceRate);
            }
        }


        private class IntegratedSystemSettingsProvider<T> : IIntegratedSystemSettingsProvider<T> where T : IIntegratedSystemSettings, new()
        {
            private T _currentSettings;
            private bool _bpConversionsUpdated = false;

            public void UpdateBpConversions(decimal midPriceRate)
            {
                this._currentSettings.UpdateBpConversions(midPriceRate);
                if (midPriceRate > 0m)
                    _bpConversionsUpdated = true;
            }

            public IntegratedSystemSettingsProvider(IIntegratedSystemSettingsBag venueSystemSettingsBag, decimal midPriceRate, IRiskManager riskManager)
            {
                this._currentSettings = new T();
                this._currentSettings.Initialize(venueSystemSettingsBag);
                if (midPriceRate != 0m)
                    this.UpdateBpConversions(midPriceRate);
                this.TargetVenueCounterparty = venueSystemSettingsBag.Counterparty;
                this.TargetSymbol = venueSystemSettingsBag.Symbol;

                this.PopulateSwitchesWithEvents(venueSystemSettingsBag, riskManager);
            }

            public void UpdateInternalState(IIntegratedSystemSettingsBag venueSystemSettingsBag, decimal midPriceRate, IRiskManager riskManager)
            {
                bool settingsChange = this._currentSettings.UpdateInternalState(venueSystemSettingsBag);
                if(midPriceRate != 0m)
                    this.UpdateBpConversions(midPriceRate);

                if (settingsChange && this.SettingsChanged != null)
                    SettingsChanged(this._currentSettings);

                this.PopulateSwitchesWithEvents(venueSystemSettingsBag, riskManager);
            }

            private void PopulateSwitchesWithEvents(IIntegratedSystemSettingsBag venueSystemSettingsBag, IRiskManager riskManager)
            {
                bool wasEnabled = this.Enabled;
                bool wasGoFlatOnly = this.GoFlatOnly;
                bool wasTransmissionDisabled = this.OrdersTransmissionDisabled;
                bool isChangeInSidedEnablement = this._askEnabled != venueSystemSettingsBag.AskEnabled ||
                                                 this._bidEnabled != venueSystemSettingsBag.BidEnabled;

                this.Enabled = venueSystemSettingsBag.Enabled;
                this._askEnabled = venueSystemSettingsBag.AskEnabled;
                this._bidEnabled = venueSystemSettingsBag.BidEnabled;
                this.GoFlatOnly = venueSystemSettingsBag.GoFlatOnly;
                this.OrdersTransmissionDisabled = venueSystemSettingsBag.TransmissionDisabled;

                this.LastResetRequestedUtc = venueSystemSettingsBag.LastResetRequestedUtc;


                if(
                    //change in enablement status
                    (wasEnabled != this.Enabled || wasGoFlatOnly != this.GoFlatOnly || wasTransmissionDisabled != this.OrdersTransmissionDisabled)
                    &&
                    //and system is enabled
                    this.Enabled && !this.GoFlatOnly && !this.OrdersTransmissionDisabled && !riskManager.IsGoFlatOnlyEnabled && !riskManager.IsTransmissionDisabled
                    &&
                    //but bp is still zero
                    !_bpConversionsUpdated
                    )
                {
                    LogFactory.Instance.GetLogger(null).Log(LogLevel.Fatal, "{0} is now enabled, but it's bp conversions were never recalculated and are likely set to zero", this._currentSettings.TradingSystemId);
                }

                //Invoke events once all properties are set (so that simultaneous change in countergoing switches doesn't vibrate)
                if (wasEnabled != this.Enabled && this.EnableChanged != null)
                    this.EnableChanged(this.Enabled);
                if (isChangeInSidedEnablement && this.SideEnablementChanged != null)
                    this.SideEnablementChanged(_bidEnabled, _askEnabled);
                if (wasGoFlatOnly != this.GoFlatOnly && this.GoFlatOnlyChanged != null)
                    this.GoFlatOnlyChanged(this.GoFlatOnly);
                if (wasTransmissionDisabled != this.OrdersTransmissionDisabled && this.OrdersTransmissionDisabledChanged != null)
                    this.OrdersTransmissionDisabledChanged(this.OrdersTransmissionDisabled);
            }

            public Counterparty TargetVenueCounterparty { get; private set; }
            public Symbol TargetSymbol { get; private set; }

            public T CurrentSettings { get { return this._currentSettings; } }
            public event Action<T> SettingsChanged;

            public bool Enabled { get; set; }
            public event Action<bool> EnableChanged;

            public bool IsOutgoingPriceSideEnabled(PriceSide? side)
            {
                if (side.HasValue)
                {
                    if (side.Value == PriceSide.Ask)
                        return this._askEnabled;
                    else
                        return this._bidEnabled;
                }
                else
                {
                    return this._askEnabled && this._bidEnabled;
                }
            }

            public event Action<bool, bool> SideEnablementChanged;

            public bool GoFlatOnly { get; set; }
            public event Action<bool> GoFlatOnlyChanged;
            public bool OrdersTransmissionDisabled { get; set; }
            public event Action<bool> OrdersTransmissionDisabledChanged;

            private bool _askEnabled;
            private bool _bidEnabled;

            private DateTime? _lastResetRequestedUtc;
            public DateTime? LastResetRequestedUtc
            {
                set
                {
                    if (this._lastResetRequestedUtc != value)
                    {
                        this._lastResetRequestedUtc = value;
                        if (ResetRequested != null)
                            ResetRequested();
                    }
                }
            }
            public event Action ResetRequested;


            public event Action<IIntegratedSystemSettingsProvider<T>> SystemDeleted;
            public void AnnounceSettingsDeleted()
            {
                if (SystemDeleted != null)
                    SystemDeleted(this);
            }

            internal void PerformRegularCheck(DateTime now)
            {
                if (RegularCheck != null)
                    RegularCheck(now);
            }

            public event Action<DateTime> RegularCheck;
        }
    }
}
