﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinessLayer.Systems
{
    internal class BankpoolToVenueSessionAdapter: IOrderManagement
    {
        private IOrderManagement _fullOrderManagement;
        private VenuePoolForMarketOrder _venuePoolForMarketOrder;

        public BankpoolToVenueSessionAdapter(IOrderManagement fullOrderManagement, VenuePoolForMarketOrder venuePoolForMarketOrder)
        {
            this._fullOrderManagement = fullOrderManagement;
            this._venuePoolForMarketOrder = venuePoolForMarketOrder;
        }

        public IntegratorRequestResult RegisterOrder(IClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway)
        {
            BankPoolClientOrder bankpoolOrder = clientOrder as BankPoolClientOrder;

            if (bankpoolOrder != null)
            {
                //redirect
                return this._venuePoolForMarketOrder.RegisterOrder(bankpoolOrder, dispatchGateway);
            }
            else
            {
                return this._fullOrderManagement.RegisterOrder(clientOrder, dispatchGateway);
            }
        }

        public IntegratorRequestResult CancelOrder(CancelOrderRequestInfo cancelOrderRequestInfo,
            ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this._fullOrderManagement.CancelOrder(cancelOrderRequestInfo, dispatchGateway);
        }

        public bool HasVenueOrder(Counterparty targetVenueCounterparty, string clientOrderId)
        {
            return this._fullOrderManagement.HasVenueOrder(targetVenueCounterparty, clientOrderId);
        }

        public IBankPoolClientOrder GetInternalBankpoolOrder(IClientOrder clientOrder)
        {
            return this._fullOrderManagement.GetInternalBankpoolOrder(clientOrder);
        }
    }

    public class VenuePoolForMarketOrder
    {
        private IPriceBookStoreEx _priceBookStore;
        private IPriceStreamMonitor _priceStreamMonitor;
        private IOrderFlowSession _orderFlowSession;
        private IRiskManager _riskManager;
        private ILogger _logger;
        private object _matchingLocker = new object();
        private Dictionary<string, BankPoolClientOrder> _registeredOrdersMap = new Dictionary<string, BankPoolClientOrder>(); 

        public VenuePoolForMarketOrder(
            IPriceBookStoreEx priceBookStore, IPriceStreamMonitor priceStreamMonitor,
            IOrderFlowSession orderFlowSession, IRiskManager riskManager, ILogger logger)
        {
            this._priceBookStore = priceBookStore;
            this._priceStreamMonitor = priceStreamMonitor;
            this._orderFlowSession = orderFlowSession;
            this._riskManager = riskManager;
            this._logger = logger;
        }

        private decimal GetCurrentBestPriceForOrder(IClientOrder clientOrder)
        {
            decimal price = 0m;
            PriceObjectInternal topPriceObject = null;

            PriceObjectInternal tobPrice =
                (clientOrder.OrderRequestInfo.IntegratorDealDirection.ToInitiatingPriceDirection() == PriceSide.Ask
                    ? _priceBookStore.GetChangeableAskPriceBook(clientOrder.OrderRequestInfo.Symbol)
                    : _priceBookStore.GetChangeableBidPriceBook(clientOrder.OrderRequestInfo.Symbol)).MaxNodeInternal;

            if (PriceObjectInternal.IsNullPrice(tobPrice))
            {
                price =
                    this._priceStreamMonitor.GetMedianPriceProvider(clientOrder.OrderRequestInfo.Symbol,
                        clientOrder.OrderRequestInfo.IntegratorDealDirection.ToInitiatingPriceDirection()).Median;
            }
            else
            {
                price = tobPrice.Price;
            }

            if(tobPrice != null)
                tobPrice.Release();

            return price;
        }

        public IntegratorRequestResult RegisterOrder(BankPoolClientOrder clientOrder,
            ITakerUnicastInfoForwarder dispatchGateway)
        {
            return this.RegisterOrderInternal(clientOrder, dispatchGateway, false);
        }

        private IntegratorRequestResult RegisterOrderInternal(BankPoolClientOrder clientOrder, ITakerUnicastInfoForwarder dispatchGateway,
            bool wasAlreadyRegistered)
        {
            if (this._orderFlowSession.State != SessionState.Running)
            {
                string error =
                    string.Format(
                        "Attempt to register VenuePool-Market order {0}, but {1} session is not running, so rejecting the order",
                        clientOrder.ClientOrderIdentity, _orderFlowSession.Counterparty);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            if (this._riskManager.IsTransmissionDisabled)
            {
                string error =
                    string.Format(
                        "Attempt to register VenuePool-Market order {0}, but order transmission is currently disabled",
                        clientOrder.ClientOrderIdentity);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            if (!clientOrder.IsRiskRemovingOrder)
            {
                string error =
                    string.Format(
                        "Attempt to register VenuePool-Market order {0} that is not risk removing - it is disallowed by VenuePoolForMarketOrder module",
                        clientOrder.ClientOrderIdentity);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            if (clientOrder.OrderRequestInfo.OrderType != OrderType.Market &&
                clientOrder.OrderRequestInfo.OrderType != OrderType.MarketPreferLmax)
            {
                string error =
                    string.Format(
                        "Attempt to register VenuePool-Market order {0} of type {1} - this is unsupported (only Market orders are suppored)",
                        clientOrder.ClientOrderIdentity, clientOrder.OrderRequestInfo.OrderType);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            OrderRequestInfo ori = OrderRequestInfo.CreateLimit(
                        clientOrder.SizeBaseAbsActive,
                        this.GetCurrentBestPriceForOrder(clientOrder),
                        clientOrder.OrderRequestInfo.IntegratorDealDirection,
                        clientOrder.OrderRequestInfo.Symbol,
                        TimeInForce.ImmediateOrKill,
                        clientOrder.IsRiskRemovingOrder);



            string errorMessage;
            IIntegratorOrderExternal orderExternal = this._orderFlowSession.GetNewExternalOrder(ori, out errorMessage);
            if (orderExternal == null)
            {
                string error =
                    string.Format(
                        "Order {0} failed during submission in messaging layer",
                        clientOrder.ClientOrderIdentity);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            var riskResult = this._riskManager.AddInternalOrderAndCheckIfAllowed(clientOrder);
            if (riskResult != RiskManagementCheckResult.Accepted)
            {
                string error =
                    string.Format(
                        "Order {0} failed during risk management check ({1})",
                        clientOrder, riskResult);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }


            lock (_matchingLocker)
            {
                if (_registeredOrdersMap.ContainsKey(orderExternal.Identity) ||
                    _registeredOrdersMap.ContainsValue(clientOrder))
                {
                    string error =
                        string.Format(
                            "Order {0} (mapped to {1}) failed during adding to internal structure - already present",
                            clientOrder.Identity, orderExternal.Identity);
                    this._logger.Log(LogLevel.Fatal, error);
                    return IntegratorRequestResult.CreateFailedResult(error);
                }

                _registeredOrdersMap.Add(orderExternal.Identity, clientOrder);
            }

            clientOrder.InitializeInOrderManager(dispatchGateway);
            orderExternal.OnOrderChanged += OnOrderChanged;
            this._logger.Log(LogLevel.Info,
                             "Venue client order [{0}] mapped to external order [{1}], [{2}]",
                             clientOrder.ClientOrderIdentity, orderExternal.Identity,
                             orderExternal.UniqueInternalIdentity);

            if (this._orderFlowSession.SubmitOrder(orderExternal) == SubmissionResult.Success)
            {
                if(!wasAlreadyRegistered)
                    dispatchGateway.OnIntegratorClientOrderUpdate(
                        BankPoolClientOrderUpdateInfo.CreateOpenClientOrderUpdateInfo(clientOrder), clientOrder.OrderRequestInfo.Symbol);
            }
            else
            {
                this._riskManager.CancelInternalOrder(clientOrder, clientOrder.OrderRequestInfo.SizeBaseAbsInitial);
                orderExternal.OnOrderChanged -= OnOrderChanged;
                RemoveMappings(clientOrder);

                string error = string.Format( "Order {0} failed during submission in messaging layer", clientOrder);
                this._logger.Log(LogLevel.Fatal, error);
                return IntegratorRequestResult.CreateFailedResult(error);
            }

            return IntegratorRequestResult.GetSuccessResult();
        }


        private void OnOrderChanged(IIntegratorOrderExternal integratorOrderExternal,
            OrderChangeEventArgs orderChangeEventArgs)
        {
            BankPoolClientOrder clientOrder;

            lock (_matchingLocker)
            {
                if (!_registeredOrdersMap.TryGetValue(integratorOrderExternal.Identity, out clientOrder))
                {
                    this._logger.Log(LogLevel.Fatal,
                        "VenuePoolForMarketOrder Receiving external order [{0}] but cannot find matching internal order. This can happen after order broken.",
                        integratorOrderExternal.Identity);
                    return;
                }
            }

            IntegratorOrderExternalChange changeStatus = orderChangeEventArgs.ChangeState;

            switch (changeStatus)
            {
                case IntegratorOrderExternalChange.Submitted:
                case IntegratorOrderExternalChange.ConfirmedNew:
                case IntegratorOrderExternalChange.PendingCancel:
                case IntegratorOrderExternalChange.CancelRejected:
                    //no need to do anything
                    break;
                case IntegratorOrderExternalChange.Filled:
                case IntegratorOrderExternalChange.PartiallyFilled:
                    //if in pending list AND this is the last unknown external order - remove
                    decimal filledAmount = orderChangeEventArgs.ExecutionInfo.FilledAmount.Value;

                    this._logger.Log(LogLevel.Info, "External order [{0}] filled (amount: {1} price: {2})",
                        integratorOrderExternal.Identity, filledAmount,
                        orderChangeEventArgs.ExecutionInfo.UsedPrice.Value);
                    this.HandleFill(integratorOrderExternal, orderChangeEventArgs, clientOrder);
                    break;
                case IntegratorOrderExternalChange.Rejected:
                case IntegratorOrderExternalChange.Cancelled:
                    //log and add the amount back to order, put it back to _limit or _market list if it's in pending
                    this._logger.Log(LogLevel.Info, "External order [{0}] was {1} (amount: {2})",
                        integratorOrderExternal.Identity, changeStatus,
                        orderChangeEventArgs.RejectionInfo.RejectedAmount);
                    this.HandleReject(integratorOrderExternal, orderChangeEventArgs, clientOrder);
                    break;
                case IntegratorOrderExternalChange.InBrokenState:
                    //Log error. if in pending AND this is the last unknown external order - remove
                    this._logger.Log(LogLevel.Error, "External order [{0}] is in Broken state",
                        integratorOrderExternal.Identity);
                    this.HandleBrokenOrder(integratorOrderExternal, orderChangeEventArgs, clientOrder);
                    break;
                case IntegratorOrderExternalChange.NonConfirmedDeal:
                case IntegratorOrderExternalChange.NotSubmitted:
                default:
                    this._logger.Log(LogLevel.Fatal, "Unexpected IntegratorOrderExternalChange value: {0}", changeStatus);
                    break;
            }
        }

        private void RemoveMappings(BankPoolClientOrder clientOrder)
        {
            try
            {
                lock (_matchingLocker)
                {
                    var item = _registeredOrdersMap.First(kvp => kvp.Value == clientOrder);
                    _registeredOrdersMap.Remove(item.Key);
                }
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, e, "Problem during attempt to remove bankpool order from internal registers: {0}", clientOrder);
            }
        }


        private void HandleBrokenOrder(IIntegratorOrderExternal integratorOrderExternal,
            OrderChangeEventArgs orderChangeEventArgs, BankPoolClientOrder clientOrder)
        {
            clientOrder.OrderStatus = ClientOrderStatus.BrokenInIntegrator;
            this.RemoveMappings(clientOrder);
            clientOrder.DispatchGateway.OnCounterpartyIgnoredOrder(
                    new CounterpartyOrderIgnoringInfo(clientOrder.ClientOrderIdentity,
                                                      clientOrder.ClientIdentity,
                                                      string.Empty,
                                                      integratorOrderExternal.UniqueInternalIdentity, 
                                                      integratorOrderExternal.OrderRequestInfo.SizeBaseAbsInitial,
                                                      integratorOrderExternal.OrderRequestInfo.RequestedPrice,
                                                      clientOrder.OrderRequestInfo.IntegratorDealDirection,
                                                      integratorOrderExternal.OrderRequestInfo.Symbol,
                                                      integratorOrderExternal.IntegratorSentTimeUtc,
                                                      integratorOrderExternal.Counterparty,
                                                      TradingTargetType.BankPool,
                                                      clientOrder.IntegratedTradingSystemIdentification, null));

            clientOrder.DispatchGateway.OnIntegratorClientOrderUpdate(
                BankPoolClientOrderUpdateInfo.CreateRemovedClientOrderUpdateInfo(clientOrder, 0m, false),
                clientOrder.OrderRequestInfo.Symbol);
        }

        private void HandleReject(IIntegratorOrderExternal integratorOrderExternal,
            OrderChangeEventArgs orderChangeEventArgs, BankPoolClientOrder clientOrder)
        {
            decimal rejectedAmount = orderChangeEventArgs.RejectionInfo.RejectedAmount.Value;

            if (rejectedAmount > clientOrder.SizeBaseAbsInitial)
            {
                this._logger.Log(LogLevel.Fatal, "External order {0} rejected for size {1}, but matching internal order {2} was created for size {3} - so cutting the rejected size to initial size",
                    integratorOrderExternal.Identity, rejectedAmount, clientOrder.Identity, clientOrder.SizeBaseAbsInitial);
                rejectedAmount = clientOrder.SizeBaseAbsInitial;
            }

            //clientOrder.AddActiveAmountBack(rejectedAmount);
            this.RemoveMappings(clientOrder);

            clientOrder.DispatchGateway.OnCounterpartyRejectedOrder(
                new CounterpartyRejectionInfo(clientOrder.ClientOrderIdentity,
                    clientOrder.ClientIdentity,
                    string.Empty,
                    integratorOrderExternal.UniqueInternalIdentity,
                    integratorOrderExternal.Identity,
                    rejectedAmount,
                    integratorOrderExternal.OrderRequestInfo.RequestedPrice,
                    clientOrder.OrderRequestInfo.IntegratorDealDirection,
                    integratorOrderExternal.OrderRequestInfo.Symbol,
                    integratorOrderExternal.IntegratorSentTimeUtc,
                    integratorOrderExternal.IntegratorReceivedResultTimeUtc,
                    integratorOrderExternal.CounterpartySentResultTimeUtc,
                    integratorOrderExternal.Counterparty,
                    orderChangeEventArgs.RejectionInfo.RejectionReason, null,
                    orderChangeEventArgs == null
                        ? RejectionType.OrderReject
                        : orderChangeEventArgs.RejectionInfo.RejectionType,
                    TradingTargetType.BankPool,
                    clientOrder.IntegratedTradingSystemIdentification,
                    orderChangeEventArgs.RejectionInfo.CounterpartyClientId,
                    orderChangeEventArgs.ExecutionInfo != null
                        ? orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId
                        : null));

            var registerResult = this.RegisterOrderInternal(clientOrder, clientOrder.DispatchGateway, true);

            if (!registerResult.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Fatal, "Failed to reregister client order {0} after external order rejected. Reason: {1}. Rejecting the order",
                    clientOrder.Identity, registerResult.ErrorMessage);

                this._riskManager.CancelInternalOrder(clientOrder, rejectedAmount);
                clientOrder.SubtractAmountAndGetIsFullyDone(rejectedAmount, false);
                clientOrder.OrderStatus = ClientOrderStatus.RemovedFromIntegrator;

                clientOrder.DispatchGateway.OnIntegratorClientOrderUpdate(
                    new BankPoolClientOrderUpdateInfo(clientOrder,
                        ClientOrderCancelRequestStatus.CancelNotRequested, 0m,
                        rejectedAmount,
                        string.Format(
                            "External order [{0}] rejected and not possible to reregister internal order {1}",
                            integratorOrderExternal.Identity, clientOrder.Identity)), clientOrder.OrderRequestInfo.Symbol);
            }

            orderChangeEventArgs.RejectionInfo.AddClientSystemAllocation(
                clientOrder.IntegratedTradingSystemIdentification, rejectedAmount);
        }

        private void HandleFill(IIntegratorOrderExternal integratorOrderExternal,
            OrderChangeEventArgs orderChangeEventArgs, BankPoolClientOrder clientOrder)
        {
            decimal filledAmount = orderChangeEventArgs.ExecutionInfo.FilledAmount.Value;
            decimal usedPrice = orderChangeEventArgs.ExecutionInfo.UsedPrice.Value;
            this._logger.Log(LogLevel.Info, "VenuePoolForMarketOrder: External order [{0}] (mapped to internal order {3}) filled (amount: {1} price: {2})",
                             integratorOrderExternal.Identity, filledAmount, usedPrice, clientOrder.ClientOrderIdentity);

            bool fullyFilled = clientOrder.SubtractAmountAndGetIsFullyDone(filledAmount, true);

            if (fullyFilled)
            {
                this._logger.Log(LogLevel.Info,
                                 "VenuePoolForMarketOrder: Internal order {0} fully filled by external order {1}. Removing it from lists",
                                 clientOrder.ClientOrderIdentity, integratorOrderExternal.Identity);

                this.RemoveMappings(clientOrder);
            }

            string counterpartyTransactionId = orderChangeEventArgs.ExecutionInfo.CounterpartyTransactionId;
            string internalTransactionId = orderChangeEventArgs.ExecutionInfo.IntegratorTransactionId;

            clientOrder.DispatchGateway.OnIntegratorDealInternal(
                new IntegratorDealInternal(
                    clientOrder.ClientOrderIdentity, null,
                    clientOrder.ClientIdentity,
                    integratorOrderExternal.Identity,
                    counterpartyTransactionId,
                    internalTransactionId,
                    filledAmount, fullyFilled,
                    usedPrice,
                    integratorOrderExternal.OrderRequestInfo.Side,
                    integratorOrderExternal.OrderRequestInfo.Symbol,
                    integratorOrderExternal.IntegratorSentTimeUtc,
                    integratorOrderExternal.IntegratorReceivedResultTimeUtc,
                    integratorOrderExternal.CounterpartySentResultTimeUtc,
                    integratorOrderExternal.CounterpartyTransactionTimeUtc,
                    orderChangeEventArgs.ExecutionInfo.SettlementDate.Value,
                    orderChangeEventArgs.ExecutionInfo.SettlementTimeCounterpartyUtc,
                    orderChangeEventArgs.ExecutionInfo.SettlementTimeCentralBankUtc,
                    integratorOrderExternal.Counterparty, false,
                    orderChangeEventArgs.ExecutionInfo.FlowSide ??
                    integratorOrderExternal.OrderRequestInfo.FlowSideAtSubmitTime,
                    integratorOrderExternal.Counterparty.TradingTargetType, null,
                    clientOrder.IntegratedTradingSystemIdentification,
                    orderChangeEventArgs.ExecutionInfo.CounterpartyClientId));

            if (fullyFilled)
            {
                clientOrder.DispatchGateway.OnIntegratorClientOrderUpdate(
                            BankPoolClientOrderUpdateInfo.CreateRemovedClientOrderUpdateInfo(clientOrder, 0m, false), clientOrder.OrderRequestInfo.Symbol);
            }
            orderChangeEventArgs.ExecutionInfo.AddClientSystemAllocation(clientOrder.IntegratedTradingSystemIdentification, filledAmount);
        }



        
    }
}
