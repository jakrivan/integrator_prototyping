﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertEditSystem.aspx.cs" Inherits="KGTKillSwitchWeb.InsertEditSystem" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register Src="~/SystemDetailsStripsGrid.ascx" TagPrefix="uc1" TagName="VenueSettingsWithPnl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.7.1213, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= Utils.GetUatPrefix(Session) + Utils.GetGroupName(Request) + " : " + Request.QueryString["TradingSystemType"] + " " + Request.QueryString["VenueCounterparty"]%></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">

        function OnSkewChckClicked()
        {
            if (document.getElementById('SkewEnabledChck').checked)
                document.getElementById('SkewRow').style.display = '';
            else
                document.getElementById('SkewRow').style.display = 'none';
        }

        function OnSpreadDropDownChanged()
        {
            if (document.getElementById('SpreadTypeDropDown').value == 'Fixed')
            {
                document.getElementById('FixedSpreadRow').style.display = '';
                document.getElementById('DynamicSpreadRow1').style.display = 'none';
                document.getElementById('DynamicSpreadRow2').style.display = 'none';
            } else
            {
                document.getElementById('FixedSpreadRow').style.display = 'none';
                document.getElementById('DynamicSpreadRow1').style.display = '';
                document.getElementById('DynamicSpreadRow2').style.display = '';
            }
        }

        function HandleOnLoad()
        {
            OnSkewChckClicked();
            OnSpreadDropDownChanged();
        }

    </script>

</head>
<body onload="HandleOnLoad()">
    <form id="form1" runat="server"  autocomplete="off">
    <div>
        
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
        
        
        <asp:Panel runat="server" CssClass="fadeMe" ID="FadingPanel" Visible="True" />

        <asp:Panel runat="server" ID="InsertNewPanel" Visible="True" CssClass="InsertPanel">
            
        <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
            <h2  style="text-align: center; color: red">There was an error during performing selected action.</h2>
            <p style="color: red"><asp:Literal ID="exceptionLiteral" runat="server" Text="?"></asp:Literal></p>
            <br/>
            <br/>
        </asp:Panel>    
            
        <table style="text-align: right">
        
        <tr>
            <td><b>
                <asp:Literal runat="server" ID="GroupNameLiteral"></asp:Literal>:&nbsp;    
                    <asp:Literal runat="server" ID="VenueTypeLiteral"></asp:Literal> <asp:Literal runat="server" ID="TradingTypeLiteral"/>
                </b></td>
        </tr>
        <tr>
            <td>
                <b><asp:Literal runat="server" ID="ActionTypeLiteral"/></b>
            </td>
        </tr>
        <tr>
            <td><br/></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="symbolLbl" ForeColor="Black" EnableViewState="True" ViewStateMode="Enabled"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><br/></td>
        </tr>
            
            <asp:PlaceHolder ID="CrossPlaceholder" runat="server" Visible="False">
                <tr>
                <td>
                    Min Size:
                    <asp:TextBox runat="server" ID="MinimumSizeToTradeTxt" Text="100000"></asp:TextBox>
                    <asp:CustomValidator ID="MinSizeValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinimumSizeToTradeTxt" Display="Dynamic" ErrorMessage="Min Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="minSizesCmpCrossValidator" ControlToValidate="MinimumSizeToTradeTxt" controltocompare="MinimumFileSizeCrossTxt" operator="GreaterThanEqual" type="Currency" errormessage="The Min Fill Size must be lower or equal to Min Size" Display="Dynamic"  ForeColor="Red" />
                </td>
                </tr>
                <tr>
                <td>
                    Min Fill Size:
                    <asp:TextBox runat="server" ID="MinimumFileSizeCrossTxt" Text="10000"></asp:TextBox>
                    <asp:CustomValidator ID="MinimumFileSizeCrossValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinimumFileSizeCrossTxt" Display="Dynamic" ErrorMessage="Min Size Invalid" ValidateEmptyText="True" OnServerValidate="MinFillSizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Max Size:
                    <asp:TextBox runat="server" ID="MaximumSizeToTradeTxt" Text="500000"></asp:TextBox>
                    <asp:CustomValidator ID="MaxSizeValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaximumSizeToTradeTxt" Display="Dynamic" ErrorMessage="Max Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="maxSizesCmpCrossValidator" ControlToValidate="MaximumSizeToTradeTxt" controltocompare="MinimumSizeToTradeTxt" operator="GreaterThanEqual" type="Currency" errormessage="The Max Size must be higher or equal to Min Size" Display="Dynamic"  ForeColor="Red" />
                </td>
                </tr>
                <tr>
                <td>
                    Min Discount bp:
                    <asp:TextBox runat="server" ID="MinDiscountBpTxt" Text="0.25"></asp:TextBox>
                    <asp:CustomValidator ID="MinDiscountBpValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinDiscountBpTxt" Display="Dynamic" ErrorMessage="Min discount Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator> 
                </td>
                </tr>
                <tr>
                <td>
                    Min Pause sec:
                    <asp:TextBox runat="server" ID="MinTimeTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="MinTimeValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinTimeTxt" Display="Dynamic" ErrorMessage="Min time Invalid" ValidateEmptyText="True" OnServerValidate="MinTimeBetweenValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                
                <asp:PlaceHolder ID="FirmCrossPlaceholder" runat="server" Visible="False">
                    <tr>
                    <td>
                        Max Impr Wait ms:
                        <asp:TextBox runat="server" ID="MaxWaitImprovementTxtCross" Text="0"></asp:TextBox>
                        <asp:CustomValidator ID="MaxWaitImprovementTxtCrossValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxWaitImprovementTxtCross" Display="Dynamic" ErrorMessage="Max vait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxTickWaitTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </td>
                    </tr>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="SmartCrossPlaceholder" runat="server" Visible="False">
                    <tr>
                    <td>
                        Min True Gain Gross bp:
                        <asp:TextBox runat="server" ID="MinTrueGainGrossTxt" Text="1"></asp:TextBox>
                        <asp:CustomValidator ID="MinTrueGainGrossTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinTrueGainGrossTxt" Display="Dynamic" ErrorMessage="Min discount Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator> 
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Cover Distance bp:
                        <asp:TextBox runat="server" ID="CoverDistanceGrossTxt" Text="1"></asp:TextBox>
                        <asp:CustomValidator ID="MinGrossGainToCoverTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="CoverDistanceGrossTxt" Display="Dynamic" ErrorMessage="Min discount Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator> 
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Cover Timeout ms:
                        <asp:TextBox runat="server" ID="CoverTimeourTxt" Text="300"></asp:TextBox>
                        <asp:CustomValidator ID="CoverTimeourTxtTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="CoverTimeourTxt" Display="Dynamic" ErrorMessage="time Invalid" ValidateEmptyText="True" OnServerValidate="MaxTickWaitTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </td>
                    </tr>
                </asp:PlaceHolder> 
            </asp:PlaceHolder>
            
            
            <asp:PlaceHolder ID="MMPlaceHolder" runat="server" Visible="False">
                <tr>
                <td>
                    Min Size:
                    <asp:TextBox runat="server" ID="MinimumSizeToTradeMmTxt" Text="10000"></asp:TextBox>
                    <asp:CustomValidator ID="MinimumSizeToTradeMmValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinimumSizeToTradeMmTxt" Display="Dynamic" ErrorMessage="Min Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="minSizesCmpMMValidator" ControlToValidate="MinimumSizeToTradeMmTxt" controltocompare="MinimumFileSizeMmTxt" operator="GreaterThanEqual" type="Currency" errormessage="The Min Fill Size must be lower or equal to Min Size" Display="Dynamic"  ForeColor="Red" />
                </td>
                </tr>
                <tr>
                <td>
                    Min Fill Size:
                    <asp:TextBox runat="server" ID="MinimumFileSizeMmTxt" Text="10000"></asp:TextBox>
                    <asp:CustomValidator ID="MinimumFileSizeMmValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinimumFileSizeMmTxt" Display="Dynamic" ErrorMessage="Min Size Invalid" ValidateEmptyText="True" OnServerValidate="MinFillSizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Max Size:
                    <asp:TextBox runat="server" ID="MaximumSizeTxt" Text="500000"></asp:TextBox>
                    <asp:CustomValidator ID="SizeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaximumSizeTxt" Display="Dynamic" ErrorMessage="Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="maxSizesCmpMMValidator" ControlToValidate="MaximumSizeTxt" controltocompare="MinimumSizeToTradeMmTxt" operator="GreaterThanEqual" type="Currency" errormessage="The Max Size must be higher or equal to Min Size" Display="Dynamic"  ForeColor="Red" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="True" TextAlign="left" Text="Send Bids" ID="SendBidsMmChkBx" ClientIDMode="Static" 
                        OnClick="if(!document.getElementById('SendBidsMmChkBx').checked && !document.getElementById('SendAsksMmChkBx').checked) document.getElementById('EnableCheckBox').checked = false;"/>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="True" TextAlign="left" Text="Send Asks" ID="SendAsksMmChkBx" ClientIDMode="Static" 
                        OnClick="if(!document.getElementById('SendBidsMmChkBx').checked && !document.getElementById('SendAsksMmChkBx').checked) document.getElementById('EnableCheckBox').checked = false;"/>
                </td>
                </tr>
                <tr>
                <td>
                    Offset bp:
                    <asp:TextBox runat="server" ID="OffsetBpTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="OffsetBpTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="OffsetBpTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="BestPriceImprovementOffsetValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                   
                </td>
                </tr>
                <tr>
                <td>
                   Min Cross for FastCxl bp:
                    <asp:TextBox runat="server" ID="MinBpToFastCxlMmTxt" Text="2.5"></asp:TextBox>
                    <asp:CustomValidator ID="MinBpToFastCxlMmValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinBpToFastCxlMmTxt" Display="Dynamic" ErrorMessage="Bp Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                   
                </td>
                </tr>
                <tr>
                <td>
                   Max Discount bp:
                    <asp:TextBox runat="server" ID="MaxDiscountTxt" Text="0.25"></asp:TextBox>
                    <asp:CustomValidator ID="MaxDiscountTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxDiscountTxt" Display="Dynamic" ErrorMessage="Bp Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                   
                </td>
                </tr>
                <tr>
                <td>
                    Max Impr Wait (ms):
                    <asp:TextBox Enabled="False" runat="server" ID="MaxWaitImprovementTxtMm" Text="0"></asp:TextBox>
                    <asp:CustomValidator ID="MaxWaitImprovementTxtMmValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxWaitImprovementTxtMm" Display="Dynamic" ErrorMessage="Max wait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxTickWaitTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Min Price Life (ms):
                    <asp:TextBox runat="server" ID="MinFirmPriceLifeTxt" Text="0"></asp:TextBox>
                    <asp:CustomValidator ID="MinFirmPriceLifeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinFirmPriceLifeTxt" Display="Dynamic" ErrorMessage="Min price life time Invalid" ValidateEmptyText="True" OnServerValidate="MinPriceLifeTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Max Source Tiers:
                    <asp:TextBox runat="server" ID="MaxSourceTiersMmTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="MaxSourceTiersMmValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxSourceTiersMmTxt" Display="Dynamic" ErrorMessage="Max vait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxSourceTiersValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="StreamPlaceHolder" runat="server" Visible="False">
                <tr>
                <td>
                    Min Size:
                    <asp:TextBox runat="server" ID="MinimumSizeToTradeStreamTxt" Text="10000"></asp:TextBox>
                    <asp:CustomValidator ID="MinimumSizeToTradeStreamValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinimumSizeToTradeStreamTxt" Display="Dynamic" ErrorMessage="Min Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="minSizesCmpStreamValidator" ControlToValidate="MinimumSizeToTradeStreamTxt" controltocompare="MinimumFileSizeStreamTxt" operator="GreaterThanEqual" type="Currency" errormessage="The Min Fill Size must be lower or equal to Min Size"  Display="Dynamic"  ForeColor="Red"/>
                </td>
                </tr>
                <tr>
                <td>
                    Min Fill Size:
                    <asp:TextBox runat="server" ID="MinimumFileSizeStreamTxt" Text="10000"></asp:TextBox>
                    <asp:CustomValidator ID="MinimumFileSizeStreamValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinimumFileSizeStreamTxt" Display="Dynamic" ErrorMessage="Min Size Invalid" ValidateEmptyText="True" OnServerValidate="MinFillSizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Max Size:
                    <asp:TextBox runat="server" ID="StreamMaximumSizeTxt" Text="500000"></asp:TextBox>
                    <asp:CustomValidator ID="StreamSizeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="StreamMaximumSizeTxt" Display="Dynamic" ErrorMessage="Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="maxSizesCmpStreamValidator" ControlToValidate="StreamMaximumSizeTxt" controltocompare="MinimumSizeToTradeStreamTxt" operator="GreaterThanEqual" type="Currency" errormessage="The Max Size must be higher or equal to Min Size" Display="Dynamic"  ForeColor="Red" />
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="True" TextAlign="left" Text="Send Bids" ID="SendBidsStreamChkBx" ClientIDMode="Static" 
                        OnClick="if(!document.getElementById('SendBidsStreamChkBx').checked && !document.getElementById('SendAsksStreamChkBx').checked) document.getElementById('EnableCheckBox').checked = false;"/>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="True" TextAlign="left" Text="Send Asks" ID="SendAsksStreamChkBx" ClientIDMode="Static" 
                        OnClick="if(!document.getElementById('SendBidsStreamChkBx').checked && !document.getElementById('SendAsksStreamChkBx').checked) document.getElementById('EnableCheckBox').checked = false;"/>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="False" TextAlign="left" Text="Reference LMAX price only" ID="RefLmaxOnlyChkbx"/>
                </td>
                </tr>
                <tr>
                <td>
                    Offset bp:
                    <asp:TextBox runat="server" ID="StreamOffsetBpTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="StreamOffsetBpTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="StreamOffsetBpTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="BestPriceImprovementOffsetValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                   
                </td>
                </tr>
                <tr>
                <td>
                   Min Cross for FastCxl bp:
                    <asp:TextBox runat="server" ID="MinBpToFastCxlStreamTxt" Text="2.5"></asp:TextBox>
                    <asp:CustomValidator ID="MinBpToFastCxlStreamValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinBpToFastCxlStreamTxt" Display="Dynamic" ErrorMessage="Bp Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                   
                </td>
                </tr>
                <tr>
                <td>
                   Max Discount (bp):
                    <asp:TextBox runat="server" ID="StreamMaxDiscountTxt" Text="0.25"></asp:TextBox>
                    <asp:CustomValidator ID="StreamMaxDiscountTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="StreamMaxDiscountTxt" Display="Dynamic" ErrorMessage="Bp Invalid" ValidateEmptyText="True" OnServerValidate="BpValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                   
                </td>
                </tr>
                <tr>
                <td>
                    Min Soft Price Life (ms):
                    <asp:TextBox runat="server" ID="MinSoftPriceLifeTxt" Text="0"></asp:TextBox>
                    <asp:CustomValidator ID="MinSoftPriceLifeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinSoftPriceLifeTxt" Display="Dynamic" ErrorMessage="Min price life time Invalid" ValidateEmptyText="True" OnServerValidate="MinPriceLifeTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Max Source Tiers:
                    <asp:TextBox runat="server" ID="MaxSourceTiersStreamTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="MaxSourceTiersStreamValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxSourceTiersStreamTxt" Display="Dynamic" ErrorMessage="Max vait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxSourceTiersValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    LL ms (KGT internal):
                    <asp:TextBox runat="server" ID="LLTime" Text="50"></asp:TextBox>
                    <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" ID="LLCompareValidator" ControlToValidate="LLTime" controltocompare="MaxLLInternalValue" operator="LessThan" type="Integer" errormessage="Too high LL time" Display="Dynamic"  ForeColor="Red"></asp:CompareValidator>

                    <%--<asp:PlaceHolder ID="NonHotspotPlaceholder" runat="server" Visible="False">
                        <asp:CustomValidator ID="LLTimeValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="LLTime" Display="Dynamic" ErrorMessage="Max vait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxLLTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="HotspotPlaceholder" runat="server" Visible="False">
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="InsertValidation" ControlToValidate="LLTime" Display="Dynamic" ErrorMessage="Max vait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxHotspotLLTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </asp:PlaceHolder>--%>
                </td>
                </tr>
                <tr>
                <td>
                    LL ms (RT): <asp:TextBox ReadOnly="True" Enabled="False" runat="server" ID="LLRTTimeLbl"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td>
                    LL ms (RT max - hard limit): <asp:TextBox ReadOnly="True" Enabled="False" runat="server" ID="LLRTMaxTimeLbl"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="MaxLLInternalValue"/>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="True" TextAlign="left" Text="Prefer LL MTM Check on LMAX" ID="LlCheckLmaxChck"/>
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="True" TextAlign="left" Text="Prefer Cover to LMAX" ID="PreferLmaxHedgeChck"/>
                </td>
                </tr>
                <asp:PlaceHolder ID="StreamPlaceHolder2" runat="server" Visible="False">
                    <tr>
                    <td>
                        Min Gross MTM to Accept (bp):
                        <asp:TextBox runat="server" ID="MinBpToAccept" Text="0"></asp:TextBox>
                        <asp:CustomValidator ID="MinBpToAcceptValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinBpToAccept" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="LLCheckBestPriceImprovementOffsetValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </td>
                    </tr>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="StreamGlidePlaceholder" runat="server" Visible="False">
                    <tr>
                    <td>
                        Min Gross Gain (bp):
                        <asp:TextBox runat="server" ID="MinBpGainTxt" Text="0"></asp:TextBox>
                        <asp:CustomValidator ID="MinBpGainTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinBpGainTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="LLCheckBestPriceImprovementOffsetValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Min Gross Gain If Zero Glide Fill (bp):
                        <asp:TextBox runat="server" ID="MinBpGainIfZeroFillTxt" Text="0"></asp:TextBox>
                        <asp:CustomValidator ID="MinBpGainIfZeroFillTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinBpGainIfZeroFillTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="LLCheckBestPriceImprovementOffsetValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Min Glide Price Life (ms):
                        <asp:TextBox runat="server" ID="GlideTTLTxt" Text="10"></asp:TextBox>
                        <asp:CustomValidator ID="GlideTTLTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="GlideTTLTxt" Display="Dynamic" ErrorMessage="Max vait time Invalid" ValidateEmptyText="True" OnServerValidate="MaxGlideTimeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                        <asp:CompareValidator runat="server" ValidationGroup="InsertValidation" id="GlideTimeCompareValidator" ControlToValidate="GlideTTLTxt" controltocompare="LLTime" operator="LessThan" type="Integer" errormessage="The Glide price life must be lower than LL time" Display="Dynamic"  ForeColor="Red" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Glide On:
                        <asp:DropDownList runat="server" ID="GlideOnDropDown" Width="54">
                            <asp:ListItem Value="L0*" Selected="True">L0*</asp:ListItem>
                            <asp:ListItem Value="LX1">LX1</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Glide Cutoff before LL end (ms):
                        <asp:TextBox ReadOnly="True" Enabled="False" runat="server" ID="GlideCutoffTxt" Text="5"></asp:TextBox>
                    </td>
                    </tr>
                </asp:PlaceHolder>


            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="QuotingPlaceHolder" runat="server" Visible="False">
                <tr>
                <td>
                    Max Position:
                    <asp:TextBox runat="server" ID="MaxSizeTxt" Text="500000"></asp:TextBox>
                    <asp:CustomValidator ID="MaxSizeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxSizeTxt" Display="Dynamic" ErrorMessage="Size Invalid" ValidateEmptyText="True" OnServerValidate="SizeValidatorZeroAllowed_ServerValidate" ForeColor="Red"></asp:CustomValidator>
                </td>
                </tr>
                <tr>
                <td>
                    Spread Type:
                    <asp:DropDownList runat="server" ID="SpreadTypeDropDown" Width="54" ClientIDMode="Static"
                        OnChange="OnSpreadDropDownChanged();">
                        <asp:ListItem Value="Fixed" Selected="True">Fixed</asp:ListItem>
                        <asp:ListItem Value="Dynamic">Dynamic</asp:ListItem>
                    </asp:DropDownList>
                </td>
                </tr>
                <tr id="FixedSpreadRow">
                <td>
                    Fixed Spread Bp:
                    <asp:TextBox runat="server" ID="FixedSpreadBpTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="FixedSpreadBpTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="FixedSpreadBpTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="SpreadValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>   
                </td>
                </tr>
                <tr>
                <td id="DynamicSpreadRow1">
                    Min Dynamic Spread Bp:
                    <asp:TextBox runat="server" ID="MinDynamicSpreadBpTxt" Text="1"></asp:TextBox>
                    <asp:CustomValidator ID="MinDynamicSpreadBpTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinDynamicSpreadBpTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="SpreadValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>   
                </td>
                </tr>
                <tr id="DynamicSpreadRow2">
                <td>
                    Dynamic Markup decimal:
                    <asp:TextBox runat="server" ID="DynamicMarkupDecimalTxt" Text="0"></asp:TextBox>
                    <asp:CustomValidator ID="DynamicMarkupDecimalTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="DynamicMarkupDecimalTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="PriceAdjustmentDecimal_ServerValidate" ForeColor="Red"></asp:CustomValidator>   
                </td>
                </tr>
                <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="False" TextAlign="left" Text="Enable Skew" ID="SkewEnabledChck" ClientIDMode="Static"
                        OnClick="OnSkewChckClicked();"/>
                </td>
                </tr>
                <tr id="SkewRow">
                <td>
                    Skew Decimal:
                    <asp:TextBox runat="server" ID="SkewDecimalTxt" Text="0" ClientIDMode="Static"></asp:TextBox>
                    <asp:CustomValidator ID="SkewDecimalTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="SkewDecimalTxt" Display="Dynamic" ErrorMessage="Offset Invalid" ValidateEmptyText="True" OnServerValidate="PriceAdjustmentPositiveDecimal_ServerValidate" ForeColor="Red"></asp:CustomValidator>   
                </td>
                </tr>
                <tr>
                <td>
                   SL Net USD:
                    <asp:TextBox runat="server" ID="StopLossTxt" Text="1000"></asp:TextBox>
                    <asp:CustomValidator ID="StopLossTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="StopLossTxt" Display="Dynamic" ErrorMessage="Stop Loss Invalid" ValidateEmptyText="True" OnServerValidate="StopLossValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                
                
                <tr>
                <td>
                   Inactivity Timeout sec:
                    <asp:TextBox runat="server" ID="InactivityTimeoutSecTxt" Text="5"></asp:TextBox>
                    <asp:CustomValidator ID="InactivityTimeoutSecTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="InactivityTimeoutSecTxt" Display="Dynamic" ErrorMessage="Inactivity timeout Invalid" ValidateEmptyText="True" OnServerValidate="InactivityValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                <tr>
                <td>
                   Liquidation Spread bp:
                    <asp:TextBox runat="server" ID="LiquidationSpreadBpTxt" Text="0.01"></asp:TextBox>
                    <asp:CustomValidator ID="LiquidationSpreadBpTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="LiquidationSpreadBpTxt" Display="Dynamic" ErrorMessage="LiquidationSpread Invalid" ValidateEmptyText="True" OnServerValidate="LiquidationSpreadValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                <tr>
                <td>
                   LT Lookback sec:
                    <asp:TextBox runat="server" ID="LtLookBackSecTxt" Text="60"></asp:TextBox>
                    <asp:CustomValidator ID="LtLookBackSecTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="LtLookBackSecTxt" Display="Dynamic" ErrorMessage="LT lookback Invalid" ValidateEmptyText="True" OnServerValidate="LtLookbackSecValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                <tr>
                <td>
                   Min LT Volume USD:
                    <asp:TextBox runat="server" ID="MinLtVolumeTxt" Text="5000000"></asp:TextBox>
                    <asp:CustomValidator ID="MinLtVolumeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinLtVolumeTxt" Display="Dynamic" ErrorMessage="LT Volume Invalid" ValidateEmptyText="True" OnServerValidate="LtVolumeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                <tr>
                <td>
                   Max LT Volume USD:
                    <asp:TextBox runat="server" ID="MaxLtVolumeTxt" Text="1000000000"></asp:TextBox>
                    <asp:CustomValidator ID="MaxLtVolumeTxtValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxLtVolumeTxt" Display="Dynamic" ErrorMessage="LT Volume Invalid" ValidateEmptyText="True" OnServerValidate="LtVolumeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                <tr>
                <td>
                   Min LT Avg Deal Size USD:
                    <asp:TextBox runat="server" ID="MinLtAvgDealSize" Text="0"></asp:TextBox>
                    <asp:CustomValidator ID="MinLtAvgDealSizeValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MinLtAvgDealSize" Display="Dynamic" ErrorMessage="LT Deal Size Invalid" ValidateEmptyText="True" OnServerValidate="LtDealSizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>
                <tr>
                <td>
                   Max LT Avg Deal Size USD:
                    <asp:TextBox runat="server" ID="MaxLtAvgDealSize" Text="100000"></asp:TextBox>
                    <asp:CustomValidator ID="MaxLtAvgDealSizeValidator" runat="server" ValidationGroup="InsertValidation" ControlToValidate="MaxLtAvgDealSize" Display="Dynamic" ErrorMessage="LT Deal Size Invalid" ValidateEmptyText="True" OnServerValidate="LtDealSizeValidator_ServerValidate" ForeColor="Red"></asp:CustomValidator>       
                </td>
                </tr>

                

            </asp:PlaceHolder>
            
            <tr>
                <td><br/></td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox runat="server" Checked="False" TextAlign="left" Text="Immediately Enable" ID="EnableCheckBox" ClientIDMode="Static"    
                        OnClick="if(document.getElementById('SendBidsStreamChkBx') != null) if(!document.getElementById('SendBidsStreamChkBx').checked && !document.getElementById('SendAsksStreamChkBx').checked) document.getElementById('EnableCheckBox').checked = false; if(document.getElementById('SendBidsMmChkBx') != null) if(!document.getElementById('SendBidsMmChkBx').checked && !document.getElementById('SendAsksMmChkBx').checked) document.getElementById('EnableCheckBox').checked = false;"/>
                </td>
            </tr>
            <tr>
                <td><br/></td>
            </tr>
            <tr>
            <td>
                <div id="ConfirmButtons">
                <asp:HiddenField runat="server" ID="HiddenLastUpdatedUtc"/>
                    <%--CommandName="InsertNewConfirmed" OnCommand="OnSymbolCommand"--%>
                <asp:LinkButton runat="server" ID="InsertButton" CausesValidation="True" Text="Add" ValidationGroup="InsertValidation" OnClick="InsertButton_OnClick" OnClientClick="Page_ClientValidate(); if(Page_IsValid){this.parentNode.style.display = 'none'; document.getElementById('ProgressText').style.display = '';}"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                <asp:LinkButton runat="server" Text="Cancel" ID="CancelButton"></asp:LinkButton>
                </div>
                <div id="ProgressText" style="display: none"><b>Operation is in progress...</b></div>
            </td>

        </tr>
        </table>
    </asp:Panel>
            
    <ajaxToolkit:AlwaysVisibleControlExtender runat="server"
        TargetControlID="InsertNewPanel"
        VerticalSide="Middle"
        HorizontalSide="Center"
        />
        
        
        

        <uc1:VenueSettingsWithPnl runat="server" ID="VenueSettingsWithPnl"/>
    </div>
    </form>
</body>
</html>
