﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemDetailsStrip.ascx.cs" Inherits="KGTKillSwitchWeb.SystemDetailsStrip" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="KGTKillSwitchWeb" %>
<%@ Register TagPrefix="uc1" Namespace="WebControls" Assembly="WebControls" %>
<%@ Register Src="~/RotatedText.ascx" TagPrefix="uc1" TagName="RotatedText" %>




<asp:repeater ID="StatsRepeater" runat="server" EnableViewState="False" ViewStateMode="Disabled">

        <HeaderTemplate>
            <div style="padding: 5px 10px">
            <table class="SingleTable">
            <thead>
            <tr>
                <th style="border: solid 1px #c1c1c1; padding: 3px;">Symbol</th>
                <th class="DividerCell"></th>
                
                <th> <uc1:RotatedText runat="server" Value="Open Position" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Volume USD M" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Deals" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Cpt Rejections" /> </th>
                <th> <uc1:RotatedText runat="server" Value="KGT Rejections" /> </th>
                <th class="DividerCell"></th>
                
                
                <%--<th> <uc1:RotatedText runat="server" Value="Pnl Gross USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Pnl Net USD" /> <uc1:RotatedText runat="server" Value="Per M USD" /> </th>--%>
                <th> <uc1:RotatedText runat="server" Value="Pnl Gross USD Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD Per M USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Pnl Net USD Per M USD" /> </th>
                <th class="DividerCell"></th>
                
                <th> <uc1:RotatedText runat="server" Value="Pnl Gross USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Comm USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Pnl Net USD" /> </th>
                <th class="DividerCell"></th>
                
                <th> <uc1:RotatedText runat="server" Value="Active" /> </th>
                <th> <uc1:RotatedText runat="server" Value="On" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Remove" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Reset" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Modify" /> </th>
                <th class="DividerCell"></th>
                
                <uc1:DataPlaceHolder ID="DataPlaceHolder1" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Cross || this.TradingSystemType == TradingSystemType.SmartCross %>'>
                    
                <th> <uc1:RotatedText runat="server" Value="Min Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Fill Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Discount to trade dec" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Discount to trade bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Pause sec" /> </th>
                    
                <uc1:DataPlaceHolder ID="DataPlaceHolder7" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Cross %>'>
                    <th> <uc1:RotatedText runat="server" Value="Max Impr Wait ms" /> </th>  
                </uc1:DataPlaceHolder> 
                <uc1:DataPlaceHolder ID="DataPlaceHolder8" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.SmartCross %>'>
                    <th> <uc1:RotatedText runat="server" Value="Min True Gain Gross dec" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Min True Gain Gross bp" /> </th> 
                    <th> <uc1:RotatedText runat="server" Value="Cover Distance Gross dec" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Cover Distance Gross bp" /> </th> 
                    <th> <uc1:RotatedText runat="server" Value="Cover Timeout ms" /> </th>
                </uc1:DataPlaceHolder>
                    

                </uc1:DataPlaceHolder>
                
                <uc1:DataPlaceHolder ID="DataPlaceHolder2" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.MM %>'>
                    
                <th> <uc1:RotatedText runat="server" Value="Min Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Fill Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Send Bids" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Send Asks" /> </th>       
                <th> <uc1:RotatedText runat="server" Value="Offset decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Offset bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Cross for FastCxl bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Discount bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Impr Wait ms" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Price Life ms" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Source Tiers" /> </th>
                
                </uc1:DataPlaceHolder>
                
                <uc1:DataPlaceHolder ID="DataPlaceHolder4" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Stream || this.TradingSystemType == TradingSystemType.Glider %>'>
                    
                    
                <th> <uc1:RotatedText runat="server" Value="Min Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Fill Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Size" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Send Bids" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Send Asks" /> </th>   
                <th> <uc1:RotatedText runat="server" Value="Reference LMAX price only" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Offset decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Offset bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Cross for FastCxl bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Discount bp" /> </th>     
                <th> <uc1:RotatedText runat="server" Value="Min Soft Price Life ms" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max Source Tiers" /> </th>     
                <th> <uc1:RotatedText runat="server" Value="LL ms (KGT internal)" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="LL ms (counterparty RT)" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="LL ms (RT max - hard limit)" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Prefer LL MTM Check on LMAX" /> </th>
                <uc1:DataPlaceHolder ID="DataPlaceHolder5" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Stream %>'>
                    <th> <uc1:RotatedText runat="server" Value="Min Gross MTM to Accept dec" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Min Gross MTM to Accept bp" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Prefer Cover to LMAX" /> </th>   
                </uc1:DataPlaceHolder> 
                <uc1:DataPlaceHolder ID="DataPlaceHolder6" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Glider %>'>
                    <th> <uc1:RotatedText runat="server" Value="Min Gross Gain dec" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Min Gross Gain bp" /> </th> 
                    <th> <uc1:RotatedText runat="server" Value="Min Gross Gain If Zero Glide Fill dec" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Min Gross Gain If Zero Glide Fill bp" /> </th> 
                    <th> <uc1:RotatedText runat="server" Value="Min Glide Price Life ms" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Glide Cutoff before LL end ms" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Glide On" /> </th>
                    <th> <uc1:RotatedText runat="server" Value="Prefer LMAX execution after LL-time" /> </th> 
                </uc1:DataPlaceHolder>       
                   
                    
                </uc1:DataPlaceHolder>
                
                <uc1:DataPlaceHolder ID="DataPlaceHolder3" runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Quoting %>'>
                    
                <th> <uc1:RotatedText runat="server" Value="Maximum Position Base Abs" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Spread Type" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Fixed Spread decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Fixed Spread Bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Dynamic Spread Bp" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Min Dynamic Spread decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Dynamic Markup decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Skew Enabled" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Skew Decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Stop Loss Net Usd" /> </th>    
                <th> <uc1:RotatedText runat="server" Value="Current Spread decimal" /> </th>                    
                <th> <uc1:RotatedText runat="server" Value="Inactivity Timeout sec" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Liquidation Spread bp" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Liquidation Spread decimal" /> </th>
                <th> <uc1:RotatedText runat="server" Value="LT Lookback sec" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Min LT Volume USD" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Current LT Volume USD" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Paid Flow %" /> </th>
                <th> <uc1:RotatedText runat="server" Value="Max LT Volume USD" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Min LT Avg Deal Size USD" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Current LT Avg Deal Size USD" /> </th> 
                <th> <uc1:RotatedText runat="server" Value="Max LT Avg Deal Size USD" /> </th> 

                </uc1:DataPlaceHolder>
            </tr>
               <%-- <tr>
                    <th colspan="24">HEADER</th>
                </tr>--%>
            </thead>
            <tbody>
                <tr>
                    <td class="DividerCell"/>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
            
            <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# ((DataRowView)Container.DataItem)["Symbol"] == DBNull.Value %>'> 
            <tr>
                <td class="DividerCell"/>
            </tr>
            </asp:PlaceHolder>
            
            <uc1:DataPlaceHolder runat="server" Visible='<%# ((DataRowView)Container.DataItem)["Symbol"] != DBNull.Value %>'>
                
                
                <tr title='<%# string.Format("{2}System id: {0}, description: {1}", 
                ((DataRowView)Container.DataItem)["TradingSystemId"], ((DataRowView)Container.DataItem)["TradingSystemDescription"], (bool)((DataRowView)Container.DataItem)["HardBlocked"] ? "BLOCKED " : string.Empty) %>'
                class='<%# this.IsRecentlyUpdated(((DataRowView)Container.DataItem)["LastUpdatedUtc"]) ? "changedRow" : string.Empty %>'    
                    >
                    <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%# ((DataRowView)Container.DataItem)["SymbolSubrows"] != DBNull.Value %>'> 
                        <td rowspan='<%# ((DataRowView)Container.DataItem)["SymbolSubrows"] %>' class="Symbol">
                            <a href='<%# string.Format("InsertEditSystem.aspx{0}&Action=Insert&SymbolsNum=1&Symbols={1}", this.GetQueryStringWithNoAction(), Utils.RemoveSlashFromSymbol(((DataRowView)Container.DataItem)["Symbol"].ToString())) %>'><%# ((DataRowView)Container.DataItem)["Symbol"] %></a> 
                        </td>
                    </asp:PlaceHolder>   
                    <td class="DividerCell"></td>
                    <td class='<%# Utils.GetNumberCellStyle(((DataRowView)Container.DataItem)["NopBasePol"])  %>'>
                        <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["NopBasePol"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["VolumeUsdM"], 1, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtDeal) %>'>
                        <%# ((DataRowView)Container.DataItem)["DealsNum"] %>
                    </td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.CptRejection) %>'>
                        <%# ((DataRowView)Container.DataItem)["CtpRejectionsNum"] %>
                    </td>
                    <td class='<%# Utils.GetDealActionCellStyle(Container.DataItem, Utils.DealAction.KgtRejection) %>'>
                        <%# ((DataRowView)Container.DataItem)["KGTRejectionsNum"] %>
                    </td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlGrossUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlNetUsdPerMUsd"], 2, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class="DividerCell"></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlGrossUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CommissionsUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class='<%# Utils.GetNumberCellStyle(((DataRowView)Container.DataItem)["PnlNetUsd"])  %>'><%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["PnlNetUsd"], 0, Utils.NullsHandling.DbNullToEmpty) %></td>
                    <td class="DividerCell"></td>
                    <td <%# (bool)((DataRowView)Container.DataItem)["Active"] ? "style='background-color:red'" : string.Empty  %>></td>
                    <td>
                        <a runat="server" Visible='<%# (bool)((DataRowView)Container.DataItem)["Enabled"] %>' href='<%# string.Format("{0}&DisableSystemById={1}", this.RefreshUrlWithNoAction, ((DataRowView)Container.DataItem)["TradingSystemId"]) %>'><img src="Styles/Icon-On-16-pixels.png"/></a>
                        <a runat="server" Visible='<%# !(bool)((DataRowView)Container.DataItem)["Enabled"] %>' href='<%# string.Format("{0}&EnableSystemById={1}", this.RefreshUrlWithNoAction, ((DataRowView)Container.DataItem)["TradingSystemId"]) %>'><img src="Styles/Icon-Off-16-pixels.png"/></a>
                    </td>
                    <td>
                        <a runat="server" Visible='<%# !(bool)((DataRowView)Container.DataItem)["Enabled"] %>' href='<%# string.Format("{0}&DeleteSystemById={1}", this.RefreshUrlWithNoAction, ((DataRowView)Container.DataItem)["TradingSystemId"]) %>' onclick="return confirm('Are you sure you want to delete selected system?')"><img src="Styles/Icon-Remove-Faded-16-pixels.png"/></a>
                    </td>
                    <td <%# (bool)((DataRowView)Container.DataItem)["HardBlocked"] ? "style='background-color:red'" : string.Empty  %>>
                        <a runat="server" Visible='<%# !(bool)((DataRowView)Container.DataItem)["Enabled"] %>' href='<%# string.Format("{0}&ResetSystemById={1}", this.RefreshUrlWithNoAction, ((DataRowView)Container.DataItem)["TradingSystemId"]) %>' onclick="return confirm('Are you sure you want to reset selected system?')"><img src="Styles/Icon-Reset-16-pixels.png"/></a>
                    </td>
                    <td>
                        <a runat="server" href='<%# string.Format("InsertEditSystem.aspx{0}&Action=Edit&TradingSystemId={1}&SymbolsNum=1&Symbols={2}", this.GetQueryStringWithNoAction(), ((DataRowView)Container.DataItem)["TradingSystemId"], ((DataRowView)Container.DataItem)["Symbol"]) %>'><img src="Styles/Icon-Modify-16-pixels.png"/></a>
                    </td>
                    <td class="DividerCell"></td>
                    
                    
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Cross || this.TradingSystemType == TradingSystemType.SmartCross %>'>

                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumSizeToTrade"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>       
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumFillSize"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>    
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumSizeToTrade"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumDiscountToTradeDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumDiscountBasisPointsToTrade"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumTimeBetweenSingleCounterpartySignals_seconds"], 1, Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Cross %>'>    
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# ((DataRowView)Container.DataItem)["MaximumWaitTimeOnImprovementTick_milliseconds"] %>
                        </td>   
                    </uc1:DataPlaceHolder>
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.SmartCross %>'>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumTrueGainGrossDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                        </td>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumTrueGainGrossBasisPoints"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                        </td>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CoverDistanceGrossDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                        </td>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CoverDistanceGrossBasisPoints"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                        </td> 
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# ((DataRowView)Container.DataItem)["CoverTimeout_milliseconds"] %>
                        </td> 
                    </uc1:DataPlaceHolder>
                    
                    </uc1:DataPlaceHolder>
                    
                    
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.MM %>'>
                        
                        
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumSizeToTrade"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumFillSize"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>                         
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumSizeToTrade"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>     
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="SendBidsMmChkBx" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["BidEnabled"] %>'></uc1:StatelessCheckBox>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="SendAsksMmChkBx" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["AskEnabled"] %>'></uc1:StatelessCheckBox>
                    </td>   
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["BestPriceImprovementOffsetDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["BestPriceImprovementOffsetBasisPoints"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                    </td>         
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumBasisPointsFromKgtPriceToFastCancel"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                    </td>    
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MaximumDiscountBasisPointsToTrade"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["MaximumWaitTimeOnImprovementTick_milliseconds"] %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["MinimumIntegratorPriceLife_milliseconds"] %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumSourceBookTiers"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>  
                    
                    </uc1:DataPlaceHolder>
                    
                    
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Stream || this.TradingSystemType == TradingSystemType.Glider %>'>
                        
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumSizeToTrade"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumFillSize"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumSizeToTrade"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>  
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="SendBidsStreamChkBx" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["BidEnabled"] %>'></uc1:StatelessCheckBox>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="SendAsksStreamChkBx" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["AskEnabled"] %>'></uc1:StatelessCheckBox>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="RefLmaxOnlyChkbx" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["ReferenceLmaxBookOnly"] %>'></uc1:StatelessCheckBox>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["BestPriceImprovementOffsetDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["BestPriceImprovementOffsetBasisPoints"], 2, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumBasisPointsFromKgtPriceToFastCancel"], 2, Utils.NullsHandling.DbNullToEmpty) %>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MaximumDiscountBasisPointsToTrade"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                    </td>  
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["MinimumIntegratorPriceLife_milliseconds"] %>
                    </td>  
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumSourceBookTiers"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>  
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["KGTLLTime_milliseconds"] %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# (int) ((DataRowView)Container.DataItem)["KGTLLTime_milliseconds"] + Math.Ceiling(2*StreamingSettingsDal.Instance.GetExpectedRTHalfTime(this.VenueCounterparty).TotalMilliseconds) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# (int) StreamingSettingsDal.Instance.GetMaxLLTimeOnDestination(this.VenueCounterparty).TotalMilliseconds %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="LlCheckLmaxChck" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["PreferLLDestinationCheckToLmax"] %>'></uc1:StatelessCheckBox>
                    </td> 
 
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Stream %>'>           
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumGrossToAcceptDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                        </td>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumBPGrossToAccept"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                        </td>   
                    </uc1:DataPlaceHolder> 
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Glider %>'>             
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumGrossGainDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                        </td>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumBPGrossGain"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                        </td>  
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumGrossGainIfZeroGlideFillDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                        </td>
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                                <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumBPGrossGainIfZeroGlideFill"], 2, Utils.NullsHandling.DbNullToEmpty) %> 
                        </td> 
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["MinimumIntegratorGlidePriceLife_milliseconds"] %>
                        </td>  
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["GlidingCutoffSpan_milliseconds"] %>
                        </td>  
                        <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.BackendGlidingCounterpartyToUi(((DataRowView)Container.DataItem)["GlidingCounterparty"].ToString()) %>
                        </td>
                    </uc1:DataPlaceHolder>    
                        
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="PreferLmaxHedgeChck" Enabled="False" Checked='<%# (bool) ((DataRowView)Container.DataItem)["PreferLmaxDuringHedging"] %>'></uc1:StatelessCheckBox>
                    </td>

                    </uc1:DataPlaceHolder>
                    
                    <uc1:DataPlaceHolder runat="server" Visible='<%# this.TradingSystemType == TradingSystemType.Quoting %>'>

                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumPositionBaseAbs"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <%# ((DataRowView)Container.DataItem)["SpreadType"] %>
                    </td>    
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["FixedSpreadDecimal"], 5, Utils.NullsHandling.DbNullToNA) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["FixedSpreadBasisPoints"], 2, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>    
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumDynamicSpreadBp"], 2, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["MinimumDynamicSpreadDecimal"], 5, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["DynamicMarkupDecimal"], 6, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <uc1:StatelessCheckBox runat="server" ID="SkewEnabled" Enabled="False" Checked='<%# (bool)((DataRowView)Container.DataItem)["EnableSkew"] %>'></uc1:StatelessCheckBox>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["SkewDecimal"], 6, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>    
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["StopLossNetInUsd"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>     
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? "selectedSetting" : string.Empty %>'>
                        <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["CurrentSpreadDecimal"], 6, Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] && !Utils.NullOrZero(((DataRowView)Container.DataItem)["NopBasePol"]) ? (this.IsQuotingInActivityTimeoutRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["InactivityTimeoutSec"] %>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] && !Utils.NullOrZero(((DataRowView)Container.DataItem)["NopBasePol"]) ? (this.IsQuotingInActivityTimeoutRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["LiquidationSpreadBp"], 2, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] && !Utils.NullOrZero(((DataRowView)Container.DataItem)["NopBasePol"]) ? (this.IsQuotingInActivityTimeoutRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# Utils.FormatDecimalNumber(((DataRowView)Container.DataItem)["LiquidationSpreadDecimal"], 5, Utils.NullsHandling.DbNullToEmpty) %>  
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# ((DataRowView)Container.DataItem)["LmaxTickerLookbackSec"] %>
                    </td> 
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumLmaxTickerVolumeUsd"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# ((bool) ((DataRowView)Container.DataItem)["Enabled"] && Utils.NotNullOrFalse(((DataRowView)Container.DataItem)["CalculatedFromFullInterval"])) ? string.Format("{0:#,0}", ((DataRowView)Container.DataItem)["CurrentLmaxTickerVolumeUsd"]) : string.Empty %>
                    </td>     
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? this.GetQuotingPaidFlowPctBackground((DataRowView)Container.DataItem) : string.Empty %>'>
                            <%# ((bool) ((DataRowView)Container.DataItem)["Enabled"] && Utils.NotNullOrFalse(((DataRowView)Container.DataItem)["CalculatedFromFullInterval"]) && !Utils.NullOrZero(((DataRowView)Container.DataItem)["CurrentLmaxTickerVolumeUsd"])) ? string.Format("{0:F0}", (decimal)((DataRowView)Container.DataItem)["CurrentLmaxTickerPaidVolumeRatio"] * 100m) : string.Empty %>
                    </td>    
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumLmaxTickerVolumeUsd"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MinimumLmaxTickerAvgDealSizeUsd"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# ((bool) ((DataRowView)Container.DataItem)["Enabled"] && Utils.NotNullOrFalse(((DataRowView)Container.DataItem)["CalculatedFromFullInterval"])) ? string.Format("{0:#,0}", ((DataRowView)Container.DataItem)["CurrentLmaxTickerAvgDealSizeUsd"]) : string.Empty %>
                    </td>
                    <td class='<%# (bool) ((DataRowView)Container.DataItem)["Enabled"] ? (this.IsQuotingInLTBandRestrictionMode((DataRowView)Container.DataItem) ? "quotingInLiquidation" : "quotingUnrestricted") : string.Empty %>'>
                            <%# Utils.FormatLargeNumber(((DataRowView)Container.DataItem)["MaximumLmaxTickerAvgDealSizeUsd"], Utils.NullsHandling.DbNullToEmpty) %>
                    </td>
                    
                    </uc1:DataPlaceHolder>

                </tr>
            </uc1:DataPlaceHolder>
            
        </ItemTemplate>
        
        <FooterTemplate>
            </tbody>
            </table> 
            </div>
        </FooterTemplate>

    </asp:repeater>