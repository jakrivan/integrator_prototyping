﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common.CommandTrees;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public static class SqlDataReaderExtensions
    {
        public static DateTime GetDateTimeMayBeNull(this SqlDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? (DateTime)SqlDateTime.MinValue : reader.GetDateTime(ordinal);
        }
    }

    public partial class InsertEditSystem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.VenueSettingsWithPnl.DisableAutoRefresh();
            this.PopulateEditBox();
        }

        protected TradingSystemType TradingSystemType { get { return (TradingSystemType)Enum.Parse(typeof(TradingSystemType), Request.QueryString["TradingSystemType"]); } }
        protected VenueCounterparty VenueCounterparty { get { return (VenueCounterparty)Enum.Parse(typeof(VenueCounterparty), Request.QueryString["VenueCounterparty"]); } }

        protected int? _tradingGroupId;

        private enum ActionType
        {
            Insert,
            Edit,
            Unknown
        }

        private ActionType CurrentActionType { get { return (ActionType)Enum.Parse(typeof(ActionType), Request.QueryString["Action"]); } }

        private void PopulateEditBox()
        {
            this.CancelButton.PostBackUrl = "~/SystemDetails.aspx" +
                                            this.VenueSettingsWithPnl.GetQueryStringWithNoAction();

            string groupName = null;
            if (Utils.TryParseIntIntToNullable(Request.QueryString["GroupId"], out _tradingGroupId))
            {
                groupName = GlobalState.GetGroupName(_tradingGroupId.Value);
            }
            groupName = groupName ?? Server.HtmlEncode("<All groups - unfiltered>");
            groupName = Utils.GetUatPrefix(Session) + groupName;
            groupName = groupName.Replace(" ", "&nbsp;");
            string venueCtp = this.TradingSystemType.ToString().Replace(" ", "&nbsp;");

            GroupNameLiteral.Text = groupName;
            if (!_tradingGroupId.HasValue)
            {
                this.ReportError("Systems can be inserted only on a view filtered by trading group");
            }

            VenueTypeLiteral.Text = this.VenueCounterparty.ToString();
            TradingTypeLiteral.Text = venueCtp;

            switch (this.CurrentActionType)
            {
                case ActionType.Insert:
                    ActionTypeLiteral.Text = "INSERT new system";
                    break;
                case ActionType.Edit:
                    ActionTypeLiteral.Text = "EDIT system id " + Request.QueryString["TradingSystemId"];
                    this.EnableCheckBox.Visible = false;
                    this.InsertButton.Text = "Save";
                    break;
                case ActionType.Unknown:
                default:
                    ActionTypeLiteral.Text = "UNKNOWN ACTION";
                    break;
            }

            this.CrossPlaceholder.Visible = this.TradingSystemType == TradingSystemType.Cross || this.TradingSystemType == TradingSystemType.SmartCross;
            this.FirmCrossPlaceholder.Visible = this.TradingSystemType == TradingSystemType.Cross;
            this.SmartCrossPlaceholder.Visible = this.TradingSystemType == TradingSystemType.SmartCross;
            this.MMPlaceHolder.Visible = this.TradingSystemType == TradingSystemType.MM;
            this.StreamPlaceHolder.Visible = this.TradingSystemType == TradingSystemType.Stream || this.TradingSystemType == TradingSystemType.Glider;
            this.StreamPlaceHolder2.Visible = this.TradingSystemType == TradingSystemType.Stream;
            this.StreamGlidePlaceholder.Visible = this.TradingSystemType == TradingSystemType.Glider;
            //this.HotspotPlaceholder.Visible = Utils.VenueTypeString(this.VenueCounterparty) == "Hotspot";
            //this.NonHotspotPlaceholder.Visible = Utils.VenueTypeString(this.VenueCounterparty) != "Hotspot";
            this.QuotingPlaceHolder.Visible = this.TradingSystemType == TradingSystemType.Quoting;

            this.PopulateDataOnEdit();
            this.PopulateDataOnInsert();

            if (Utils.VenueTypeString(this.VenueCounterparty) == "LMAX")
            {
                string minFillSize = this.VenueCounterparty == VenueCounterparty.LM2 ? "10000" : "1000";

                (this.InsertNewPanel.FindControl("MinimumFileSizeCrossTxt") as TextBox).Enabled = false;
                (this.InsertNewPanel.FindControl("MinimumFileSizeCrossTxt") as TextBox).Text = minFillSize;
                (this.InsertNewPanel.FindControl("MinimumFileSizeMmTxt") as TextBox).Enabled = false;
                (this.InsertNewPanel.FindControl("MinimumFileSizeMmTxt") as TextBox).Text = minFillSize;
                (this.InsertNewPanel.FindControl("MinimumFileSizeStreamTxt") as TextBox).Enabled = false;
                (this.InsertNewPanel.FindControl("MinimumFileSizeStreamTxt") as TextBox).Text = minFillSize;
            }

            bool addingAll = int.Parse(Request.QueryString["SymbolsNum"]) > 1;
            string symbol = addingAll ? _addingAllText : Request.QueryString["Symbols"];
            symbolLbl.Text = Utils.AddSlashToSymbol(symbol);

            if (!addingAll)
            {
                string unsupportedCurrency;
                if (GlobalState.HasSymbolUnsupportedCurrency(symbol, out unsupportedCurrency))
                {
                    this.ReportError(string.Format("Currency {0} not supported by PB", unsupportedCurrency));
                    return;
                }
            }
        }

        private string _addingAllText = "All unconfigured symbols";

        private void PopulateStreamData(SqlDataReader reader)
        {
            this.MinBpToAccept.Text =
                Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumBPGrossToAccept")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
        }

        private void PopulateGliderData(SqlDataReader reader)
        {
            this.MinBpGainTxt.Text =
                Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumBPGrossGain")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.MinBpGainIfZeroFillTxt.Text =
                Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumBPGrossGainIfZeroGlideFill")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.GlideTTLTxt.Text = reader.GetInt32(reader.GetOrdinal("MinimumIntegratorGlidePriceLife_milliseconds")).ToString();
            if (this.CurrentActionType == ActionType.Edit)
                this.GlideCutoffTxt.Text = reader.GetInt32(reader.GetOrdinal("GlidingCutoffSpan_milliseconds")).ToString();
            this.GlideOnDropDown.SelectedValue =
                Utils.BackendGlidingCounterpartyToUi(reader.GetString(reader.GetOrdinal("GlidingCounterparty")));
        }


        private void PopulateStreamAndGliderData(SqlDataReader reader)
        {
            //(this.InsertNewPanel.FindControl("StreamMaximumSizeTxt") as TextBox).Text
            this.StreamMaximumSizeTxt.Text = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade")).ToString();
            this.StreamMaxDiscountTxt.Text =
                Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MaximumDiscountBasisPointsToTrade")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.StreamOffsetBpTxt.Text =
                Utils.FormatDecimalNumber(
                    reader.GetDecimal(reader.GetOrdinal("BestPriceImprovementOffsetBasisPoints")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.LLTime.Text = reader.GetInt32(reader.GetOrdinal("KGTLLTime_milliseconds")).ToString();
            this.MinSoftPriceLifeTxt.Text = reader.GetInt32(reader.GetOrdinal("MinimumIntegratorPriceLife_milliseconds")).ToString();
            this.MinimumSizeToTradeStreamTxt.Text =
                reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade")).ToString();
            this.MinimumFileSizeStreamTxt.Text = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize")).ToString();
            this.MaxSourceTiersStreamTxt.Text = reader.GetInt32(reader.GetOrdinal("MaximumSourceBookTiers")).ToString();
            this.MinBpToFastCxlStreamTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumBasisPointsFromKgtPriceToFastCancel")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.RefLmaxOnlyChkbx.Checked = reader.GetBoolean(reader.GetOrdinal("ReferenceLmaxBookOnly"));
            this.SendBidsStreamChkBx.Checked = reader.GetBoolean(reader.GetOrdinal("BidEnabled"));
            this.SendAsksStreamChkBx.Checked = reader.GetBoolean(reader.GetOrdinal("AskEnabled"));

            int rtTime =
                (int)Math.Ceiling(2 * StreamingSettingsDal.Instance.GetExpectedRTHalfTime(this.VenueCounterparty).TotalMilliseconds);
            this.LLRTTimeLbl.Text = (reader.GetInt32(reader.GetOrdinal("KGTLLTime_milliseconds")) + rtTime).ToString();
            int rtMax =
                (int) StreamingSettingsDal.Instance.GetMaxLLTimeOnDestination(this.VenueCounterparty).TotalMilliseconds;
            this.LLRTMaxTimeLbl.Text = rtMax.ToString();
            
            int maxLLValue =
                (int) StreamingSettingsDal.Instance.GetMaxLLTimeOnDestination(this.VenueCounterparty).TotalMilliseconds -
                rtTime - 3;
            this.MaxLLInternalValue.Text = maxLLValue.ToString();
            this.LLCompareValidator.ErrorMessage =
                string.Format("LLTime must be less or equal to {0} (RTMax ({1}) - RT time ({2}) - 3 ms)", maxLLValue,
                    rtMax, rtTime);

            this.LlCheckLmaxChck.Checked = reader.GetBoolean(reader.GetOrdinal("PreferLLDestinationCheckToLmax"));

            this.PreferLmaxHedgeChck.Checked = reader.GetBoolean(reader.GetOrdinal("PreferLmaxDuringHedging"));
            if (this.TradingSystemType == TradingSystemType.Glider)
                this.PreferLmaxHedgeChck.Text = "Prefer LMAX execution after LL-time";
        }

        private void PopulateCrossData(SqlDataReader reader)
        {
            this.MinimumSizeToTradeTxt.Text = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade")).ToString();
            this.MaximumSizeToTradeTxt.Text = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade")).ToString();
            this.MinDiscountBpTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumDiscountBasisPointsToTrade")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.MinTimeTxt.Text =
                Utils.FormatDecimalNumber(
                    reader.GetDecimal(reader.GetOrdinal("MinimumTimeBetweenSingleCounterpartySignals_seconds")), 1,
                    Utils.NullsHandling.DbNullToEmpty);
            this.MinimumFileSizeCrossTxt.Text = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize")).ToString();
        }

        private void PopulateFirmCrossData(SqlDataReader reader)
        {
            this.MaxWaitImprovementTxtCross.Text =
                reader.GetInt32(reader.GetOrdinal("MaximumWaitTimeOnImprovementTick_milliseconds")).ToString();
        }

        private void PopulateSmartCrossData(SqlDataReader reader)
        {
            this.CoverDistanceGrossTxt.Text = Utils.FormatDecimalNumber(
                reader.GetDecimal(reader.GetOrdinal("CoverDistanceGrossBasisPoints")), 2,
                Utils.NullsHandling.DbNullToEmpty);
            this.MinTrueGainGrossTxt.Text = Utils.FormatDecimalNumber(
                reader.GetDecimal(reader.GetOrdinal("MinimumTrueGainGrossBasisPoints")), 2,
                Utils.NullsHandling.DbNullToEmpty);
            this.CoverTimeourTxt.Text =
                reader.GetInt32(reader.GetOrdinal("CoverTimeout_milliseconds")).ToString();
        }

        private void PopulateMMData(SqlDataReader reader, bool isEdit)
        {
            this.MaximumSizeTxt.Text = reader.GetDecimal(reader.GetOrdinal("MaximumSizeToTrade")).ToString();
            this.MaxDiscountTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MaximumDiscountBasisPointsToTrade")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.OffsetBpTxt.Text = Utils.FormatDecimalNumber(
                reader.GetDecimal(reader.GetOrdinal("BestPriceImprovementOffsetBasisPoints")), 2,
                Utils.NullsHandling.DbNullToEmpty);
            if(isEdit)
                this.MaxWaitImprovementTxtMm.Text =
                    reader.GetInt32(reader.GetOrdinal("MaximumWaitTimeOnImprovementTick_milliseconds")).ToString();
            this.MinFirmPriceLifeTxt.Text =
                reader.GetInt32(reader.GetOrdinal("MinimumIntegratorPriceLife_milliseconds")).ToString();
            this.MinimumSizeToTradeMmTxt.Text = reader.GetDecimal(reader.GetOrdinal("MinimumSizeToTrade")).ToString();
            this.MinimumFileSizeMmTxt.Text = reader.GetDecimal(reader.GetOrdinal("MinimumFillSize")).ToString();
            this.MaxSourceTiersMmTxt.Text = reader.GetInt32(reader.GetOrdinal("MaximumSourceBookTiers")).ToString();
            this.MinBpToFastCxlMmTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumBasisPointsFromKgtPriceToFastCancel")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.SendBidsMmChkBx.Checked = reader.GetBoolean(reader.GetOrdinal("BidEnabled"));
            this.SendAsksMmChkBx.Checked = reader.GetBoolean(reader.GetOrdinal("AskEnabled"));
        }

        private void PopulateQuotingData(SqlDataReader reader)
        {
            this.MaxSizeTxt.Text = reader.GetDecimal(reader.GetOrdinal("MaximumPositionBaseAbs")).ToString();
            this.SpreadTypeDropDown.SelectedValue = reader.GetString(reader.GetOrdinal("SpreadType"));
            this.FixedSpreadBpTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("FixedSpreadBasisPoints")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.MinDynamicSpreadBpTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("MinimumDynamicSpreadBp")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.DynamicMarkupDecimalTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("DynamicMarkupDecimal")), 6,
                    Utils.NullsHandling.DbNullToEmpty);
            this.SkewEnabledChck.Checked = reader.GetBoolean(reader.GetOrdinal("EnableSkew"));
            this.SkewDecimalTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("SkewDecimal")), 6,
                    Utils.NullsHandling.DbNullToEmpty);
            this.StopLossTxt.Text = reader.GetInt64(reader.GetOrdinal("StopLossNetInUsd")).ToString();
            this.InactivityTimeoutSecTxt.Text = reader.GetInt32(reader.GetOrdinal("InactivityTimeoutSec")).ToString();
            this.LiquidationSpreadBpTxt.Text = Utils.FormatDecimalNumber(reader.GetDecimal(reader.GetOrdinal("LiquidationSpreadBp")), 2,
                    Utils.NullsHandling.DbNullToEmpty);
            this.LtLookBackSecTxt.Text = reader.GetInt32(reader.GetOrdinal("LmaxTickerLookbackSec")).ToString();
            this.MinLtVolumeTxt.Text = reader.GetInt64(reader.GetOrdinal("MinimumLmaxTickerVolumeUsd")).ToString();
            this.MaxLtVolumeTxt.Text = reader.GetInt64(reader.GetOrdinal("MaximumLmaxTickerVolumeUsd")).ToString();
            this.MinLtAvgDealSize.Text = reader.GetInt64(reader.GetOrdinal("MinimumLmaxTickerAvgDealSizeUsd")).ToString();
            this.MaxLtAvgDealSize.Text = reader.GetInt64(reader.GetOrdinal("MaximumLmaxTickerAvgDealSizeUsd")).ToString();
        }


        private void PopulateDataOnInsert()
        {
            if (this.CurrentActionType != ActionType.Insert || this.IsPostBack)
                return;

            //GetUpdatedVenueStreamSystemSettings_SP
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];
            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        string commandText = string.Format("[dbo].[GetLastVenue{0}Settings_SP]",
                            this.TradingSystemType);
                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                switch (this.TradingSystemType)
                                {
                                    case TradingSystemType.Cross:
                                        this.PopulateCrossData(reader);
                                        this.PopulateFirmCrossData(reader);
                                        break;
                                    case TradingSystemType.SmartCross:
                                        this.PopulateCrossData(reader);
                                        this.PopulateSmartCrossData(reader);
                                        break;
                                    case TradingSystemType.MM:
                                        this.PopulateMMData(reader, false);
                                        break;
                                    case TradingSystemType.Quoting:
                                        this.PopulateQuotingData(reader);
                                        break;
                                    case TradingSystemType.Stream:
                                        this.PopulateStreamAndGliderData(reader);
                                        this.PopulateStreamData(reader);
                                        break;
                                    case TradingSystemType.Glider:
                                        this.PopulateStreamAndGliderData(reader);
                                        this.PopulateGliderData(reader);
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException("TradingSystemType");
                                }
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                this.ReportError("Connection string is missing");
            }
        }

        private void PopulateDataOnEdit()
        {
            if (this.CurrentActionType != ActionType.Edit || this.IsPostBack)
                return;

            //GetUpdatedVenueStreamSystemSettings_SP
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];
            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        string commandText = string.Format("[dbo].[GetUpdatedVenue{0}SystemSettings_SP]",
                            this.TradingSystemType);
                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@LastUpdateTimeUtc", SqlDateTime.MinValue);
                        command.Parameters.AddWithValue("@TradingSystemId",
                            int.Parse(Request.QueryString["TradingSystemId"]));
                        command.Parameters.Add(new SqlParameter("@LastUpdateTimeServerSideUtc",
                            SqlDbType.DateTime2)
                        {
                            Direction = ParameterDirection.Output
                        });

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            reader.Read();

                            
                            this.HiddenLastUpdatedUtc.Value =
                                reader.GetDateTimeMayBeNull(reader.GetOrdinal("LastUpdatedUtc"))
                                    .ToString("yyyy-MM-dd HH:mm:ss.ffffff");

                            switch (this.TradingSystemType)
                            {
                                case TradingSystemType.Cross:
                                    this.PopulateCrossData(reader);
                                    this.PopulateFirmCrossData(reader);
                                    break;
                                case TradingSystemType.SmartCross:
                                    this.PopulateCrossData(reader);
                                    this.PopulateSmartCrossData(reader);
                                    break;
                                case TradingSystemType.MM:
                                    this.PopulateMMData(reader, true);
                                    break;
                                case TradingSystemType.Quoting:
                                    this.PopulateQuotingData(reader);
                                    break;
                                case TradingSystemType.Stream:
                                    this.PopulateStreamAndGliderData(reader);
                                    this.PopulateStreamData(reader);
                                    break;
                                case TradingSystemType.Glider:
                                    this.PopulateStreamAndGliderData(reader);
                                    this.PopulateGliderData(reader);
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException("TradingSystemType");
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                this.ReportError("Connection string is missing");
            }
        }

        //protected void OnSymbolCommand(object sender, CommandEventArgs e)
        //{
        //    if (e.CommandName == "InsertNewConfirmed" && Page.IsValid)
        //    {
        //        //this.InsertNewPanel.Visible = false;
        //        //this.FadingPanel.Visible = false;
        //        if(this.OnInsertEditConfirmed())
        //            Response.Redirect("~/SystemDetails.aspx" +
        //                                    this.VenueSettingsWithPnl.GetQueryStringWithNoAction());
        //    }
        //}

        protected void InsertButton_OnClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //this.InsertNewPanel.Visible = false;
                //this.FadingPanel.Visible = false;
                if (this.OnInsertEditConfirmed())
                    Response.Redirect("~/SystemDetails.aspx" +
                                            this.VenueSettingsWithPnl.GetQueryStringWithNoAction());
            }
        }

        private TradingSystemInfoBagBase CollectInsertInfoBag(Control item)
        {
            DateTime lastUpdated = DateTime.MinValue;
            DateTime.TryParse(HiddenLastUpdatedUtc.Value, out lastUpdated);
            int tradingSystemId = -1;
            if (!string.IsNullOrEmpty(Request.QueryString["TradingSystemId"]))
                tradingSystemId = int.Parse(Request.QueryString["TradingSystemId"]);
            bool enabled = (item.FindControl("EnableCheckBox") as CheckBox).Checked;

            if (this.TradingSystemType == TradingSystemType.Cross || this.TradingSystemType == TradingSystemType.SmartCross)
            {
                decimal minSizeToTrade = decimal.Parse((item.FindControl("MinimumSizeToTradeTxt") as TextBox).Text);
                decimal maxSizeToTrade = decimal.Parse((item.FindControl("MaximumSizeToTradeTxt") as TextBox).Text);
                decimal minDiscBp = decimal.Parse((item.FindControl("MinDiscountBpTxt") as TextBox).Text);
                decimal minTimeBetweenSignals = decimal.Parse((item.FindControl("MinTimeTxt") as TextBox).Text);
                decimal minimumFillSize = decimal.Parse((item.FindControl("MinimumFileSizeCrossTxt") as TextBox).Text);

                if (this.TradingSystemType == TradingSystemType.Cross)
                {
                    int maxWaitTime = int.Parse((item.FindControl("MaxWaitImprovementTxtCross") as TextBox).Text);

                    return new FirmCrossTradingSystemInfoBag(
                        tradingSystemId, this.VenueCounterparty.ToString(), lastUpdated, enabled, minSizeToTrade,
                        maxSizeToTrade, minDiscBp, minTimeBetweenSignals, maxWaitTime, minimumFillSize)
                    {
                        SymbolString = symbolLbl.Text
                    };
                }
                else if (this.TradingSystemType == TradingSystemType.SmartCross)
                {
                    decimal coverDistanceGross = decimal.Parse((item.FindControl("CoverDistanceGrossTxt") as TextBox).Text);
                    decimal minTrueGainGross = decimal.Parse((item.FindControl("MinTrueGainGrossTxt") as TextBox).Text);
                    int coverTimeourMs = int.Parse((item.FindControl("CoverTimeourTxt") as TextBox).Text);

                    return new SmartCrossTradingSystemInfoBag(
                        tradingSystemId, this.VenueCounterparty.ToString(), lastUpdated, enabled, minSizeToTrade,
                        maxSizeToTrade, minDiscBp, minTimeBetweenSignals, minimumFillSize, coverDistanceGross,
                        coverTimeourMs, minTrueGainGross)
                    {
                        SymbolString = symbolLbl.Text
                    };
                }
            }
            else if (this.TradingSystemType == TradingSystemType.MM)
            {
                decimal sizeToTrade = decimal.Parse((item.FindControl("MaximumSizeTxt") as TextBox).Text);
                decimal maxDiscBp = decimal.Parse((item.FindControl("MaxDiscountTxt") as TextBox).Text);
                decimal bestPriceOffset = decimal.Parse((item.FindControl("OffsetBpTxt") as TextBox).Text);
                int maxWaitTime = int.Parse((item.FindControl("MaxWaitImprovementTxtMm") as TextBox).Text);
                int priceLifeTime = int.Parse((item.FindControl("MinFirmPriceLifeTxt") as TextBox).Text);
                decimal minimumSizeToTrade = decimal.Parse((item.FindControl("MinimumSizeToTradeMmTxt") as TextBox).Text);
                decimal minimumFillSize = decimal.Parse((item.FindControl("MinimumFileSizeMmTxt") as TextBox).Text);
                int maxSourceBookTiers = int.Parse((item.FindControl("MaxSourceTiersMmTxt") as TextBox).Text);
                decimal minBpToCxl = decimal.Parse((item.FindControl("MinBpToFastCxlMmTxt") as TextBox).Text);
                bool bidEnabled = (item.FindControl("SendBidsMmChkBx") as CheckBox).Checked;
                bool askEnabled = (item.FindControl("SendAsksMmChkBx") as CheckBox).Checked;

                return new MMTradingSystemInfoBag(
                    tradingSystemId, this.VenueCounterparty.ToString(), lastUpdated, enabled, sizeToTrade,
                    bestPriceOffset, maxDiscBp, maxWaitTime, priceLifeTime,
                    minimumSizeToTrade, minimumFillSize, maxSourceBookTiers, minBpToCxl, bidEnabled, askEnabled) { SymbolString = symbolLbl.Text };
            }
            else if (this.TradingSystemType == TradingSystemType.Stream || this.TradingSystemType == TradingSystemType.Glider)
            {
                decimal sizeToTrade = decimal.Parse((item.FindControl("StreamMaximumSizeTxt") as TextBox).Text);
                decimal maxDiscBp = decimal.Parse((item.FindControl("StreamMaxDiscountTxt") as TextBox).Text);
                decimal bestPriceOffset = decimal.Parse((item.FindControl("StreamOffsetBpTxt") as TextBox).Text);
                int llTime = int.Parse((item.FindControl("LLTime") as TextBox).Text);
                int priceLifeTime = int.Parse((item.FindControl("MinSoftPriceLifeTxt") as TextBox).Text);
                decimal minimumSizeToTrade = decimal.Parse((item.FindControl("MinimumSizeToTradeStreamTxt") as TextBox).Text);
                decimal minimumFillSize = decimal.Parse((item.FindControl("MinimumFileSizeStreamTxt") as TextBox).Text);
                int maxSourceBookTiers = int.Parse((item.FindControl("MaxSourceTiersStreamTxt") as TextBox).Text);
                decimal minBpToCxl = decimal.Parse((item.FindControl("MinBpToFastCxlStreamTxt") as TextBox).Text);
                bool referenceLmaxOnly = (item.FindControl("RefLmaxOnlyChkbx") as CheckBox).Checked;
                bool bidEnabled = (item.FindControl("SendBidsStreamChkBx") as CheckBox).Checked;
                bool askEnabled = (item.FindControl("SendAsksStreamChkBx") as CheckBox).Checked;
                bool llDestLmax = (item.FindControl("LlCheckLmaxChck") as CheckBox).Checked;
                bool preferLmaxHedging = (item.FindControl("PreferLmaxHedgeChck") as CheckBox).Checked;

                if (this.TradingSystemType == TradingSystemType.Stream)
                {
                    decimal minBpToAccept = decimal.Parse((item.FindControl("MinBpToAccept") as TextBox).Text);
                    

                    return new StreamTradingSystemInfoBag(
                        tradingSystemId, this.VenueCounterparty.ToString(), lastUpdated, enabled, sizeToTrade,
                        bestPriceOffset, maxDiscBp, llTime, llDestLmax, minBpToAccept, preferLmaxHedging, priceLifeTime,
                        minimumSizeToTrade, minimumFillSize, maxSourceBookTiers, minBpToCxl, referenceLmaxOnly,
                        bidEnabled, askEnabled) {SymbolString = symbolLbl.Text};
                }
                else if (this.TradingSystemType == TradingSystemType.Glider)
                {
                    decimal minBpGain = decimal.Parse((item.FindControl("MinBpGainTxt") as TextBox).Text);
                    decimal minBpGainIfZero = decimal.Parse((item.FindControl("MinBpGainIfZeroFillTxt") as TextBox).Text);
                    int glideTTL = int.Parse((item.FindControl("GlideTTLTxt") as TextBox).Text);
                    int glidingCutoff = int.Parse((item.FindControl("GlideCutoffTxt") as TextBox).Text);
                    string glidingCounterparty = Utils.UiGlidingCounterpartyToBackend((item.FindControl("GlideOnDropDown") as DropDownList).SelectedValue);

                    return new GliderTradingSystemInfoBag(
                        tradingSystemId, this.VenueCounterparty.ToString(), lastUpdated, enabled, sizeToTrade,
                        bestPriceOffset, maxDiscBp, llTime, priceLifeTime,
                        minimumSizeToTrade, minimumFillSize, maxSourceBookTiers, minBpToCxl, referenceLmaxOnly,
                        llDestLmax, preferLmaxHedging, bidEnabled, askEnabled, minBpGain, minBpGainIfZero, glideTTL, 
                        glidingCutoff, glidingCounterparty) 
                        { SymbolString = symbolLbl.Text };
                }
            }
            else if (this.TradingSystemType == TradingSystemType.Quoting)
            {
                decimal maxSizeToTrade = decimal.Parse((item.FindControl("MaxSizeTxt") as TextBox).Text);
                string spreadType = (item.FindControl("SpreadTypeDropDown") as DropDownList).SelectedValue;
                decimal fixedSpreadBp = decimal.Parse((item.FindControl("FixedSpreadBpTxt") as TextBox).Text);
                decimal minDynamicSpread = decimal.Parse((item.FindControl("MinDynamicSpreadBpTxt") as TextBox).Text);
                decimal dynamicMarkupDecimal = decimal.Parse((item.FindControl("DynamicMarkupDecimalTxt") as TextBox).Text);
                bool skewEnabled = (item.FindControl("SkewEnabledChck") as CheckBox).Checked;
                decimal skewDecimal = decimal.Parse((item.FindControl("SkewDecimalTxt") as TextBox).Text);
                long stopLoss = long.Parse((item.FindControl("StopLossTxt") as TextBox).Text);

                int inactivityTimeoutSec = int.Parse((item.FindControl("InactivityTimeoutSecTxt") as TextBox).Text);
                decimal liquidationSpreadBp = decimal.Parse((item.FindControl("LiquidationSpreadBpTxt") as TextBox).Text);
                int lmaxTickerLookbackSec = int.Parse((item.FindControl("LtLookBackSecTxt") as TextBox).Text);
                long minimumLmaxTickerVolumeUsd = long.Parse((item.FindControl("MinLtVolumeTxt") as TextBox).Text);
                long maximumLmaxTickerVolumeUsd = long.Parse((item.FindControl("MaxLtVolumeTxt") as TextBox).Text);
                long minimumLmaxTickerAvgDealSizeUsd = long.Parse((item.FindControl("MinLtAvgDealSize") as TextBox).Text);
                long maximumLmaxTickerAvgDealSizeUsd = long.Parse((item.FindControl("MaxLtAvgDealSize") as TextBox).Text);

                return new QuotingTradingSystemInfoBag(
                    tradingSystemId, this.VenueCounterparty.ToString(), lastUpdated, enabled, maxSizeToTrade,
                    spreadType, fixedSpreadBp, minDynamicSpread, dynamicMarkupDecimal, skewEnabled, skewDecimal, stopLoss,
                    inactivityTimeoutSec, liquidationSpreadBp, lmaxTickerLookbackSec, minimumLmaxTickerVolumeUsd,
                    maximumLmaxTickerVolumeUsd, minimumLmaxTickerAvgDealSizeUsd, maximumLmaxTickerAvgDealSizeUsd) { SymbolString = symbolLbl.Text };
            }

            throw new ArgumentOutOfRangeException();
        }

        protected bool OnInsertEditConfirmed()
        {
            TradingSystemInfoBagBase tradingSystemInfoBag = this.CollectInsertInfoBag(this.InsertNewPanel);
            bool insertAll = tradingSystemInfoBag.SymbolString.Equals(_addingAllText, StringComparison.OrdinalIgnoreCase);

            if (this.CurrentActionType == ActionType.Edit)
            {
                return this.UpdateOne(tradingSystemInfoBag);
            }
            else if (insertAll)
            {
                int addedSystemsCnt = 0;
                bool failed = false;
                foreach (
                    string availableSymbol in
                        Request.QueryString["Symbols"].Split(new char[] {','}).Select(Utils.AddSlashToSymbol))
                {
                    tradingSystemInfoBag.SymbolString = availableSymbol;
                    if (this.InsertOne(tradingSystemInfoBag).HasValue)
                        addedSystemsCnt++;
                    else
                        failed = true;
                }

                return !failed;
            }
            else
            {
                return this.InsertOne(tradingSystemInfoBag).HasValue;
            }
        }

        private bool CanInsertSystem()
        {
            int pageId;

            return
                _tradingGroupId.HasValue
                && Request.QueryString["PageId"] != null
                && int.TryParse(Request.QueryString["PageId"], out pageId)
                && this.DoPageExist(_tradingGroupId.Value, pageId);
        }

        private bool DoPageExist(int groupId, int pageId)
        {
            try
            {
                ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

                if (connectionString != null)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("[dbo].[TradingSystemPageExists_SP]", connection);
                        command.Parameters.AddWithValue("@GroupId", groupId);
                        command.Parameters.AddWithValue("@PageId", pageId);
                        command.CommandType = CommandType.StoredProcedure;
                        var val = command.ExecuteScalar();
                        if (val != DBNull.Value && val is bool && val.Equals(true))
                            return true;
                    }
                }
                else
                {
                    this.ReportError("Connection string is missing");
                }
            }
            catch (Exception e)
            {
                this.ReportException(e);
            }

            return false;
        }

        private enum CommandActionType
        {
            Insert,
            Update
        }

        private SqlCommand PrepareCommand(CommandActionType commandActionType, TradingSystemInfoBagBase tradingSystemInfoBag, SqlConnection connection)
        {
            SqlCommand command;

            command = new SqlCommand(string.Format("[dbo].[Venue{0}Settings{1}Single_SP]", this.TradingSystemType, commandActionType), connection);
            command.CommandType = CommandType.StoredProcedure;

            if (this.TradingSystemType == TradingSystemType.Cross || this.TradingSystemType == TradingSystemType.SmartCross)
            {
                CrossTradingSystemInfoBagBase crossTradingSystemInfoBag =
                    tradingSystemInfoBag as CrossTradingSystemInfoBagBase;
                command.Parameters.AddWithValue("@MinimumSizeToTrade", crossTradingSystemInfoBag.MinimumSizeToTrade);
                command.Parameters.AddWithValue("@MaximumSizeToTrade", crossTradingSystemInfoBag.MaximumSizeToTrade);
                command.Parameters.AddWithValue("@MinimumDiscountBasisPointsToTrade", crossTradingSystemInfoBag.MinimumDiscountBasisPointsToTrade);
                command.Parameters.AddWithValue("@MinimumTimeBetweenSingleCounterpartySignals_seconds", crossTradingSystemInfoBag.MinimumTimeBetweenSingleCounterpartySignals_seconds);
                command.Parameters.AddWithValue("@MinimumFillSize", crossTradingSystemInfoBag.MinimumFillSize);

                if (this.TradingSystemType == TradingSystemType.Cross)
                {
                    FirmCrossTradingSystemInfoBag firmCrossTradingSystemInfoBag =
                        tradingSystemInfoBag as FirmCrossTradingSystemInfoBag;

                    command.Parameters.AddWithValue("@MaximumWaitTimeOnImprovementTick_milliseconds", firmCrossTradingSystemInfoBag.MaximumWaitTimeOnImprovementTick_milliseconds);
                }
                else if (this.TradingSystemType == TradingSystemType.SmartCross)
                {
                    SmartCrossTradingSystemInfoBag smartCrossTradingSystemInfoBag =
                        tradingSystemInfoBag as SmartCrossTradingSystemInfoBag;

                    command.Parameters.AddWithValue("@CoverDistanceGrossBasisPoints", smartCrossTradingSystemInfoBag.CoverDistanceGrossBasisPoints);
                    command.Parameters.AddWithValue("@CoverTimeout_milliseconds", smartCrossTradingSystemInfoBag.CoverTimeout_milliseconds);
                    command.Parameters.AddWithValue("@MinimumTrueGainGrossBasisPoints", smartCrossTradingSystemInfoBag.MinimumTrueGainGrossBasisPoints);
                }
                
            }
            else if (this.TradingSystemType == TradingSystemType.MM)
            {
                MMTradingSystemInfoBag mmTradingSystemInfoBag =
                    tradingSystemInfoBag as MMTradingSystemInfoBag;
                command.Parameters.AddWithValue("@MaximumSizeToTrade", mmTradingSystemInfoBag.MaximumSizeToTrade);
                command.Parameters.AddWithValue("@BestPriceImprovementOffsetBasisPoints", mmTradingSystemInfoBag.BestPriceImprovementOffsetBasisPoints);
                command.Parameters.AddWithValue("@MaximumDiscountBasisPointsToTrade", mmTradingSystemInfoBag.MaximumDiscountBasisPointsToTrade);
                command.Parameters.AddWithValue("@MaximumWaitTimeOnImprovementTick_milliseconds", mmTradingSystemInfoBag.MaximumWaitTimeOnImprovementTick_milliseconds);
                command.Parameters.AddWithValue("@MinimumIntegratorPriceLife_milliseconds", mmTradingSystemInfoBag.MinimumIntegratorPriceLife_milliseconds);
                command.Parameters.AddWithValue("@MinimumSizeToTrade", mmTradingSystemInfoBag.MinimumSizeToTrade);
                command.Parameters.AddWithValue("@MinimumFillSize", mmTradingSystemInfoBag.MinimumFillSize);
                command.Parameters.AddWithValue("@MaximumSourceBookTiers", mmTradingSystemInfoBag.MaximumSourceBookTiers);
                command.Parameters.AddWithValue("@MinimumBasisPointsFromKgtPriceToFastCancel", mmTradingSystemInfoBag.MinimumBasisPointsFromKgtPriceToFastCancel);
                command.Parameters.AddWithValue("@BidEnabled", mmTradingSystemInfoBag.BidEnabled);
                command.Parameters.AddWithValue("@AskEnabled", mmTradingSystemInfoBag.AskEnabled);
            }
            else if (this.TradingSystemType == TradingSystemType.Stream || this.TradingSystemType == TradingSystemType.Glider)
            {
                StreamTradingSystemInfoBagBase streamAndGliderTradingSystemInfoBag =
                    tradingSystemInfoBag as StreamTradingSystemInfoBagBase;
                command.Parameters.AddWithValue("@MaximumSizeToTrade", streamAndGliderTradingSystemInfoBag.MaximumSizeToTrade);
                command.Parameters.AddWithValue("@BestPriceImprovementOffsetBasisPoints", streamAndGliderTradingSystemInfoBag.BestPriceImprovementOffsetBasisPoints);
                command.Parameters.AddWithValue("@MaximumDiscountBasisPointsToTrade", streamAndGliderTradingSystemInfoBag.MaximumDiscountBasisPointsToTrade);
                command.Parameters.AddWithValue("@KGTLLTime_milliseconds", streamAndGliderTradingSystemInfoBag.KGTLLTime_milliseconds);
                command.Parameters.AddWithValue("@MinimumIntegratorPriceLife_milliseconds", streamAndGliderTradingSystemInfoBag.MinimumIntegratorPriceLife_milliseconds);
                command.Parameters.AddWithValue("@MinimumSizeToTrade", streamAndGliderTradingSystemInfoBag.MinimumSizeToTrade);
                command.Parameters.AddWithValue("@MinimumFillSize", streamAndGliderTradingSystemInfoBag.MinimumFillSize);
                command.Parameters.AddWithValue("@MaximumSourceBookTiers", streamAndGliderTradingSystemInfoBag.MaximumSourceBookTiers);
                command.Parameters.AddWithValue("@MinimumBasisPointsFromKgtPriceToFastCancel", streamAndGliderTradingSystemInfoBag.MinimumBasisPointsFromKgtPriceToFastCancel);
                command.Parameters.AddWithValue("@ReferenceLmaxBookOnly", streamAndGliderTradingSystemInfoBag.ReferenceLmaxBookOnly);
                command.Parameters.AddWithValue("@BidEnabled", streamAndGliderTradingSystemInfoBag.BidEnabled);
                command.Parameters.AddWithValue("@AskEnabled", streamAndGliderTradingSystemInfoBag.AskEnabled);
                command.Parameters.AddWithValue("@PreferLLDestinationCheckToLmax", streamAndGliderTradingSystemInfoBag.PreferLLDestinationCheckToLmax);
                command.Parameters.AddWithValue("@PreferLmaxDuringHedging", streamAndGliderTradingSystemInfoBag.PreferLmaxDuringHedging);

                if (this.TradingSystemType == TradingSystemType.Stream)
                {
                    StreamTradingSystemInfoBag streamTradingSystemInfoBag =
                    tradingSystemInfoBag as StreamTradingSystemInfoBag;

                    command.Parameters.AddWithValue("@MinimumBPGrossToAccept", streamTradingSystemInfoBag.MinimumBPGrossToAccept);
                    
                }
                else if (this.TradingSystemType == TradingSystemType.Glider)
                {
                    GliderTradingSystemInfoBag gliderTradingSystemInfoBag =
                    tradingSystemInfoBag as GliderTradingSystemInfoBag;

                    command.Parameters.AddWithValue("@MinimumIntegratorGlidePriceLife_milliseconds", gliderTradingSystemInfoBag.MinimumGlidePriceLife_milliseconds);
                    command.Parameters.AddWithValue("@GlidingCutoffSpan_milliseconds", gliderTradingSystemInfoBag.GlidingCutoffSpan_milliseconds);
                    command.Parameters.AddWithValue("@MinimumBPGrossGain", gliderTradingSystemInfoBag.MinimumBPGain);
                    command.Parameters.AddWithValue("@MinimumBPGrossGainIfZeroGlideFill", gliderTradingSystemInfoBag.MinimumBPGainIfZero);
                    command.Parameters.AddWithValue("@GlidingCounterpartyCode", gliderTradingSystemInfoBag.GlidingCounterparty);
                }
            }
            else if (this.TradingSystemType == TradingSystemType.Quoting)
            {
                QuotingTradingSystemInfoBag quotingTradingSystemInfoBag =
                    tradingSystemInfoBag as QuotingTradingSystemInfoBag;
                command.Parameters.AddWithValue("@MaximumPositionBaseAbs", quotingTradingSystemInfoBag.MaximumPositionBaseAbs);
                command.Parameters.AddWithValue("@FixedSpreadBasisPoints", quotingTradingSystemInfoBag.FixedSpreadBasisPoints);
                command.Parameters.AddWithValue("@StopLossNetInUsd", quotingTradingSystemInfoBag.StopLossNetInUsd);
                command.Parameters.AddWithValue("@SpreadType", quotingTradingSystemInfoBag.SpreadType);
                command.Parameters.AddWithValue("@MinimumDynamicSpreadBp", quotingTradingSystemInfoBag.MinDynamicSpreadBp);
                command.Parameters.AddWithValue("@DynamicMarkupDecimal", quotingTradingSystemInfoBag.DynamicMarkupDecimal);
                command.Parameters.AddWithValue("@EnableSkew", quotingTradingSystemInfoBag.SkewEnabled);
                command.Parameters.AddWithValue("@SkewDecimal", quotingTradingSystemInfoBag.SkewDecimal);
                command.Parameters.AddWithValue("@InactivityTimeoutSec", quotingTradingSystemInfoBag.InactivityTimeoutSec);
                command.Parameters.AddWithValue("@LiquidationSpreadBp", quotingTradingSystemInfoBag.LiquidationSpreadBp);
                command.Parameters.AddWithValue("@LmaxTickerLookbackSec", quotingTradingSystemInfoBag.LmaxTickerLookbackSec);
                command.Parameters.AddWithValue("@MinimumLmaxTickerVolumeUsd", quotingTradingSystemInfoBag.MinimumLmaxTickerVolumeUsd);
                command.Parameters.AddWithValue("@MaximumLmaxTickerVolumeUsd", quotingTradingSystemInfoBag.MaximumLmaxTickerVolumeUsd);
                command.Parameters.AddWithValue("@MinimumLmaxTickerAvgDealSizeUsd", quotingTradingSystemInfoBag.MinimumLmaxTickerAvgDealSizeUsd);
                command.Parameters.AddWithValue("@MaximumLmaxTickerAvgDealSizeUsd", quotingTradingSystemInfoBag.MaximumLmaxTickerAvgDealSizeUsd);
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }

            if (commandActionType == CommandActionType.Insert)
            {
                command.Parameters.AddWithValue("@TradingGroupId", _tradingGroupId.Value);
                command.Parameters.AddWithValue("@Symbol", tradingSystemInfoBag.SymbolString);
                command.Parameters.AddWithValue("@CounterpartyCode", tradingSystemInfoBag.CounterpatyCode);
                command.Parameters.AddWithValue("@Enabled", tradingSystemInfoBag.Enabled);
            }
            else if (commandActionType == CommandActionType.Update)
            {
                command.Parameters.AddWithValue("@TradingSystemId", tradingSystemInfoBag.TradingSystemId);
                command.Parameters.Add("@LastUpdateTimeUtc", SqlDbType.DateTime2).Value = tradingSystemInfoBag.LastUpdateTimeUtc;
            }

            return command;
        }

        private int? InsertOne(TradingSystemInfoBagBase tradingSystemInfoBag)
        {
            if (!this.CanInsertSystem())
            {
                this.ReportError("Cannot insert new systems on current page as it was either deleted or pre-insert validation failed");
                return null;
            }

            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = this.PrepareCommand(CommandActionType.Insert, tradingSystemInfoBag,
                            connection);

                        SqlParameter tradingSystemIdParam = new SqlParameter("@TradingSystemId", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output
                        };
                        command.Parameters.Add(tradingSystemIdParam);

                        command.ExecuteNonQuery();

                        if (tradingSystemIdParam.Value != null && tradingSystemIdParam.Value != DBNull.Value)
                        {
                            return (int)tradingSystemIdParam.Value;
                        }
                    }
                }
                catch (SqlException exc)
                {
                    this.ReportError(exc.Message.Replace(Environment.NewLine, "<BR />"));
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                ReportError("Connection string missing");
            }

            return null;
        }

        private bool UpdateOne(TradingSystemInfoBagBase tradingSystemInfoBag)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["KillSwitchBackend"];

            if (connectionString != null)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = this.PrepareCommand(CommandActionType.Update, tradingSystemInfoBag,
                            connection);

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (Exception exc)
                {
                    ReportException(exc);
                }
            }
            else
            {
                ReportError("Connection string missing");
            }

            return false;
        }

        private void ReportException(Exception e)
        {
            string errorDetails;

            if (Utils.GetIsDevUser())
            {
                errorDetails = e.ToString();
            }
            else
            {
                errorDetails = string.Format("Please report the error to KGT (Error code: {0})", e.GetType().ToString());
            }

            this.ReportError(errorDetails);
        }

        private void ReportError(string error)
        {
            ErrorPanel.Visible = true;
            InsertButton.Visible = false;
            exceptionLiteral.Text = error;
        }

        protected void SizeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.SizeValidator_ServerValidate(source, args, 10000m);
        }

        protected void MinFillSizeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.SizeValidator_ServerValidate(source, args, 1000m);
        }

        protected void MaxSourceTiersValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MaxSourceTiersValidator_ServerValidate(source, args);
        }

        protected void SizeValidatorZeroAllowed_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.SizeValidator_ServerValidate(source, args, 0m);
        }

        protected void BpValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.BpValidator_ServerValidate(source, args);
        }

        protected void MinTimeBetweenValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MinTimeBetweenValidator_ServerValidate(source, args);
        }

        protected void MaxTickWaitTimeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MaxTickWaitTimeValidator_ServerValidate(source, args, null);
        }

        protected void MinPriceLifeTimeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MaxTickWaitTimeValidator_ServerValidate(source, args, 200);
        }

        protected void MaxLLTimeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MaxTickWaitTimeValidator_ServerValidate(source, args, 80);
        }

        protected void MaxHotspotLLTimeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MaxTickWaitTimeValidator_ServerValidate(source, args, 115);
        }

        protected void MaxGlideTimeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.MaxTickWaitTimeValidator_ServerValidate(source, args, 80, 5);
        }

        protected void BestPriceImprovementOffsetValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.BestPriceImprovementOffsetValidator_ServerValidate(source, args);
        }

        protected void LLCheckBestPriceImprovementOffsetValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //<-5;10>
            ValidatorUtils.BpValidator_ServerValidate(source, args);
        }

        protected void SpreadValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.SpreadValidator_ServerValidate(source, args);
        }

        protected void PriceAdjustmentDecimal_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.DecimalNum_ServerValidate(source, args, -10, 10, 6);
        }

        protected void PriceAdjustmentPositiveDecimal_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.DecimalNum_ServerValidate(source, args, 0, 10, 6);
        }

        protected void StopLossValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.StopLossValidator_ServerValidate(source, args);
        }

        protected void InactivityValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.IntArg_ServerValidate(source, args, 30, 1);
        }

        protected void LiquidationSpreadValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.DecimalNum_ServerValidate(source, args, -1, 10, 2);
        }

        protected void LtLookbackSecValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.IntArg_ServerValidate(source, args, 900, 10, 5);
        }

        protected void LtVolumeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.IntArg_ServerValidate(source, args, 1000000000, 0);
        }

        protected void LtDealSizeValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ValidatorUtils.IntArg_ServerValidate(source, args, 100000000, 0);
        }
    }


    public static class ValidatorUtils
    {
        public static void SizeValidator_ServerValidate(object source, ServerValidateEventArgs args, decimal minValue)
        {
            decimal result;
            if (!decimal.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid decimal number";
                args.IsValid = false;
                return;
            }

            if (result < minValue || result > 4000000)
            {
                (source as CustomValidator).ErrorMessage = string.Format("Out of allowed ranges <{0}; 4M>", minValue);
                args.IsValid = false;
                return;
            }

            if (result % 1000 != 0)
            {
                (source as CustomValidator).ErrorMessage = "Disallowed size increment (need to e increment of 1000.0)";
                args.IsValid = false;
            }
        }

        public static void BpValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            decimal result;
            if (!decimal.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid decimal number";
                args.IsValid = false;
                return;
            }

            if (result < -5 || result > 10)
            {
                (source as CustomValidator).ErrorMessage = "Out of allowed ranges <-5; 10>";
                args.IsValid = false;
            }

            if (BitConverter.GetBytes(decimal.GetBits(result)[3])[2] > 2)
            {
                (source as CustomValidator).ErrorMessage = "Disallowed number of decimal places (max 2)";
                args.IsValid = false;
            }
        }

        public static void MinTimeBetweenValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            decimal result;
            if (!decimal.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid decimal number";
                args.IsValid = false;
                return;
            }

            if (result <= 0)
            {
                (source as CustomValidator).ErrorMessage = "Out of allowed ranges (positive)";
                args.IsValid = false;
            }

            if (BitConverter.GetBytes(decimal.GetBits(result)[3])[2] > 1)
            {
                (source as CustomValidator).ErrorMessage = "Disallowed number of decimal places (max 1)";
                args.IsValid = false;
            }
        }


        public static void MaxTickWaitTimeValidator_ServerValidate(object source, ServerValidateEventArgs args, int? maxValue)
        {
            MaxTickWaitTimeValidator_ServerValidate(source, args, maxValue, 0);
        }

        public static void MaxTickWaitTimeValidator_ServerValidate(object source, ServerValidateEventArgs args, int? maxValue, int minValue)
        {
            IntArg_ServerValidate(source, args, maxValue, minValue);
        }

        public static void IntArg_ServerValidate(object source, ServerValidateEventArgs args, int? maxValue, int minValue)
        {
            IntArg_ServerValidate(source, args, maxValue, minValue, 1);
        }

        public static void IntArg_ServerValidate(object source, ServerValidateEventArgs args, int? maxValue, int minValue, int granularity)
        {
            long result;
            if (!long.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid integer number";
                args.IsValid = false;
                return;
            }

            if (result < minValue)
            {
                (source as CustomValidator).ErrorMessage = string.Format("Out of allowed ranges (>= {0})", minValue);
                args.IsValid = false;
            }

            if (maxValue.HasValue && result > maxValue)
            {
                (source as CustomValidator).ErrorMessage = "Out of allowed ranges (<= " + maxValue + ")";
                args.IsValid = false;
            }

            if (result%granularity != 0)
            {
                (source as CustomValidator).ErrorMessage = "Disallowed granularity - must be multiple of " + granularity;
                args.IsValid = false;
            }
        }

        public static void MaxSourceTiersValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int result;
            if (!int.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid integer number";
                args.IsValid = false;
                return;
            }

            if (result < 1 || result > 8)
            {
                (source as CustomValidator).ErrorMessage = "Out of allowed ranges <1; 8>";
                args.IsValid = false;
            }
        }

        public static void BestPriceImprovementOffsetValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DecimalNum_ServerValidate(source, args, -1, 50, 2);
        }

        public static void SpreadValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DecimalNum_ServerValidate(source, args, 0, 100, 2);
        }

        public static void DecimalNum_ServerValidate(object source, ServerValidateEventArgs args, int minValue, int maxValue, int maxDecimalPlaces)
        {
            decimal result;
            if (!decimal.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid decimal number";
                args.IsValid = false;
                return;
            }

            if (result < minValue || result > maxValue)
            {
                (source as CustomValidator).ErrorMessage = string.Format("Out of allowed ranges <{0}; {1}>", minValue, maxValue);
                args.IsValid = false;
            }

            if (BitConverter.GetBytes(decimal.GetBits(result)[3])[2] > maxDecimalPlaces)
            {
                (source as CustomValidator).ErrorMessage = string.Format("Disallowed number of decimal places (max {0})", maxDecimalPlaces);
                args.IsValid = false;
            }
        }

        public static void StopLossValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            long result;
            if (!long.TryParse(args.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                (source as CustomValidator).ErrorMessage = "Invalid long number";
                args.IsValid = false;
                return;
            }

            if (result < 0 || result > 5000)
            {
                (source as CustomValidator).ErrorMessage = "Out of allowed ranges <0; 5000>";
                args.IsValid = false;
            }
        }

    }


    #region TradingSystemInfoBags


    public abstract class TradingSystemInfoBagBase
    {
        protected TradingSystemInfoBagBase(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled)
        {
            this.TradingSystemId = tradingSystemId;
            this.CounterpatyCode = counterpatyCode;
            this.LastUpdateTimeUtc = lastUpdateTimeUtc;
            this.Enabled = enabled;
        }

        public int TradingSystemId { get; private set; }
        public string CounterpatyCode { get; private set; }
        public DateTime LastUpdateTimeUtc { get; private set; }
        public bool Enabled { get; private set; }
        public string SymbolString { get; set; }
    }

    public abstract class CrossTradingSystemInfoBagBase : TradingSystemInfoBagBase
    {
        protected CrossTradingSystemInfoBagBase(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled,
            decimal minimumSizeToTrade, decimal maximumSizeToTrade, decimal minimumDiscountBasisPointsToTrade,
            decimal minimumTimeBetweenSingleCounterpartySignalsSeconds,
            decimal minimumFillSize)
            : base(tradingSystemId, counterpatyCode, lastUpdateTimeUtc, enabled)
        {
            this.MinimumSizeToTrade = minimumSizeToTrade;
            this.MaximumSizeToTrade = maximumSizeToTrade;
            this.MinimumDiscountBasisPointsToTrade = minimumDiscountBasisPointsToTrade;
            this.MinimumTimeBetweenSingleCounterpartySignals_seconds =
                minimumTimeBetweenSingleCounterpartySignalsSeconds;

            this.MinimumFillSize = minimumFillSize;
        }

        public decimal MinimumSizeToTrade { get; private set; }
        public decimal MaximumSizeToTrade { get; private set; }
        public decimal MinimumDiscountBasisPointsToTrade { get; private set; }
        public decimal MinimumTimeBetweenSingleCounterpartySignals_seconds { get; private set; }
        public decimal MinimumFillSize { get; private set; }
    }

    public class FirmCrossTradingSystemInfoBag : CrossTradingSystemInfoBagBase
    {
        public FirmCrossTradingSystemInfoBag(int tradingSystemId, string counterpatyCode, DateTime lastUpdateTimeUtc,
            bool enabled, decimal minimumSizeToTrade, decimal maximumSizeToTrade,
            decimal minimumDiscountBasisPointsToTrade, decimal minimumTimeBetweenSingleCounterpartySignalsSeconds,
            int maximumWaitTimeOnImprovementTick_milliseconds, decimal minimumFillSize)
            : base(
                tradingSystemId, counterpatyCode, lastUpdateTimeUtc, enabled, minimumSizeToTrade, maximumSizeToTrade,
                minimumDiscountBasisPointsToTrade, minimumTimeBetweenSingleCounterpartySignalsSeconds,
                minimumFillSize)
        {
            this.MaximumWaitTimeOnImprovementTick_milliseconds = maximumWaitTimeOnImprovementTick_milliseconds;
        }

        public int MaximumWaitTimeOnImprovementTick_milliseconds { get; private set; }
    }

    public class SmartCrossTradingSystemInfoBag : CrossTradingSystemInfoBagBase
    {
        public SmartCrossTradingSystemInfoBag(int tradingSystemId, string counterpatyCode, DateTime lastUpdateTimeUtc,
            bool enabled, decimal minimumSizeToTrade, decimal maximumSizeToTrade,
            decimal minimumDiscountBasisPointsToTrade, decimal minimumTimeBetweenSingleCounterpartySignalsSeconds,
            decimal minimumFillSize, decimal coverDistanceGrossBasisPoints,
            int coverTimeout_milliseconds, decimal minimumTrueGainGrossBasisPoints)
            : base(
                tradingSystemId, counterpatyCode, lastUpdateTimeUtc, enabled, minimumSizeToTrade, maximumSizeToTrade,
                minimumDiscountBasisPointsToTrade, minimumTimeBetweenSingleCounterpartySignalsSeconds,
                minimumFillSize)
        {
            this.CoverDistanceGrossBasisPoints = coverDistanceGrossBasisPoints;
            this.CoverTimeout_milliseconds = coverTimeout_milliseconds;
            this.MinimumTrueGainGrossBasisPoints = minimumTrueGainGrossBasisPoints;
        }

        public decimal CoverDistanceGrossBasisPoints { get; private set; }
        public int CoverTimeout_milliseconds { get; private set; }
        public decimal MinimumTrueGainGrossBasisPoints { get; private set; }

    }

    public class MMTradingSystemInfoBag : TradingSystemInfoBagBase
    {
        public MMTradingSystemInfoBag(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled,
            decimal maximumSizeToTrade, decimal bestPriceImprovementOffsetBasisPoints,
            decimal maximumDiscountBasisPointsToTrade,
            int maximumWaitTimeOnImprovementTick_milliseconds, int minimumIntegratorPriceLife_milliseconds,
            decimal minimumSizeToTrade, decimal minimumFillSize, int maximumSourceBookTiers,
            decimal minimumBasisPointsFromKgtPriceToFastCancel
            , bool bidEnabled, bool askEnabled)
            : base(tradingSystemId, counterpatyCode, lastUpdateTimeUtc, enabled)
        {
            this.MaximumSizeToTrade = maximumSizeToTrade;
            this.BestPriceImprovementOffsetBasisPoints = bestPriceImprovementOffsetBasisPoints;
            this.MaximumDiscountBasisPointsToTrade = maximumDiscountBasisPointsToTrade;
            this.MaximumWaitTimeOnImprovementTick_milliseconds = maximumWaitTimeOnImprovementTick_milliseconds;
            this.MinimumIntegratorPriceLife_milliseconds = minimumIntegratorPriceLife_milliseconds;
            this.MinimumFillSize = minimumFillSize;
            this.MinimumSizeToTrade = minimumSizeToTrade;
            this.MaximumSourceBookTiers = maximumSourceBookTiers;
            this.MinimumBasisPointsFromKgtPriceToFastCancel = minimumBasisPointsFromKgtPriceToFastCancel;
            this.BidEnabled = bidEnabled;
            this.AskEnabled = askEnabled;
        }

        public decimal MaximumSizeToTrade { get; private set; }
        public decimal BestPriceImprovementOffsetBasisPoints { get; private set; }
        public decimal MaximumDiscountBasisPointsToTrade { get; private set; }
        public int MaximumWaitTimeOnImprovementTick_milliseconds { get; private set; }
        public int MinimumIntegratorPriceLife_milliseconds { get; private set; }
        public decimal MinimumSizeToTrade { get; private set; }
        public decimal MinimumFillSize { get; private set; }
        public int MaximumSourceBookTiers { get; private set; }
        public decimal MinimumBasisPointsFromKgtPriceToFastCancel { get; private set; }
        public bool BidEnabled { get; private set; }
        public bool AskEnabled { get; private set; }
    }

    public abstract class StreamTradingSystemInfoBagBase : TradingSystemInfoBagBase
    {
        protected StreamTradingSystemInfoBagBase(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled,
            decimal maximumSizeToTrade, decimal bestPriceImprovementOffsetBasisPoints,
            decimal maximumDiscountBasisPointsToTrade,
            int kgtLLTime_milliseconds, int minimumIntegratorPriceLife_milliseconds,
            decimal minimumSizeToTrade, decimal minimumFillSize, int maximumSourceBookTiers,
            decimal minimumBasisPointsFromKgtPriceToFastCancel,
            bool referenceLmaxBookOnly, bool preferLLDestinationCheckToLmax,
            bool preferLmaxDuringHedging, bool bidEnabled, bool askEnabled)
            : base(tradingSystemId, counterpatyCode, lastUpdateTimeUtc, enabled)
        {
            this.MaximumSizeToTrade = maximumSizeToTrade;
            this.BestPriceImprovementOffsetBasisPoints = bestPriceImprovementOffsetBasisPoints;
            this.MaximumDiscountBasisPointsToTrade = maximumDiscountBasisPointsToTrade;
            this.KGTLLTime_milliseconds = kgtLLTime_milliseconds;
            this.MinimumIntegratorPriceLife_milliseconds = minimumIntegratorPriceLife_milliseconds;
            this.MinimumFillSize = minimumFillSize;
            this.MinimumSizeToTrade = minimumSizeToTrade;
            this.MaximumSourceBookTiers = maximumSourceBookTiers;
            this.MinimumBasisPointsFromKgtPriceToFastCancel = minimumBasisPointsFromKgtPriceToFastCancel;
            this.ReferenceLmaxBookOnly = referenceLmaxBookOnly;
            this.PreferLLDestinationCheckToLmax = preferLLDestinationCheckToLmax;
            this.PreferLmaxDuringHedging = preferLmaxDuringHedging;
            this.BidEnabled = bidEnabled;
            this.AskEnabled = askEnabled;
        }

        public decimal MaximumSizeToTrade { get; private set; }
        public decimal BestPriceImprovementOffsetBasisPoints { get; private set; }
        public decimal MaximumDiscountBasisPointsToTrade { get; private set; }
        public int KGTLLTime_milliseconds { get; private set; }
        public int MinimumIntegratorPriceLife_milliseconds { get; private set; }
        public decimal MinimumSizeToTrade { get; private set; }
        public decimal MinimumFillSize { get; private set; }
        public int MaximumSourceBookTiers { get; private set; }
        public decimal MinimumBasisPointsFromKgtPriceToFastCancel { get; private set; }
        public bool ReferenceLmaxBookOnly { get; private set; }
        public bool PreferLLDestinationCheckToLmax { get; private set; }
        public bool PreferLmaxDuringHedging { get; private set; }
        public bool BidEnabled { get; private set; }
        public bool AskEnabled { get; private set; }
    }

    public class StreamTradingSystemInfoBag : StreamTradingSystemInfoBagBase
    {
        public StreamTradingSystemInfoBag(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled,
            decimal maximumSizeToTrade, decimal bestPriceImprovementOffsetBasisPoints,
            decimal maximumDiscountBasisPointsToTrade,
            int kgtLLTime_milliseconds, bool preferLLDestinationCheckToLmax, decimal minimumBPGrossToAccept,
            bool preferLmaxDuringHedging, int minimumIntegratorPriceLife_milliseconds,
            decimal minimumSizeToTrade, decimal minimumFillSize, int maximumSourceBookTiers,
            decimal minimumBasisPointsFromKgtPriceToFastCancel,
            bool referenceLmaxBookOnly, bool bidEnabled, bool askEnabled)
            : base(tradingSystemId, counterpatyCode,
                lastUpdateTimeUtc, enabled,
                maximumSizeToTrade, bestPriceImprovementOffsetBasisPoints,
                maximumDiscountBasisPointsToTrade,
                kgtLLTime_milliseconds, minimumIntegratorPriceLife_milliseconds,
                minimumSizeToTrade, minimumFillSize, maximumSourceBookTiers,
                minimumBasisPointsFromKgtPriceToFastCancel,
                referenceLmaxBookOnly, preferLLDestinationCheckToLmax, preferLmaxDuringHedging, bidEnabled, askEnabled)
        {
            this.MinimumBPGrossToAccept = minimumBPGrossToAccept;
        }

        public decimal MinimumBPGrossToAccept { get; private set; }
    }

    public class GliderTradingSystemInfoBag : StreamTradingSystemInfoBagBase
    {
        public GliderTradingSystemInfoBag(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled,
            decimal maximumSizeToTrade, decimal bestPriceImprovementOffsetBasisPoints,
            decimal maximumDiscountBasisPointsToTrade,
            int kgtLLTime_milliseconds, int minimumIntegratorPriceLife_milliseconds,
            decimal minimumSizeToTrade, decimal minimumFillSize, int maximumSourceBookTiers,
            decimal minimumBasisPointsFromKgtPriceToFastCancel,
            bool referenceLmaxBookOnly, bool preferLLDestinationCheckToLmax,
            bool preferLmaxDuringHedging, bool bidEnabled, bool askEnabled,
            decimal minimumBpGain, decimal minimumBPGainIfZero, int minimumGlidePriceLife, int glidingCutoffSpan,
            string glidingCounterparty)
            : base(tradingSystemId, counterpatyCode,
                lastUpdateTimeUtc, enabled,
                maximumSizeToTrade, bestPriceImprovementOffsetBasisPoints,
                maximumDiscountBasisPointsToTrade,
                kgtLLTime_milliseconds, minimumIntegratorPriceLife_milliseconds,
                minimumSizeToTrade, minimumFillSize, maximumSourceBookTiers,
                minimumBasisPointsFromKgtPriceToFastCancel,
                referenceLmaxBookOnly, preferLLDestinationCheckToLmax, preferLmaxDuringHedging, bidEnabled, askEnabled)
        {
            this.MinimumBPGain = minimumBpGain;
            this.MinimumBPGainIfZero = minimumBPGainIfZero;
            this.MinimumGlidePriceLife_milliseconds = minimumGlidePriceLife;
            this.GlidingCutoffSpan_milliseconds = glidingCutoffSpan;
            this.GlidingCounterparty = glidingCounterparty;
        }

        public decimal MinimumBPGain { get; private set; }
        public decimal MinimumBPGainIfZero { get; private set; }
        public int MinimumGlidePriceLife_milliseconds { get; private set; }
        public int GlidingCutoffSpan_milliseconds { get; private set; }
        public string GlidingCounterparty { get; private set; }
    }

    public class QuotingTradingSystemInfoBag : TradingSystemInfoBagBase
    {
        public QuotingTradingSystemInfoBag(int tradingSystemId, string counterpatyCode,
            DateTime lastUpdateTimeUtc, bool enabled,
            decimal maximumPositionBaseAbs, string spreadType, decimal fixedSpreadBasisPoints,
            decimal minDynamicSpreadBp, decimal dynamicMarkupDecimal, bool skewEnabled,
            decimal skewDecimal, long stopLossNetInUsd,
            int inactivityTimeoutSec, decimal liquidationSpreadBp, int lmaxTickerLookbackSec,
            long minimumLmaxTickerVolumeUsd, long maximumLmaxTickerVolumeUsd, long minimumLmaxTickerAvgDealSizeUsd,
            long maximumLmaxTickerAvgDealSizeUsd
            )
            : base(tradingSystemId, counterpatyCode, lastUpdateTimeUtc, enabled)
        {
            this.MaximumPositionBaseAbs = maximumPositionBaseAbs;
            this.SpreadType = spreadType;
            this.FixedSpreadBasisPoints = fixedSpreadBasisPoints;
            this.MinDynamicSpreadBp = minDynamicSpreadBp;
            this.DynamicMarkupDecimal = dynamicMarkupDecimal;
            this.SkewEnabled = skewEnabled;
            this.SkewDecimal = skewDecimal;
            this.StopLossNetInUsd = stopLossNetInUsd;
            this.InactivityTimeoutSec = inactivityTimeoutSec;
            this.LiquidationSpreadBp = liquidationSpreadBp;
            this.LmaxTickerLookbackSec = lmaxTickerLookbackSec;
            this.MinimumLmaxTickerVolumeUsd = minimumLmaxTickerVolumeUsd;
            this.MaximumLmaxTickerVolumeUsd = maximumLmaxTickerVolumeUsd;
            this.MinimumLmaxTickerAvgDealSizeUsd = minimumLmaxTickerAvgDealSizeUsd;
            this.MaximumLmaxTickerAvgDealSizeUsd = maximumLmaxTickerAvgDealSizeUsd;
        }

        public decimal MaximumPositionBaseAbs { get; private set; }
        public string SpreadType { get; private set; }
        public decimal FixedSpreadBasisPoints { get; private set; }
        public decimal MinDynamicSpreadBp { get; private set; }
        public decimal DynamicMarkupDecimal { get; private set; }
        public bool SkewEnabled { get; private set; }
        public decimal SkewDecimal { get; private set; }
        public long StopLossNetInUsd { get; private set; }
        public int InactivityTimeoutSec { get; private set; }
        public decimal LiquidationSpreadBp { get; private set; }
        public int LmaxTickerLookbackSec { get; private set; }
        public long MinimumLmaxTickerVolumeUsd { get; private set; }
        public long MaximumLmaxTickerVolumeUsd { get; private set; }
        public long MinimumLmaxTickerAvgDealSizeUsd { get; private set; }
        public long MaximumLmaxTickerAvgDealSizeUsd { get; private set; }
    }


#endregion

}