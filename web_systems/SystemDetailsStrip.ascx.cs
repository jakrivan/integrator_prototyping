﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGTKillSwitchWeb
{
    public static class HighlightDurationConsts
    {
        public static readonly TimeSpan UPDATED_SYSTEM_HIGHLIGHT_DURATION = TimeSpan.FromSeconds(5);
        public static readonly TimeSpan TRADING_ACTION_SYSTEM_HIGHLIGHT_DURATION = TimeSpan.FromSeconds(5);
    }

    public partial class SystemDetailsStrip : System.Web.UI.UserControl
    {
        private KGTKillSwitchWeb.AutorefreshContainer AC;

        protected TradingSystemType TradingSystemType { get { return (TradingSystemType)Enum.Parse(typeof(TradingSystemType), Request.QueryString["TradingSystemType"]); } }
        protected VenueCounterparty VenueCounterparty { get { return (VenueCounterparty)Enum.Parse(typeof(VenueCounterparty), Request.QueryString["VenueCounterparty"]); } }

        protected string RefreshUrlWithNoAction { get; private set; }

        protected override void OnInit(EventArgs e)
        {
            //HACK for the insert/edit page
            if (this.Parent.Parent.Parent.Parent != null)
            {

                //HACK: so that we don't have to pass this throug. It's complicated to pass it to be here oninit
                AC = this.Parent.Parent.Parent.Parent.Parent.Parent as AutorefreshContainer;

                if (AC == null)
                    throw new Exception("AC property must be populated");
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        { }

        public void BindToDataTable(DataTable dt, string refreshUrlWithNoAction)
        {
            this.RefreshUrlWithNoAction = refreshUrlWithNoAction;
            if (dt == null)
            {
                this.StatsRepeater.Visible = false;
            }
            else
            {
                this.StatsRepeater.Visible = true;
                this.StatsRepeater.DataSource = dt;
                this.StatsRepeater.DataBind();
            }
        }

        protected bool IsRecentlyUpdated(object dbObject)
        {
            return dbObject != null && dbObject != DBNull.Value && dbObject is DateTime &&
                   DateTime.UtcNow - (DateTime) dbObject < HighlightDurationConsts.UPDATED_SYSTEM_HIGHLIGHT_DURATION;
        }

        protected bool IsQuotingInActivityTimeoutRestrictionMode(DataRowView row)
        {
            var dbVal = row["InactivityRestrictionOn"];

            return dbVal == DBNull.Value || (bool) dbVal;
        }

        protected bool IsQuotingInLTBandRestrictionMode(DataRowView row)
        {
            var dbVal = row["LTBandRestrictionOn"];

            return dbVal == DBNull.Value || (bool)dbVal;
        }

        protected string GetQuotingPaidFlowPctBackground(DataRowView row)
        {
            var dbVal = row["CurrentLmaxTickerPaidVolumeRatio"];
            var dbVolVal = row["CurrentLmaxTickerVolumeUsd"];

            if (dbVal == DBNull.Value || !(dbVal is decimal) || Utils.NullOrZero(dbVolVal) || !Utils.NotNullOrFalse(row["CalculatedFromFullInterval"]))
                return string.Empty;

            decimal pct = (decimal)dbVal;

            if (pct < 0.3m)
                return "quotingPaidFlowLow";
            if(pct > 0.7m)
                return "quotingPaidFlowHigh";
             
            return string.Empty;
        }

        public string GetQueryStringWithNoAction()
        {
            return
                string.Format(
                    "?TradingSystemType={0}&VenueCounterparty={1}&GroupId={2}&PageId={3}&StartSymbol={4}&EndSymbol={5}&NumOfStrips={6}",
                    this.TradingSystemType, this.VenueCounterparty, Request.QueryString["GroupId"], Request.QueryString["PageId"],
                    Utils.RemoveSlashFromSymbol(this.GetStartSybol()), Utils.RemoveSlashFromSymbol(this.GetEndSybol()),
                    GetNumberOfStrips());
        }

        public int GetNumberOfStrips()
        {
            int numOfStrips;

            if (Request.QueryString["NumOfStrips"] == null ||
                !int.TryParse(Request.QueryString["NumOfStrips"], out numOfStrips) ||
                numOfStrips < 1)
            {
                numOfStrips = 2;
            }

            return numOfStrips;
        }

        public string GetStartSybol()
        {
            string startSymbol;

            if (Request.QueryString["StartSymbol"] == null ||
                Request.QueryString["StartSymbol"].Length != 6)
            {
                startSymbol = "AUDCAD";
            }
            else
            {
                startSymbol = Request.QueryString["StartSymbol"];
            }

            return Utils.AddSlashToSymbol(startSymbol);
        }

        public string GetEndSybol()
        {
            string endSymbol;

            if (Request.QueryString["EndSymbol"] == null ||
                Request.QueryString["EndSymbol"].Length != 6)
            {
                endSymbol = "ZARJPY";
            }
            else
            {
                endSymbol = Request.QueryString["EndSymbol"];
            }

            return Utils.AddSlashToSymbol(endSymbol);
        }

        private void ReportException(Exception e)
        {
            string errorDetails;

            if (Utils.GetIsDevUser())
            {
                errorDetails = e.ToString();
            }
            else
            {
                errorDetails = string.Format("Please report the error to KGT (Error code: {0})", e.GetType().ToString());
            }

            this.ReportError(errorDetails);
        }

        private void ReportError(string error)
        {
            this.AC.ReportError(error);
            this.AC.AllowAutoRefresh = false;
        }

        private void ReportValueChanged()
        {
            this.ReportError("Some of the settings for edited row changed in the meantime. CHANGES WERE NOT PERFORMED, you can see latest value now and try again.");
        }
    }
}