﻿using System;
using System.Collections.Generic;

namespace QuickFix
{
    /// <summary>
    /// In-memory message store implementation
    /// </summary>
    public class MemoryStore : IMessageStore
    {
        #region Private Members

        System.Collections.Generic.Dictionary<int, string> messages_;
        int nextSenderMsgSeqNum_;
        int nextTargetMsgSeqNum_;
        DateTime? creationTime;

        private readonly bool _cacheMessages;

        #endregion

        public MemoryStore()
            :this(true)
        { }

        public MemoryStore(bool cacheMessages)
        {
            this._cacheMessages = cacheMessages;
            if(cacheMessages)
                messages_ = new System.Collections.Generic.Dictionary<int, string>();
            Reset();
        }

        public void Get(int begSeqNo, int endSeqNo, List<string> messages)
        {
            if(!this._cacheMessages)
                return;

            for (int current = begSeqNo; current <= endSeqNo; current++)
            {
                if (messages_.ContainsKey(current))
                    messages.Add(messages_[current]);
            }
        }

        #region MessageStore Members

        public bool Set(int msgSeqNum, string msg)
        {
            if (this._cacheMessages)
            {
                messages_[msgSeqNum] = msg;
            }
            return true;
        }

        public int GetNextSenderMsgSeqNum()
        { return nextSenderMsgSeqNum_; }

        public int GetNextTargetMsgSeqNum()
        { return nextTargetMsgSeqNum_; }

        public void SetNextSenderMsgSeqNum(int value)
        { nextSenderMsgSeqNum_ = value; }

        public void SetNextTargetMsgSeqNum(int value)
        { nextTargetMsgSeqNum_ = value; }

        public void IncrNextSenderMsgSeqNum()
        { ++nextSenderMsgSeqNum_; }

        public void IncrNextTargetMsgSeqNum()
        { ++nextTargetMsgSeqNum_; }

        public System.DateTime? CreationTime
        {
            get
            {
                return creationTime;
            }
        }

        [System.Obsolete("Use CreationTime instead")]
        public DateTime GetCreationTime()
        {
            throw new NotImplementedException();
        }


        public void Reset()
        {
            nextSenderMsgSeqNum_ = 1;
            nextTargetMsgSeqNum_ = 1;
            if (this._cacheMessages)
                messages_.Clear();
            creationTime = DateTime.UtcNow;
        }

        public void Refresh()
        { }

        #endregion

        public void Dispose()
        {
        }
    }
}
