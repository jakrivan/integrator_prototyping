﻿using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Kreslik.Integrator.LowLatencyUtils;

namespace QuickFix
{
    [Serializable]
    public class MessageParsingException : Exception
    {
        public MessageParsingException()
        { }

        public MessageParsingException(string message)
            : base(message)
        { }

        public MessageParsingException(string message, Exception inner)
            : base(message, inner)
        { }
    }

    [Serializable]
    public class TagParsingException : MessageParsingException
    {
        public int TagId { get; private set; }

        public TagParsingException(int tagId)
            :base("Tag Id: " + tagId)
        {
            this.TagId = tagId;
        }

        public TagParsingException(string message, int tagId)
            : base(message + ". Tag Id: " + tagId)
        {
            this.TagId = tagId;
        }

        public TagParsingException(string message, Exception inner, int tagId)
            : base(message + ". Tag Id: " + tagId, inner)
        {
            this.TagId = tagId;
        }
    }

    [Serializable]
    public class NonHaltingTagException : TagParsingException
    {
        public NonHaltingTagException(int tagId)
            :base(tagId)
        { }

        public NonHaltingTagException(string message, int tagId)
            : base(message, tagId)
        { }

        public NonHaltingTagException(string message, Exception inner, int tagId)
            : base(message, inner, tagId)
        { }
    }

    [Serializable]
    public class HaltingTagException : TagParsingException
    {
        public HaltingTagException(int tagId)
            :base(tagId)
        { }

        public HaltingTagException(string message, int tagId)
            : base(message, tagId)
        { }

        public HaltingTagException(string message, Exception inner, int tagId)
            : base(message, inner, tagId)
        { }
    }

    /// <summary>
    /// Handles a connection with an acceptor.
    /// </summary>
    public class SocketInitiatorThread : IResponder
    {
        //[Obsolete("Use SessionID instead, otherwise all access to Session must be syncronized")]
        public Session Session { get { return session_; } }

        public SessionID SessionID { get { return session_.SessionID; } }
        public Transport.SocketInitiator Initiator { get { return initiator_; } }

        public const int BUF_SIZE = 512;
        public const int PREFETCH_SEGMENTS_BATCH_SIZE = 10;

        //private Thread thread_ = null;
        private Task _connectTask = null;
        private byte[] readBuffer_;// = new byte[BUF_SIZE];
        private QuickFixParentBufferSegmentItem _parentBufferSegment;// = BufferSegment.CreateReadingBufferSegment();
        private QuickFixSubsegment[] _subsegmentsBatch;
        private QuickFixSegmentsManager _segmentsManager;
        private BufferParser _parser;
        private Parser parser_;
        protected Stream stream_;
        private Transport.SocketInitiator initiator_;
        private Session session_;
        private IPEndPoint socketEndPoint_;
        protected SocketSettings socketSettings_;
        private bool isDisconnectRequested_ = false;
        private Timer _resetTimer;

        public SocketInitiatorThread(Transport.SocketInitiator initiator, Session session, IPEndPoint socketEndPoint, SocketSettings socketSettings)
        {
            bool isMdSession = session.RelaxValidationForMessageTypes == null;

            isDisconnectRequested_ = false;
            initiator_ = initiator;
            session_ = session;
            socketEndPoint_ = socketEndPoint;
            //_parser = new BufferParser(this._bufferSegment, BufferSegment.CreateBufferSegmentView(this._bufferSegment, 0, 0));
            //parser_ = new Parser();
            session_ = session;
            socketSettings_ = socketSettings;

            if (session_.PoolBuffers || session_.BypassMessageParsing)
            {
                _segmentsManager = QuickFixSegmentsManager.Instance;
                if(isMdSession)
                    _subsegmentsBatch = new QuickFixSubsegment[PREFETCH_SEGMENTS_BATCH_SIZE];
                _parser = new BufferParser(this._segmentsManager, _subsegmentsBatch);
                session_.Log.OnExpectedEvent("Starting socket initiator WITH buffer pooling and " +
                                             (session_.BypassMessageParsing ? string.Empty : "NO ") + "bypassing");
                if (session_.BypassMessageParsing && !session_.Application.CanCrackRawMessages)
                    session_.Log.OnFatalEvent(
                        "Session has BypassParsing set, but it is not capable of parsing any raw messages - this is likely misconfiguration and should be investigated. (Everything will work, but performance might suffer)");
            }
            else
            {
                readBuffer_ = new byte[BUF_SIZE];
                parser_ = new Parser();
                session_.Log.OnExpectedEvent("Starting socket initiator WITHOUT buffer pooling");
            }

            _resetTimer = new Timer(TimerResetCallback, null, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));
        }

        public void Start()
        {
            isDisconnectRequested_ = false;
            _connectTask = Task.Factory.StartNew(() => Transport.SocketInitiator.SocketInitiatorThreadStart(this),
                TaskCreationOptions.LongRunning);
        }

        public void Join()
        {
            if (null == _connectTask)
                return;
            Disconnect();
            _connectTask.Wait(5000);
            _connectTask = null;
        }

        public void Connect()
        {
            Debug.Assert(stream_ == null);

            stream_ = SetupStream();
            //if sender doesn't send any data for HB interval, then blocking read will throw excetion
            // and we can continu snchronously on the same thread; however avaid excesive exceptions therefore the HB interval
            stream_.ReadTimeout = session_.HeartBtInt * 1000 - 400;
            session_.SetResponder(this);
        }

        /// <summary>
        /// Setups/Connect to the other party.
        /// Override this in order to setup other types of streams with other settings
        /// </summary>
        /// <returns>Stream representing the (network)connection to the other party</returns>
        protected virtual Stream SetupStream()
        {
            return QuickFix.Transport.StreamFactory.CreateClientStream(socketEndPoint_, socketSettings_, session_.Log, session_.Application);
        }

        public void PrepareForReading()
        {
            if (!session_.PoolBuffers && !session_.BypassMessageParsing)
                return;

            if (_parentBufferSegment != null)
            {
                this.session_.Log.OnFatalEvent("Reading memory segment not null before reading starts");
                _parentBufferSegment.Release();
                _parentBufferSegment = null;
            }

            _parentBufferSegment = _segmentsManager.FetchNextSegmet();
        }

        public void TearDownAfterReading()
        {
            //defensive here - PrepareForReading() might not have been called

            if (_parentBufferSegment != null)
            {
                _parentBufferSegment.Release();
                _parentBufferSegment = null;
            }

            if (_subsegmentsBatch != null)
            {
                for (int idx = 0; idx < _subsegmentsBatch.Length; idx++)
                {
                    if (_subsegmentsBatch[idx] != null)
                    {
                        this._segmentsManager.Return(_subsegmentsBatch[idx]);
                        _subsegmentsBatch[idx] = null;
                    }
                }
            }
        }

        public bool Read()
        {
            if (session_.PoolBuffers || session_.BypassMessageParsing)
            {
                return Read_New();
            }
            else
            {
                return Read_Old();
            }
        }

        private int _remainingMessagesToCallNext = 100;

        private void TimerResetCallback(object unused)
        {
            //not synchronized, but it will write hrough eventually
            _remainingMessagesToCallNext = 0;
        }

        private bool Read_New()
        {
            try
            {
                // SslStream don't play nice with timeouts so we never timeout and perform session management on another thread
                int bytesRead = ReadSome(_parentBufferSegment);
                if (bytesRead > 0)
                {
                    this.session_.Application.OnBytesRead(bytesRead);
                    //parser_.AddToStream(System.Text.Encoding.UTF8.GetString(readBuffer_, 0, bytesRead));
                }
                else if (null != session_)
                {
                    session_.Next();
                    _remainingMessagesToCallNext = 100;
                }
                else
                {
                    throw new QuickFIXException("Initiator timed out while reading socket");
                }

                ProcessStream( /*_bufferSegment*/);
                _parentBufferSegment = _segmentsManager.FetchNextIfNeeded(_parentBufferSegment);
                if (_segmentsManager.NeedsMoveToLargeUnpooledSegment(_parentBufferSegment))
                {
                    //session_.Log.OnEvent("Allocation of large message segment was required");
                    session_.Application.OnUnverifiedMessage();
                    _parentBufferSegment = _segmentsManager.MoveToLargeUnpooledSegment(_parentBufferSegment);
                }
                return true;
            }
            catch (System.ObjectDisposedException e)
            {
                // this exception means socket_ is already closed when poll() is called
                if (isDisconnectRequested_ == false)
                {
                    // for lack of a better idea, do what the general exception does
                    if (null != session_)
                        session_.Disconnect(e.ToString());
                    else
                        Disconnect();
                }
            }
            catch (SocketException e)
            {
                // this exception means socket_ is already closed when poll() is called
                if (isDisconnectRequested_ == false)
                {
                    // for lack of a better idea, do what the general exception does
                    if (null != session_)
                        session_.Disconnect(e.ToString());
                    else
                        Disconnect();
                }
            }
            catch (IOException e)
            {
                // this exception means socket_ is already closed when poll() is called
                if (isDisconnectRequested_ == false)
                {
                    // for lack of a better idea, do what the general exception does
                    if (null != session_)
                        session_.Disconnect(e.ToString());
                    else
                        Disconnect();
                }
            }
            catch (System.Exception e)
            {
                if (null != session_)
                    session_.ForcefullLogout(e.ToString(), e.GetType().ToString());
                else
                    Disconnect();
            }

            return false;
        }

        private bool Read_Old()
        {
            try
            {
                while (true)
                {
                    // SslStream don't play nice with timeouts so we never timeout and perform session management on another thread
                    int bytesRead = ReadSome_Old(readBuffer_, 2000);
                    if (bytesRead > 0)
                    {
                        this.session_.Application.OnBytesRead(bytesRead);
                        parser_.AddToStream(System.Text.Encoding.UTF8.GetString(readBuffer_, 0, bytesRead));
                    }
                    else if (null != session_)
                    {
                        session_.Next();
                    }
                    else
                    {
                        throw new QuickFIXException("Initiator timed out while reading socket");
                    }

                    ProcessStream_Old();
                    return true;
                }
            }
            catch (System.ObjectDisposedException e)
            {
                // this exception means socket_ is already closed when poll() is called
                if (isDisconnectRequested_ == false)
                {
                    // for lack of a better idea, do what the general exception does
                    if (null != session_)
                        session_.Disconnect(e.ToString());
                    else
                        Disconnect();
                }
            }
            catch (SocketException e)
            {
                // this exception means socket_ is already closed when poll() is called
                if (isDisconnectRequested_ == false)
                {
                    // for lack of a better idea, do what the general exception does
                    if (null != session_)
                        session_.Disconnect(e.ToString());
                    else
                        Disconnect();
                }
            }
            catch (IOException e)
            {
                // this exception means socket_ is already closed when poll() is called
                if (isDisconnectRequested_ == false)
                {
                    // for lack of a better idea, do what the general exception does
                    if (null != session_)
                        session_.Disconnect(e.ToString());
                    else
                        Disconnect();
                }
            }
            catch (System.Exception e)
            {
                if (null != session_)
                    session_.ForcefullLogout(e.ToString(), e.GetType().ToString());
                else
                    Disconnect();
            }
            return false;
        }

        /// <summary>
        /// Keep a handle to the current outstanding read request (if any)
        /// </summary>
        private IAsyncResult currentReadRequest_;
        /// <summary>
        /// Reads data from the network into the specified buffer.
        /// It will wait up to the specified number of milliseconds for data to arrive,
        /// if no data has arrived after the specified number of milliseconds then the function returns 0
        /// </summary>
        /// <param name="bufferSegment">The buffer.</param>
        /// <returns>The number of bytes read into the buffer</returns>
        /// <exception cref="System.Net.Sockets.SocketException">On connection reset</exception>
        protected int ReadSome(QuickFixParentBufferSegmentItem bufferSegment)
        {
            try
            {
                int bytesRead = stream_.Read(bufferSegment.Buffer, bufferSegment.FreeSpaceStart,
                    bufferSegment.BytesAvailableTowardsEnd);
                if (0 == bytesRead)
                    throw new SocketException(System.Convert.ToInt32(SocketError.ConnectionReset));
                bufferSegment.IncreaseContentSize(bytesRead);
                return bytesRead;
            }
            catch (System.IO.IOException ex) // Timeout
            {
                var inner = ex.InnerException as SocketException;
                if (inner != null && inner.SocketErrorCode == SocketError.TimedOut)
                {
                    // Nothing read 
                    return 0;
                }
                else if (inner != null)
                {
                    throw inner; //rethrow SocketException part (which we have exception logic for)
                }
                else
                    throw; //rethrow original exception
            }
        }

        private void ProcessStream( /*BufferSegment bufferSegment*/)
        {
            //TODO: checking checksum
            QuickFixSubsegment nextMessageSegment;
            while ((nextMessageSegment = _parser.ReadFixMessage(this._parentBufferSegment)) != null)
            {
                try
                {
                    bool quickParsed = false;

                    if (session_.BypassMessageParsing)
                    {
                        try
                        {
                            quickParsed = session_.Application.CrackRawMessage(nextMessageSegment);
                            if (quickParsed)
                                session_.NextTargetMessageReceived();
                            else
                                session_.Application.OnSlowParsedMessage();
                        }
                        catch (MessageParsingException e)
                        {
                            quickParsed = true;
                            this.HandleMessageParsingException(e, nextMessageSegment.GetContentAsAsciiString());
                            session_.Application.OnUnverifiedMessage();
                        }

                        if (quickParsed && --_remainingMessagesToCallNext <= 0)
                        {
                            session_.Next();
                            _remainingMessagesToCallNext = 100;
                        }
                    }

                    if (!quickParsed)
                    {
                        string msg = nextMessageSegment.GetContentAsAsciiString();

                        if (!session_.BypassMessageParsing || !session_.Application.CanCrackRawMessages)
                            session_.Log.OnIncoming(msg);
                        session_.Next(msg);
                        _remainingMessagesToCallNext = 100;
                    }
                }
                finally
                {
                    nextMessageSegment.Release();
                }
            }
        }

        private void HandleMessageParsingException(MessageParsingException e, string message)
        {
            session_.Log.OnFatalEvent(string.Format("Exception during parsing message [{0}]: {1}", message, e));
            int? tagId = null;
            if (e is TagParsingException)
                tagId = (e as TagParsingException).TagId;

            session_.GenerateReject(message, tagId);
        }



        /// //////////////////////////////////////////////
        /// 

        protected int ReadSome_Old(byte[] buffer, int timeoutMilliseconds)
        {
            // NOTE: THIS FUNCTION IS EXACTLY THE SAME AS THE ONE IN SocketReader any changes here should 
            // also be performed there
            try
            {
                // Begin read if it is not already started
                if (currentReadRequest_ == null)
                    currentReadRequest_ = stream_.BeginRead(buffer, 0, buffer.Length, callback: null, state: null);

                // Wait for it to complete (given timeout)
                currentReadRequest_.AsyncWaitHandle.WaitOne(timeoutMilliseconds);

                if (currentReadRequest_.IsCompleted)
                {
                    // Make sure to set currentReadRequest_ to before retreiving result 
                    // so a new read can be started next time even if an exception is thrown
                    var request = currentReadRequest_;
                    currentReadRequest_ = null;

                    int bytesRead = stream_.EndRead(request);
                    if (0 == bytesRead)
                        throw new SocketException(System.Convert.ToInt32(SocketError.ConnectionReset));

                    return bytesRead;
                }
                else
                    return 0;
            }
            catch (System.IO.IOException ex) // Timeout
            {
                var inner = ex.InnerException as SocketException;
                if (inner != null && inner.SocketErrorCode == SocketError.TimedOut)
                {
                    // Nothing read 
                    return 0;
                }
                else if (inner != null)
                {
                    throw inner; //rethrow SocketException part (which we have exception logic for)
                }
                else
                    throw; //rethrow original exception
            }
        }



        private void ProcessStream_Old()
        {
            string msg;
            while (parser_.ReadFixMessage(out msg))
            {
                session_.Log.OnIncoming(msg);
                session_.Next(msg);
            }
        }

        /// 
        /// //////////////////////////////////////////////

        #region Responder Members

        public bool Send(string data)
        {
            byte[] rawData = System.Text.Encoding.UTF8.GetBytes(data);
            stream_.Write(rawData, 0, rawData.Length);
            return true;
        }

        public void Disconnect()
        {
            isDisconnectRequested_ = true;
            if (stream_ != null)
                stream_.Close();
        }

        #endregion
    }
}
