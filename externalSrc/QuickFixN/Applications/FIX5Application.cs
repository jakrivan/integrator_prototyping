﻿
using System.Xml;

namespace QuickFix.Applications
{
    public class FIX5Application : IApplication
    {
        public void FromAdmin(Message message, SessionID sessionID)
        {
            System.Diagnostics.Debug.WriteLine("Received FIX admin message: " + message.ToString());
        }

        public void FromApp(Message message, SessionID sessionID)
        {
            System.Diagnostics.Debug.WriteLine("Received FIX app message: " + message.ToString());
        }

        public void OnCreate(SessionID sessionID) { }
        public void OnPhysicllyConnected(SessionID sessionID) { }
        public void OnLogout(SessionID sessionID) { }
        public void OnLogon(SessionID sessionID) { }
        public void ToAdmin(Message message, SessionID sessionID) { }
        public void ToApp(Message message, SessionID sessionID) { }
        public void OnBytesRead(int bytesCount) { }
        public void OnUnverifiedMessage() { }
        public void OnSlowParsedMessage() { }
        public byte[] GetCertificateFile(string certificateName)
        {
            return null;
        }
        public XmlDocument GetXmlDictionary(string dictionaryName)
        {
            return null;
        }

        public bool CrackRawMessage(Kreslik.Integrator.LowLatencyUtils.QuickFixSubsegment bufferSegment)
        {
            return false;
        }

        public bool CanCrackRawMessages { get { return false; } }

        public void OverrideCommonMessageParser(Kreslik.Integrator.Contracts.Internal.IFixMessageCracker parser)
        { }
    }
}
