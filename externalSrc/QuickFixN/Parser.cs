﻿
using System.Linq.Expressions;
using Kreslik.Integrator.LowLatencyUtils;

namespace QuickFix
{

    public class BufferParser
    {
        //private readonly QuickFixSubsegment _inputBufferSegment;
        //public QuickFixSubsegment OutputBufferSegment { get; private set; }

        private const byte _ZERO = 0x30; //0
        private const byte _ONE = 0x31; //1
        private const byte _MSG_START = 0x38; //8
        private const byte _MSGSIZE_START = 0x39; //9
        private const byte _VAL_SEPARATOR = 0x3D; //=
        private const byte _MSG_SEPARATOR = 0x01; //SOH
        private readonly byte[] _MSG_LEN_SEQUNCE = new byte[] { _MSG_SEPARATOR, _MSGSIZE_START, _VAL_SEPARATOR };

        private QuickFixSegmentsManager _quickFixSegmentsManager;
        private QuickFixSubsegment[] _subsegmentsBatch;
        private int _subsegmentIdx = 0;

        public BufferParser(QuickFixSegmentsManager quickFixSegmentsManager, QuickFixSubsegment[] subsegmentsBatch)
        {
            this._quickFixSegmentsManager = quickFixSegmentsManager;
            this._subsegmentsBatch = subsegmentsBatch;
        }

        public QuickFixSubsegment ReadFixMessage(QuickFixParentBufferSegmentItem inputBufferSegment)
        {
            //anything in the buffer?
            if (inputBufferSegment.ContentSize < 2)
                return null;

            //find message start and remove if anything before (just advance start idx)
            while (inputBufferSegment.ContentSize > 0 &&
                   inputBufferSegment.Buffer[inputBufferSegment.ContentStart] != _MSG_START)
            {
                inputBufferSegment.CutContentFromBegining(1);
            }

            //find length and evaluat if it can be in content
            int messageBodyStart = 0;
            //int length = this.ExtractLength(out messageBodyStart);
            if (!inputBufferSegment.FindByteSequence(_MSG_LEN_SEQUNCE, ref messageBodyStart))
                return null;
            messageBodyStart += _MSG_LEN_SEQUNCE.Length;
            int length = inputBufferSegment.ToInt(ref messageBodyStart);

            if (length <= 0)
                return null;

            // if no and if it wouln't find - shif the content
            //  + 1 because we compare index with size - size is 1 higher than index
            if (messageBodyStart + length + 7 + 1 > inputBufferSegment.ContentSize)
            {
                //this we cannot do as we are sharing buffers
                //if (messageBodyStart + length >
                //    inputBufferSegment.ContentSize + inputBufferSegment.BytesAvailableTowardsEnd)
                //{
                //    inputBufferSegment.ShiftContentToBegining();
                //}

                return null;
            }

            //if yes - find the end end create message
            if(!
                (
                //'<SOH>10=xxx<SOH>'
                inputBufferSegment.Buffer[inputBufferSegment.ContentStart + messageBodyStart + length] == _MSG_SEPARATOR &&
                inputBufferSegment.Buffer[inputBufferSegment.ContentStart + messageBodyStart + length + 1] == _ONE &&
                inputBufferSegment.Buffer[inputBufferSegment.ContentStart + messageBodyStart + length + 2] == _ZERO &&
                inputBufferSegment.Buffer[inputBufferSegment.ContentStart + messageBodyStart + length + 3] == _VAL_SEPARATOR &&
                inputBufferSegment.Buffer[inputBufferSegment.ContentStart + messageBodyStart + length + 7] == _MSG_SEPARATOR
                )
             )
            {    
                throw new MessageParseError("Invalid body length");
            }


            QuickFixSubsegment childSegment = this.GetNextChildSubsegment();
            //  + 1 because we compare index with size - size is 1 higher than index
            return _quickFixSegmentsManager.MoveContentFragmentFromParentToChild(inputBufferSegment, childSegment,
                messageBodyStart + 1, length);
        }

        private QuickFixSubsegment GetNextChildSubsegment()
        {
            QuickFixSubsegment childSegment;
            if (_subsegmentsBatch != null)
            {
                if (_subsegmentIdx >= _subsegmentsBatch.Length || _subsegmentsBatch[_subsegmentIdx] == null)
                {
                    _quickFixSegmentsManager.TryFetchBatchOfChildSubsegments(_subsegmentsBatch);
                    _subsegmentIdx = 0;
                }

                childSegment = _subsegmentsBatch[_subsegmentIdx];
                //not needed - for fail fast purposes
                _subsegmentsBatch[_subsegmentIdx] = null;
                _subsegmentIdx++;
            }
            else
            {
                childSegment = _quickFixSegmentsManager.FetchNextChildSubsegment();
            }

            return childSegment;
        }
    }

    /// <summary>
    /// TODO use a byte[]/char[] for buffer_ instead of a string
    /// </summary>
    public class Parser
    {
        private string buffer_ = "";

        public void AddToStream(string data)
        {
            buffer_ += data;
        }

        public bool ReadFixMessage(out string msg)
        {
            msg = "";
            
            if(buffer_.Length < 2)
                return false;
            
            int pos = 0;

            pos = buffer_.IndexOf("8=");
            if(-1 == pos)
                return false;

            buffer_ = buffer_.Remove(0, pos);
            pos = 0;

            int length = 0;
            int msgBodyStart = 0;
            int msgBodyEnd = 0;

            try
            {
                if (!ExtractLength(out length, out pos, buffer_))
                    return false;

                msgBodyStart = pos;
                
                pos += length;
                if (buffer_.Length < pos)
                    return false;

                pos = buffer_.IndexOf("\x01" + "10=", pos - 1);
                if (-1 == pos)
                    return false;
                msgBodyEnd = pos + 1;
                pos += 4;

                pos = buffer_.IndexOf("\x01", pos);
                if (-1 == pos)
                    return false;
                pos += 1;

                if ((msgBodyEnd - msgBodyStart) != length)
                    throw new MessageParseError("Invalid body length");

                msg = buffer_.Substring(0, pos);
                buffer_ = buffer_.Remove(0, pos);
                return true;
            }
            catch (MessageParseError e)
            {
                if ((length > 0) && (pos <= buffer_.Length))
                    buffer_ = buffer_.Remove(0, pos);
                else
                    buffer_ = buffer_.Remove(0, buffer_.Length);
                throw e;
            }
        }

        public bool ExtractLength(out int length, out int pos, string buf)
        {
            length = 0;
            pos = 0;

            if (buf.Length < 1)
                return false;

            int startPos = buf.IndexOf("\x01" + "9=", 0);
            if(-1 == startPos)
                return false;
            startPos +=3;

            int endPos = buf.IndexOf("\x01", startPos);
            if(-1 == endPos)
                return false;

            string strLength = buf.Substring(startPos, endPos - startPos);

            try
            {
                length = Fields.Converters.IntConverter.Convert(strLength);
                if(length < 0)
                    throw new MessageParseError("Invalid BodyLength (" + length + ")");
            }
            catch(FieldConvertError e)
            {
                throw new MessageParseError(e.Message, e);
            }

            pos = endPos + 1;
            return true;
        }

        private bool Fail(string what)
        {
            System.Console.WriteLine("Parser failed: " + what);
            return false;
        }
    }
}
