﻿
namespace QuickFix
{
    /// <summary>
    /// Creates a message store that stores all data in memory
    /// </summary>
    public class MemoryStoreFactory : IMessageStoreFactory
    {
        #region MessageStoreFactory Members

        private readonly bool _cacheMessages;

        public MemoryStoreFactory(bool cacheMessages)
        {
            this._cacheMessages = cacheMessages;
        }

        public IMessageStore Create(SessionID sessionID)
        {
            return new MemoryStore(this._cacheMessages);
        }

        #endregion
    }
}
