﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace FIXDicParser
{
    public static class ORDLogsParser
    {
        public static string GetAllPairsNop(string directoryWithOrdLogs)
        {
            decimal[] buyQtys = new decimal[Symbol.ValuesCount];
            decimal[] sellQtys = new decimal[Symbol.ValuesCount];

            foreach (string logfile in Directory.EnumerateFiles(directoryWithOrdLogs))
            {
                int ordersNum = 0;

                foreach (string line in File.ReadAllLines(logfile))
                {
                    //Execution report for filled orders. Only incoming! (for 2 way heandshake we als send the message back - so we don't want to count it twice)
                    if (line.Contains("EXECUTION_REPORT") && line.Contains("[OrdStatus]=2 [Filled]") && line.Contains("<--- Incoming"))
                    {
                        //[OrdStatus]=2 [Filled]
                        //[CumQty]=500000.00

                        ordersNum++;

                        int startIdx = line.IndexOf("[CumQty]=");
                        if (startIdx <= 0)
                        {
                            throw new Exception();
                        }
                        startIdx += "[CumQty]=".Length;
                        int endIndex = line.IndexOfAny(new char[] { ' ', '.' }, startIdx);
                        if (endIndex <= startIdx)
                        {
                            throw new Exception();
                        }

                        decimal qty = decimal.Parse(line.Substring(startIdx, endIndex - startIdx));

                        int symbolCount = 0;
                        int symbolIdx = 1000;
                        foreach (Symbol symbol in Symbol.Values)
                        {
                            if (line.Contains(symbol.ShortName) || line.Contains(symbol.ToString()))
                            {
                                symbolIdx = (int)symbol;
                                symbolCount++;
                            }
                        }

                        if (symbolCount != 1)
                        {
                            throw new Exception();
                        }


                        if (line.Contains("Buy"))
                        {
                            buyQtys[symbolIdx] += qty;
                        }

                        if (line.Contains("Sell"))
                        {
                            sellQtys[symbolIdx] += qty;
                        }


                        if (line.Contains("Buy") && line.Contains("Sell"))
                        {
                            throw new Exception();
                        }

                        if (!(line.Contains("Buy") || line.Contains("Sell")))
                        {
                            throw new Exception();
                        }

                    }
                }

                if (ordersNum == 0)
                {
                    //throw new Exception("0 Orders!");
                    Console.WriteLine("0 Orders in file {0}!", logfile);
                }

            }

            StringBuilder issues = new StringBuilder();
            StringBuilder allPositions = new StringBuilder();

            foreach (Symbol symbol in Symbol.Values)
            {
                string line = string.Format("{0}: buy: {1} sell: {2}", symbol.ToString(), buyQtys[(int)symbol],
                                            sellQtys[(int)symbol]);

                allPositions.AppendLine(line);

                if (buyQtys[(int)symbol] != sellQtys[(int)symbol])
                {
                    issues.AppendLine(line);
                }
            }

            string isssuesString = issues.ToString();
            string allPositionsString = allPositions.ToString();

            if (!string.IsNullOrWhiteSpace(isssuesString))
            {
                Console.WriteLine(isssuesString);
            }

            return allPositionsString;
        }
    }
}
