﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace FIXDicParser
{
    class Program
    {
        public static string CutString(string line, string startString, string endString)
        {
            if (string.IsNullOrEmpty(line) || string.IsNullOrEmpty(startString) || string.IsNullOrEmpty(endString))
                return string.Empty;

            int startIdx = line.IndexOf(startString);  

            if (startIdx < 0)
            {
                return string.Empty;
            }

            startIdx += startString.Length;

            int endIdx = line.IndexOf(endString, startIdx);

            if (endIdx < 0)
            {
                return string.Empty;
            }

            return line.Substring(startIdx, endIdx - startIdx);
        }

        public static string FilterOutUnusedFieldDefinitions(string filename)
        {
            string fieldUsageStartString = "<field name=\"";
            string groupUsageStartString = "<group name=\"";
            string fieldNumberStartString = "<field number=\"";
            string endQuote = "\"";
            string nameStartString = "name=\"";
            string numberStartString = "number=\"";

            Dictionary<string, string> usedFieldCodes = new Dictionary<string, string>();
            List<string> unusedFields = new List<string>();

            string[] lines = File.ReadAllLines(
                filename);

            foreach (var line in lines)
            {
                if (line.Contains(fieldUsageStartString))
                {
                    usedFieldCodes[CutString(line, fieldUsageStartString, endQuote)] = "-";
                }

                if (line.Contains(groupUsageStartString))
                {
                    usedFieldCodes[CutString(line, groupUsageStartString, endQuote)] = "-";
                }

                if (line.Contains(fieldNumberStartString))
                {
                    string fieldCode = CutString(line, fieldNumberStartString, endQuote);
                    string fieldName = CutString(line, nameStartString, endQuote);

                    if (usedFieldCodes.ContainsKey(fieldName))
                    {
                        usedFieldCodes[fieldName] = fieldCode;
                    }
                    else
                    {
                        unusedFields.Add(fieldCode);
                    }
                }
            }

            string unusedFieldsString = string.Join(Environment.NewLine, unusedFields);
            StringBuilder usedFieldsStringBld = new StringBuilder();

            foreach (KeyValuePair<string, string> usedFieldCode in usedFieldCodes)
            {
                usedFieldsStringBld.AppendFormat("{0}\t{1}", usedFieldCode.Key, usedFieldCode.Value);
                usedFieldsStringBld.AppendLine();
            }

            String usedFieldsString = usedFieldsStringBld.ToString();


            // now remove them

            HashSet<string> unusedFiledsSet = new HashSet<string>(unusedFields/*.Select(s => "number=\"" + s + "\"")*/);

            StringBuilder resultFileBld = new StringBuilder();

            bool previousLineRemoved = false;
            bool filteringFields = false;
            foreach (string line in lines)
            {
                if (filteringFields)
                {
                    if (line.Contains("</fields>"))
                    {
                        filteringFields = false;
                        resultFileBld.AppendLine(line);
                        continue;
                    }

                    string fieldId = CutString(line, numberStartString, endQuote);
                    if (string.IsNullOrEmpty(fieldId))
                    {
                        if (!previousLineRemoved) resultFileBld.AppendLine(line);
                    }
                    else
                    {
                        if (unusedFiledsSet.Contains(fieldId))
                        {
                            previousLineRemoved = true;
                        }
                        else
                        {
                            previousLineRemoved = false;
                            resultFileBld.AppendLine(line);
                        }
                    }
                }
                else
                {
                    resultFileBld.AppendLine(line);
                    if (line.Contains("<fields>"))
                    {
                        filteringFields = true;
                    }
                }
            }


            return resultFileBld.ToString();
        }

        static Dictionary<string, string> GetTagMappings(string fixDictionaryFile)
        {
            string fieldNumberStartString = "<field number=\"";
            string endQuote = "\"";
            string nameStartString = "name=\"";

            Dictionary<string, string> fieldMapping = new Dictionary<string, string>();

            foreach (string line in File.ReadAllLines(fixDictionaryFile))
            {
                if (line.Contains(fieldNumberStartString))
                {
                    string fieldCode = CutString(line, fieldNumberStartString, endQuote);
                    string fieldName = CutString(line, nameStartString, endQuote);

                    fieldMapping[fieldCode] = fieldName;
                }
            }

            return fieldMapping;
        }

        private static void ExtractFixMessagesFromLogs(IEnumerable<string> logFilenames,
                                                       out Dictionary<string, HashSet<string>> incomingMessages,
                                                       out Dictionary<string, HashSet<string>> outgoingMessages)
        {
            const string incomingMessagePrefix = "<--- Incoming Raw:";
            const string outgoingMessagePrefix = "---> Outgoing Raw:";
            int incomingMessagePrefixLength = incomingMessagePrefix.Length;
            int outgoingMessagePrefixLength = outgoingMessagePrefix.Length;

            incomingMessages = new Dictionary<string, HashSet<string>>();
            outgoingMessages = new Dictionary<string, HashSet<string>>();

            foreach (string logFilename in logFilenames)
            {
                foreach (string line in File.ReadAllLines(logFilename))
                {
                    int incomingMsgIdx = line.IndexOf(incomingMessagePrefix, StringComparison.Ordinal);
                    if (incomingMsgIdx > 0)
                    {
                        string message = line.Substring(incomingMsgIdx + incomingMessagePrefixLength);
                        string messageType = CutString(message, msgTypePrefix, fieldsSplitter.ToString());
                        HashSet<string> tags;
                        if (incomingMessages.ContainsKey(messageType))
                        {
                            tags = incomingMessages[messageType];
                        }
                        else
                        {
                            tags = new HashSet<string>();
                            incomingMessages[messageType] = tags;
                        }

                        PurgeValuesFromFIXMessage(message, ref tags);
                    }
                    else
                    {
                        int outgoingMsgIdx = line.IndexOf(outgoingMessagePrefix, StringComparison.Ordinal);
                        if (outgoingMsgIdx > 0)
                        {
                            string message = line.Substring(outgoingMsgIdx + outgoingMessagePrefixLength);
                            string messageType = CutString(message, msgTypePrefix, fieldsSplitter.ToString());
                            HashSet<string> tags;
                            if (outgoingMessages.ContainsKey(messageType))
                            {
                                tags = outgoingMessages[messageType];
                            }
                            else
                            {
                                tags = new HashSet<string>();
                                outgoingMessages[messageType] = tags;
                            }

                            PurgeValuesFromFIXMessage(message, ref tags);
                        }
                    }
                }
            }
        }

        private static bool ContainsValue(Dictionary<string, HashSet<string>> messages, string tagToSearch)
        {
            return
                messages.Values.SelectMany(s => s.Select(str => str))
                        .Any(tag => String.Equals(tagToSearch, tag, StringComparison.OrdinalIgnoreCase));

            //foreach (HashSet<string> tags in messages.Values)
            //{
            //    foreach (string tag in tags)
            //    {
            //        if (String.Equals(tagToSearch, tag, StringComparison.OrdinalIgnoreCase))
            //        {
            //            return true;
            //        }
            //    }
            //}

            //return false;
        }

        private static string PurgeMessagesDictionary(Dictionary<string, HashSet<string>> incomingMessages,
                                                      Dictionary<string, HashSet<string>> outgoingMessages,
                                                      string fixDictionaryFile)
        {
            string headerStart = @"<header>";
            string headerEnd = @"</header>";
            string trailerStart = @"<trailer>";
            string trailerEnd = @"</trailer>";
            string messagesStart = @"<messages>";
            string messagesEnd = @"</messages>";
            string messageStart = "<message name=\"";
            string messageEnd = @"</message>";

            string fieldUsageStartString = "<field name=\"";
            string groupUsageStartString = "<group name=\"";
            string fieldNumberStartString = "<field number=\"";
            string endQuote = "\"";
            string nameStartString = "name=\"";
            string numberStartString = "number=\"";
            string messageTypeStart = "msgtype=\"";

            StringBuilder resultFileBuilder = new StringBuilder();
            HashSet<string> headerTags = new HashSet<string>();

            Dictionary<string, string> tagNameMappings = GetTagMappings(fixDictionaryFile);
            
            //Create one dictionary with tag names (not numbers)
            Dictionary<string, HashSet<string>> messages = new Dictionary<string, HashSet<string>>();

            foreach (KeyValuePair<string, HashSet<string>> keyValuePair in incomingMessages)
            {
                messages[keyValuePair.Key] =
                    new HashSet<string>(
                        keyValuePair.Value.Select(s => tagNameMappings.ContainsKey(s) ? tagNameMappings[s] : s));
            }

            foreach (KeyValuePair<string, HashSet<string>> keyValuePair in outgoingMessages)
            {
                if (messages.ContainsKey(keyValuePair.Key))
                {
                    messages[keyValuePair.Key].UnionWith(keyValuePair.Value.Select(s => tagNameMappings[s]));
                }
                else
                {
                    messages[keyValuePair.Key] =
                        new HashSet<string>(
                            keyValuePair.Value.Select(s => tagNameMappings.ContainsKey(s) ? tagNameMappings[s] : s));
                }
            }

            bool filetringHeader = false;
            bool filteringMessages = false;
            bool deletingMessage = false;
            bool filteringMessage = false;
            string messageType = null;
            foreach (string line in File.ReadAllLines(fixDictionaryFile))
            {
                if (filetringHeader)
                {
                    if (line.Contains(fieldUsageStartString))
                    {
                        string tagName = CutString(line, fieldUsageStartString, endQuote);
                        if (ContainsValue(messages, tagName))
                        {
                            headerTags.Add(tagName);
                            resultFileBuilder.AppendLine(line);
                        }
                    } 
                    else if (line.Contains(groupUsageStartString))
                    {
                        string tagName = CutString(line, groupUsageStartString, endQuote);
                        if (ContainsValue(messages, tagName))
                        {
                            headerTags.Add(tagName);
                            resultFileBuilder.AppendLine(line);
                        }
                    }
                    else if (line.Contains(headerEnd) || line.Contains(trailerEnd))
                    {
                        filetringHeader = false;
                        resultFileBuilder.AppendLine(line);
                    }
                }
                else if (filteringMessages)
                {
                    if (deletingMessage)
                    {
                        if (line.Contains(messageEnd))
                        {
                            deletingMessage = false;
                        }
                    }
                    else if (filteringMessage)
                    {
                        if (line.Contains(fieldUsageStartString))
                        {
                            string tagName = CutString(line, fieldUsageStartString, endQuote);
                            if (messages[messageType].Contains(tagName))
                            {
                                resultFileBuilder.AppendLine(line);
                                messages[messageType].Remove(tagName);
                            }
                        }
                        else if (line.Contains(groupUsageStartString))
                        {
                            string tagName = CutString(line, groupUsageStartString, endQuote);
                            if (messages[messageType].Contains(tagName))
                            {
                                resultFileBuilder.AppendLine(line);
                                messages[messageType].Remove(tagName);
                            }
                            
                        }
                        else if (line.Contains(messageEnd))
                        {
                            filteringMessage = false;
                            foreach (string missingTag in messages[messageType])
                            {
                                if (!headerTags.Contains(missingTag))
                                {
                                    Console.WriteLine("MISSING TAG: {0} {1}", messageType, missingTag);
                                    resultFileBuilder.AppendFormat("MISSING TAG: {0} {1}", messageType, missingTag);
                                    resultFileBuilder.AppendLine();
                                }
                            }
                            messageType = null;
                            resultFileBuilder.AppendLine(line);
                        }
                        else
                        {
                            resultFileBuilder.AppendLine(line);
                        }
                    }
                    else
                    {
                        if (line.Contains(messagesEnd))
                        {
                            filteringMessages = false;
                            resultFileBuilder.AppendLine(line);
                        }
                        else if(line.Contains(messageStart))
                        {
                            messageType = CutString(line, messageTypeStart, endQuote);
                            if (messages.ContainsKey(messageType))
                            {
                                filteringMessage = true;
                                resultFileBuilder.AppendLine(line);
                            }
                            else
                            {
                                deletingMessage = true;
                            }
                        }
                        else
                        {
                            resultFileBuilder.AppendLine(line);
                        }
                    }
                }
                else
                {
                    resultFileBuilder.AppendLine(line);
                    if (line.Contains(headerStart) || line.Contains(trailerStart))
                    {
                        filetringHeader = true;
                    }
                    else if (line.Contains(messagesStart))
                    {
                        filteringMessages = true;
                    }
                }
            }

            return resultFileBuilder.ToString();
        }

        static void ExtractFixMessagesFromLogIntoFiles(IEnumerable<string> logFilenames, string outputDirectory,
                                                       string outgoingMessagesFile, string incomingMessagesFile, Dictionary<string, string> tagNameMappings)
        {
            const string incomingMessagePrefix = "<--- Incoming Raw:";
            const string outgoingMessagePrefix = "---> Outgoing Raw:";
            int incomingMessagePrefixLength = incomingMessagePrefix.Length;
            int outgoingMessagePrefixLength = outgoingMessagePrefix.Length;

            if (!Directory.Exists(outputDirectory))
            {
                throw new Exception("Output directory doesn't exist");
            }

            outgoingMessagesFile = Path.Combine(outputDirectory, outgoingMessagesFile);
            incomingMessagesFile = Path.Combine(outputDirectory, incomingMessagesFile);

            if (File.Exists(outgoingMessagesFile) || File.Exists(incomingMessagesFile))
            {
                throw new Exception("Output files already exists");
            }

            Dictionary<string, HashSet<string>> incomingMessages;
            Dictionary<string, HashSet<string>> outgoingMessages;

            ExtractFixMessagesFromLogs(logFilenames, out incomingMessages, out outgoingMessages);

            File.WriteAllLines(incomingMessagesFile,
                               incomingMessages.Select(
                                   pair =>
                                   pair.Key + ": " +
                                   string.Join(" ",
                                               pair.Value.Select(
                                                   tag =>
                                                   tagNameMappings != null && tagNameMappings.ContainsKey(tag)
                                                       ? tagNameMappings[tag]
                                                       : tag))));
            File.WriteAllLines(outgoingMessagesFile,
                               outgoingMessages.Select(
                                   pair =>
                                   pair.Key + ": " +
                                   string.Join(" ",
                                               pair.Value.Select(
                                                   tag =>
                                                   tagNameMappings != null && tagNameMappings.ContainsKey(tag)
                                                       ? tagNameMappings[tag]
                                                       : tag))));
        }

        private static char fieldsSplitter = '\u0001';
        private static char fieldPartsSplitter = '=';
        private static string msgTypePrefix = "35=";

        static void PurgeValuesFromFIXMessage(string message,ref HashSet<string> tags)
        {
            Regex reg = new Regex(fieldPartsSplitter + ".*?" + fieldsSplitter);
            message = reg.Replace(message, " ");

            foreach (string tag in message.Split(new char[0], StringSplitOptions.RemoveEmptyEntries))
            {
                tags.Add(tag);
            }
        }

        static void Main(string[] args)
        {

            string s = ORDLogsParser.GetAllPairsNop(@"E:\KGT\data\ORDLogs_01_07_2013");


            //List<string> logFiles = new List<string>()
            //    {
            //        @"E:\KGT\src\Integrator_daily\prototypes\Kreslik.Integrator.ConsoleFIXMessagingDriver\bin\Debug\logs\2013-04-11\BOA_QUO.log",
            //        @"E:\KGT\src\Integrator_daily\prototypes\Kreslik.Integrator.ConsoleFIXMessagingDriver\bin\Debug\logs\2013-04-11\BOA_ORD.log",

            //        @"E:\KGT\src\Integrator_daily\prototypes\Kreslik.Integrator.ConsoleFIXMessagingDriver\bin\Debug\logs\2013-04-12\BOA_QUO.log",
            //        @"E:\KGT\src\Integrator_daily\prototypes\Kreslik.Integrator.ConsoleFIXMessagingDriver\bin\Debug\logs\2013-04-12\BOA_ORD.log"
            //    };

            //string dictionaryPath =
            //    @"E:\KGT\src\Integrator_daily\src\Kreslik.Integrator.FIXMessaging\CounterpartSpecific\FIXdictionaries\FIX43_BOA.xml";

            //Dictionary<string, HashSet<string>> incomingMessages;
            //Dictionary<string, HashSet<string>> outgoingMessages;

            //ExtractFixMessagesFromLogs(logFiles, out incomingMessages, out outgoingMessages);

            //string newFile = PurgeMessagesDictionary(incomingMessages, outgoingMessages, dictionaryPath);


            //then owerwrite the file and consult helpfile to add any unusual missing messages and tags (not captured in log files)
            // and then purge tag definitions by:

            string newFile =
                FilterOutUnusedFieldDefinitions(
                    @"E:\KGT\src\Integrator_daily\src\Kreslik.Integrator.FIXMessaging\CounterpartSpecific\FIXdictionaries\FIX42_MGS2.xml");


            //ExtractFixMessagesFromLogIntoFiles(logFiles,
            //                                   @"E:\KGT\src\Integrator_daily\prototypes\Kreslik.Integrator.ConsoleFIXMessagingDriver\bin\Debug\logs\2013-04-12",
            //                                   "BOA_outgoing.txt", "BOA_incoming.txt",
            //                                   GetTagMappings(
            //                                       @"E:\KGT\src\Integrator_daily\src\Kreslik.Integrator.FIXMessaging\CounterpartSpecific\FIXdictionaries\FIX43_BOA.xml"));
        }
    }
}
