﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.WinformFIXMessagingDriver;

namespace Kreslik.Integrator.WinformFIXMessagingDriver
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            PriceObjectPool.InitializeInstance(500);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            WindowMain winmain = new WindowMain();


            Application.Run(winmain);
        }
    }
}
