﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;
using System.Linq;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.RiskManagement;
using Kreslik.Integrator.SessionManagement;

//using Kreslik.Integrator.QuickFixTest.Core;

namespace Kreslik.Integrator.WinformFIXMessagingDriver
{
    public partial class WindowMain : Form
    {
        //private IMarketDataSession _marketDataSession;
        //private IOrderFlowSession _ordersSession;
        public Dictionary<SubscriptionRequestInfo, ISubscription> _subscriptions = new Dictionary<SubscriptionRequestInfo, ISubscription>();
        private DealInfo? _dealInfo;
        private TakerConnectionObjectsKeeper _takerConnectionObjectsKeeper;
        private IConnectionObjectsKeeper _connectionObjectsKeeper;
        private IEnumerable<IStpFlowSession> _stpConnectionSessions;
        private STPLogging _stpLogging;

        public WindowMain()
        {
            InitializeComponent();

            _initializeCounterpartComboBox();
            _initializeFxPairsComboBox();
            _initializeSenderSubIdComboBox();
            _initializeDealDirectionComboBox();
            _initializeDealConfirmCombobox();
        }

        void _initializeFxPairsComboBox()
        {
            //comboBoxFxPairs.Items.Add("AUD/USD");
            //comboBoxFxPairs.Items.Add("EUR/CHF");
            //comboBoxFxPairs.Items.Add("EUR/GBP");
            //comboBoxFxPairs.Items.Add("EUR/JPY");
            //comboBoxFxPairs.Items.Add("EUR/USD");
            //comboBoxFxPairs.Items.Add("GBP/JPY");
            //comboBoxFxPairs.Items.Add("GBP/USD");
            //comboBoxFxPairs.Items.Add("NZD/USD");
            //comboBoxFxPairs.Items.Add("USD/CAD");
            //comboBoxFxPairs.Items.Add("USD/CHF");
            //comboBoxFxPairs.Items.Add("USD/HKD");
            //comboBoxFxPairs.Items.Add("USD/JPY");
            //comboBoxFxPairs.Items.Add("USD/SGD");

            comboBoxFxPairs.DataSource = Symbol.StringValues.ToList();

            comboBoxFxPairs.SelectedItem = Symbol.EUR_USD.ToString();
        }

        private const string NODEALCONFIRMATION = "NOTHING";
        void _initializeDealConfirmCombobox()
        {
            comboDealConfirm.Items.Add(NODEALCONFIRMATION);
            foreach (var value in Enum.GetValues(typeof(DealConfirmationAction)))
            {
                comboDealConfirm.Items.Add(value.ToString());
            }
            comboDealConfirm.SelectedItem = NODEALCONFIRMATION;

            this.grpBxDealConfirmation.Visible = false;
        }

        void _initializeCounterpartComboBox()
        {
            counterpartComboBox.DataSource = Counterparty.StringValues.ToList();

            counterpartComboBox.SelectedItem = Counterparty.MGS.ToString();
        }

        void _initializeSenderSubIdComboBox()
        {
            comboBoxSenderSubId.Items.Add("KGT");
            comboBoxSenderSubId.Items.Add("KGT2");
            comboBoxSenderSubId.Items.Add("KGT3");
            comboBoxSenderSubId.Items.Add("KGT4");
            comboBoxSenderSubId.Items.Add("KGT_PRICE");

            comboBoxSenderSubId.SelectedItem = "KGT3";
        }

        void _initializeDealDirectionComboBox()
        {
            comboBoxDealDirection.Items.Add("BUY");
            comboBoxDealDirection.Items.Add("SELL");

            comboBoxDealDirection.SelectedItem = "BUY";
        }

        private void btnStartMarketDataInitiator_Click(object sender, EventArgs e)
        {
            if (this._connectionObjectsKeeper.MDSession != null)
                Task.Factory.StartNew(this._connectionObjectsKeeper.MDSession.Start);
        }

        private void btnStopMarketDataInitiator_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(this._connectionObjectsKeeper.MDSession.Stop);
        }

        private void btnStartOrderFlowInitiator_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(this._connectionObjectsKeeper.OFSession.Start);
            if (this._stpConnectionSessions != null)
            {
                Parallel.ForEach(_stpConnectionSessions, session => session.Start());
            }
        }

        private void btnStopOrderFlowInitiator_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(this._connectionObjectsKeeper.OFSession.Stop);
            if (this._stpConnectionSessions != null)
            {
                Parallel.ForEach(_stpConnectionSessions, session => session.Stop());
            }
        }



        private void btnSubscribe_Click(object sender, EventArgs e)
        {
            SubscriptionRequestInfo info = _getSubscriptionInfoFromForm();
            var s = this._takerConnectionObjectsKeeper.MarketDataSession.Subscribe(info);
            if (s != null)
            {
                this._subscriptions[info] = s;
            }
            else
            {
                MessageBox.Show("Subscription failed.", "Error");
            }
        }

        private void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            SubscriptionRequestInfo info = _getSubscriptionInfoFromForm();
            ISubscription subscription;
            if (this._subscriptions.TryGetValue(info, out subscription))
            {
                this._takerConnectionObjectsKeeper.MarketDataSession.Unsubscribe(subscription);
            }
            else
            {
                MessageBox.Show("Unsubscription failed.", "Error");
            }
        }

        private void btnDealOnNextQuote_Click(object sender, EventArgs e)
        {
            //Sessions.MarketDataSession.DealOnNextPrice(_getDealInfoFromForm());

            //this._sessionsWatcher.DealOnNextQuote(_getDealInfoFromForm());

            this._dealInfo = _getDealInfoFromForm();
        }

        private void btnDealWithoutQuote_Click(object sender, EventArgs e)
        {
            DealInfo dealInfo = _getDealInfoFromForm();

            if (dealInfo.Price.HasValue)
            {

                OrderRequestInfo order;

                if (dealInfo.IsPegged)
                {
                    if (dealInfo.MinQty.HasValue)
                    {
                        order = OrderRequestInfo.CreatePegged(dealInfo.DealSize, dealInfo.PegType.Value, dealInfo.PegDifference.Value, dealInfo.Price.Value,
                                                             dealInfo.DealDirection, (Symbol)dealInfo.Symbol,
                                                             dealInfo.TimeInForce, dealInfo.MinQty.Value, dealInfo.IsRiskRemoving);
                    }
                    else
                    {
                        order = OrderRequestInfo.CreatePegged(dealInfo.DealSize, dealInfo.PegType.Value, dealInfo.PegDifference.Value, dealInfo.Price.Value,
                                                             dealInfo.DealDirection, (Symbol)dealInfo.Symbol,
                                                             dealInfo.TimeInForce, dealInfo.IsRiskRemoving);
                    }
                }
                else
                {
                    if (dealInfo.MinQty.HasValue)
                    {
                        order = OrderRequestInfo.CreateLimit(dealInfo.DealSize, dealInfo.Price.Value,
                                                             dealInfo.DealDirection, (Symbol)dealInfo.Symbol,
                                                             dealInfo.TimeInForce, dealInfo.MinQty.Value, dealInfo.IsRiskRemoving);
                    }
                    else
                    {
                        order = OrderRequestInfo.CreateLimit(dealInfo.DealSize, dealInfo.Price.Value,
                                                             dealInfo.DealDirection, (Symbol)dealInfo.Symbol,
                                                             dealInfo.TimeInForce, dealInfo.IsRiskRemoving);
                    }
                }

                //order.SetRejectableDefaults(dealInfo.RejectableDealDefaults);

                this._takerConnectionObjectsKeeper.OrderFlowSession.SendOrder(order);
            }
            else
            {
                MessageBox.Show("Cannot send LIMIT order without price entered");
            }
        }


        SubscriptionRequestInfo _getSubscriptionInfoFromForm()
        {
            var symbol = comboBoxFxPairs.Text;
            var senderSubId = _getSenderSubIdOrNull();
            var subscriptionSize = decimal.Parse(textBoxSize.Text);

            return new SubscriptionRequestInfo((Symbol)symbol, subscriptionSize, senderSubId);
        }

        string _getSenderSubIdOrNull()
        {
            return checkBoxEnableSenderSubId.Checked ? comboBoxSenderSubId.Text : null;
        }

        DealInfo _getDealInfoFromForm()
        {
            var dealSize = decimal.Parse(textBoxSize.Text);
            var senderSubId = _getSenderSubIdOrNull();
            var symbol = comboBoxFxPairs.Text;
            var dealDirectionStr = comboBoxDealDirection.Text;
            DealDirection dealDirection;
            TimeInForce timeInForce = chckIoc.Checked ? TimeInForce.ImmediateOrKill : TimeInForce.Day;
            decimal? price = null;
            decimal? minQty = null;
            decimal? pegDifference = null;
            PegType? pegType = null;

            switch (dealDirectionStr)
            {
                case "BUY":
                    dealDirection = DealDirection.Buy;
                    break;

                case "SELL":
                    dealDirection = DealDirection.Sell;
                    break;

                default:
                    throw new Exception(string.Format("Unexpected deal direction: {0}", dealDirectionStr));
            }

            if (checkBoxPrice.Checked)
            {
                price = decimal.Parse(textBoxPrice.Text);
            }

            if (chckBxMinQty.Checked)
            {
                minQty = decimal.Parse(txtMinQty.Text);
            }

            if (chckBxPeg.Checked)
            {
                pegDifference = decimal.Parse(txtPegDifference.Text);
                pegType = chckMktPeg.Checked ? PegType.MarketPeg : PegType.PrimaryPeg;
            }

            RejectableDealDefaults rejectableDealDefaults = null;
            if (timeInForce == TimeInForce.Day && comboDealConfirm.Text != NODEALCONFIRMATION)
            {
                rejectableDealDefaults =
                    new RejectableDealDefaults(TimeSpan.FromMilliseconds(int.Parse(txtAutoconfirmDelay.Text)),
                                               (DealConfirmationAction)
                                               Enum.Parse(typeof(DealConfirmationAction), comboDealConfirm.Text));
            }

            bool isRiskRemoving = !chckRiskAdd.Checked;

            return new DealInfo()
            {
                DealSize = dealSize,
                SenderSubId = senderSubId,
                Symbol = symbol,
                DealDirection = dealDirection,
                Price = price,
                TimeInForce = timeInForce,
                MinQty = minQty,
                IsPegged = chckBxPeg.Checked,
                PegDifference = pegDifference,
                PegType = pegType,
                RejectableDealDefaults = rejectableDealDefaults,
                IsRiskRemoving = isRiskRemoving
            };
        }

        private void checkBoxPrice_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxPrice.Checked)
                textBoxPrice.Text = "";

            textBoxPrice.Visible = checkBoxPrice.Checked;
        }

        private void checkBoxEnableSenderSubId_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxSenderSubId.Visible = checkBoxEnableSenderSubId.Checked;
        }


        private List<string> tempFilesToDelete = new List<string>();

        private void counterpartSelectBtn_Click(object sender, EventArgs e)
        {
            this.groupBox1.Visible = false;

            bool resubscribe = this.resubscribeChkBx.Checked;

            string marketDataConfig = null;
            string orderFlowConfig = null;
            string settingsXmlFile = null;

            if (this.overrideMDSettingsChkbx.Checked)
            {
                marketDataConfig = System.IO.Path.GetTempFileName();
                tempFilesToDelete.Add(marketDataConfig);
                System.IO.File.WriteAllText(marketDataConfig, this.overrideMDSettingsTxt.Text);
            }

            if (this.overrideOFSettingsChkbx.Checked)
            {
                orderFlowConfig = System.IO.Path.GetTempFileName();
                tempFilesToDelete.Add(orderFlowConfig);
                System.IO.File.WriteAllText(orderFlowConfig, this.overrideOFSettingsTxt.Text);
            }

            if (this.overrideSettingsFileChkbx.Checked)
            {
                settingsXmlFile = System.IO.Path.GetTempFileName();
                tempFilesToDelete.Add(settingsXmlFile);
                System.IO.File.WriteAllText(settingsXmlFile, this.overrideSettingsFileTxt.Text, Encoding.Unicode);
            }

            //this._connectionObjectsKeeper = new TestObjectsFactory((Counterparty)this.counterpartComboBox.SelectedText, resubscribe, marketDataConfig, orderFlowConfig, settingsXmlFile);


            var priceStreamMonitor = DiagnosticsManager.CreatePriceStreamMonitor();
            var symbolsInfoProvider = new SymbolsInfoProvider(LogFactory.Instance.GetLogger("SymbolsProvider"), new PriceHistoryProvider());
            IRiskManager riskManager = new RiskManager(priceStreamMonitor, symbolsInfoProvider.SymbolsInfo, DiagnosticsManager.GetSettlementDatesKeeper(FXCMMMCounterparty.FS1));
            IDealStatistics dealStatistics = new DealStatistics(priceStreamMonitor);
            ILogFactory logFactory = new NLogLogFactory();

            _stpLogging = new STPLogging();
            _stpLogging.Show();
            Counterparty selectedCounterparty = (Counterparty)this.counterpartComboBox.SelectedItem.ToString();

            DelayEvaluatorDal delayEvaluatorDal = new DelayEvaluatorDal(DiagnosticsManager.DiagLogger, riskManager);

            if (Counterparty.IsCounterpartyForTakeing(selectedCounterparty))
            {
                if (ToBconverter.SupportedCounterparties.Select(c => (Counterparty)c).Contains(selectedCounterparty))
                {
                    this._takerConnectionObjectsKeeper = TakerConnectionObjectsKeeper
                        .CreateHotspotObjectsKeeper_TestingOnly
                        (
                            (HotspotCounterparty)selectedCounterparty, riskManager, dealStatistics, logFactory,
                            delayEvaluatorDal.GetDelayEvaluatorMultiDestination(),
                            delayEvaluatorDal.GetDelayEvaluator(selectedCounterparty));
                    this._connectionObjectsKeeper = this._takerConnectionObjectsKeeper;
                }
                else
                {
                    this._takerConnectionObjectsKeeper =
                    TakerConnectionObjectsKeeper.CreateConnectionObjectsKeeper(
                        selectedCounterparty, resubscribe, riskManager, dealStatistics, logFactory,
                        delayEvaluatorDal.GetDelayEvaluator(selectedCounterparty));
                    this._connectionObjectsKeeper = this._takerConnectionObjectsKeeper;
                }          
            }
            else
            {
                this._connectionObjectsKeeper =
                    MakerConnectionObjectsKeeper.CreateMakerConnectionObjectsKeeper(selectedCounterparty, dealStatistics,
                        riskManager, symbolsInfoProvider.SymbolsInfo, null,
                        delayEvaluatorDal.GetDelayEvaluator(selectedCounterparty));
            }

            delayEvaluatorDal.SetSessionStateWatchers(new List<IFIXChannel>() { this._connectionObjectsKeeper.MDSession });

            if (this._connectionObjectsKeeper.MDSession != null)
            {
                this._connectionObjectsKeeper.MDSession.OnStarted +=
                    () => this._connectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info, "SESSION STARTED (from main)");
                this._connectionObjectsKeeper.MDSession.OnStopped +=
                    () => this._connectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info, "SESSION STOPPED (from main)");
                if (this._takerConnectionObjectsKeeper != null)
                {
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnSubscriptionChanged +=
                        subscription =>
                            this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                "(from main) subscription [{0}] changed to [{1}]",
                                subscription.Identity, subscription.SubscriptionStatus);
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnNewPrice +=
                        (PriceObjectInternal price, ref bool call) =>
                            this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                "(from main) new PRICE: {0}", price);

                    var venueMktSession = this._takerConnectionObjectsKeeper.MarketDataSession as VenueMarketDataSession;
                    if (venueMktSession != null)
                    {
                        venueMktSession.NewVenueDeal +=
                            (VenueDealObjectInternal deal) =>
                                this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                    "(from main) new TICKER: {0}", deal);
                    }

                    this._takerConnectionObjectsKeeper.MarketDataSession.OnNewNonExecutablePrice +=
                        (PriceObjectInternal price) =>
                            this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                "(from main) new NONEXECUTABLE PRICE: {0}", price);
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnIgnoredPrice +=
                        (PriceObjectInternal price, ref bool call) =>
                            this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                "(from main) DELAYED PRICE: {0}", price);
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnCancelLastPrice += (symbol, counterparty, side) =>
                        this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                "(from main) cancelling price from {0} on {1} {2}", counterparty, symbol, side);
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnCancelLastQuote +=
                        (s, c) =>
                            this._takerConnectionObjectsKeeper.QuotesLogger.Log(LogLevel.Info,
                                "(from main) cancelling quote from subscription {0}", s);
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnNewPrice += OnIncommingPrice;
                    this._takerConnectionObjectsKeeper.MarketDataSession.OnIgnoredPrice += OnIncommingPrice;
                }
            }

            if (this._connectionObjectsKeeper.OFSession != null)
            {
                this._connectionObjectsKeeper.OFSession.OnStarted +=
                    () =>
                        this._connectionObjectsKeeper.OrdersLogger.Log(LogLevel.Info, "ORD SESSION STARTED (from main)");
                this._connectionObjectsKeeper.OFSession.OnStopped +=
                    () =>
                        this._connectionObjectsKeeper.OrdersLogger.Log(LogLevel.Info, "ORD SESSION STOPPED (from main)");
            }

            if (this._takerConnectionObjectsKeeper != null)
            {
                this._takerConnectionObjectsKeeper.OrderFlowSession.OnOrderChanged +=
                    (order, eventArgs) =>
                        this._takerConnectionObjectsKeeper.OrdersLogger.Log(LogLevel.Info,
                            "(from main) order changed: {0}, event args: {1}", order, eventArgs);
            }


            if (chkBxConnectToStp.Checked)
            {
                _stpConnectionSessions = StpConnectionObjectsKeeper.CreateStpSessions(logFactory, dealStatistics);
            }
            else
            {
                _stpLogging.Close();
            }
        }

        private void OnIncommingPrice(PriceObject price, ref bool heldWithoutAcquireCall)
        {
            if (this._dealInfo != null && price != null && price.Symbol == (Symbol)_dealInfo.Value.Symbol && price.Side.ToOutgoingDealDirection() == this._dealInfo.Value.DealDirection)
            {
                OrderRequestInfo order = OrderRequestInfo.CreateQuoted(this._dealInfo.Value.DealSize,
                                                                 this._dealInfo.Value.Price.HasValue
                                                                     ? this._dealInfo.Value.Price.Value
                                                                     : price.Price,
                                                                 this._dealInfo.Value.DealDirection, price, this._dealInfo.Value.IsRiskRemoving);

                if (!order.Equals(OrderRequestInfo.INVALID_ORDER_REQUEST))
                {
                    this._takerConnectionObjectsKeeper.OrderFlowSession.SendOrder(order);

                    this._dealInfo = null;
                }
            }
        }

        private void WindowMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (string fileToDelete in tempFilesToDelete)
            {
                System.IO.File.Delete(fileToDelete);
            }

            if (this._connectionObjectsKeeper != null)
            {
                if (this._connectionObjectsKeeper.MDSession != null &&
                    (this._connectionObjectsKeeper.MDSession.State != SessionState.Idle ||
                    this._connectionObjectsKeeper.MDSession.State != SessionState.Stopping))
                {
                    this._connectionObjectsKeeper.MDSession.Stop();
                }


                if (this._connectionObjectsKeeper.OFSession != null
                    &&
                    (this._connectionObjectsKeeper.OFSession.State != SessionState.Idle ||
                    this._connectionObjectsKeeper.OFSession.State != SessionState.Stopping))
                {
                    this._connectionObjectsKeeper.OFSession.Stop();
                }
            }

            if (this._stpConnectionSessions != null)
            {
                foreach (IStpFlowSession stpConnectionSession in _stpConnectionSessions)
                {
                    if ((stpConnectionSession.State != SessionState.Idle ||
                         stpConnectionSession.State != SessionState.Stopping))
                    {
                        stpConnectionSession.Stop();
                    }
                }
            }

            if (_stpLogging != null && _stpLogging.Visible)
            {
                _stpLogging.Close();
            }
        }

        private void overrideMDSettingsChkbx_CheckedChanged(object sender, EventArgs e)
        {
            this.overrideMDSettingsTxt.Enabled = this.overrideMDSettingsChkbx.Checked;
        }

        private void overrideOFSettingsChkbx_CheckedChanged(object sender, EventArgs e)
        {
            this.overrideOFSettingsTxt.Enabled = this.overrideOFSettingsChkbx.Checked;
        }

        private void overrideSettingsFileChkbx_CheckedChanged(object sender, EventArgs e)
        {
            this.overrideSettingsFileTxt.Enabled = this.overrideSettingsFileChkbx.Checked;
        }

        private struct DealInfo
        {
            public DealDirection DealDirection { get; set; }
            public decimal DealSize { get; set; }
            public string Symbol { get; set; }
            public string SenderSubId { get; set; }
            public decimal? Price { get; set; }
            public TimeInForce TimeInForce { get; set; }
            public decimal? MinQty { get; set; }
            public bool IsPegged { get; set; }
            public PegType? PegType { get; set; }
            public decimal? PegDifference { get; set; }
            public RejectableDealDefaults RejectableDealDefaults { get; set; }
            public bool IsRiskRemoving { get; set; }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            if (this._takerConnectionObjectsKeeper != null && this._takerConnectionObjectsKeeper.OrderFlowSession != null)
            {
                comboOrderIds.Items.Clear();

                foreach (IIntegratorOrderExternal integratorOrderExternal in this._takerConnectionObjectsKeeper.OrderFlowSession.ActiveOrderList)
                {
                    comboOrderIds.Items.Add(integratorOrderExternal.Identity);
                }

                comboOrderIds.Items.Add("ALL");
            }


        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this._takerConnectionObjectsKeeper == null ||
                    (this._takerConnectionObjectsKeeper.Counterparty.TradingTargetCategory != TradingTargetCategory.Venue))
            {
                MessageBox.Show("Cancel operation allowed only for Venues connectors");
                return;
            }

            string orderId = comboOrderIds.Text;

            if (chckReplace.Checked)
            {
                if (orderId.Equals("ALL"))
                {
                    MessageBox.Show("Cannot replace ALL orders. It's only possible to cancel all.");
                    return;
                }

                DealInfo dealInfo = _getDealInfoFromForm();

                if (dealInfo.Price.HasValue)
                {
                    OrderRequestInfo replacingOrder;

                    if (dealInfo.MinQty.HasValue)
                    {
                        replacingOrder = OrderRequestInfo.CreateLimit(dealInfo.DealSize, dealInfo.Price.Value,
                                                                          dealInfo.DealDirection, (Symbol)dealInfo.Symbol, dealInfo.TimeInForce, dealInfo.MinQty.Value, dealInfo.IsRiskRemoving);
                    }
                    else
                    {
                        replacingOrder = OrderRequestInfo.CreateLimit(dealInfo.DealSize, dealInfo.Price.Value,
                                                                          dealInfo.DealDirection, (Symbol)dealInfo.Symbol, dealInfo.TimeInForce, dealInfo.IsRiskRemoving);
                    }

                    //replacingOrder.SetRejectableDefaults(dealInfo.RejectableDealDefaults);

                    string errorMessage;
                    (this._takerConnectionObjectsKeeper.OrderFlowSession as VenueOrderFlowSession)
                        .ReplaceOrder(orderId, this._takerConnectionObjectsKeeper.OrderFlowSession.GetNewExternalOrder(replacingOrder, out errorMessage));
                }
                else
                {
                    MessageBox.Show("Cannot send (replacing) LIMIT order without price entered");
                }
            }
            else
            {
                if (orderId.Equals("ALL"))
                {
                    (this._takerConnectionObjectsKeeper.OrderFlowSession as VenueOrderFlowSession).CancelAllOrders();
                }
                else
                {
                    (this._takerConnectionObjectsKeeper.OrderFlowSession as VenueOrderFlowSession).CancelOrder(orderId);
                }
            }
        }

        private void chckbxDay_CheckedChanged(object sender, EventArgs e)
        {
            if (chckbxDay.Checked == true)
            {
                if (this._takerConnectionObjectsKeeper == null ||
                    (this._takerConnectionObjectsKeeper.Counterparty.TradingTargetCategory != TradingTargetCategory.Venue))
                {
                    MessageBox.Show("Time in force 'Day' allowed only for Venues connectors");
                    chckIoc.Checked = true;
                }

                this.grpBxDealConfirmation.Visible = true;
            }
            else
            {
                this.grpBxDealConfirmation.Visible = false;
            }
        }

        private void chckBxMinQty_CheckedChanged(object sender, EventArgs e)
        {
            txtMinQty.Visible = true;
        }

        private void chckBxPeg_CheckedChanged(object sender, EventArgs e)
        {
            txtPegDifference.Visible = chckBxPeg.Checked;
            lblPegDifference.Visible = chckBxPeg.Checked;
            chckMktPeg.Visible = chckBxPeg.Checked;
            chckPrimaryPeg.Visible = chckBxPeg.Checked;
        }

        private RejectableMMDeal _deal = null;
        private string _counterpartyId;
        private void btn_sendDummyStp_Click(object sender, EventArgs e)
        {
            DealInfo dealInfo = _getDealInfoFromForm();

            _deal = RejectableMMDeal.CreateStoredRejectableMMDeal((Symbol)dealInfo.Symbol,
                this._takerConnectionObjectsKeeper.Counterparty,
                dealInfo.DealDirection,
                dealInfo.Price ?? 1.2m, dealInfo.DealSize, "cptId_" + DateTime.Now.Second.ToString(), "quote2",
                DateTime.UtcNow, DateTime.UtcNow, "execId_" + DateTime.Now.Second.ToString(), null,
                DateTime.UtcNow.AddDays(2).Date);

            foreach (IStpFlowSession stpConnectionSession in _stpConnectionSessions)
            {
                if (stpConnectionSession.State == SessionState.Running && stpConnectionSession.StpCounterparty == STPCounterparty.Traiana)
                {
                    (stpConnectionSession as StpFlowSession).DealStatisticsPublisherOnNewDeal(_deal, _deal, false);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_deal == null)
                return;

            foreach (IStpFlowSession stpConnectionSession in _stpConnectionSessions)
            {
                if (stpConnectionSession.State == SessionState.Running && stpConnectionSession.StpCounterparty == STPCounterparty.Traiana)
                {
                    (stpConnectionSession as StpFlowSession).DealStatisticsPublisherOnNewDeal(_deal, _deal, true);
                }
            }
        }



    }
}