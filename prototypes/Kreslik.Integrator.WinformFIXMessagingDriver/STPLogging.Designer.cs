﻿namespace Kreslik.Integrator.WinformFIXMessagingDriver
{
    partial class STPLogging
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stpLogsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.traianaStpRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // stpLogsRichTextBox
            // 
            this.stpLogsRichTextBox.Location = new System.Drawing.Point(13, 13);
            this.stpLogsRichTextBox.Name = "stpLogsRichTextBox";
            this.stpLogsRichTextBox.Size = new System.Drawing.Size(673, 297);
            this.stpLogsRichTextBox.TabIndex = 0;
            this.stpLogsRichTextBox.Text = "";
            // 
            // traianaStpRichTextBox
            // 
            this.traianaStpRichTextBox.Location = new System.Drawing.Point(13, 316);
            this.traianaStpRichTextBox.Name = "traianaStpRichTextBox";
            this.traianaStpRichTextBox.Size = new System.Drawing.Size(673, 297);
            this.traianaStpRichTextBox.TabIndex = 0;
            this.traianaStpRichTextBox.Text = "";
            // 
            // STPLogging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 620);
            this.Controls.Add(this.traianaStpRichTextBox);
            this.Controls.Add(this.stpLogsRichTextBox);
            this.Name = "STPLogging";
            this.Text = "STPLogging";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox stpLogsRichTextBox;
        private System.Windows.Forms.RichTextBox traianaStpRichTextBox;
    }
}