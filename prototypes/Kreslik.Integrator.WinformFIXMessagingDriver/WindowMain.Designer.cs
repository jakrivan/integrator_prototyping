﻿namespace Kreslik.Integrator.WinformFIXMessagingDriver
{
    partial class WindowMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartMarketDataInitiator = new System.Windows.Forms.Button();
            this.btnStopMarketDataInitiator = new System.Windows.Forms.Button();
            this.groupBoxMarketData = new System.Windows.Forms.GroupBox();
            this.txtMarketSession = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_sendDummyStp = new System.Windows.Forms.Button();
            this.grpBxDealConfirmation = new System.Windows.Forms.GroupBox();
            this.txtAutoconfirmDelay = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboDealConfirm = new System.Windows.Forms.ComboBox();
            this.txtPegDifference = new System.Windows.Forms.TextBox();
            this.lblPegDifference = new System.Windows.Forms.Label();
            this.chckPrimaryPeg = new System.Windows.Forms.RadioButton();
            this.chckMktPeg = new System.Windows.Forms.RadioButton();
            this.chckBxPeg = new System.Windows.Forms.CheckBox();
            this.txtMinQty = new System.Windows.Forms.TextBox();
            this.chckBxMinQty = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboOrderIds = new System.Windows.Forms.ComboBox();
            this.chckReplace = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxEnableSenderSubId = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chckbxDay = new System.Windows.Forms.RadioButton();
            this.chckIoc = new System.Windows.Forms.RadioButton();
            this.btnDealWithoutQuote = new System.Windows.Forms.Button();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.checkBoxPrice = new System.Windows.Forms.CheckBox();
            this.comboBoxDealDirection = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDealOnNextQuote = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnSubscribe = new System.Windows.Forms.Button();
            this.btnUnsubscribe = new System.Windows.Forms.Button();
            this.textBoxSize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxSenderSubId = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxFxPairs = new System.Windows.Forms.ComboBox();
            this.groupBoxOrderFlow = new System.Windows.Forms.GroupBox();
            this.txtOrderSession = new System.Windows.Forms.RichTextBox();
            this.btnStopOrderFlowInitiator = new System.Windows.Forms.Button();
            this.btnStartOrderFlowInitiator = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkBxConnectToStp = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.overrideSettingsFileTxt = new System.Windows.Forms.TextBox();
            this.overrideSettingsFileChkbx = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.overrideOFSettingsTxt = new System.Windows.Forms.TextBox();
            this.overrideOFSettingsChkbx = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.overrideMDSettingsTxt = new System.Windows.Forms.TextBox();
            this.overrideMDSettingsChkbx = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.counterpartSelectBtn = new System.Windows.Forms.Button();
            this.resubscribeChkBx = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.counterpartComboBox = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chckRiskAdd = new System.Windows.Forms.CheckBox();
            this.groupBoxMarketData.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpBxDealConfirmation.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBoxOrderFlow.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStartMarketDataInitiator
            // 
            this.btnStartMarketDataInitiator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartMarketDataInitiator.Location = new System.Drawing.Point(6, 270);
            this.btnStartMarketDataInitiator.Name = "btnStartMarketDataInitiator";
            this.btnStartMarketDataInitiator.Size = new System.Drawing.Size(75, 23);
            this.btnStartMarketDataInitiator.TabIndex = 1;
            this.btnStartMarketDataInitiator.Text = "Start";
            this.btnStartMarketDataInitiator.UseVisualStyleBackColor = true;
            this.btnStartMarketDataInitiator.Click += new System.EventHandler(this.btnStartMarketDataInitiator_Click);
            // 
            // btnStopMarketDataInitiator
            // 
            this.btnStopMarketDataInitiator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStopMarketDataInitiator.Location = new System.Drawing.Point(87, 270);
            this.btnStopMarketDataInitiator.Name = "btnStopMarketDataInitiator";
            this.btnStopMarketDataInitiator.Size = new System.Drawing.Size(75, 23);
            this.btnStopMarketDataInitiator.TabIndex = 1;
            this.btnStopMarketDataInitiator.Text = "Stop";
            this.btnStopMarketDataInitiator.UseVisualStyleBackColor = true;
            this.btnStopMarketDataInitiator.Click += new System.EventHandler(this.btnStopMarketDataInitiator_Click);
            // 
            // groupBoxMarketData
            // 
            this.groupBoxMarketData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMarketData.Controls.Add(this.txtMarketSession);
            this.groupBoxMarketData.Controls.Add(this.groupBox3);
            this.groupBoxMarketData.Controls.Add(this.btnStopMarketDataInitiator);
            this.groupBoxMarketData.Controls.Add(this.btnStartMarketDataInitiator);
            this.groupBoxMarketData.Location = new System.Drawing.Point(6, 3);
            this.groupBoxMarketData.Name = "groupBoxMarketData";
            this.groupBoxMarketData.Size = new System.Drawing.Size(857, 478);
            this.groupBoxMarketData.TabIndex = 2;
            this.groupBoxMarketData.TabStop = false;
            this.groupBoxMarketData.Text = "MarketData";
            // 
            // txtMarketSession
            // 
            this.txtMarketSession.HideSelection = false;
            this.txtMarketSession.Location = new System.Drawing.Point(15, 20);
            this.txtMarketSession.Name = "txtMarketSession";
            this.txtMarketSession.Size = new System.Drawing.Size(831, 243);
            this.txtMarketSession.TabIndex = 11;
            this.txtMarketSession.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.btn_sendDummyStp);
            this.groupBox3.Controls.Add(this.grpBxDealConfirmation);
            this.groupBox3.Controls.Add(this.txtPegDifference);
            this.groupBox3.Controls.Add(this.lblPegDifference);
            this.groupBox3.Controls.Add(this.chckPrimaryPeg);
            this.groupBox3.Controls.Add(this.chckMktPeg);
            this.groupBox3.Controls.Add(this.chckBxPeg);
            this.groupBox3.Controls.Add(this.txtMinQty);
            this.groupBox3.Controls.Add(this.chckBxMinQty);
            this.groupBox3.Controls.Add(this.groupBox9);
            this.groupBox3.Controls.Add(this.checkBoxEnableSenderSubId);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.textBoxSize);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.comboBoxSenderSubId);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboBoxFxPairs);
            this.groupBox3.Location = new System.Drawing.Point(15, 299);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(831, 173);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Cancel last Stp";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_sendDummyStp
            // 
            this.btn_sendDummyStp.Location = new System.Drawing.Point(13, 114);
            this.btn_sendDummyStp.Name = "btn_sendDummyStp";
            this.btn_sendDummyStp.Size = new System.Drawing.Size(110, 23);
            this.btn_sendDummyStp.TabIndex = 26;
            this.btn_sendDummyStp.Text = "Send Dummy Stp";
            this.btn_sendDummyStp.UseVisualStyleBackColor = true;
            this.btn_sendDummyStp.Click += new System.EventHandler(this.btn_sendDummyStp_Click);
            // 
            // grpBxDealConfirmation
            // 
            this.grpBxDealConfirmation.Controls.Add(this.txtAutoconfirmDelay);
            this.grpBxDealConfirmation.Controls.Add(this.label6);
            this.grpBxDealConfirmation.Controls.Add(this.comboDealConfirm);
            this.grpBxDealConfirmation.Location = new System.Drawing.Point(620, 120);
            this.grpBxDealConfirmation.Name = "grpBxDealConfirmation";
            this.grpBxDealConfirmation.Size = new System.Drawing.Size(204, 47);
            this.grpBxDealConfirmation.TabIndex = 25;
            this.grpBxDealConfirmation.TabStop = false;
            this.grpBxDealConfirmation.Text = "DealConfirmation";
            // 
            // txtAutoconfirmDelay
            // 
            this.txtAutoconfirmDelay.Location = new System.Drawing.Point(154, 18);
            this.txtAutoconfirmDelay.Name = "txtAutoconfirmDelay";
            this.txtAutoconfirmDelay.Size = new System.Drawing.Size(32, 20);
            this.txtAutoconfirmDelay.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(94, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "After [ms]:";
            // 
            // comboDealConfirm
            // 
            this.comboDealConfirm.FormattingEnabled = true;
            this.comboDealConfirm.Location = new System.Drawing.Point(7, 19);
            this.comboDealConfirm.Name = "comboDealConfirm";
            this.comboDealConfirm.Size = new System.Drawing.Size(81, 21);
            this.comboDealConfirm.TabIndex = 0;
            // 
            // txtPegDifference
            // 
            this.txtPegDifference.Location = new System.Drawing.Point(761, 20);
            this.txtPegDifference.Name = "txtPegDifference";
            this.txtPegDifference.Size = new System.Drawing.Size(64, 20);
            this.txtPegDifference.TabIndex = 24;
            this.txtPegDifference.Visible = false;
            // 
            // lblPegDifference
            // 
            this.lblPegDifference.AutoSize = true;
            this.lblPegDifference.Location = new System.Drawing.Point(702, 22);
            this.lblPegDifference.Name = "lblPegDifference";
            this.lblPegDifference.Size = new System.Drawing.Size(59, 13);
            this.lblPegDifference.TabIndex = 23;
            this.lblPegDifference.Text = "Difference:";
            this.lblPegDifference.Visible = false;
            // 
            // chckPrimaryPeg
            // 
            this.chckPrimaryPeg.AutoSize = true;
            this.chckPrimaryPeg.Location = new System.Drawing.Point(613, 20);
            this.chckPrimaryPeg.Name = "chckPrimaryPeg";
            this.chckPrimaryPeg.Size = new System.Drawing.Size(84, 17);
            this.chckPrimaryPeg.TabIndex = 22;
            this.chckPrimaryPeg.TabStop = true;
            this.chckPrimaryPeg.Text = "Peimary Peg";
            this.chckPrimaryPeg.UseVisualStyleBackColor = true;
            this.chckPrimaryPeg.Visible = false;
            // 
            // chckMktPeg
            // 
            this.chckMktPeg.AutoSize = true;
            this.chckMktPeg.Checked = true;
            this.chckMktPeg.Location = new System.Drawing.Point(533, 20);
            this.chckMktPeg.Name = "chckMktPeg";
            this.chckMktPeg.Size = new System.Drawing.Size(80, 17);
            this.chckMktPeg.TabIndex = 21;
            this.chckMktPeg.TabStop = true;
            this.chckMktPeg.Text = "Market Peg";
            this.chckMktPeg.UseVisualStyleBackColor = true;
            this.chckMktPeg.Visible = false;
            // 
            // chckBxPeg
            // 
            this.chckBxPeg.AutoSize = true;
            this.chckBxPeg.Location = new System.Drawing.Point(484, 20);
            this.chckBxPeg.Name = "chckBxPeg";
            this.chckBxPeg.Size = new System.Drawing.Size(45, 17);
            this.chckBxPeg.TabIndex = 20;
            this.chckBxPeg.Text = "Peg";
            this.chckBxPeg.UseVisualStyleBackColor = true;
            this.chckBxPeg.CheckedChanged += new System.EventHandler(this.chckBxPeg_CheckedChanged);
            // 
            // txtMinQty
            // 
            this.txtMinQty.Location = new System.Drawing.Point(637, 41);
            this.txtMinQty.Name = "txtMinQty";
            this.txtMinQty.Size = new System.Drawing.Size(57, 20);
            this.txtMinQty.TabIndex = 19;
            this.txtMinQty.Visible = false;
            // 
            // chckBxMinQty
            // 
            this.chckBxMinQty.AutoSize = true;
            this.chckBxMinQty.Location = new System.Drawing.Point(484, 43);
            this.chckBxMinQty.Name = "chckBxMinQty";
            this.chckBxMinQty.Size = new System.Drawing.Size(147, 17);
            this.chckBxMinQty.TabIndex = 18;
            this.chckBxMinQty.Text = "Minimum Fillable Qunatity:";
            this.chckBxMinQty.UseVisualStyleBackColor = true;
            this.chckBxMinQty.CheckedChanged += new System.EventHandler(this.chckBxMinQty_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboOrderIds);
            this.groupBox9.Controls.Add(this.chckReplace);
            this.groupBox9.Controls.Add(this.btnCancel);
            this.groupBox9.Controls.Add(this.label5);
            this.groupBox9.Location = new System.Drawing.Point(192, 119);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(421, 47);
            this.groupBox9.TabIndex = 17;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Cancellation";
            // 
            // comboOrderIds
            // 
            this.comboOrderIds.FormattingEnabled = true;
            this.comboOrderIds.Location = new System.Drawing.Point(61, 15);
            this.comboOrderIds.Name = "comboOrderIds";
            this.comboOrderIds.Size = new System.Drawing.Size(163, 21);
            this.comboOrderIds.TabIndex = 5;
            this.comboOrderIds.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            // 
            // chckReplace
            // 
            this.chckReplace.AutoSize = true;
            this.chckReplace.Location = new System.Drawing.Point(262, 17);
            this.chckReplace.Name = "chckReplace";
            this.chckReplace.Size = new System.Drawing.Size(66, 17);
            this.chckReplace.TabIndex = 3;
            this.chckReplace.Text = "Replace";
            this.chckReplace.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(334, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(74, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Order:";
            // 
            // checkBoxEnableSenderSubId
            // 
            this.checkBoxEnableSenderSubId.AutoSize = true;
            this.checkBoxEnableSenderSubId.Location = new System.Drawing.Point(161, 33);
            this.checkBoxEnableSenderSubId.Name = "checkBoxEnableSenderSubId";
            this.checkBoxEnableSenderSubId.Size = new System.Drawing.Size(91, 17);
            this.checkBoxEnableSenderSubId.TabIndex = 16;
            this.checkBoxEnableSenderSubId.Text = "SenderSubId:";
            this.checkBoxEnableSenderSubId.UseVisualStyleBackColor = true;
            this.checkBoxEnableSenderSubId.CheckedChanged += new System.EventHandler(this.checkBoxEnableSenderSubId_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chckRiskAdd);
            this.groupBox5.Controls.Add(this.chckbxDay);
            this.groupBox5.Controls.Add(this.chckIoc);
            this.groupBox5.Controls.Add(this.btnDealWithoutQuote);
            this.groupBox5.Controls.Add(this.textBoxPrice);
            this.groupBox5.Controls.Add(this.checkBoxPrice);
            this.groupBox5.Controls.Add(this.comboBoxDealDirection);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.btnDealOnNextQuote);
            this.groupBox5.Location = new System.Drawing.Point(192, 61);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(632, 52);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Deal";
            // 
            // chckbxDay
            // 
            this.chckbxDay.AutoSize = true;
            this.chckbxDay.Location = new System.Drawing.Point(289, 20);
            this.chckbxDay.Name = "chckbxDay";
            this.chckbxDay.Size = new System.Drawing.Size(44, 17);
            this.chckbxDay.TabIndex = 19;
            this.chckbxDay.Text = "Day";
            this.chckbxDay.UseVisualStyleBackColor = true;
            this.chckbxDay.CheckedChanged += new System.EventHandler(this.chckbxDay_CheckedChanged);
            // 
            // chckIoc
            // 
            this.chckIoc.AutoSize = true;
            this.chckIoc.Checked = true;
            this.chckIoc.Location = new System.Drawing.Point(242, 20);
            this.chckIoc.Name = "chckIoc";
            this.chckIoc.Size = new System.Drawing.Size(41, 17);
            this.chckIoc.TabIndex = 18;
            this.chckIoc.TabStop = true;
            this.chckIoc.Text = "IoC";
            this.chckIoc.UseVisualStyleBackColor = true;
            // 
            // btnDealWithoutQuote
            // 
            this.btnDealWithoutQuote.Location = new System.Drawing.Point(517, 20);
            this.btnDealWithoutQuote.Name = "btnDealWithoutQuote";
            this.btnDealWithoutQuote.Size = new System.Drawing.Size(109, 23);
            this.btnDealWithoutQuote.TabIndex = 17;
            this.btnDealWithoutQuote.Text = "Deal without quote";
            this.btnDealWithoutQuote.UseVisualStyleBackColor = true;
            this.btnDealWithoutQuote.Click += new System.EventHandler(this.btnDealWithoutQuote_Click);
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(168, 18);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(68, 20);
            this.textBoxPrice.TabIndex = 16;
            this.textBoxPrice.Visible = false;
            // 
            // checkBoxPrice
            // 
            this.checkBoxPrice.AutoSize = true;
            this.checkBoxPrice.Location = new System.Drawing.Point(118, 20);
            this.checkBoxPrice.Name = "checkBoxPrice";
            this.checkBoxPrice.Size = new System.Drawing.Size(53, 17);
            this.checkBoxPrice.TabIndex = 14;
            this.checkBoxPrice.Text = "Price:";
            this.checkBoxPrice.UseVisualStyleBackColor = true;
            this.checkBoxPrice.CheckedChanged += new System.EventHandler(this.checkBoxPrice_CheckedChanged);
            // 
            // comboBoxDealDirection
            // 
            this.comboBoxDealDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDealDirection.FormattingEnabled = true;
            this.comboBoxDealDirection.Location = new System.Drawing.Point(58, 18);
            this.comboBoxDealDirection.Name = "comboBoxDealDirection";
            this.comboBoxDealDirection.Size = new System.Drawing.Size(55, 21);
            this.comboBoxDealDirection.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Direction:";
            // 
            // btnDealOnNextQuote
            // 
            this.btnDealOnNextQuote.Location = new System.Drawing.Point(409, 20);
            this.btnDealOnNextQuote.Name = "btnDealOnNextQuote";
            this.btnDealOnNextQuote.Size = new System.Drawing.Size(107, 23);
            this.btnDealOnNextQuote.TabIndex = 11;
            this.btnDealOnNextQuote.Text = "Deal on next quote";
            this.btnDealOnNextQuote.UseVisualStyleBackColor = true;
            this.btnDealOnNextQuote.Click += new System.EventHandler(this.btnDealOnNextQuote_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnSubscribe);
            this.groupBox4.Controls.Add(this.btnUnsubscribe);
            this.groupBox4.Location = new System.Drawing.Point(6, 58);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(180, 50);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Market data request";
            // 
            // btnSubscribe
            // 
            this.btnSubscribe.Location = new System.Drawing.Point(10, 17);
            this.btnSubscribe.Name = "btnSubscribe";
            this.btnSubscribe.Size = new System.Drawing.Size(77, 23);
            this.btnSubscribe.TabIndex = 3;
            this.btnSubscribe.Text = "Subscribe";
            this.btnSubscribe.UseVisualStyleBackColor = true;
            this.btnSubscribe.Click += new System.EventHandler(this.btnSubscribe_Click);
            // 
            // btnUnsubscribe
            // 
            this.btnUnsubscribe.Location = new System.Drawing.Point(93, 17);
            this.btnUnsubscribe.Name = "btnUnsubscribe";
            this.btnUnsubscribe.Size = new System.Drawing.Size(77, 23);
            this.btnUnsubscribe.TabIndex = 3;
            this.btnUnsubscribe.Text = "Unsubscribe";
            this.btnUnsubscribe.UseVisualStyleBackColor = true;
            this.btnUnsubscribe.Click += new System.EventHandler(this.btnUnsubscribe_Click);
            // 
            // textBoxSize
            // 
            this.textBoxSize.Location = new System.Drawing.Point(400, 35);
            this.textBoxSize.Name = "textBoxSize";
            this.textBoxSize.Size = new System.Drawing.Size(57, 20);
            this.textBoxSize.TabIndex = 6;
            this.textBoxSize.Text = "100000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "FX pair:";
            // 
            // comboBoxSenderSubId
            // 
            this.comboBoxSenderSubId.FormattingEnabled = true;
            this.comboBoxSenderSubId.Location = new System.Drawing.Point(256, 31);
            this.comboBoxSenderSubId.Name = "comboBoxSenderSubId";
            this.comboBoxSenderSubId.Size = new System.Drawing.Size(87, 21);
            this.comboBoxSenderSubId.TabIndex = 10;
            this.comboBoxSenderSubId.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(364, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Size:";
            // 
            // comboBoxFxPairs
            // 
            this.comboBoxFxPairs.FormattingEnabled = true;
            this.comboBoxFxPairs.Location = new System.Drawing.Point(56, 31);
            this.comboBoxFxPairs.Name = "comboBoxFxPairs";
            this.comboBoxFxPairs.Size = new System.Drawing.Size(87, 21);
            this.comboBoxFxPairs.TabIndex = 9;
            // 
            // groupBoxOrderFlow
            // 
            this.groupBoxOrderFlow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxOrderFlow.Controls.Add(this.txtOrderSession);
            this.groupBoxOrderFlow.Controls.Add(this.btnStopOrderFlowInitiator);
            this.groupBoxOrderFlow.Controls.Add(this.btnStartOrderFlowInitiator);
            this.groupBoxOrderFlow.Location = new System.Drawing.Point(6, 3);
            this.groupBoxOrderFlow.Name = "groupBoxOrderFlow";
            this.groupBoxOrderFlow.Size = new System.Drawing.Size(857, 347);
            this.groupBoxOrderFlow.TabIndex = 3;
            this.groupBoxOrderFlow.TabStop = false;
            this.groupBoxOrderFlow.Text = "OrderFlow";
            // 
            // txtOrderSession
            // 
            this.txtOrderSession.HideSelection = false;
            this.txtOrderSession.Location = new System.Drawing.Point(15, 20);
            this.txtOrderSession.Name = "txtOrderSession";
            this.txtOrderSession.Size = new System.Drawing.Size(831, 292);
            this.txtOrderSession.TabIndex = 2;
            this.txtOrderSession.Text = "";
            // 
            // btnStopOrderFlowInitiator
            // 
            this.btnStopOrderFlowInitiator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStopOrderFlowInitiator.Location = new System.Drawing.Point(87, 318);
            this.btnStopOrderFlowInitiator.Name = "btnStopOrderFlowInitiator";
            this.btnStopOrderFlowInitiator.Size = new System.Drawing.Size(75, 23);
            this.btnStopOrderFlowInitiator.TabIndex = 1;
            this.btnStopOrderFlowInitiator.Text = "Stop";
            this.btnStopOrderFlowInitiator.UseVisualStyleBackColor = true;
            this.btnStopOrderFlowInitiator.Click += new System.EventHandler(this.btnStopOrderFlowInitiator_Click);
            // 
            // btnStartOrderFlowInitiator
            // 
            this.btnStartOrderFlowInitiator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartOrderFlowInitiator.Location = new System.Drawing.Point(6, 318);
            this.btnStartOrderFlowInitiator.Name = "btnStartOrderFlowInitiator";
            this.btnStartOrderFlowInitiator.Size = new System.Drawing.Size(75, 23);
            this.btnStartOrderFlowInitiator.TabIndex = 1;
            this.btnStartOrderFlowInitiator.Text = "Start";
            this.btnStartOrderFlowInitiator.UseVisualStyleBackColor = true;
            this.btnStartOrderFlowInitiator.Click += new System.EventHandler(this.btnStartOrderFlowInitiator_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkBxConnectToStp);
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(3, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(871, 841);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // chkBxConnectToStp
            // 
            this.chkBxConnectToStp.AutoSize = true;
            this.chkBxConnectToStp.Location = new System.Drawing.Point(323, 177);
            this.chkBxConnectToStp.Name = "chkBxConnectToStp";
            this.chkBxConnectToStp.Size = new System.Drawing.Size(185, 17);
            this.chkBxConnectToStp.TabIndex = 5;
            this.chkBxConnectToStp.Text = "Connect to STP tickets consumer";
            this.chkBxConnectToStp.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.overrideSettingsFileTxt);
            this.groupBox8.Controls.Add(this.overrideSettingsFileChkbx);
            this.groupBox8.Location = new System.Drawing.Point(586, 242);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(282, 581);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            // 
            // overrideSettingsFileTxt
            // 
            this.overrideSettingsFileTxt.Enabled = false;
            this.overrideSettingsFileTxt.Location = new System.Drawing.Point(6, 24);
            this.overrideSettingsFileTxt.Multiline = true;
            this.overrideSettingsFileTxt.Name = "overrideSettingsFileTxt";
            this.overrideSettingsFileTxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.overrideSettingsFileTxt.Size = new System.Drawing.Size(273, 551);
            this.overrideSettingsFileTxt.TabIndex = 6;
            this.overrideSettingsFileTxt.WordWrap = false;
            // 
            // overrideSettingsFileChkbx
            // 
            this.overrideSettingsFileChkbx.AutoSize = true;
            this.overrideSettingsFileChkbx.Enabled = false;
            this.overrideSettingsFileChkbx.Location = new System.Drawing.Point(17, 0);
            this.overrideSettingsFileChkbx.Name = "overrideSettingsFileChkbx";
            this.overrideSettingsFileChkbx.Size = new System.Drawing.Size(139, 17);
            this.overrideSettingsFileChkbx.TabIndex = 0;
            this.overrideSettingsFileChkbx.Text = "Override settings xml file";
            this.overrideSettingsFileChkbx.UseVisualStyleBackColor = true;
            this.overrideSettingsFileChkbx.CheckedChanged += new System.EventHandler(this.overrideSettingsFileChkbx_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.overrideOFSettingsTxt);
            this.groupBox7.Controls.Add(this.overrideOFSettingsChkbx);
            this.groupBox7.Location = new System.Drawing.Point(297, 242);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(283, 581);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            // 
            // overrideOFSettingsTxt
            // 
            this.overrideOFSettingsTxt.Enabled = false;
            this.overrideOFSettingsTxt.Location = new System.Drawing.Point(5, 24);
            this.overrideOFSettingsTxt.Multiline = true;
            this.overrideOFSettingsTxt.Name = "overrideOFSettingsTxt";
            this.overrideOFSettingsTxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.overrideOFSettingsTxt.Size = new System.Drawing.Size(273, 551);
            this.overrideOFSettingsTxt.TabIndex = 6;
            this.overrideOFSettingsTxt.WordWrap = false;
            // 
            // overrideOFSettingsChkbx
            // 
            this.overrideOFSettingsChkbx.AutoSize = true;
            this.overrideOFSettingsChkbx.Enabled = false;
            this.overrideOFSettingsChkbx.Location = new System.Drawing.Point(16, 0);
            this.overrideOFSettingsChkbx.Name = "overrideOFSettingsChkbx";
            this.overrideOFSettingsChkbx.Size = new System.Drawing.Size(190, 17);
            this.overrideOFSettingsChkbx.TabIndex = 5;
            this.overrideOFSettingsChkbx.Text = "Override OrderFlow Session Config";
            this.overrideOFSettingsChkbx.UseVisualStyleBackColor = true;
            this.overrideOFSettingsChkbx.CheckedChanged += new System.EventHandler(this.overrideOFSettingsChkbx_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.overrideMDSettingsTxt);
            this.groupBox6.Controls.Add(this.overrideMDSettingsChkbx);
            this.groupBox6.Location = new System.Drawing.Point(5, 242);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(286, 581);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            // 
            // overrideMDSettingsTxt
            // 
            this.overrideMDSettingsTxt.Enabled = false;
            this.overrideMDSettingsTxt.Location = new System.Drawing.Point(7, 24);
            this.overrideMDSettingsTxt.Multiline = true;
            this.overrideMDSettingsTxt.Name = "overrideMDSettingsTxt";
            this.overrideMDSettingsTxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.overrideMDSettingsTxt.Size = new System.Drawing.Size(273, 551);
            this.overrideMDSettingsTxt.TabIndex = 6;
            this.overrideMDSettingsTxt.WordWrap = false;
            // 
            // overrideMDSettingsChkbx
            // 
            this.overrideMDSettingsChkbx.AutoSize = true;
            this.overrideMDSettingsChkbx.Enabled = false;
            this.overrideMDSettingsChkbx.Location = new System.Drawing.Point(12, 0);
            this.overrideMDSettingsChkbx.Name = "overrideMDSettingsChkbx";
            this.overrideMDSettingsChkbx.Size = new System.Drawing.Size(198, 17);
            this.overrideMDSettingsChkbx.TabIndex = 5;
            this.overrideMDSettingsChkbx.Text = "Override MarketData Session Config";
            this.overrideMDSettingsChkbx.UseVisualStyleBackColor = true;
            this.overrideMDSettingsChkbx.CheckedChanged += new System.EventHandler(this.overrideMDSettingsChkbx_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.counterpartSelectBtn);
            this.groupBox2.Controls.Add(this.resubscribeChkBx);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.counterpartComboBox);
            this.groupBox2.Location = new System.Drawing.Point(235, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(431, 91);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // counterpartSelectBtn
            // 
            this.counterpartSelectBtn.Location = new System.Drawing.Point(338, 51);
            this.counterpartSelectBtn.Name = "counterpartSelectBtn";
            this.counterpartSelectBtn.Size = new System.Drawing.Size(87, 23);
            this.counterpartSelectBtn.TabIndex = 1;
            this.counterpartSelectBtn.Text = "Select";
            this.counterpartSelectBtn.UseVisualStyleBackColor = true;
            this.counterpartSelectBtn.Click += new System.EventHandler(this.counterpartSelectBtn_Click);
            // 
            // resubscribeChkBx
            // 
            this.resubscribeChkBx.AutoSize = true;
            this.resubscribeChkBx.Checked = true;
            this.resubscribeChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            this.resubscribeChkBx.Location = new System.Drawing.Point(120, 53);
            this.resubscribeChkBx.Name = "resubscribeChkBx";
            this.resubscribeChkBx.Size = new System.Drawing.Size(148, 17);
            this.resubscribeChkBx.TabIndex = 3;
            this.resubscribeChkBx.Text = "Auto-resubscribe to prices";
            this.resubscribeChkBx.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(32, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(241, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select counterpart to connect to:";
            // 
            // counterpartComboBox
            // 
            this.counterpartComboBox.FormattingEnabled = true;
            this.counterpartComboBox.Location = new System.Drawing.Point(19, 51);
            this.counterpartComboBox.Name = "counterpartComboBox";
            this.counterpartComboBox.Size = new System.Drawing.Size(95, 21);
            this.counterpartComboBox.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(8, 7);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBoxMarketData);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxOrderFlow);
            this.splitContainer1.Size = new System.Drawing.Size(866, 841);
            this.splitContainer1.SplitterDistance = 484;
            this.splitContainer1.TabIndex = 16;
            // 
            // chckRiskRemove
            // 
            this.chckRiskAdd.AutoSize = true;
            this.chckRiskAdd.Checked = true;
            this.chckRiskAdd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chckRiskAdd.Location = new System.Drawing.Point(339, 22);
            this.chckRiskAdd.Name = "chckRiskAdd";
            this.chckRiskAdd.Size = new System.Drawing.Size(59, 17);
            this.chckRiskAdd.TabIndex = 20;
            this.chckRiskAdd.Text = "Risk++";
            this.chckRiskAdd.UseVisualStyleBackColor = true;
            // 
            // WindowMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 851);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "WindowMain";
            this.Text = "FIX connection test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WindowMain_FormClosing);
            this.groupBoxMarketData.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpBxDealConfirmation.ResumeLayout(false);
            this.grpBxDealConfirmation.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBoxOrderFlow.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStartMarketDataInitiator;
        private System.Windows.Forms.Button btnStopMarketDataInitiator;
        private System.Windows.Forms.GroupBox groupBoxMarketData;
        private System.Windows.Forms.Button btnUnsubscribe;
        private System.Windows.Forms.Button btnSubscribe;
        private System.Windows.Forms.GroupBox groupBoxOrderFlow;
        private System.Windows.Forms.Button btnStopOrderFlowInitiator;
        private System.Windows.Forms.Button btnStartOrderFlowInitiator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSize;
        private System.Windows.Forms.ComboBox comboBoxFxPairs;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBoxSenderSubId;
        private System.Windows.Forms.ComboBox comboBoxDealDirection;
        private System.Windows.Forms.Button btnDealOnNextQuote;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox checkBoxPrice;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.CheckBox checkBoxEnableSenderSubId;
        private System.Windows.Forms.RichTextBox txtMarketSession;
        private System.Windows.Forms.RichTextBox txtOrderSession;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button counterpartSelectBtn;
        private System.Windows.Forms.ComboBox counterpartComboBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox resubscribeChkBx;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox overrideSettingsFileTxt;
        private System.Windows.Forms.CheckBox overrideSettingsFileChkbx;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox overrideOFSettingsTxt;
        private System.Windows.Forms.CheckBox overrideOFSettingsChkbx;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox overrideMDSettingsTxt;
        private System.Windows.Forms.CheckBox overrideMDSettingsChkbx;
        private System.Windows.Forms.Button btnDealWithoutQuote;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox chckReplace;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboOrderIds;
        private System.Windows.Forms.RadioButton chckbxDay;
        private System.Windows.Forms.RadioButton chckIoc;
        private System.Windows.Forms.TextBox txtMinQty;
        private System.Windows.Forms.CheckBox chckBxMinQty;
        private System.Windows.Forms.TextBox txtPegDifference;
        private System.Windows.Forms.Label lblPegDifference;
        private System.Windows.Forms.RadioButton chckPrimaryPeg;
        private System.Windows.Forms.RadioButton chckMktPeg;
        private System.Windows.Forms.CheckBox chckBxPeg;
        private System.Windows.Forms.CheckBox chkBxConnectToStp;
        private System.Windows.Forms.GroupBox grpBxDealConfirmation;
        private System.Windows.Forms.TextBox txtAutoconfirmDelay;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboDealConfirm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_sendDummyStp;
        private System.Windows.Forms.CheckBox chckRiskAdd;
    }
}

