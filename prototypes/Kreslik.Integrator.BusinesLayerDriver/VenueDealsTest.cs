﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class VenueDealsTest : IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;
            _clientGateway.Targets.Hotspot.ClientOrderUpdated += ClientGatewayOnClientOrderUpdated;
            _clientGateway.Targets.Hotspot.NewDealDone += ClientGatewayOnNewDealDone;
            _clientGateway.Targets.Hotspot.CounterpartyRejectedIntegratorOrder += ClientGatewayOnCounterpartyRejectedIntegratorOrder;
            _clientGateway.Targets.Hotspot.CounterpartyIgnoredIntegratorOrder += ClientGatewayOnCounterpartyIgnoredIntegratorOrder;
            _clientGateway.Targets.Hotspot.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
            _clientGateway.Targets.Hotspot.VenueDeals.NewUpdateAvailable += HotspotOnNewVenueDealReported;
        }

        private void HotspotOnNewVenueDealReported(VenueDealObject venueDealObject)
        {
            this._logger.Log(LogLevel.Info, "Client receiving new venue deal: {0}", venueDealObject);
        }

        private void ClientGatewayOnCounterpartyIgnoredIntegratorOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            this._logger.Log(LogLevel.Fatal, "Client receiving Counterparty Ignoting Info: {0}", counterpartyOrderIgnoringInfo);
        }

        private void ClientGatewayOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            this._logger.Log(LogLevel.Fatal, "Client receiving CounterpartyRejection: {0}", counterpartyRejectionInfo);
        }

        private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Fatal, "Price Update: {0}", priceUpdateEventArgs);
        }

        private void ClientGatewayOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Fatal, "VenueDealsTest Receiving NewDeal. {0}", integratorDealInternal);
        }

        private void ClientGatewayOnClientOrderUpdated(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Fatal, "VenueDealsTest Receiving OrderUpdate: {0}", clientOrderUpdateInfo);
        }

        public void Start()
        {
            Task.Factory.StartNew(Test1);
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e, "Unhandled exception");
            return false;
        }

        private void Test1()
        {
            try
            {
                var result = _clientGateway.Targets.Hotspot.VenueDeals.SubscribeToUpdates(
                _clientGateway.Targets.Hotspot.VenueDeals.GetSubscriptionRequest(Symbol.AUD_CHF, HotspotCounterparty.HTA));

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, result.ErrorMessage);
                }

                result = _clientGateway.Targets.Hotspot.VenueDeals.SubscribeToUpdates(
                _clientGateway.Targets.Hotspot.VenueDeals.GetSubscriptionRequest(Symbol.EUR_USD, HotspotCounterparty.HTF));

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, result.ErrorMessage);
                }

                System.Threading.Thread.Sleep(2000);

                result = _clientGateway.Targets.Hotspot.VenueDeals.UnsubscribeFromUpdates(
                _clientGateway.Targets.Hotspot.VenueDeals.GetUnsubscriptionRequest(Symbol.AUD_CHF, HotspotCounterparty.HTA));

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, result.ErrorMessage);
                }

                System.Threading.Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                this._logger.LogException(LogLevel.Fatal, "Exception", e);
            }
        }
    }
}
