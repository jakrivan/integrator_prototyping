﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class HotspotOrdersTest: IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(Common.ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;
            _clientGateway.Targets.Hotspot.ClientOrderUpdated += ClientGatewayOnClientOrderUpdated;
            _clientGateway.Targets.Hotspot.NewDealDone += ClientGatewayOnNewDealDone;
            _clientGateway.Targets.Hotspot.CounterpartyRejectedIntegratorOrder += ClientGatewayOnCounterpartyRejectedIntegratorOrder;
            _clientGateway.Targets.Hotspot.CounterpartyIgnoredIntegratorOrder += ClientGatewayOnCounterpartyIgnoredIntegratorOrder;
        }

        private void ClientGatewayOnCounterpartyIgnoredIntegratorOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            this._logger.Log(LogLevel.Info, "Client receiving Counterparty Ignoting Info: {0}", counterpartyOrderIgnoringInfo);
        }

        private void ClientGatewayOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            this._logger.Log(LogLevel.Info, "Client receiving CounterpartyRejection: {0}", counterpartyRejectionInfo);
        }

        public void Start()
        {
            //this.Start(HotspotCounterparty.HTA);
            //this.Start(HotspotCounterparty.HTF);

            //this.Start_CancelAll(HotspotCounterparty.HTA);

            //this.Start_Pegged(HotspotCounterparty.HTA);
            this.Start(HotspotCounterparty.HTA);
        }

        private void Start_Pegged(HotspotCounterparty counterparty)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");

                    _clientGateway.Targets.Hotspot.PricesTopOfBook.NewUpdateAvailable += HotspotOnPriceUpdate;
                    _clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += BankOnPriceUpdate;
                    _clientGateway.Targets.BankPool.PricesIndividual.NewUpdateAvailable += BankOnIndividualPriceUpdate;
                    
                    _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(Symbol.AUD_CAD, HotspotCounterparty.HTA));
                    _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(Symbol.USD_CAD, HotspotCounterparty.HTF));


                    _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD));


                    _clientGateway.Targets.BankPool.PricesIndividual.SubscribeToUpdates(
                        _clientGateway.Targets.BankPool.PricesIndividual.GetSubscriptionRequest(Symbol.ZAR_JPY));


                    HotspotClientOrderRequestInfo cori1 = _clientGateway.Targets.Hotspot.CreateDayPegOrderRequest(
                        2000000m, 1000000m, 130m, PegType.MarketPeg, 0.00001m, DealDirection.Sell, Symbol.EUR_JPY, counterparty, false);

                    HotspotClientOrder clientOrder1 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1));


                    HotspotClientOrderRequestInfo cori2 = _clientGateway.Targets.Hotspot.CreateDayPegOrderRequest(
                        2000000m, 1000000m, 140m, PegType.PrimaryPeg, -0.00001m, DealDirection.Buy, Symbol.EUR_JPY, counterparty, false);

                    HotspotClientOrder clientOrder2 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    this._clientGateway.Targets.All.CancelAllMyOrders();

                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        //private void BankPoolOnPricesRemoved(Counterparty counterparty)
        //{
        //    this._logger.Log(LogLevel.Info, "Bank Prices Removed: {0}", counterparty);
        //}

        //private void BankPoolOnPriceExistingInvalidated(PriceObject priceObject)
        //{
        //    this._logger.Log(LogLevel.Info, "Bank Price Invalidated: {0}", priceObject);
        //}

        //private void BankPoolOnPriceNewArrived(PriceObject priceObject)
        //{
        //    this._logger.Log(LogLevel.Info, "Bank Price Arrived: {0}", priceObject);
        //}

        private void HotspotOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Hot ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnIndividualPriceUpdate(PriceIndividualUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank price Individual Price Update: {0}", priceUpdateEventArgs);
        }

        //private void HotspotOnPriceOnTopOfBookImproved(PriceObject priceObject)
        //{
        //    this._logger.Log(LogLevel.Info, "Hot Price Improvement: {0}", priceObject);
        //}

        //private void HotspotOnPriceOnTopOfBookDeteriorated(PriceObject priceObject)
        //{
        //    this._logger.Log(LogLevel.Info, "Hot Price Deterioration: {0}", priceObject);
        //}

        //private void BankOnPriceOnTopOfBookImproved(PriceObject priceObject)
        //{
        //    this._logger.Log(LogLevel.Info, "Bank Price Improvement: {0}", priceObject);
        //}

        //private void BankOnPriceOnTopOfBookDeteriorated(PriceObject priceObject)
        //{
        //    this._logger.Log(LogLevel.Info, "Bank Price Deterioration: {0}", priceObject);
        //}

        private void Start_CancelAll(HotspotCounterparty counterparty)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");
                    this._clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD));

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    HotspotClientOrderRequestInfo cori1 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 1000000m, 2.9m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, false);

                    HotspotClientOrder clientOrder1 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1));




                    HotspotClientOrderRequestInfo cori2 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 1000000m, 2.9m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, false);

                    HotspotClientOrder clientOrder2 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1));



                    this._clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(
                        _clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_USD));
                    this._clientGateway.Targets.All.CancelAllMyOrders();

                    Thread.Sleep(TimeSpan.FromSeconds(2));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        private void Start(HotspotCounterparty counterparty)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    HotspotClientOrderRequestInfo cori1 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 1000000m, 1.9m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, true);

                    HotspotClientOrder clientOrder1 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    HotspotClientOrderRequestInfo cori1Replacing = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 300000m,
                                                                                             1.9m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, true);
                    HotspotClientOrderRequestInfo cori2 =
                        _clientGateway.Targets.Hotspot.CreateOrderReplaceRequest(clientOrder1, cori1Replacing);

                    HotspotClientOrder clientOrder2 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }


                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    HotspotClientOrderRequestInfo cori2Replacing = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 400000m,
                                                                                             1.9m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, false);
                    HotspotClientOrderRequestInfo cori3 =
                        _clientGateway.Targets.Hotspot.CreateOrderReplaceRequest(clientOrder2, cori2Replacing);

                    HotspotClientOrder clientOrder3 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori3);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder3).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    //now simultaneous cancels
                    _clientGateway.Targets.Hotspot.CancelOrder(clientOrder2);

                    _clientGateway.Targets.Hotspot.CancelOrder(clientOrder3);




                    Thread.Sleep(TimeSpan.FromSeconds(2));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        private void Start1(HotspotCounterparty counterparty)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    HotspotClientOrderRequestInfo cori1 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 1000000m, 1.5m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, false);

                    HotspotClientOrder clientOrder1 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    HotspotClientOrderRequestInfo cori1Replacing = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 300000m,
                                                                                             1.5m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, false);
                    HotspotClientOrderRequestInfo cori2 =
                        _clientGateway.Targets.Hotspot.CreateOrderReplaceRequest(clientOrder1, cori1Replacing);

                    HotspotClientOrder clientOrder2 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }


                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    HotspotClientOrderRequestInfo cori2Replacing = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(2000000m, 400000m,
                                                                                             1.9m,
                                                                                             DealDirection.Sell,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, counterparty, false);
                    HotspotClientOrderRequestInfo cori3 =
                        _clientGateway.Targets.Hotspot.CreateOrderReplaceRequest(clientOrder2, cori2Replacing);


                    if (!_clientGateway.Targets.Hotspot.SubmitOrder(_clientGateway.Targets.Hotspot.CreateClientOrder(cori3)).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(2));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        private void Start2(HotspotCounterparty counterparty)
        {
            Task.Factory.StartNew(() =>
                {
                    try
                    {

                        this._logger.Log(LogLevel.Info, "Starting tests");

                        Thread.Sleep(TimeSpan.FromSeconds(3));

                        HotspotClientOrderRequestInfo cori1 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(100000m, 100000m, 1.1m,
                                                                                                 DealDirection.Sell,
                                                                                                 Symbol.EUR_USD,
                                                                                                 TimeInForce
                                                                                                     .ImmediateOrKill, counterparty, false);
                        if (!_clientGateway.Targets.Hotspot.SubmitOrder(_clientGateway.Targets.Hotspot.CreateClientOrder(cori1)).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                        }

                        Thread.Sleep(TimeSpan.FromSeconds(1));

                        HotspotClientOrderRequestInfo cori2 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(10000000m, 2900000m,
                                                                                                 1.30m,
                                                                                                 DealDirection.Sell,
                                                                                                 Symbol.EUR_USD,
                                                                                                 TimeInForce
                                                                                                     .ImmediateOrKill, counterparty, false);
                        if (!_clientGateway.Targets.Hotspot.SubmitOrder(_clientGateway.Targets.Hotspot.CreateClientOrder(cori2)).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                        }


                        Thread.Sleep(TimeSpan.FromSeconds(2));

                        HotspotClientOrderRequestInfo cori3 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(10000000m, 2900000m,
                                                                                                 1.30m,
                                                                                                 DealDirection.Buy,
                                                                                                 Symbol.EUR_USD,
                                                                                                 TimeInForce.Day, counterparty, false);
                        HotspotClientOrder clOrder3 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori3);

                        if (!_clientGateway.Targets.Hotspot.SubmitOrder(clOrder3).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit order #3");
                        }


                        Thread.Sleep(TimeSpan.FromSeconds(2));

                        HotspotClientOrderRequestInfo cori4Replacement = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(8000000m,
                                                                                                            2900000m,
                                                                                                            1.39m,
                                                                                                            DealDirection
                                                                                                                .Buy,
                                                                                                            Symbol
                                                                                                                .EUR_USD,
                                                                                                            TimeInForce
                                                                                                                .Day, counterparty, false);
                        HotspotClientOrderRequestInfo cori4 = _clientGateway.Targets.Hotspot.CreateOrderReplaceRequest(clOrder3,
                                                                                                               cori4Replacement);

                        HotspotClientOrder clOrder4 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori4);
                        if (!_clientGateway.Targets.Hotspot.SubmitOrder(clOrder4).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit order #4");
                        }




                        Thread.Sleep(TimeSpan.FromSeconds(2));

                        HotspotClientOrderRequestInfo cori5 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(100000m, 100000m, 1.01m,
                                                                                                 DealDirection.Buy,
                                                                                                 Symbol.EUR_USD,
                                                                                                 TimeInForce.Day, counterparty, false);
                        HotspotClientOrder clOrder5 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori5);

                        if (!_clientGateway.Targets.Hotspot.SubmitOrder(clOrder5).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit order #5");
                        }


                        Thread.Sleep(TimeSpan.FromSeconds(2));

                        HotspotClientOrderRequestInfo cori6 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(300000m, 300000m, 1.11m,
                                                                                                 DealDirection.Buy,
                                                                                                 Symbol.EUR_USD,
                                                                                                 TimeInForce.Day, counterparty, false);
                        HotspotClientOrder clOrder6 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori6);

                        if (!_clientGateway.Targets.Hotspot.SubmitOrder(clOrder6).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit order #6");
                        }



                        Thread.Sleep(TimeSpan.FromSeconds(2));

                        if (!_clientGateway.Targets.Hotspot.CancelOrder(clOrder5).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't cancel order #5");
                        }





                        //Thread.Sleep(TimeSpan.FromSeconds(2));


                        //if (!_clientGateway.Targets.Hotspot.CancelAllOrdersForAllClients())
                        //{
                        //    this._logger.Log(LogLevel.Error, "Couldn't cancel all orders");
                        //}

                        this._logger.Log(LogLevel.Info, "Finishing tests");

                    }
                    catch (Exception e)
                    {
                        this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                    }

                });
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e, "Unhandled exception");
            return false;
        }

        private void ClientGatewayOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "Hotspot TRADING CLIENT Receiving NewDeal. {0}", integratorDealInternal);
        }

        private void ClientGatewayOnClientOrderUpdated(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            //if (clientOrderUpdateInfo.ClientOrder is HotspotClientOrder)
            //{
            //    Counterparty ctp = ((HotspotClientOrder) clientOrderUpdateInfo.ClientOrder).HotspotClientOrderRequestInfo.Counterparty;
            //}

            _logger.Log(LogLevel.Info, "Hotspot TRADING CLIENT Receiving OrderUpdate: {0}", clientOrderUpdateInfo);
        }
    }
}
