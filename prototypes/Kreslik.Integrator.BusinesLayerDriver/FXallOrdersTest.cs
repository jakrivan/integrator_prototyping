﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class FXallOrdersTest : IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(Common.ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;
            _clientGateway.Targets.FXall.ClientOrderUpdated += FXallOnClientOrderUpdated;
            _clientGateway.Targets.FXall.NewDealDone += FXallOnNewDealDone;
            _clientGateway.Targets.FXall.CounterpartyRejectedIntegratorOrder += FXallOnCounterpartyRejectedIntegratorOrder;
            _clientGateway.Targets.FXall.CounterpartyIgnoredIntegratorOrder += ClientGatewayOnCounterpartyIgnoredIntegratorOrder;
        }

        private void ClientGatewayOnCounterpartyIgnoredIntegratorOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            this._logger.Log(LogLevel.Info, "FXall Client receiving Counterparty Ignoting Info: {0}", counterpartyOrderIgnoringInfo);
        }

        public void Start()
        {
            //this.Start(HotspotCounterparty.HTA);
            //this.Start(HotspotCounterparty.HTF);

            //this.Start_CancelAll(HotspotCounterparty.HTA);

            //this.Start_Pegged(HotspotCounterparty.HTA);
            this.StartInternal();
        }

        private void FXallOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "FXall ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnIndividualPriceUpdate(PriceIndividualUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank price Individual Price Update: {0}", priceUpdateEventArgs);
        }

        private void StartInternal()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");

                    _clientGateway.Targets.FXall.PricesTopOfBook.NewUpdateAvailable += FXallOnPriceUpdate;
                    _clientGateway.Targets.FXall.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.FXall.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_JPY, FXallCounterparty.FA1));
                    _clientGateway.Targets.FXall.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.FXall.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD, FXallCounterparty.FA1));

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    _clientGateway.Targets.FXall.PricesTopOfBook.UnsubscribeFromUpdates(
                        _clientGateway.Targets.FXall.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_USD, FXallCounterparty.FA1));



                    FXallClientOrderRequestInfo cori1 = _clientGateway.Targets.FXall.CreateLimitOrderRequest(500000000m, 0m, 1.368m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, false);

                    FXallClientOrder clientOrder1 = _clientGateway.Targets.FXall.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.FXall.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1.5));

                    FXallClientOrderRequestInfo cori1Replacing = _clientGateway.Targets.FXall.CreateLimitOrderRequest(500000m, 0m,
                                                                                             1.35m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, false);
                    FXallClientOrderRequestInfo cori2 =
                        _clientGateway.Targets.FXall.CreateOrderReplaceRequest(clientOrder1, cori1Replacing);

                    FXallClientOrder clientOrder2 = _clientGateway.Targets.FXall.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.FXall.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(3));


                    FXallClientOrderRequestInfo cori3 = _clientGateway.Targets.FXall.CreateLimitOrderRequest(500000m, 0m, 1.368m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, false);

                    FXallClientOrder clientOrder3 = _clientGateway.Targets.FXall.CreateClientOrder(cori3);

                    if (!_clientGateway.Targets.FXall.SubmitOrder(clientOrder3).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #3");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    //Thread.Sleep(TimeSpan.FromSeconds(1));

                    //FXallClientOrderRequestInfo cori2Replacing = _clientGateway.Targets.FXall.CreateLimitOrderRequest(2000000m, 400000m,
                    //                                                                         1.9m,
                    //                                                                         DealDirection.Sell,
                    //                                                                         Symbol.EUR_USD,
                    //                                                                         TimeInForce
                    //                                                                             .Day);
                    //FXallClientOrderRequestInfo cori3 =
                    //    _clientGateway.Targets.FXall.CreateOrderReplaceRequest(clientOrder2, cori2Replacing);

                    //FXallClientOrder clientOrder3 = _clientGateway.Targets.FXall.CreateClientOrder(cori3);

                    //if (!_clientGateway.Targets.FXall.SubmitOrder(clientOrder3).RequestSucceeded)
                    //{
                    //    this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    //}

                    ////now simultaneous cancels
                    //_clientGateway.Targets.FXall.CancelOrder(clientOrder2);

                    //_clientGateway.Targets.FXall.CancelOrder(clientOrder3);




                    Thread.Sleep(TimeSpan.FromSeconds(8));

                    _logger.Log(LogLevel.Info, "Cancelling all orders");
                    _clientGateway.Targets.All.CancelAllMyOrders();
                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        private void FXallOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            _logger.Log(LogLevel.Info, "FXall TRADING CLIENT Receiving cpunterparty rejection. {0}", counterpartyRejectionInfo);
        }

        private void FXallOnClientOrderUpdated(FXallClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info,
                        "FXall TRADING CLIENT Receiving order update. {0}, rejected: {1}, cancelled: {2} remaining: {3}",
                        clientOrderUpdateInfo, clientOrderUpdateInfo.SizeBaseAbsRejected,
                        clientOrderUpdateInfo.SizeBaseAbsCancelled, clientOrderUpdateInfo.SizeBaseAbsTotalRemaining);
        }

        private void FXallOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "FXall TRADING CLIENT Receiving NewDeal. {0}", integratorDealInternal);
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e, "Unhandled exception");
            return false;
        }
    }
}
