﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class RandomMiniTests
    {
        public static void Assert(bool value)
        {
            if(!value)
                throw new Exception();
        }

        public void RoundingTest()
        {
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysToZero, 0.05m, 3.26m).RoundedValue == 3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysToZero, 0.05m, -3.26m).RoundedValue == -3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysToZero, 0.05m, 3.29m).RoundedValue == 3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysToZero, 0.05m, -3.29m).RoundedValue == -3.25m);

            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysAwayFromZero, 0.05m, 3.26m).RoundedValue == 3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysAwayFromZero, 0.05m, -3.26m).RoundedValue == -3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysAwayFromZero, 0.05m, 3.29m).RoundedValue == 3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.AlwaysAwayFromZero, 0.05m, -3.29m).RoundedValue == -3.30m);

            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToNearest, 0.05m, 3.26m).RoundedValue == 3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToNearest, 0.05m, -3.26m).RoundedValue == -3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToNearest, 0.05m, 3.29m).RoundedValue == 3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToNearest, 0.05m, -3.29m).RoundedValue == -3.30m);

            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToCeiling, 0.05m, 3.26m).RoundedValue == 3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToCeiling, 0.05m, -3.26m).RoundedValue == -3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToCeiling, 0.05m, 3.29m).RoundedValue == 3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToCeiling, 0.05m, -3.29m).RoundedValue == -3.25m);

            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToFloor, 0.05m, 3.26m).RoundedValue == 3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToFloor, 0.05m, -3.26m).RoundedValue == -3.30m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToFloor, 0.05m, 3.29m).RoundedValue == 3.25m);
            Assert(SymbolsInfoUtils.TryRoundToIncrement(RoundingKind.ToFloor, 0.05m, -3.29m).RoundedValue == -3.30m);
        }
    }
}
