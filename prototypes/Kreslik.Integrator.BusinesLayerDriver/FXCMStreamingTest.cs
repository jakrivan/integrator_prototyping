﻿using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class FXCMStreamingTest : IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;

            _clientGateway.Targets.FXCMStreaming.AllClients.NewStreamingDealRejectedByCounterparty += FxcmStreamingDealRejectedByOnNewStreamingDealRejectedByCounterparty;
            _clientGateway.Targets.FXCMStreaming.AllClients.NewStreamingDealAcceptedByIntegrator += FxcmStreamingOnNewStreamingDealAcceptedByIntegrator;
            _clientGateway.Targets.FXCMStreaming.AllClients.NewStreamingDealRejectedByIntegrator += FxcmStreamingDealRejectedByOnNewStreamingDealRejectedByIntegrator;
        }

        public void Start()
        {
            


            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");

                    var result = _clientGateway.Targets.FXCMStreaming.AllClients.SubscribeToUpdates(new ClientStreamingForwardingRequest(Symbol.EUR_USD,
                        ClientStreamingForwardingRequestType.AllStreamingEvents));

                    this._logger.Log(LogLevel.Info, "Streaming subscription: {0}", result);

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e, "Unhandled exception");
            return false;
        }


        private void FxcmStreamingDealRejectedByOnNewStreamingDealRejectedByIntegrator(IStreamingDealRejectedByIntegrator o)
        {
            this._logger.Log(LogLevel.Info, "New Integrator reject: {0}", o);
        }

        private void FxcmStreamingOnNewStreamingDealAcceptedByIntegrator(IStreamingDealAcceptedByIntegrator o)
        {
            this._logger.Log(LogLevel.Info, "New Integrator deal: {0}", o);
        }

        private void FxcmStreamingDealRejectedByOnNewStreamingDealRejectedByCounterparty(IStreamingDealRejectedByCounterparty o)
        {
            this._logger.Log(LogLevel.Info, "New Counterparty reject: {0}", o);
        }
    }
}
