﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class FXCMTest : IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(Common.ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;
            _clientGateway.Targets.FXCMTaking.ClientOrderUpdated += FXCMOnClientOrderUpdated;
            _clientGateway.Targets.FXCMTaking.NewDealDone += FXCMOnNewDealDone;
            _clientGateway.Targets.FXCMTaking.CounterpartyRejectedIntegratorOrder += FXCMOnCounterpartyRejectedIntegratorOrder;
            _clientGateway.Targets.FXCMTaking.CounterpartyIgnoredIntegratorOrder += ClientGatewayOnCounterpartyIgnoredIntegratorOrder;
            _clientGateway.Targets.FXCMTaking.AllClients.ClientOrderUpdated += AllClientsOnClientOrderUpdated;
        }

        private void ClientGatewayOnCounterpartyIgnoredIntegratorOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            this._logger.Log(LogLevel.Info, "FXCM Client receiving Counterparty Ignoting Info: {0}", counterpartyOrderIgnoringInfo);
        }

        public void Start()
        {
            this.StartInternal();
        }

        private void FXCMOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "FXCM ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnIndividualPriceUpdate(PriceIndividualUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank price Individual Price Update: {0}", priceUpdateEventArgs);
        }

        private void StartInternal()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {

                    this._logger.Log(LogLevel.Info, "Starting tests");

                    _clientGateway.Targets.FXCMTaking.PricesTopOfBook.NewUpdateAvailable += FXCMOnPriceUpdate;
                    _clientGateway.Targets.FXCMTaking.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.FXCMTaking.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_JPY, FXCMCounterparty.FC1));
                    _clientGateway.Targets.FXCMTaking.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.FXCMTaking.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD, FXCMCounterparty.FC1));
                    if (!_clientGateway.Targets.FXCMTaking.AllClients.SubscribeToUpdates(new ClientForwardingRequest(
                                                                                       Symbol.EUR_USD,
                                                                                       ClientForwardingRequestType
                                                                                           .OrderUpdates))
                                       .RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't subscribe to forwarding");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    _clientGateway.Targets.FXCMTaking.PricesTopOfBook.UnsubscribeFromUpdates(
                        _clientGateway.Targets.FXCMTaking.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_USD, FXCMCounterparty.FC1));



                    FXCMClientOrderRequestInfo cori1 = _clientGateway.Targets.FXCMTaking.CreateLimitOrderRequest(500000000m, 0m, 1.328m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, FXCMCounterparty.FC1, false);

                    FXCMClientOrder clientOrder1 = _clientGateway.Targets.FXCMTaking.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.FXCMTaking.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1.5));

                    FXCMClientOrderRequestInfo cori1Replacing = _clientGateway.Targets.FXCMTaking.CreateLimitOrderRequest(500000m, 0m,
                                                                                             1.75m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, FXCMCounterparty.FC1, false);
                    FXCMClientOrderRequestInfo cori2 =
                        _clientGateway.Targets.FXCMTaking.CreateOrderReplaceRequest(clientOrder1, cori1Replacing);

                    FXCMClientOrder clientOrder2 = _clientGateway.Targets.FXCMTaking.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.FXCMTaking.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(3));


                    FXCMClientOrderRequestInfo cori3 = _clientGateway.Targets.FXCMTaking.CreateLimitOrderRequest(500000m, 0m, 1.368m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, FXCMCounterparty.FC1, false);

                    FXCMClientOrder clientOrder3 = _clientGateway.Targets.FXCMTaking.CreateClientOrder(cori3);

                    if (!_clientGateway.Targets.FXCMTaking.SubmitOrder(clientOrder3).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #3");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(3));


                    Thread.Sleep(TimeSpan.FromSeconds(8));

                    _logger.Log(LogLevel.Info, "Cancelling all orders");
                    _clientGateway.Targets.All.CancelAllMyOrders();
                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        private void FXCMOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            _logger.Log(LogLevel.Info, "FXCM TRADING CLIENT Receiving cpunterparty rejection. {0}", counterpartyRejectionInfo);
        }

        private void FXCMOnClientOrderUpdated(FXCMClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info,
                        "FXCM TRADING CLIENT Receiving order update. {0}, rejected: {1}, cancelled: {2} remaining: {3}",
                        clientOrderUpdateInfo, clientOrderUpdateInfo.SizeBaseAbsRejected,
                        clientOrderUpdateInfo.SizeBaseAbsCancelled, clientOrderUpdateInfo.SizeBaseAbsTotalRemaining);
        }

        private void AllClientsOnClientOrderUpdated(FXCMClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info,
                        "FXCM TRADING CLIENT Receiving order update broadcast. {0}, rejected: {1}, cancelled: {2} remaining: {3}",
                        clientOrderUpdateInfo, clientOrderUpdateInfo.SizeBaseAbsRejected,
                        clientOrderUpdateInfo.SizeBaseAbsCancelled, clientOrderUpdateInfo.SizeBaseAbsTotalRemaining);
        }

        private void FXCMOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "FXCM TRADING CLIENT Receiving NewDeal. {0}", integratorDealInternal);
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e, "Unhandled exception");
            return false;
        }
    }
}
