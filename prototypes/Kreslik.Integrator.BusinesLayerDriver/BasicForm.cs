﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public partial class BasicForm : Form
    {
        public BasicForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text += "Button clicked" + Environment.NewLine;
            textBox1.Text += "Button clicked" + Environment.NewLine;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
