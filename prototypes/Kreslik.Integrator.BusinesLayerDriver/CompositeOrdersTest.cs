﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    //public class CompositeOrdersTest : IIntegratorClient
    //{
    //    IClientGateway _clientGateway;
    //    private ILogger _logger;
    //    private string _identity;

    //    private List<PriceObject> _lastBids = Enumerable.Repeat<PriceObject>(null, Symbol.ValuesCount).ToList();
    //    private List<PriceObject> _lastAsks = Enumerable.Repeat<PriceObject>(null, Symbol.ValuesCount).ToList();

    //    //private PriceObject _lastBid = null;
    //    //private PriceObject _lastAsk = null;
    //    private SafeTimer _timer;
    //    private TimeSpan _timeout = TimeSpan.FromSeconds(5);

    //    private Dictionary<string, decimal> _childOrdersFilling = new Dictionary<string, decimal>();
    //    private Dictionary<string, BankPoolClientOrder> _childOrders = new Dictionary<string, BankPoolClientOrder>();
    //    private Dictionary<string, BankPoolClientOrder> _parentOrders = new Dictionary<string, BankPoolClientOrder>();

    //    private int numOrdersFilledUntransformed = 0;
    //    private int numOrdersFilledTransformed = 0;


    //    public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
    //    {
    //        _logger = logger;
    //        _clientGateway = clientGateway;
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
    //        _clientGateway.Targets.BankPool.ClientOrderUpdated += ClientGatewayOnClientOrderUpdated;
    //        _clientGateway.Targets.BankPool.NewDealDone += ClientGatewayOnNewDealDone;
    //    }

    //    private void ClientGatewayOnNewDealDone(IntegratorDealInternal integratorDealInternal)
    //    {
    //        _logger.Log(LogLevel.Info, "TRADING CLIENT Receiving NewDeal. {0}", integratorDealInternal);

    //        if (!_childOrdersFilling.ContainsKey(integratorDealInternal.ClientOrderIdentity))
    //        {
    //            this._logger.Log(LogLevel.Error, "Client order {0} is unknown to this client", integratorDealInternal.ClientOrderIdentity);
    //        }
    //        else
    //        {
    //            _childOrdersFilling[integratorDealInternal.ClientOrderIdentity] -=
    //                integratorDealInternal.FilledAmountBaseAbs;

    //            this._logger.Log(LogLevel.Info, "Deal filled amount {0} in order {1} and left {2} to be filled",
    //                             integratorDealInternal.FilledAmountBaseAbs, integratorDealInternal.ClientOrderIdentity,
    //                             _childOrdersFilling[integratorDealInternal.ClientOrderIdentity]);

    //            if (_childOrdersFilling[integratorDealInternal.ClientOrderIdentity] <= 0m)
    //            {
    //                if (_childOrdersFilling[integratorDealInternal.ClientOrderIdentity] < 0m)
    //                {
    //                    _logger.Log(LogLevel.Error, "Order {0} was overfilled", integratorDealInternal.ClientOrderIdentity);
    //                }

    //                if (_childOrders[integratorDealInternal.ClientOrderIdentity].OrderRequestInfo.RequestedPrice ==
    //                    integratorDealInternal.Price)
    //                {
    //                    numOrdersFilledUntransformed++;
    //                    this._logger.Log(LogLevel.Info, "Order {0} was likely filled as quoted without transformation", integratorDealInternal.ClientOrderIdentity);
    //                }
    //                else
    //                {
    //                    numOrdersFilledTransformed++;
    //                    this._logger.Log(LogLevel.Info, "Order {0} was likely transformed to MKT order", integratorDealInternal.ClientOrderIdentity);
    //                }

    //                _childOrdersFilling.Remove(integratorDealInternal.ClientOrderIdentity);
    //                lock (_childOrders)
    //                {
    //                    _childOrders.Remove(integratorDealInternal.ClientOrderIdentity);
    //                }
    //            }
    //        }
    //    }

    //    private void ClientGatewayOnClientOrderUpdated(ClientOrderUpdateInfo clientOrderUpdateInfo)
    //    {
    //        _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving OrderUpdate: {1}", "CompositeOrdersTest", clientOrderUpdateInfo);

    //        if (clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.RejectedByIntegrator)
    //        {
    //            BankPoolClientOrder parentOrder;
    //            if (_parentOrders.TryGetValue(clientOrderUpdateInfo.ClientOrderIdentity, out parentOrder))
    //            {
    //                foreach (BankPoolClientOrder childOrder in parentOrder.ChildOrders)
    //                {
    //                    _childOrdersFilling.Remove(childOrder.ClientOrderIdentity);
    //                    lock (_childOrders)
    //                    {
    //                        _childOrders.Remove(childOrder.ClientOrderIdentity);
    //                    }
    //                }
    //                _parentOrders.Remove(clientOrderUpdateInfo.ClientOrderIdentity);
    //            }
    //            else
    //            {
    //                _logger.Log(LogLevel.Error, "Receiving OrderUpdate for unknown order: {0}", clientOrderUpdateInfo.ClientOrderIdentity);
    //            }
    //        }
    //    }

    //    private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
    //    {
    //        _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price update: {1}.", _identity, priceUpdateEventArgs);

    //        PriceObject price = priceUpdateEventArgs.PriceObject;

    //        if (price == null || PriceObjectInternal.IsNullPrice(price)) return;

    //        if (price.Side == PriceSide.Ask)
    //        {
    //            _lastAsks[(int)price.Symbol] = price;
    //        }
    //        else
    //        {
    //            _lastBids[(int)price.Symbol] = price;
    //        }
    //    }

    //    private void TimeoutCallback(object nullState)
    //    {
    //        SendCompositeOrder(Symbol.GBP_JPY);
    //        SendCompositeOrder(Symbol.EUR_USD);
    //        SendCompositeOrder(Symbol.EUR_JPY);
    //    }

    //    private TimeSpan _fillingTimeThreshold = TimeSpan.FromSeconds(2);

    //    private void OrdersCheckCallback(object nullState)
    //    {
    //        this._logger.Log(LogLevel.Info, "Unfilled orders count: {0}", _childOrders.Count);

    //        lock (_childOrders)
    //        {
    //            var agingOrders =
    //                _childOrders.Select(pair => pair.Value)
    //                            .Where(ord => HighResolutionDateTime.UtcNow - ord.CreatedUtc > _fillingTimeThreshold);

    //            int agingOrdersCount = agingOrders.Count();
    //            if (agingOrdersCount > 0)
    //            {
    //                _logger.Log(LogLevel.Warn, "There is {0} orders older than {1}", agingOrdersCount,
    //                            _fillingTimeThreshold);

    //                _logger.Log(LogLevel.Warn, "Top 10 aging orders: {0}{1}", Environment.NewLine,
    //                            string.Join(Environment.NewLine,
    //                                        agingOrders.OrderBy(ord => ord.CreatedUtc)
    //                                                   .Take(10)
    //                                                   .Select(
    //                                                       ord =>
    //                                                       string.Format("Age: {0}, Order: {1}",
    //                                                                     HighResolutionDateTime.UtcNow - ord.CreatedUtc,
    //                                                                     ord))));
    //            }
    //        }

    //        this._logger.Log(LogLevel.Info, "Orders filled UNtransformed: {0}, Orders filled transformed: {1}",
    //                         numOrdersFilledUntransformed, numOrdersFilledTransformed);
    //    }

    //    private const decimal ORDER_SIZE = 600000m;
    //    private void SendCompositeOrder(Symbol symbol)
    //    {
    //        PriceObject ask = _lastAsks[(int) symbol];
    //        PriceObject bid = _lastBids[(int) symbol];

    //        if (ask == null || PriceObjectInternal.IsNullPrice(ask) || bid == null || PriceObjectInternal.IsNullPrice(bid))
    //            return;

    //        if (ask.SizeBaseAbsRemaining < ORDER_SIZE || bid.SizeBaseAbsRemaining < ORDER_SIZE)
    //        {
    //            this._logger.Log(LogLevel.Info, "Insuficient liquidity in order to create order");
    //            return;
    //        }

    //        BankPoolClientOrderRequestInfo orderRequestInfo1 = _clientGateway.Targets.BankPool.CreateQuotedOrderRequest(ORDER_SIZE, ask.Price,
    //                                                                                       DealDirection.Buy, ask);
    //        BankPoolClientOrderRequestInfo orderRequestInfo2 = _clientGateway.Targets.BankPool.CreateQuotedOrderRequest(ORDER_SIZE, bid.Price,
    //                                                                                       DealDirection.Sell, bid);
    //        BankPoolClientOrder clOrd = _clientGateway.Targets.BankPool.CreateCompositeClientOrder(
    //            new List<BankPoolClientOrderRequestInfo>() { orderRequestInfo1, orderRequestInfo2 },
    //            CompositeOrderStrategy.AllAtOnceOrImmediateAtMarket);

    //        _logger.Log(LogLevel.Info, "Created order {0}", clOrd);

    //        foreach (BankPoolClientOrder childOrder in clOrd.ChildOrders)
    //        {
    //            _childOrdersFilling.Add(childOrder.ClientOrderIdentity, childOrder.OrderRequestInfo.SizeBaseAbsInitial);
    //            lock (_childOrders)
    //            {
    //                _childOrders.Add(childOrder.ClientOrderIdentity, childOrder);
    //            }
    //        }
    //        _parentOrders.Add(clOrd.ClientOrderIdentity, clOrd);

    //        _clientGateway.Targets.BankPool.SubmitOrder(clOrd);
    //    }

    //    public void Start()
    //    {
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.GBP_JPY));
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD));
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_JPY));

    //        _timer = new SafeTimer(TimeoutCallback, null, _timeout, _timeout);
    //        _timer = new SafeTimer(OrdersCheckCallback, null, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30));
    //    }

    //    public void Stop(string reason)
    //    {
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.GBP_JPY));
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_USD));
    //        _clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_JPY));
    //    }

    //    public bool TryHandleUnhandledException(Exception e)
    //    {
    //        throw new Exception();
    //    }
    //}
}