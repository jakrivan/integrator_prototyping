﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class ToBSubscriptionClient : IIntegratorClient
    {
        private IClientGateway _clientGateway;
        private ILogger _logger;


        public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            this._clientGateway = clientGateway;
            this._logger = logger;

            clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
            clientGateway.Targets.FXCMTaking.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
            clientGateway.Targets.FXall.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
            clientGateway.Targets.LMAX.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
            clientGateway.Targets.Hotspot.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
        }

        public void Start()
        {
            foreach (Symbol symbol in Symbol.Values)
            {
                _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(symbol));
                _clientGateway.Targets.FXCMTaking.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.FXCMTaking.PricesTopOfBook.GetSubscriptionRequest(symbol, FXCMCounterparty.FC1));
                _clientGateway.Targets.FXCMTaking.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.FXCMTaking.PricesTopOfBook.GetSubscriptionRequest(symbol, FXCMCounterparty.FC2));
                _clientGateway.Targets.LMAX.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.LMAX.PricesTopOfBook.GetSubscriptionRequest(symbol, LmaxCounterparty.L01));
                _clientGateway.Targets.LMAX.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.LMAX.PricesTopOfBook.GetSubscriptionRequest(symbol, LmaxCounterparty.LM2));
                _clientGateway.Targets.LMAX.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.LMAX.PricesTopOfBook.GetSubscriptionRequest(symbol, LmaxCounterparty.LM3));
                _clientGateway.Targets.FXall.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.FXall.PricesTopOfBook.GetSubscriptionRequest(symbol, FXallCounterparty.FA1));
                //no hotspot
                _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(symbol, HotspotCounterparty.HTF));
                _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(symbol, HotspotCounterparty.HTA));
                _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                    _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(symbol, HotspotCounterparty.HT3));
            }

        }

        public void Stop(string reason)
        {
            
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            return false;
        }

        private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            _logger.Log(LogLevel.Info, "Receiving Price update: {0}.", priceUpdateEventArgs);
        }
    }
}
