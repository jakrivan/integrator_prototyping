﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class LmaxOrdersTest : IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(Common.ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;
            _clientGateway.Targets.LMAX.ClientOrderUpdated += LmaxOnClientOrderUpdated;
            _clientGateway.Targets.LMAX.NewDealDone += LmaxOnNewDealDone;
            _clientGateway.Targets.LMAX.CounterpartyRejectedIntegratorOrder += LmaxOnCounterpartyRejectedIntegratorOrder;
            _clientGateway.Targets.LMAX.CounterpartyIgnoredIntegratorOrder += ClientGatewayOnCounterpartyIgnoredIntegratorOrder;

            _clientGateway.Targets.LMAX.AllClients.ClientOrderUpdated += AllClientsOnClientOrderUpdated;
            _clientGateway.Targets.LMAX.AllClients.NewDealDone += AllClientsOnNewDealDone;

            _clientGateway.Targets.BankPool.ClientOrderUpdated += BankPoolOnClientOrderUpdated;
            _clientGateway.Targets.BankPool.AllClients.ClientOrderUpdated += AllClientsOnClientOrderUpdated;

            _clientGateway.TradingInfo.ImportantIntegratorInformationArrived += TradingInfoOnImportantIntegratorInformationArrived;
        }

        private void ClientGatewayOnCounterpartyIgnoredIntegratorOrder(CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            this._logger.Log(LogLevel.Info, "FXall Client receiving Counterparty Ignoting Info: {0}", counterpartyOrderIgnoringInfo);
        }

        public void Start()
        {
            //this.Start(HotspotCounterparty.HTA);
            //this.Start(HotspotCounterparty.HTF);

            //this.Start_CancelAll(HotspotCounterparty.HTA);

            //this.Start_Pegged(HotspotCounterparty.HTA);
            this.StartInternal();
        }

        private void LmaxOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Lmax ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank ToB Price Update: {0}", priceUpdateEventArgs);
        }

        private void BankOnIndividualPriceUpdate(PriceIndividualUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Info, "Bank price Individual Price Update: {0}", priceUpdateEventArgs);
        }

        private void StartInternal()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {


                    var result = _clientGateway.Targets.LMAX.AllClients.SubscribeToUpdates(
                    new ClientForwardingRequest(Symbol.EUR_USD,
                                                ClientForwardingRequestType.OrderUpdates |
                                                ClientForwardingRequestType.CounterpartyRejections |
                                                ClientForwardingRequestType.Deals));
                    this._logger.Log(LogLevel.Info, "Subscription result: {0} {1}", result.RequestSucceeded, result.ErrorMessage);



                    var result2 = _clientGateway.Targets.BankPool.AllClients.SubscribeToUpdates(
                    new ClientForwardingRequest(Symbol.EUR_USD,
                                                ClientForwardingRequestType.OrderUpdates |
                                                ClientForwardingRequestType.CounterpartyRejections |
                                                ClientForwardingRequestType.Deals));
                    this._logger.Log(LogLevel.Info, "Subscription result: {0} {1}", result2.RequestSucceeded, result2.ErrorMessage);


                    this._logger.Log(LogLevel.Info, "Starting tests");


                    this._logger.Log(LogLevel.Info, "Is symbol supported: {0}",
                                     _clientGateway.TradingInfo.Symbols.IsSupported(Symbol.EUR_USD, TradingTargetType.LMAX));

                    this._logger.Log(LogLevel.Info, "Symbol granularity: {0}",
                                     _clientGateway.TradingInfo.Symbols.GetMinimumPriceIncrement(Symbol.USD_CAD, TradingTargetType.BankPool));


                    this._logger.Log(LogLevel.Info, "Is symbol supported: {0}",
                                     _clientGateway.TradingInfo.Symbols.IsSupported(Symbol.EUR_USD, TradingTargetType.FXall));

                    this._logger.Log(LogLevel.Info, "Symbol granularity: {0}",
                                     _clientGateway.TradingInfo.Symbols.GetMinimumPriceIncrement(Symbol.USD_CAD, TradingTargetType.FXall));


                    _clientGateway.Targets.LMAX.PricesTopOfBook.NewUpdateAvailable += LmaxOnPriceUpdate;
                    _clientGateway.Targets.LMAX.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.LMAX.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_SGD, LmaxCounterparty.L01));
                    _clientGateway.Targets.LMAX.PricesTopOfBook.SubscribeToUpdates(
                        _clientGateway.Targets.LMAX.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD, LmaxCounterparty.L01));


                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    _clientGateway.Targets.LMAX.PricesTopOfBook.UnsubscribeFromUpdates(
                        _clientGateway.Targets.LMAX.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_USD, LmaxCounterparty.L01));


                    {
                        BankPoolClientOrderRequestInfo bcori =
                            _clientGateway.Targets.BankPool.CreateLimitOrderRequest(100000m, 1.39m, DealDirection.Buy,
                                                                                    Symbol.EUR_USD, false);

                        BankPoolClientOrder bord = _clientGateway.Targets.BankPool.CreateClientOrder(bcori);

                        if (!_clientGateway.Targets.BankPool.SubmitOrder(bord).RequestSucceeded)
                        {
                            this._logger.Log(LogLevel.Error, "Couldn't sumbit bank order #1");
                        }
                    }




                    LmaxClientOrderRequestInfo cori1 = _clientGateway.Targets.LMAX.CreateLimitOrderRequest(500000000m, 1.368m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, LmaxCounterparty.LM2, false);

                    LmaxClientOrder clientOrder1 = _clientGateway.Targets.LMAX.CreateClientOrder(cori1);

                    if (!_clientGateway.Targets.LMAX.SubmitOrder(clientOrder1).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(1.5));

                    LmaxClientOrderRequestInfo cori1Replacing = _clientGateway.Targets.LMAX.CreateLimitOrderRequest(500000m,
                                                                                             1.35m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, LmaxCounterparty.LM2, false);
                    LmaxClientOrderRequestInfo cori2 =
                        _clientGateway.Targets.LMAX.CreateOrderReplaceRequest(clientOrder1, cori1Replacing);

                    LmaxClientOrder clientOrder2 = _clientGateway.Targets.LMAX.CreateClientOrder(cori2);

                    if (!_clientGateway.Targets.LMAX.SubmitOrder(clientOrder2).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #2");
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(3));


                    LmaxClientOrderRequestInfo cori3 = _clientGateway.Targets.LMAX.CreateLimitOrderRequest(500000m, 1.368m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_USD,
                                                                                             TimeInForce
                                                                                                 .Day, LmaxCounterparty.LM2, false);

                    LmaxClientOrder clientOrder3 = _clientGateway.Targets.LMAX.CreateClientOrder(cori3);

                    if (!_clientGateway.Targets.LMAX.SubmitOrder(clientOrder3).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #3");
                    }





                    LmaxClientOrderRequestInfo cori4 = _clientGateway.Targets.LMAX.CreateLimitOrderRequest(500000m, 1.75m,
                                                                                             DealDirection.Buy,
                                                                                             Symbol.EUR_SGD,
                                                                                             TimeInForce
                                                                                                 .Day, LmaxCounterparty.LM2, false);

                    LmaxClientOrder clientOrder4 = _clientGateway.Targets.LMAX.CreateClientOrder(cori4);

                    if (!_clientGateway.Targets.LMAX.SubmitOrder(clientOrder4).RequestSucceeded)
                    {
                        this._logger.Log(LogLevel.Error, "Couldn't sumbit order #4");
                    }



                    Thread.Sleep(TimeSpan.FromSeconds(3));




                    Thread.Sleep(TimeSpan.FromSeconds(8));

                    _logger.Log(LogLevel.Info, "Cancelling all orders");
                    _clientGateway.Targets.All.CancelAllMyOrders();
                    Thread.Sleep(TimeSpan.FromSeconds(1));

                    this._logger.Log(LogLevel.Info, "Finishing tests");

                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Fatal, e, "Unexpected e during tests");
                }

            });
        }

        private void LmaxOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            _logger.Log(LogLevel.Info, "Lmax TRADING CLIENT Receiving cpunterparty rejection. {0}", counterpartyRejectionInfo);
        }

        private void LmaxOnClientOrderUpdated(LmaxClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info,
                        "Lmax TRADING CLIENT Receiving order update. {0}, rejected: {1}, cancelled: {2} remaining: {3}",
                        clientOrderUpdateInfo, clientOrderUpdateInfo.SizeBaseAbsRejected,
                        clientOrderUpdateInfo.SizeBaseAbsCancelled, clientOrderUpdateInfo.SizeBaseAbsTotalRemaining);
        }

        private void AllClientsOnClientOrderUpdated(LmaxClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info, "ForwardedInfo: {0}", clientOrderUpdateInfo);
        }

        private void AllClientsOnClientOrderUpdated(BankPoolClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info, "ForwardedInfo (bnk): {0}", clientOrderUpdateInfo);
        }

        private void BankPoolOnClientOrderUpdated(BankPoolClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info, "ClientUpdate info (bnk): {0}", clientOrderUpdateInfo);
        }

        private void LmaxOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "Lmax TRADING CLIENT Receiving NewDeal. {0}", integratorDealInternal);
        }

        private void AllClientsOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "Lmax ForwardedInfo NewDeal. {0}", integratorDealInternal);
        }

        private void TradingInfoOnImportantIntegratorInformationArrived(IImportantIntegratorInformation importantIntegratorInformation)
        {
            _logger.Log(LogLevel.Fatal, "New Important info. {0}", importantIntegratorInformation);
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            this._logger.LogException(LogLevel.Fatal, e, "Unhandled exception");
            return false;
        }
    }
}
