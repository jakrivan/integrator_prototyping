﻿using System;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.SpreadArbitrageConsole
{
    public class ArbitrageCore : IIntegratorClient
    {
        IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            _logger = logger;
            _clientGateway = clientGateway;
            _clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
        }

        private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Trace, "{0}: [MS delay: {3}] Receiving price from {1}, update: {2}", HighResolutionDateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fffffff"), priceUpdateEventArgs.PerSymbolAndSideTopOfBookChangeTimeUtc.ToString("yyyy-MM-dd HH:mm:ss.fffffff"), priceUpdateEventArgs, (HighResolutionDateTime.UtcNow - priceUpdateEventArgs.PerSymbolAndSideTopOfBookChangeTimeUtc).TotalMilliseconds);

        }

        public void Start()
        {
            _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.GBP_JPY));
        }

        public void Stop(string reason)
        {
            _clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.GBP_JPY));
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            throw new Exception();
        }
    }
}