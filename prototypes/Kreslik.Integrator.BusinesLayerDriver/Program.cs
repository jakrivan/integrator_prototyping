﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.BusinessLayer;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.BusinesLayerDriver
{
    public class DummyIntegratorClient: IIntegratorClient
    {
        private IClientGateway _clientGateway;
        private Symbol _symbol;
        private SafeTimer _timer;
        private ILogger _logger;
        private string _identity;
        private TimeSpan _timeout;
        private Random _random = new Random();

        public DummyIntegratorClient(IClientGateway clientGateway, Symbol symbol, ILogger logger, TimeSpan timeout, string identity)
        {
            this.ConfigureInternal(clientGateway, symbol, logger, timeout, identity);
            this.Start();
        }

        public DummyIntegratorClient()
        { }

        private static Symbol defaultSymbol = Symbol.EUR_USD;
        private static string defaultIdentity = "DummyClient";
        private static TimeSpan defaultTimeout = TimeSpan.FromSeconds(5);

        public void Configure(ILogger logger, IClientGateway clientGateway, string configureString)
        {
            this.ConfigureInternal(clientGateway, defaultSymbol, logger, defaultTimeout, defaultIdentity);
        }

        private void ConfigureInternal(IClientGateway clientGateway, Symbol symbol, ILogger logger, TimeSpan timeout, string identity)
        {
            if (clientGateway == null) throw new ArgumentNullException("clientGateway");

            this._clientGateway = clientGateway;
            this._symbol = symbol;
            this._logger = logger;
            this._identity = identity;
            this._timeout = timeout;
            clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
            clientGateway.Targets.BankPool.ClientOrderUpdated += OnClientOrderUpdated;
            clientGateway.Targets.BankPool.NewDealDone += OnNewDealDone;
            clientGateway.TradingInfo.OrderTransmissionStatusChanged += TradingInfo_OrderTransmissionStatusChanged;
        }

        void TradingInfo_OrderTransmissionStatusChanged(OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs)
        {
            this._logger.Log(LogLevel.Warn, "Incoming change of order transmission (enabled: {0}), reason: {1}",
                             orderTransmissionStatusChangedEventArgs.IsOrderTransmissionEnabled,
                             orderTransmissionStatusChangedEventArgs.Reason);
        }

        public void Start()
        {


            _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(_symbol));
            _timer = new SafeTimer(TimeoutCallback, _timeout, _timeout);
        }

        public void Stop(string reason)
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }

        private void TimeoutCallback()
        
        {
            if (_bestAsk != null)
            {
                decimal requestedSize = 700000m + 100000m*_random.Next(6);
                var order1 =
                    _clientGateway.Targets.BankPool.CreateClientOrder(_clientGateway.Targets.BankPool.CreateLimitOrderRequest(requestedSize,
                                                                                        _bestAsk.Price - 0.00003m,
                                                                                        DealDirection.Buy, _symbol, false));
                _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order1);
                lock (_orders)
                {
                    _orders.Add(order1);
                    _ordersFilling.Add(order1.ClientOrderIdentity, requestedSize);
                }
                if (!_clientGateway.Targets.BankPool.SubmitOrder(order1).RequestSucceeded)
                {
                    lock (_orders)
                    {
                        _orders.Remove(order1);
                        _ordersFilling.Remove(order1.ClientOrderIdentity);
                    }
                }
            }

            if (_bestBid != null)
            {
                decimal requestedSize = 700000m + 100000m * _random.Next(6);
                var order2 =
                    _clientGateway.Targets.BankPool.CreateClientOrder(_clientGateway.Targets.BankPool.CreateLimitOrderRequest(requestedSize,
                                                                                        _bestBid.Price + 0.00003m,
                                                                                        DealDirection.Sell, _symbol, false));
                _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order2);
                lock (_orders)
                {
                    _orders.Add(order2);
                    _ordersFilling.Add(order2.ClientOrderIdentity, requestedSize);
                }
                if (!_clientGateway.Targets.BankPool.SubmitOrder(order2).RequestSucceeded)
                {
                    lock (_orders)
                    {
                        _orders.Remove(order2);
                        _ordersFilling.Remove(order2.ClientOrderIdentity);
                    }
                }
            }

            if (_random.Next(20) > 18)
            {
                decimal requestedSize = 700000m + 100000m * _random.Next(6);
                var order =
                    _clientGateway.Targets.BankPool.CreateClientOrder(_clientGateway.Targets.BankPool.CreateMarketOrderRequest(requestedSize,
                                                                                         _random.Next(3) == 2
                                                                                             ? DealDirection.Buy
                                                                                             : DealDirection.Sell,
                                                                                         this._symbol, false));
                _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order);
                lock (_orders)
                {
                    _orders.Add(order);
                    _ordersFilling.Add(order.ClientOrderIdentity, requestedSize);
                }
                if (!_clientGateway.Targets.BankPool.SubmitOrder(order).RequestSucceeded)
                {
                    lock (_orders)
                    {
                        _orders.Remove(order);
                        _ordersFilling.Remove(order.ClientOrderIdentity);
                    }
                }
            }

            if (_orders.Count > 6)
            {
                IClientOrder toRemove1;
                IClientOrder toRemove2;

                lock (_orders)
                {
                    toRemove1 = _orders.First();
                    toRemove2 = _orders[1];
                    _ordersToCancel.Add(toRemove1.ClientOrderIdentity);
                    _ordersToCancel.Add(toRemove2.ClientOrderIdentity);
                }

                _clientGateway.Targets.BankPool.CancelOrder(toRemove1.ClientOrderIdentity);
                _clientGateway.Targets.BankPool.CancelOrder(toRemove2.ClientOrderIdentity);
            }
        }

        private PriceObject _bestAsk;
        private PriceObject _bestBid;
        private List<IClientOrder> _orders = new List<IClientOrder>();
        private List<string> _ordersToCancel = new List<string>();
        private Dictionary<string, decimal> _ordersFilling = new Dictionary<string, decimal>(); 

        private void OnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving NewDeal. {1}", _identity, integratorDealInternal);
            lock (_orders)
            {
                IClientOrder order = _orders.FirstOrDefault(ord => ord.ClientOrderIdentity == integratorDealInternal.ClientOrderIdentity);

                if (order == null)
                {
                    this._logger.Log(LogLevel.Fatal, "Order {0} unkown to current client (order list)", integratorDealInternal.ClientOrderIdentity);
                }
                else if(order.OrderRequestInfo.OrderType == OrderType.Limit)
                {
                    if ((order.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy &&
                         integratorDealInternal.Price > order.OrderRequestInfo.RequestedPrice)
                        ||
                        (order.OrderRequestInfo.IntegratorDealDirection == DealDirection.Sell &&
                         integratorDealInternal.Price < order.OrderRequestInfo.RequestedPrice))
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} filled for worse then requested price: {1}", integratorDealInternal.ClientOrderIdentity, integratorDealInternal.Price);
                    }
                }

                if (order != null)
                {
                    if (order.OrderRequestInfo.Symbol != integratorDealInternal.Symbol)
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} speified symbol {1} and dealupdate {2} mismatched symbol {3}", order.ClientOrderIdentity, order.OrderRequestInfo.Symbol, integratorDealInternal.ClientOrderIdentity, integratorDealInternal.Symbol);
                    }

                    if (order.OrderRequestInfo.IntegratorDealDirection != integratorDealInternal.Direction)
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} speified direction {1} and dealupdate {2} mismatched direction {3}", order.ClientOrderIdentity, order.OrderRequestInfo.IntegratorDealDirection, integratorDealInternal.ClientOrderIdentity, integratorDealInternal.Direction);
                    }
                }

                decimal remainingSize;
                if (!_ordersFilling.TryGetValue(integratorDealInternal.ClientOrderIdentity, out remainingSize))
                {
                    this._logger.Log(LogLevel.Fatal, "Order {0} unkown to current client (order fillings dic)", integratorDealInternal.ClientOrderIdentity);
                    return;
                }

                remainingSize -= integratorDealInternal.FilledAmountBaseAbs;

                if (remainingSize <= 0)
                {
                    if(remainingSize < 0)
                        this._logger.Log(LogLevel.Fatal, "Order {0} remaining size {1} is below zero", integratorDealInternal.ClientOrderIdentity, remainingSize);

                    _orders.RemoveAll(co => co.ClientOrderIdentity.Equals(integratorDealInternal.ClientOrderIdentity));
                    _ordersFilling.Remove(integratorDealInternal.ClientOrderIdentity);
                    _ordersToCancel.Remove(integratorDealInternal.ClientOrderIdentity);
                }
                else
                {
                    _ordersFilling[integratorDealInternal.ClientOrderIdentity] = remainingSize;
                }
            }
        }

        private void OnClientOrderUpdated(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving OrderUpdate: {1}", _identity, clientOrderUpdateInfo);

            lock (_orders)
            {
                if (!_orders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrderUpdateInfo.ClientOrderIdentity)))
                {
                    this._logger.Log(LogLevel.Fatal, "Order {0} was not in internal orders list", clientOrderUpdateInfo.ClientOrderIdentity);
                }
            }

            if (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted || clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed)
            {
                lock (_orders)
                {
                    int removedCount = _ordersToCancel.RemoveAll(co => co.Equals(clientOrderUpdateInfo.ClientOrderIdentity));
                    if (removedCount != 1)
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} was {1} times in internal cancel list, expected 1", clientOrderUpdateInfo.ClientOrderIdentity, removedCount);
                    }

                    if (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted)
                    {
                        removedCount = _orders.RemoveAll(co => co.ClientOrderIdentity.Equals(clientOrderUpdateInfo.ClientOrderIdentity));
                        if (removedCount != 1)
                        {
                            this._logger.Log(LogLevel.Fatal, "Order {0} was {1} times in internal list, expected 1",
                                             clientOrderUpdateInfo.ClientOrderIdentity, removedCount);
                        }

                        if (_ordersFilling.ContainsKey(clientOrderUpdateInfo.ClientOrderIdentity))
                        {
                            if (_ordersFilling[clientOrderUpdateInfo.ClientOrderIdentity] != clientOrderUpdateInfo.SizeBaseAbsCancelled)
                            {
                                this._logger.Log(LogLevel.Fatal,
                                                 "Order [{0}] cancelled, but cancelled amount was {1} instead of expected {2}",
                                                 clientOrderUpdateInfo.ClientOrderIdentity,
                                                 clientOrderUpdateInfo.SizeBaseAbsCancelled,
                                                 _ordersFilling[clientOrderUpdateInfo.ClientOrderIdentity]);
                            }

                            _ordersFilling.Remove(clientOrderUpdateInfo.ClientOrderIdentity);
                        }
                        else
                        {
                            this._logger.Log(LogLevel.Fatal, "Order {0} was not in internal order filling list",
                                             clientOrderUpdateInfo.ClientOrderIdentity);
                        }
                    }
                }
            }
            else if (!(clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.NotActiveInIntegrator || clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator && (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelNotRequested || clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed)))
            {
                this._logger.Log(LogLevel.Fatal, "Order {0} receiving unexpected update {1} {2}", clientOrderUpdateInfo.ClientOrderIdentity,
                                 clientOrderUpdateInfo.OrderStatus, clientOrderUpdateInfo.CancelRequestStatus);
            }
        }

        private PriceObject _lastBid = null;
        private PriceObject _lastAsk = null;

        private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price update: {1}.", _identity, priceUpdateEventArgs);

            if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookDeteriorated)
            {
                this.OnPriceDeteriorated(priceUpdateEventArgs.PriceObject);
            }
            else if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookImproved)
            {
                this.OnPriceImproved(priceUpdateEventArgs.PriceObject);
            }
        }

        private void OnPriceDeteriorated(PriceObject price)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price deterioration. Price: {1}.", _identity, price == null ? "NULL" : price.ToString());
            if (price == null)
            {
                _logger.Log(LogLevel.Fatal, "Receiving price which is null");
                return;
            }

            if (PriceObjectInternal.IsNullPrice(price))
            {
                if (price.Side == PriceSide.Ask)
                {
                    _lastAsk = null;
                }
                else
                {
                    _lastBid = null;
                }
                return;
            }

            if (price.Side == PriceSide.Ask)
            {
                _bestAsk = price;
                if (_lastAsk != null)
                {
                    if(price.Price <= _lastAsk.Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving deterioration but last ASK price is {0} and new one is {1}", _lastAsk.Price, price.Price);
                }
                _lastAsk = price;
            }
            else
            {
                _bestBid = price;
                if (_lastBid != null)
                {
                    if (price.Price >= _lastBid.Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving deterioration but last BID price is {0} and new one is {1}", _lastBid.Price, price.Price);
                }
                _lastBid = price;
            }

            //if (price.ParentQuote == null)
            //{
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null");
            //    this._logger.Log(LogLevel.Fatal, "Parent quote nuul, price {0}", price);
            //}
        }

        private void OnPriceImproved(PriceObject price)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price improvement. Price: {1}.", _identity, price);
            if (price.Side == PriceSide.Ask)
            {
                _bestAsk = price;
                if (_lastAsk != null)
                {
                    if (price.Price >= _lastAsk.Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving improvement but last ASK price is {0} and new one is {1}", _lastAsk.Price, price.Price);
                }
                _lastAsk = price;
            }
            else
            {
                _bestBid = price;
                if (_lastBid != null)
                {
                    if (price.Price <= _lastBid.Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving improvement but last BID price is {0} and new one is {1}", _lastBid.Price, price.Price);
                }
                _lastBid = price;
            }

            //if (price.ParentQuote == null)
            //{
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null");
            //    this._logger.Log(LogLevel.Fatal, "Parent quote nuul, price {0}", price);
            //}
        }


        public bool TryHandleUnhandledException(Exception e)
        {
            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            RandomMiniTests rm = new RandomMiniTests();
            rm.RoundingTest();
            return;
        }
    }


    public class NullGUIClient: IIntegratorClient
    {
        private BasicForm _form;
        private IClientGateway _clientGateway;

        private delegate void UpdatePriceDelegate(PricesTopOfBookUpdateEventArgs pargs);

        public void Configure(ILogger logger, IClientGateway clientGateway, string configureString)
        {
            _form = new BasicForm();
            _form.Show();
            _form.richTextBox1.Text += string.Format("Started with arg: {0}{1}", configureString, Environment.NewLine);

            _clientGateway = clientGateway;
            clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable +=
                o => _form.Invoke(new UpdatePriceDelegate(UpdatePrice), o);
        }

        private void UpdatePrice(PricesTopOfBookUpdateEventArgs pargs)
        {
            _form.richTextBox1.Text += string.Format("Price update: {0}{1}", pargs.ToString(), Environment.NewLine);
        }

        public void Start()
        {
            _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_USD));
        }

        public void Stop(string reason)
        {
            _form.Close();
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            return false;
        }
    }


    public class SubscriptionTestClient: IIntegratorClient
    {
        private IClientGateway _clientGateway;
        private ILogger _logger;
        private SafeTimer _subscriptionTimer;
        private bool _subscribed = false;
        private DateTime _subscriptionDateTime;


        public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            this._clientGateway = clientGateway;
            this._logger = logger;

            clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;
        }

        public void Start()
        {
            _subscriptionTimer = new SafeTimer(SubscriptionTimeoutCallback, TimeSpan.Zero, TimeSpan.FromSeconds(30));
        }

        public void Stop(string reason)
        {
            if (_subscriptionTimer != null)
            {
                _subscriptionTimer.Dispose();
            }
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            return false;
        }

        private void SubscriptionTimeoutCallback()
        {
            if (_subscribed)
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    _clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(symbol));
                }
                _subscribed = false;
            }
            else
            {
                foreach (Symbol symbol in Symbol.Values)
                {
                    _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(symbol));
                }
                _subscribed = true;
            }
            
        }

        private void ProcessSubscriptionTime(PriceObject price, DateTime subscriptionTime)
        {
            if (subscriptionTime != DateTime.MinValue)
            {
                DateTime now = DateTime.UtcNow;
                _logger.Log(LogLevel.Info, "Subscription for {0} took {1} ({2} - {3})", price.Symbol, now - subscriptionTime, now, subscriptionTime);
                if (now - subscriptionTime > TimeSpan.FromSeconds(1))
                {
                    _logger.Log(LogLevel.Fatal, "Subscription to {0} took {1} ({2} - {3})", price.Symbol, now - subscriptionTime, now, subscriptionTime);
                }
            }
            else
            {
                _logger.Log(LogLevel.Fatal, "Receiving price {0}, previous price is unavailable but so is the subscription time", price);
            }
        }

        private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            _logger.Log(LogLevel.Info, "Receiving Price update: {0}.", priceUpdateEventArgs);

            if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookDeteriorated)
            {
                this.OnPriceDeteriorated(priceUpdateEventArgs.PriceObject);
            }
            else if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookImproved)
            {
                this.OnPriceImproved(priceUpdateEventArgs.PriceObject);
            }
        }

        private void OnPriceDeteriorated(PriceObject price)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT Receiving Price deterioration. Price: {0}.", price == null ? "NULL" : price.ToString());
            if (price == null)
            {
                _logger.Log(LogLevel.Fatal, "Receiving price which is null");
                return;
            }

            if (!PriceObjectInternal.IsNullPrice(price))
            {
                double milisecondsDelay = Math.Abs((DateTime.UtcNow - price.IntegratorReceivedTimeUtc).TotalMilliseconds);

                _logger.Log(LogLevel.Info, "Price milliseconds delay: {0}.  Price {1}", milisecondsDelay, price);

                if (milisecondsDelay > 10000)
                {
                    _logger.Log(LogLevel.Fatal, "Price milliseconds delay: {0}.  Price {1}", milisecondsDelay, price);
                }
            }

            //if (price.ParentQuote == null && !PriceObject.IsNullPrice(price))
            //{
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null");
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null, price {0}", price);
            //}
        }

        private void OnPriceImproved(PriceObject price)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT Receiving Price improved. Price: {0}.", price == null ? "NULL" : price.ToString());
            if (price == null)
            {
                _logger.Log(LogLevel.Fatal, "Receiving price which is null");
                return;
            }

            if (!PriceObjectInternal.IsNullPrice(price))
            {
                double milisecondsDelay = Math.Abs((DateTime.UtcNow - price.IntegratorReceivedTimeUtc).TotalMilliseconds);

                _logger.Log(LogLevel.Info, "Price milliseconds delay: {0}.  Price {1}", milisecondsDelay, price);

                if (milisecondsDelay > 10000)
                {
                    _logger.Log(LogLevel.Fatal, "Price milliseconds delay: {0}.  Price {1}", milisecondsDelay, price);
                }
            }

            //if (price.ParentQuote == null && !PriceObject.IsNullPrice(price))
            //{
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null");
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null, price {0}", price);
            //}
        }
    }


    public class BasicEventsObservingClient: IIntegratorClient
    {
        private IClientGateway _clientGateway;
        private ILogger _logger;

        public void Configure(ILogger logger, IClientGateway clientGateway, string configurationString)
        {
            this._clientGateway = clientGateway;
            this._logger = logger;

            clientGateway.ConnectionInfo.GatewayClosed += reason =>
            {
                logger.Log(LogLevel.Error, "Gateway firing GatewayClosed evnet. Reason: " + reason);
            };

            clientGateway.ConnectionInfo.ConnectionStateChanged += args =>
            {
                logger.Log(LogLevel.Warn, "Gateway firing ConnectionStateChanged evnet: " + args);
            };
        }

        private void HotspotOrdersTest(decimal midPrice)
        {
            var result = _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_CAD,
                                                                                      HotspotCounterparty.HTA));

            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Fatal, "Subscription failed: {0}", result.ErrorMessage);
            }

            result = _clientGateway.Targets.Hotspot.PricesTopOfBook.SubscribeToUpdates(
                _clientGateway.Targets.Hotspot.PricesTopOfBook.GetSubscriptionRequest(Symbol.AUD_USD,
                                                                                      HotspotCounterparty.HTF));

            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Fatal, "Subscription failed: {0}", result.ErrorMessage);
            }

            result = _clientGateway.Targets.FXall.PricesTopOfBook.SubscribeToUpdates(
                _clientGateway.Targets.FXall.PricesTopOfBook.GetSubscriptionRequest(Symbol.AUD_USD,
                                                                                      FXallCounterparty.FA1));

            if (!result.RequestSucceeded)
            {
                this._logger.Log(LogLevel.Fatal, "Subscription failed: {0}", result.ErrorMessage);
            }


            HotspotClientOrderRequestInfo cori1 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(
                1000000m, 0m, midPrice, DealDirection.Sell, Symbol.EUR_USD, TimeInForce.Day, HotspotCounterparty.HTA, false);

            //cori1.SetRejectableDefaults(new RejectableDealDefaults(TimeSpan.FromMilliseconds(50), DealConfirmationAction.Reject));

            HotspotClientOrder clientOrder1 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori1);

            if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder1).RequestSucceeded)
            {
                this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
            }


            HotspotClientOrderRequestInfo cori2 = _clientGateway.Targets.Hotspot.CreateLimitOrderRequest(
                1000000m, 0m, midPrice, DealDirection.Buy, Symbol.EUR_USD, TimeInForce.Day, HotspotCounterparty.HTA, false);

            //cori2.SetRejectableDefaults(new RejectableDealDefaults(TimeSpan.FromMilliseconds(50), DealConfirmationAction.Reject));

            HotspotClientOrder clientOrder2 = _clientGateway.Targets.Hotspot.CreateClientOrder(cori2);

            if (!_clientGateway.Targets.Hotspot.SubmitOrder(clientOrder2).RequestSucceeded)
            {
                this._logger.Log(LogLevel.Error, "Couldn't sumbit order #1");
            }
        }

        public void Start()
        {
            //
            _clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += PricesTopOfBookOnNewUpdateAvailable;
            _clientGateway.Targets.Hotspot.PricesTopOfBook.NewUpdateAvailable += HotspotPricesTopOfBookOnNewUpdateAvailable;
            _clientGateway.Targets.FXall.PricesTopOfBook.NewUpdateAvailable += FxAllPricesTopOfBookOnNewUpdateAvailable;
            _clientGateway.Targets.Hotspot.ClientOrderUpdated += HotspotOnClientOrderUpdated;
            _clientGateway.Targets.Hotspot.CounterpartyRejectedIntegratorOrder += HotspotOnCounterpartyRejectedIntegratorOrder;
            _clientGateway.Targets.Hotspot.NewUnconfirmedDealDone += HotspotOnNewUnconfirmedDealDone;
            _clientGateway.Targets.Hotspot.NewDealDone += HotspotOnNewDealDone;
            _clientGateway.Targets.Hotspot.AllClients.ClientOrderUpdated += HotspotOnClientOrderUpdatedForwarded;
            _clientGateway.Targets.Hotspot.AllClients.CounterpartyRejectedIntegratorOrder += HotspotOnCounterpartyRejectedIntegratorOrderForwarded;
            _clientGateway.Targets.Hotspot.AllClients.NewUnconfirmedDealDone += HotspotOnNewUnconfirmedDealDoneForwarded;
            _clientGateway.Targets.Hotspot.AllClients.NewDealDone += HotspotOnNewDealDoneForwarded;

            //_clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.USD_CZK));
            //_clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.EUR_CAD));
            //_clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest(Symbol.AUD_CHF));


            bool forwardingConsumer = false;

            if (forwardingConsumer)
            {
                var result = _clientGateway.Targets.Hotspot.AllClients.SubscribeToUpdates(
                    new ClientForwardingRequest(Symbol.EUR_USD,
                                                ClientForwardingRequestType.OrderUpdates |
                                                ClientForwardingRequestType.CounterpartyRejections));

                if (!result.RequestSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, "Subscription to forwarding failed: {0}", result.ErrorMessage);
                }
            }
            else
            {
                Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            decimal midprice = 1.3838m;
                            HotspotOrdersTest(midprice);

                            //_clientGateway.Targets.All.CancelAllMyOrders();

                            //Thread.Sleep(5000);

                            //_clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest(Symbol.EUR_CAD));
                        }
                        catch (Exception e)
                        {
                            this._logger.LogException(LogLevel.Fatal, "Unexpected during client tests", e);
                        }
                    });
            }

        }

        private void HotspotPricesTopOfBookOnNewUpdateAvailable(PricesTopOfBookUpdateEventArgs pricesTopOfBookUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Trace, "Hotspot: {0}", pricesTopOfBookUpdateEventArgs.ToString());
        }

        private void FxAllPricesTopOfBookOnNewUpdateAvailable(PricesTopOfBookUpdateEventArgs pricesTopOfBookUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Trace, "Fxall: {0}", pricesTopOfBookUpdateEventArgs.ToString());
        }

        private void HotspotOnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            this._logger.Log(LogLevel.Info, "Hotspot Deal: {0}", integratorDealInternal);
        }

        private void HotspotOnNewUnconfirmedDealDone(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
        {
            this._logger.Log(LogLevel.Info, "Hotspot unconfirmed deal: {0}", integratorUnconfirmedDealInternal);

            //this._clientGateway.Targets.Hotspot.FinalizeUnconfirmedDeal(
            //    new Random(Environment.TickCount).NextDouble() > 0.5
            //        ? DealConfirmationAction.Reject
            //        : DealConfirmationAction.Confirm, integratorUnconfirmedDealInternal);
        }

        private void HotspotOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            this._logger.Log(LogLevel.Info, "Hotspot counterparty reject: {0}", counterpartyRejectionInfo);
        }

        private void HotspotOnClientOrderUpdated(HotspotClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            this._logger.Log(LogLevel.Info, "Hotspot order update: {0}", clientOrderUpdateInfo);
        }

        private void HotspotOnNewDealDoneForwarded(IntegratorDealInternal integratorDealInternal)
        {
            this._logger.Log(LogLevel.Info, "FORWARDED Hotspot Deal: {0}", integratorDealInternal);
        }

        private void HotspotOnNewUnconfirmedDealDoneForwarded(IntegratorUnconfirmedDealInternal integratorUnconfirmedDealInternal)
        {
            this._logger.Log(LogLevel.Info, "FORWARDED Hotspot unconfirmed deal: {0}", integratorUnconfirmedDealInternal);
        }

        private void HotspotOnCounterpartyRejectedIntegratorOrderForwarded(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            this._logger.Log(LogLevel.Info, "FORWARDED Hotspot counterparty reject: {0}", counterpartyRejectionInfo);
        }

        private void HotspotOnClientOrderUpdatedForwarded(HotspotClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            this._logger.Log(LogLevel.Info, "FORWARDED Hotspot order update: {0}", clientOrderUpdateInfo);
        }

        private void PricesTopOfBookOnNewUpdateAvailable(PricesTopOfBookUpdateEventArgs pricesTopOfBookUpdateEventArgs)
        {
            this._logger.Log(LogLevel.Trace, pricesTopOfBookUpdateEventArgs.ToString());
        }

        public void Stop(string reason)
        {
            //
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            return false;
        }
    }

    public class DummyIntegratorClient2 : IIntegratorClient
    {
        private IClientGateway _clientGateway;
        private SafeTimer _tradingTimer;
        private SafeTimer _subscriptionTimer;
        private ILogger _logger;
        private string _identity;
        private TimeSpan _timeout;
        private Random _random = new Random();

        private bool[] _subscriptions = new bool[Symbol.ValuesCount];
        private PriceObject[] _bids = new PriceObject[Symbol.ValuesCount];
        private PriceObject[] _asks = new PriceObject[Symbol.ValuesCount];
        private DateTime[] _askSubscriptionTimeStamps = Enumerable.Repeat(DateTime.MinValue, Symbol.ValuesCount).ToArray();
        private DateTime[] _bidSubscriptionTimeStamps = Enumerable.Repeat(DateTime.MinValue, Symbol.ValuesCount).ToArray();

        public DummyIntegratorClient2(IClientGateway clientGateway, ILogger logger, TimeSpan timeout, string identity)
        {
            decimal? foo = clientGateway.TradingInfo.ConvertToUsd(Currency.EUR, 1000m, false);
            logger.Log(foo.HasValue ? LogLevel.Info : LogLevel.Error, "Converted EURs: {0}", foo);
            logger.Log(LogLevel.Info, "Connection chain versioning: {0}", clientGateway.ConnectionInfo.ConnectionChainBuildVersioningInfo);

            if (
                clientGateway.ConnectionInfo.ConnectionChainBuildVersioningInfo.HostingGatewayVersionInfo
                             .MinimumVersionNumberOfCompatibleBuild > BuildConstants.CURRENT_BUILD_VERSION)
            {
                this._logger.Log(LogLevel.Fatal, "Current client builded against unsupported version: {0}",
                                 BuildConstants.CURRENT_BUILD_VERSION);
            }


            this.ConfigureInternal(clientGateway, logger, timeout, identity);
            this.Start();
        }

        public DummyIntegratorClient2()
        { }

        private static string defaultIdentity = "DummyClient";
        private static TimeSpan defaultTimeout = TimeSpan.FromSeconds(30);


        private Form _form;

        public void Configure(ILogger logger, IClientGateway clientGateway, string configureString)
        {
            decimal? foo = clientGateway.TradingInfo.ConvertToUsd(Currency.EUR, 1000m, false);
            logger.Log(foo.HasValue ? LogLevel.Info : LogLevel.Error, "Converted EURs: {0}", foo);
            logger.Log(LogLevel.Info, "Connection chain versioning: {0}", clientGateway.ConnectionInfo.ConnectionChainBuildVersioningInfo);

            if (
                clientGateway.ConnectionInfo.ConnectionChainBuildVersioningInfo.HostingGatewayVersionInfo
                             .MinimumVersionNumberOfCompatibleBuild > BuildConstants.CURRENT_BUILD_VERSION)
            {
                this._logger.Log(LogLevel.Fatal, "Current client builded against unsupported version: {0}",
                                 BuildConstants.CURRENT_BUILD_VERSION);
            }

            this.ConfigureInternal(clientGateway, logger, defaultTimeout, defaultIdentity);

            _form = new BasicForm();
            _form.Show();
        }

        private void ConfigureInternal(IClientGateway clientGateway, ILogger logger, TimeSpan timeout, string identity)
        {
            if (clientGateway == null) throw new ArgumentNullException("clientGateway");

            this._clientGateway = clientGateway;
            this._logger = logger;
            this._identity = identity;
            this._timeout = timeout;
            clientGateway.Targets.BankPool.PricesTopOfBook.NewUpdateAvailable += OnPriceUpdate;

            //clientGateway.Targets.Hotspot.PriceOnTopOfBookImproved += HotspotOnPriceOnTopOfBookImproved;
            //clientGateway.Targets.Hotspot.PriceOnTopOfBookDeteriorated += HotspotOnPriceOnTopOfBookDeteriorated;

            //clientGateway.Targets.Hotspot.SubscribeToPriceUpdates(new IntegratorSubscriptionRequestInfo(Symbol.AUD_CAD,
            //                                                                                            HotspotCounterparty
            //                                                                                                .HTA));

            //clientGateway.Targets.BankPool.NewPriceArrived += OnNewPriceArrived;
            //clientGateway.Targets.BankPool.PricesRemoved += OnPricesRemoved;
            //clientGateway.Targets.BankPool.ExistingPriceInvalidated += OnExistingPriceInvalidated;
            clientGateway.Targets.BankPool.ClientOrderUpdated += OnClientOrderUpdated;
            clientGateway.Targets.BankPool.CounterpartyRejectedIntegratorOrder += ClientGatewayOnCounterpartyRejectedIntegratorOrder;
            clientGateway.Targets.BankPool.CounterpartyIgnoredIntegratorOrder += ClientGatewayOnCounterpartyIgnoredIntegratorOrder;
            clientGateway.Targets.BankPool.NewDealDone += OnNewDealDone;
            clientGateway.TradingInfo.OrderTransmissionStatusChanged +=TradingInfo_OrderTransmissionStatusChanged;

            clientGateway.ConnectionInfo.GatewayClosed += reason =>
                {
                    logger.Log(LogLevel.Error, "Gateway firing GatewayClosed evnet. Reason: " + reason);
                };

            clientGateway.ConnectionInfo.ConnectionStateChanged += args =>
                {
                    logger.Log(LogLevel.Warn, "Gateway firing ConnectionStateChanged evnet: " + args);
                };
        }

        void TradingInfo_OrderTransmissionStatusChanged(OrderTransmissionStatusChangedEventArgs orderTransmissionStatusChangedEventArgs)
        {
            this._logger.Log(LogLevel.Warn, "Incoming change of order transmission (enabled: {0}), reason: {1}",
                             orderTransmissionStatusChangedEventArgs.IsOrderTransmissionEnabled,
                             orderTransmissionStatusChangedEventArgs.Reason);
        }

        private void ClientGatewayOnCounterpartyIgnoredIntegratorOrder(
            CounterpartyOrderIgnoringInfo counterpartyOrderIgnoringInfo)
        {
            this._logger.Log(LogLevel.Info, "Receiving counterparty order ignoring info {0}",
                             counterpartyOrderIgnoringInfo);

            decimal remainingSize;
            if (!_ordersFilling.TryGetValue(counterpartyOrderIgnoringInfo.ClientOrderIdentity, out remainingSize))
            {
                this._logger.Log(LogLevel.Fatal, "Order {0} unkown to current client (order fillings dic)",
                                 counterpartyOrderIgnoringInfo.ClientOrderIdentity);
                return;
            }

            remainingSize -= counterpartyOrderIgnoringInfo.IgnoredAmountBaseAbs;

            if (remainingSize <= 0)
            {
                if (remainingSize < 0)
                    this._logger.Log(LogLevel.Fatal, "Order {0} remaining size {1} is below zero",
                                     counterpartyOrderIgnoringInfo.ClientOrderIdentity, remainingSize);

                _orders.RemoveAll(co => co.ClientOrderIdentity.Equals(counterpartyOrderIgnoringInfo.ClientOrderIdentity));
                _ordersFilling.Remove(counterpartyOrderIgnoringInfo.ClientOrderIdentity);
                _ordersToCancel.Remove(counterpartyOrderIgnoringInfo.ClientOrderIdentity);
            }
            else
            {
                _ordersFilling[counterpartyOrderIgnoringInfo.ClientOrderIdentity] = remainingSize;
            }
        }

        private void ClientGatewayOnCounterpartyRejectedIntegratorOrder(CounterpartyRejectionInfo counterpartyRejectionInfo)
        {
            this._logger.Log(LogLevel.Info, "Receiving counterparty rejection info {0}", counterpartyRejectionInfo);
        }

        private SafeTimer _marketOrdersTimer;

        public void Start()
        {
            _tradingTimer = new SafeTimer(TimeoutCallback, _timeout, _timeout);
            _subscriptionTimer = new SafeTimer(SubscriptionTimeoutCallback, TimeSpan.Zero, TimeSpan.FromSeconds(5));
            //_marketOrdersTimer = new SafeTimer(MarketOrdersTimeoutCallback, null, TimeSpan.FromSeconds(20), Timeout.InfiniteTimeSpan);

            decimal? foo = _clientGateway.TradingInfo.ConvertToUsd(Currency.EUR, 1000m, false);
            _logger.Log(LogLevel.Error, "Coverted EURs: {0}", foo);
        }

        public void Stop(string reason)
        {
            if (_tradingTimer != null)
            {
                _tradingTimer.Dispose();
            }
        }

        //private void MarketOrdersTimeoutCallback(object nullState)
        //{

        //    Stopwatch swOverall = new Stopwatch();
        //    Stopwatch sw = new Stopwatch();

        //    swOverall.Start();
        //    for (int i = 0; i < 10; i++)
        //    {
        //        var order =
        //            _clientGateway.CreateClientOrder(ClientOrderRequestInfo.CreateMarket(500000m,
        //                                                                                 _random.Next(3) == 2
        //                                                                                     ? DealDirection.Buy
        //                                                                                     : DealDirection.Sell,
        //                                                                                 Symbol.EUR_USD));
        //        _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order);

        //        sw.Reset();
        //        sw.Start();

        //        if (!_clientGateway.SubmitOrder(order))
        //        {
        //            _logger.Log(LogLevel.Error, "Failed to submit order {0}", order.Identity);
        //        }

        //        sw.Stop();
        //        _logger.Log(LogLevel.Error, "{0} order submission. Duration: {1}", i, sw.Elapsed);
        //    }
        //    swOverall.Stop();
        //    _logger.Log(LogLevel.Fatal, "Overal test duration {0}", swOverall.Elapsed);

            
            
        //}

        private void SubscriptionTimeoutCallback()
        {
            DoSubscriptionAction();
            DoSubscriptionAction();
        }

        private void DoSubscriptionAction()
        {
            int idx = _random.Next(Symbol.ValuesCount);
            if (_subscriptions[idx])
            {
                _clientGateway.Targets.BankPool.PricesTopOfBook.UnsubscribeFromUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetUnsubscriptionRequest((Symbol)idx));
            }
            else
            {
                _asks[idx] = null;
                _bids[idx] = null;
                _askSubscriptionTimeStamps[idx] = DateTime.UtcNow;
                _bidSubscriptionTimeStamps[idx] = DateTime.UtcNow;
                _clientGateway.Targets.BankPool.PricesTopOfBook.SubscribeToUpdates(_clientGateway.Targets.BankPool.PricesTopOfBook.GetSubscriptionRequest((Symbol)idx));
            }
            _subscriptions[idx] = !_subscriptions[idx];
        }

        private void TimeoutCallback()
        {
            decimal? foo = _clientGateway.TradingInfo.ConvertToUsd(Currency.EUR, 1000m, false);
            _logger.Log(foo.HasValue ? LogLevel.Info : LogLevel.Error, "Converted EURs: {0}", foo);

            int idx = _random.Next(Symbol.ValuesCount);
            PriceObject askPrice = _asks[idx];
            if (askPrice == null || PriceObjectInternal.IsNullPrice(askPrice))
            {
                idx = _random.Next(Symbol.ValuesCount);
                askPrice = _asks[idx];
            }
            if (askPrice == null || PriceObjectInternal.IsNullPrice(askPrice))
            {
                idx = _random.Next(Symbol.ValuesCount);
                askPrice = _asks[idx];
            }

            if (askPrice != null && !PriceObjectInternal.IsNullPrice(askPrice))
            {
                decimal requestedSize = 700000m + 100000m * _random.Next(6);
                decimal requestedPrice = askPrice.Price*0.99997m;
                var priceRoundResult = _clientGateway.TradingInfo.Symbols.
                    TryRoundToSupportedPriceIncrement(askPrice.Symbol, TradingTargetType.BankPool, RoundingKind.ToNearest, requestedPrice);
                if (!priceRoundResult.RoundingSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, "Couldn't round requested price: {0}", priceRoundResult.ErrorMessage);
                }

                var sizeRoundResult = _clientGateway.TradingInfo.Symbols.
                    TryRoundToSupportedSizeIncrement(askPrice.Symbol, TradingTargetType.BankPool, RoundingKind.ToNearest, requestedSize);
                if (!sizeRoundResult.RoundingSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, "Couldn't round requested size: {0}", sizeRoundResult.ErrorMessage);
                }

                var order1 =
                    _clientGateway.Targets.BankPool.CreateClientOrder(_clientGateway.Targets.BankPool.CreateLimitOrderRequest(sizeRoundResult.RoundedValue,
                                                                                        priceRoundResult.RoundedValue,
                                                                                        DealDirection.Buy, askPrice.Symbol, false));
                _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order1);
                lock (_orders)
                {
                    _orders.Add(order1);
                    _ordersFilling.Add(order1.ClientOrderIdentity, requestedSize);

                    if (!_clientGateway.Targets.BankPool.SubmitOrder(order1).RequestSucceeded)
                    {
                        _orders.Remove(order1);
                        _ordersFilling.Remove(order1.ClientOrderIdentity);
                    }
                }
            }


            PriceObject bidPrice = _bids[idx];
            if (bidPrice == null || PriceObjectInternal.IsNullPrice(bidPrice))
            {
                idx = _random.Next(Symbol.ValuesCount);
                bidPrice = _bids[idx];
            }
            if (bidPrice == null || PriceObjectInternal.IsNullPrice(bidPrice))
            {
                idx = _random.Next(Symbol.ValuesCount);
                bidPrice = _bids[idx];
            }


            if (bidPrice != null && !PriceObjectInternal.IsNullPrice(bidPrice))
            {
                decimal requestedSize = 700000m + 100000m * _random.Next(6);
                decimal requestedPrice = bidPrice.Price * 1.00003m;
                var priceRoundResult = _clientGateway.TradingInfo.Symbols.
                    TryRoundToSupportedPriceIncrement(bidPrice.Symbol, TradingTargetType.BankPool, 
                    RoundingKind.ToNearest, requestedPrice);
                if (!priceRoundResult.RoundingSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, "Couldn't round requested price: {0}", priceRoundResult.ErrorMessage);
                }

                var sizeRoundResult = _clientGateway.TradingInfo.Symbols.
                    TryRoundToSupportedPriceIncrement(bidPrice.Symbol, TradingTargetType.BankPool,
                    RoundingKind.ToNearest, requestedSize);
                if (!sizeRoundResult.RoundingSucceeded)
                {
                    this._logger.Log(LogLevel.Fatal, "Couldn't round requested price: {0}", sizeRoundResult.ErrorMessage);
                }

                var order2 =
                    _clientGateway.Targets.BankPool.CreateClientOrder(_clientGateway.Targets.BankPool.CreateLimitOrderRequest(sizeRoundResult.RoundedValue,
                                                                                        priceRoundResult.RoundedValue,
                                                                                        DealDirection.Sell, bidPrice.Symbol, false));
                _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order2);
                lock (_orders)
                {
                    _orders.Add(order2);
                    _ordersFilling.Add(order2.ClientOrderIdentity, requestedSize);
                }
                if (!_clientGateway.Targets.BankPool.SubmitOrder(order2).RequestSucceeded)
                {
                    lock (_orders)
                    {
                        _orders.Remove(order2);
                        _ordersFilling.Remove(order2.ClientOrderIdentity);
                    }
                }
            }

            if (_random.Next(20) > 18)
            {
                decimal requestedSize = 700000m + 100000m * _random.Next(6);
                var order =
                    _clientGateway.Targets.BankPool.CreateClientOrder(_clientGateway.Targets.BankPool.CreateMarketOrderRequest(requestedSize,
                                                                                         _random.Next(3) == 2
                                                                                             ? DealDirection.Buy
                                                                                             : DealDirection.Sell,
                                                                                         (Symbol)idx, false));
                _logger.Log(LogLevel.Info, "Trading client creating order: {0}", order);
                lock (_orders)
                {
                    _orders.Add(order);
                    _ordersFilling.Add(order.ClientOrderIdentity, requestedSize);
                }
                if (!_clientGateway.Targets.BankPool.SubmitOrder(order).RequestSucceeded)
                {
                    lock (_orders)
                    {
                        _orders.Remove(order);
                        _ordersFilling.Remove(order.ClientOrderIdentity);
                    }
                }
            }

            if (_orders.Count > 6)
            {
                IClientOrder toRemove1;
                IClientOrder toRemove2;

                lock (_orders)
                {
                    toRemove1 = _orders.First();
                    toRemove2 = _orders[1];
                    _ordersToCancel.Add(toRemove1.ClientOrderIdentity);
                    _ordersToCancel.Add(toRemove2.ClientOrderIdentity);
                }

                _clientGateway.Targets.BankPool.CancelOrder(toRemove1.ClientOrderIdentity);
                _clientGateway.Targets.BankPool.CancelOrder(toRemove2.ClientOrderIdentity);
            }
        }

        //private PriceObject _bestAsk;
        //private PriceObject _bestBid;
        private List<IClientOrder> _orders = new List<IClientOrder>();
        private List<string> _ordersToCancel = new List<string>();
        private Dictionary<string, decimal> _ordersFilling = new Dictionary<string, decimal>();

        private void OnNewDealDone(IntegratorDealInternal integratorDealInternal)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving NewDeal. {1}", _identity, integratorDealInternal);
            lock (_orders)
            {
                IClientOrder order = _orders.FirstOrDefault(ord => ord.ClientOrderIdentity == integratorDealInternal.ClientOrderIdentity);

                if (order == null)
                {
                    this._logger.Log(LogLevel.Fatal, "Order {0} unkown to current client (order list)", integratorDealInternal.ClientOrderIdentity);
                }
                else if (order.OrderRequestInfo.OrderType == OrderType.Limit)
                {
                    if ((order.OrderRequestInfo.IntegratorDealDirection == DealDirection.Buy &&
                         integratorDealInternal.Price > order.OrderRequestInfo.RequestedPrice)
                        ||
                        (order.OrderRequestInfo.IntegratorDealDirection == DealDirection.Sell &&
                         integratorDealInternal.Price < order.OrderRequestInfo.RequestedPrice))
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} filled for worse then requested price: {1}", integratorDealInternal.ClientOrderIdentity, integratorDealInternal.Price);
                    }
                }

                if (order != null)
                {
                    if (order.OrderRequestInfo.Symbol != integratorDealInternal.Symbol)
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} speified symbol {1} and dealupdate {2} mismatched symbol {3}", order.ClientOrderIdentity, order.OrderRequestInfo.Symbol, integratorDealInternal.ClientOrderIdentity, integratorDealInternal.Symbol);
                    }

                    if (order.OrderRequestInfo.IntegratorDealDirection != integratorDealInternal.Direction)
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} speified direction {1} and dealupdate {2} mismatched direction {3}", order.ClientOrderIdentity, order.OrderRequestInfo.IntegratorDealDirection, integratorDealInternal.ClientOrderIdentity, integratorDealInternal.Direction);
                    }
                }

                decimal remainingSize;
                if (!_ordersFilling.TryGetValue(integratorDealInternal.ClientOrderIdentity, out remainingSize))
                {
                    this._logger.Log(LogLevel.Fatal, "Order {0} unkown to current client (order fillings dic)", integratorDealInternal.ClientOrderIdentity);
                    return;
                }

                remainingSize -= integratorDealInternal.FilledAmountBaseAbs;

                if (remainingSize <= 0)
                {
                    if (remainingSize < 0)
                        this._logger.Log(LogLevel.Fatal, "Order {0} remaining size {1} is below zero", integratorDealInternal.ClientOrderIdentity, remainingSize);

                    _orders.RemoveAll(co => co.ClientOrderIdentity.Equals(integratorDealInternal.ClientOrderIdentity));
                    _ordersFilling.Remove(integratorDealInternal.ClientOrderIdentity);
                    _ordersToCancel.Remove(integratorDealInternal.ClientOrderIdentity);
                }
                else
                {
                    _ordersFilling[integratorDealInternal.ClientOrderIdentity] = remainingSize;
                }     
            }
        }

        private void OnClientOrderUpdated(ClientOrderUpdateInfo clientOrderUpdateInfo)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving OrderUpdate: {1}", _identity, clientOrderUpdateInfo);

            if (clientOrderUpdateInfo.ClientOrder == null)
            {
                _logger.Log(LogLevel.Fatal, "ClientOrder is null in ClientOrderUpdateInfo: {0}", clientOrderUpdateInfo);
            }

            lock (_orders)
            {
                if (!_orders.Any(ord => ord.ClientOrderIdentity.Equals(clientOrderUpdateInfo.ClientOrderIdentity)))
                {
                    this._logger.Log(LogLevel.Fatal, "Order {0} was not in internal orders list", clientOrderUpdateInfo.ClientOrderIdentity);
                }
            }

            if (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted || clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed)
            {
                lock (_orders)
                {
                    int removedCount = _ordersToCancel.RemoveAll(co => co.Equals(clientOrderUpdateInfo.ClientOrderIdentity));
                    if (removedCount != 1)
                    {
                        this._logger.Log(LogLevel.Fatal, "Order {0} was {1} times in internal cancel list, expected 1", clientOrderUpdateInfo.ClientOrderIdentity, removedCount);
                    }

                    if (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestAccepted)
                    {
                        removedCount = _orders.RemoveAll(co => co.ClientOrderIdentity.Equals(clientOrderUpdateInfo.ClientOrderIdentity));
                        if (removedCount != 1)
                        {
                            this._logger.Log(LogLevel.Fatal, "Order {0} was {1} times in internal list, expected 1",
                                             clientOrderUpdateInfo.ClientOrderIdentity, removedCount);
                        }

                        if (_ordersFilling.ContainsKey(clientOrderUpdateInfo.ClientOrderIdentity))
                        {
                            if (_ordersFilling[clientOrderUpdateInfo.ClientOrderIdentity] != clientOrderUpdateInfo.SizeBaseAbsCancelled)
                            {
                                this._logger.Log(LogLevel.Fatal, "Order [{0}] cancelled, but cancelled amount was {1} instead of expected {2}", clientOrderUpdateInfo.ClientOrderIdentity, clientOrderUpdateInfo.SizeBaseAbsCancelled, _ordersFilling[clientOrderUpdateInfo.ClientOrderIdentity]);
                            }

                            _ordersFilling.Remove(clientOrderUpdateInfo.ClientOrderIdentity);
                        }
                        else
                        {
                            this._logger.Log(LogLevel.Fatal, "Order {0} was not in internal order filling list",
                                             clientOrderUpdateInfo.ClientOrderIdentity);
                        }
                    }
                }
            }
            else if (!(clientOrderUpdateInfo.OrderStatus == ClientOrderStatus.OpenInIntegrator && (clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelNotRequested || clientOrderUpdateInfo.CancelRequestStatus == ClientOrderCancelRequestStatus.CancelRequestFailed)))
            {
                this._logger.Log(LogLevel.Fatal, "Order {0} receiving unexpected update {1} {2}", clientOrderUpdateInfo.ClientOrderIdentity,
                                 clientOrderUpdateInfo.OrderStatus, clientOrderUpdateInfo.CancelRequestStatus);
            }
        }

        //private PriceObject _lastBid = null;
        //private PriceObject _lastAsk = null;

        private void ProcessSubscriptionTime(PriceObject price, DateTime subscriptionTime)
        {
            if (subscriptionTime != DateTime.MinValue)
            {
                DateTime now = DateTime.UtcNow;
                _logger.Log(LogLevel.Info, "Subscription for {0} took {1} ({2} - {3})", price.Symbol, now - subscriptionTime, now, subscriptionTime);
                //if (now - subscriptionTime > TimeSpan.FromSeconds(1))
                //{
                //    _logger.Log(LogLevel.Fatal, "Subscription to {0} took {1} ({2} - {3})", price.Symbol, now - subscriptionTime, now, subscriptionTime);
                //}
            }
            else
            {
                _logger.Log(LogLevel.Fatal, "Receiving price {0}, previous price is unavailable but so is the subscription time", price);
            }
        }

        private void OnPriceUpdate(PricesTopOfBookUpdateEventArgs priceUpdateEventArgs)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price update: {1}.", _identity, priceUpdateEventArgs);

            if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookDeteriorated)
            {
                this.OnPriceDeteriorated(priceUpdateEventArgs.PriceObject);
            }
            else if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookImproved)
            {
                this.OnPriceImproved(priceUpdateEventArgs.PriceObject);
            }
            else if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookRemoved)
            {
                if (priceUpdateEventArgs.Side == PriceSide.Ask)
                    _asks[(int)priceUpdateEventArgs.Symbol] = null;
                else
                    _bids[(int)priceUpdateEventArgs.Symbol] = null;
            }
            else if (priceUpdateEventArgs.EventType == PricesTopOfBookUpdateEventArgs.PricesTopOfBookUpdateEventType.TopOfBookAdded)
            {
                if (priceUpdateEventArgs.Side == PriceSide.Ask)
                    _asks[(int)priceUpdateEventArgs.Symbol] = priceUpdateEventArgs.PriceObject;
                else
                    _bids[(int)priceUpdateEventArgs.Symbol] = priceUpdateEventArgs.PriceObject;
            }
        }

        private void OnPriceDeteriorated(PriceObject price)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price deterioration. Price: {1}.", _identity, price == null ? "NULL" : price.ToString());
            if (price == null)
            {
                _logger.Log(LogLevel.Fatal, "Receiving price which is null");
                return;
            }

            if (price.Side == PriceSide.Ask)
            {
                if (_asks[(int) price.Symbol] != null)
                {
                    if (price.Price <= _asks[(int)price.Symbol].Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving deterioration but last ASK price is {0} and new one is {1}", _asks[(int)price.Symbol].Price, price.Price);
                }
                else
                {
                    ProcessSubscriptionTime(price, _askSubscriptionTimeStamps[(int) price.Symbol]);
                }
                _asks[(int) price.Symbol] = price;
            }
            else
            {
                if (_bids[(int)price.Symbol] != null)
                {
                    if (price.Price >= _bids[(int)price.Symbol].Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving deterioration but last BID price is {0} and new one is {1}", _bids[(int)price.Symbol].Price, price.Price);
                }
                else
                {
                    ProcessSubscriptionTime(price, _bidSubscriptionTimeStamps[(int)price.Symbol]);
                }
                _bids[(int)price.Symbol] = price;
            }

            //if (price.ParentQuote == null && !PriceObject.IsNullPrice(price))
            //{
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null");
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null, price {0}", price);
            //}
        }

        private void OnPriceImproved(PriceObject price)
        {
            _logger.Log(LogLevel.Info, "TRADING CLIENT {0} Receiving Price improvement. Price: {1}.", _identity, price);
            if (price.Side == PriceSide.Ask)
            {
                if (_asks[(int)price.Symbol] != null)
                {
                    if (price.Price >= _asks[(int)price.Symbol].Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving improvement but last ASK price is {0} and new one is {1}", _asks[(int)price.Symbol].Price, price.Price);
                }
                else
                {
                    ProcessSubscriptionTime(price, _askSubscriptionTimeStamps[(int)price.Symbol]);
                }
                _asks[(int)price.Symbol] = price;
            }
            else
            {
                if (_bids[(int)price.Symbol] != null)
                {
                    if (price.Price <= _bids[(int)price.Symbol].Price)
                        this._logger.Log(LogLevel.Fatal, "Receiving improvement but last BID price is {0} and new one is {1}", _bids[(int)price.Symbol].Price, price.Price);
                }
                else
                {
                    ProcessSubscriptionTime(price, _bidSubscriptionTimeStamps[(int)price.Symbol]);
                }
                _bids[(int)price.Symbol] = price;
            }

            //if (price.ParentQuote == null && !PriceObject.IsNullPrice(price))
            //{
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null");
            //    this._logger.Log(LogLevel.Fatal, "Parent quote null, price {0}", price);
            //}
        }

        private void HotspotOnPriceOnTopOfBookImproved(PriceObject priceObject)
        {
            throw new NotImplementedException();
        }

        private void HotspotOnPriceOnTopOfBookDeteriorated(PriceObject priceObject)
        {
            throw new NotImplementedException();
        }

        public bool TryHandleUnhandledException(Exception e)
        {
            return false;
        }
    }
}
