﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kreslik.Integrator.LargeZipsUnzipper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (chkBxUnzipMode.CheckState == CheckState.Checked)
            {
                if (!string.IsNullOrWhiteSpace(txtZipFileName.Text))
                {
                    if (File.Exists(txtZipFileName.Text))
                    {
                        openFileDialog1.FileName = txtZipFileName.Text;
                    }
                    else if (Directory.Exists(txtZipFileName.Text))
                    {
                        openFileDialog1.InitialDirectory = txtZipFileName.Text;
                    }
                }

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtZipFileName.Text = openFileDialog1.FileName;
                }
            }
            else if (chkBxUnzipMode.CheckState == CheckState.Unchecked)
            {
                if (!string.IsNullOrWhiteSpace(txtZipFileName.Text))
                {
                    if (Directory.Exists(txtZipFileName.Text))
                    {
                        folderBrowserDialog2.SelectedPath = txtZipFileName.Text;
                    }
                }

                if (folderBrowserDialog2.ShowDialog() == DialogResult.OK)
                {
                    txtZipFileName.Text = folderBrowserDialog2.SelectedPath;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDestinationFolderName.Text))
            {
                if (Directory.Exists(txtDestinationFolderName.Text))
                {
                    folderBrowserDialog1.SelectedPath = txtDestinationFolderName.Text;
                }
            }

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtDestinationFolderName.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string zipFile = txtZipFileName.Text;

            if (chkBxUnzipMode.CheckState == CheckState.Checked)
            {
                if (string.IsNullOrWhiteSpace(zipFile))
                {
                    MessageBox.Show("Zip archive file for unzipping wasn't selected", "Error");
                }
                else
                {
                    if (!File.Exists(zipFile))
                    {
                        MessageBox.Show(string.Format("Selected zip archive file ({0}) doesn't exist", zipFile), "Error");
                        zipFile = string.Empty;
                    }
                }
            }
            else if (chkBxUnzipMode.CheckState == CheckState.Unchecked)
            {
                if (string.IsNullOrWhiteSpace(zipFile))
                {
                    MessageBox.Show("Folder for zipping wasn't selected", "Error");
                }
                else
                {
                    if (!Directory.Exists(zipFile))
                    {
                        MessageBox.Show(string.Format("Selected folder for zipping ({0}) doesn't exist", zipFile), "Error");
                        zipFile = string.Empty;
                    }
                }
            }

            string destiantionFolder = txtDestinationFolderName.Text;

            if (string.IsNullOrWhiteSpace(destiantionFolder))
            {
                MessageBox.Show("Destination folder wasn't selected", "Error");
            }
            else
            {
                if (!Directory.Exists(destiantionFolder))
                {
                    MessageBox.Show(string.Format("Selected destination folder ({0}) doesn't exist", destiantionFolder), "Error");
                    destiantionFolder = string.Empty;
                }
            }

            if (!string.IsNullOrWhiteSpace(zipFile) && !string.IsNullOrWhiteSpace(destiantionFolder))
            {
                panelUnzipping.Visible = true;
                CancellationTokenSource cts = new CancellationTokenSource();

                Task updateUiTask = Task.Factory.StartNew(() => UpdateUI(cts.Token, chkBxUnzipMode.CheckState == CheckState.Checked));

                Func<string> action;
                if (chkBxUnzipMode.CheckState == CheckState.Checked)
                {
                    action = () => PerformUnzip(zipFile, destiantionFolder);
                }
                else
                {
                    action = () => PerformZip(zipFile, destiantionFolder);
                }

                Task.Factory.StartNew<string>(action).ContinueWith(t =>
                    {
                        cts.Cancel();
                        panelUnzipping.Invoke((MethodInvoker) delegate { panelUnzipping.Visible = false; });

                        if (string.IsNullOrEmpty(t.Result))
                        {
                            MessageBox.Show("Operation completed successfuly", "Success");
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Operation Failed:{0}{1}", Environment.NewLine, t.Result),
                                            "Error");
                        }
                    });
            }
        }

        private void UpdateUI(CancellationToken ct, bool unzipping)
        {
            string[] unzipLabels = {"Unzipping", "Unzipping .", "Unzipping ..", "Unzipping ..."};
            string[] zipLabels = { "Zipping", "Zipping .", "Zipping ..", "Zipping ..." };
            int lableIdx = 0;

            while (!ct.IsCancellationRequested)
            {
                int idx = lableIdx;
                lblUnzipping.Invoke((MethodInvoker)delegate { lblUnzipping.Text = unzipping ? unzipLabels[idx] : zipLabels[idx]; });
                lableIdx = (lableIdx + 1)%unzipLabels.Length;
                ct.WaitHandle.WaitOne(TimeSpan.FromMilliseconds(500));
            }
        }

        private string PerformUnzip(string zipFile, string directory)
        {
            try
            {
                System.IO.Compression.ZipFile.ExtractToDirectory(zipFile, directory);
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            return string.Empty;
        }

        private string PerformZip(string srcDirectory, string dstDdirectory)
        {
            try
            {
                string destArchiveName = Path.Combine(dstDdirectory, Path.GetFileName(srcDirectory) + ".zip");
                System.IO.Compression.ZipFile.CreateFromDirectory(srcDirectory, destArchiveName);
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            return string.Empty;
        }

        private void chkBxUnzipMode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBxUnzipMode.CheckState == CheckState.Checked)
            {
                label1.Text = "Zip Archive:";
                label2.Text = "Unzip to Folder";
                button3.Text = "Unzip";
            }
            else if (chkBxUnzipMode.CheckState == CheckState.Unchecked)
            {
                label1.Text = "Folder to zip:";
                label2.Text = "Zip to Folder";
                button3.Text = "Zip";
            }
        }
    }
}
