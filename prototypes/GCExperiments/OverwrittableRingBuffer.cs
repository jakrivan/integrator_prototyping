﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GCExperiments
{
    public interface IOverwrittableRingBufferItem
    {
        int IdOfGroup { get; }
    }

    //It might be better to have this class inside the buffer class, however readers need have access to this
    public sealed class SkipIndicationItem : IOverwrittableRingBufferItem
    {
        public SkipIndicationItem(int idOfGroup)
        {
            this.IdOfGroup = idOfGroup;
        }

        public int ItemsOverwriten { get; set; }
        public int IdOfGroup { get; private set; }
    }

    public interface IRingBufferItem
    {
        IOverwrittableRingBufferItem Item { get; }
    }

    [Serializable]
    public class ObjectPoolException : Exception
    {
        public enum ReasonStatus
        {
            PoolIsFull,
            PoolIsEmpty
        }

        public ObjectPoolException(ReasonStatus reason) { this.Reason = reason; }
        public ObjectPoolException(string message, ReasonStatus reason) : base(message) { this.Reason = reason; }
        public ObjectPoolException(string message, Exception inner, ReasonStatus reason) : base(message, inner) { this.Reason = reason; }
        protected ObjectPoolException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public ReasonStatus Reason { get; private set; }
    }

    [Serializable]
    public class RingBufferException : Exception
    {
        public enum ReasonStatus
        {
            ItemWithHighGroupId,
            InsertedItemIsNull,
            SilentOverwriting
        }

        public RingBufferException(ReasonStatus reason) { this.Reason = reason; }
        public RingBufferException(string message, ReasonStatus reason) : base(message) { this.Reason = reason; }
        public RingBufferException(string message, Exception inner, ReasonStatus reason) : base(message, inner) { this.Reason = reason; }
        protected RingBufferException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public ReasonStatus Reason { get; private set; }
    }

    public interface IOverwrittableRingBuffer
    {
        /// <summary>
        /// Claims uninitilized item from the pool. Pool is more generous than buffe, so there should be allways
        ///  enaugh items to claim. However if there are noteanugh items it can throw <see cref="ObjectPoolException"/>
        /// Call <see cref="IOverwrittableRingBuffer{T}.TryClaimUninitializedItem"/> to avoid possibility of exception
        /// </summary>
        /// <returns></returns>
        IOverwrittableRingBufferItem ClaimUninitializedItem();
        bool TryClaimUninitializedItem(out IOverwrittableRingBufferItem item);
        void PublishInitializedItem(IOverwrittableRingBufferItem item);

        IOverwrittableRingBufferItem GetNextInitializedItemBlocking();
        bool TryGetNextInitializedItem(out IOverwrittableRingBufferItem item);
        void ReturnItem(IOverwrittableRingBufferItem item);
    }

    public class OverwrittableRingBuffer : IOverwrittableRingBuffer
    {
        private IObjectPool<IOverwrittableRingBufferItem> _uninitializedItemsPool;
        private IOverwrittableRingBufferItem[] _ringBuffer;

        private int[] _itemsPerGroup;
        //One set of items for even run, other for odd run
        // During each run we insert skip items but, we might encounter skip items from previous run an we need to copy over their counts
        // So we need separate set for previous run
        private SkipIndicationItem[][] _skipItems;
        private long _purgeRunCount;

        //Be careful! for volatile read can be readered before write (see http://www.albahari.com/threading/part4.aspx#_Nonblocking_Synchronization)

        //-2 to be on a sure side that ther is no option to increment it and use as valid index
        private const int READ_IDX_INDICATING_PURGE_PROGRESS = -2;
        private int _currentReadIndex;     // _currentReadIndex == _nextPublishIndex  means bufffer is full 
        private int _nextPublishIndex = 1; // publish is allways at least 1 ahead - unless the buffer is full
        private int _ringBufferSize;
        private int _maxItemsAfterPurgePerGroup;
        private int _numberOfGroups;

        private ManualResetEventSlim _purgingDoneResetEvent = new ManualResetEventSlim(true);
        private ManualResetEventSlim _bufferHasItemsResetEvent = new ManualResetEventSlim(false);

        //Spin lock is a lock free user-mode synchronization mechanism
        // To avoid memory allocations and ABA problems (see eg. http://www.codeproject.com/Articles/190200/Locking-free-synchronization)
        // See comments inside TryFetch_WRONG for more detailed info
        private SpinLock _lock = new SpinLock(false);


        // Other possibility is to have even the pool in ring buffer - as follows (however then we need to block even for pooled items)
        //
        //private int _currentReadIndex;
        //private int _currentPublishIndex;
        //private int _currentClaimIndex;
        //
        ////      claimIdx   writeIdx     readIdx
        ////         |         |           | 
        //// . . . . * * * * * - - - - - - - . . . .

        public OverwrittableRingBuffer(int numberOfGroups, int maxItemsAfterPurgePerGroup, int capacityPerGroup, Func<IOverwrittableRingBufferItem> factoryMethod, Action<IOverwrittableRingBufferItem> releaseItemMethod)
        {
            //TODO validate argumets - min > capacity etc.
            if (maxItemsAfterPurgePerGroup <= capacityPerGroup)
                throw new ArgumentException("maxItemsAfterPurgePerGroup must be higher than capacityPerGroup");

            this._maxItemsAfterPurgePerGroup = maxItemsAfterPurgePerGroup;
            this._numberOfGroups = numberOfGroups;

            //TODO - allow only predefined # of consumers (and producers?)
            // as if we have N consumers, each can block 1 item (from the pool), other M items can be
            //  taken from the pool if M is a max size of ring buffer (buffer is full) - so M + N sizing
            //  of the pool guarantees that we will not underflow the pool
            // We still can however overrun the buffer - so we need overwritting strategy

            _uninitializedItemsPool = new GCFreeObjectPool<IOverwrittableRingBufferItem>(factoryMethod, releaseItemMethod, (numberOfGroups + 1) * capacityPerGroup);
            //One item extra to distinguish empty/full situations (more solutions here: http://en.wikipedia.org/wiki/Circular_buffer#Full_.2F_Empty_Buffer_Distinction)
            _ringBufferSize = (numberOfGroups) * capacityPerGroup + 1;
            _ringBuffer = new IOverwrittableRingBufferItem[_ringBufferSize];


            _itemsPerGroup = Enumerable.Repeat(0, numberOfGroups).ToArray();
            _skipItems = new SkipIndicationItem[][]
                {new SkipIndicationItem[numberOfGroups], new SkipIndicationItem[numberOfGroups]};
            for (int grpIdx = 0; grpIdx < numberOfGroups; grpIdx++)
            {
                _skipItems[0][grpIdx] = new SkipIndicationItem(grpIdx);
                _skipItems[1][grpIdx] = new SkipIndicationItem(grpIdx);
            }

            _purgeRunCount = 0;
        }

        public IOverwrittableRingBufferItem ClaimUninitializedItem()
        {
            return _uninitializedItemsPool.Fetch();
        }

        public bool TryClaimUninitializedItem(out IOverwrittableRingBufferItem item)
        {
            return _uninitializedItemsPool.TryFetch(out item);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetRingBufferIncrement(int index)
        {
            return index == this._ringBufferSize - 1 ? 0 : index + 1;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetRingBufferIncrement(int index, int increment)
        {
            index += increment;

            while (index >= this._ringBufferSize)
                index -= this._ringBufferSize;

            return index;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetRingBufferDecrement(int index)
        {
            return index == 0 ? this._ringBufferSize - 1 : index - 1;
        }

        private void PerformPurge()
        {
            //this is exclusive boundary of where the pruging ends
            int lastReadIdxBeforePurge = Interlocked.Exchange(ref this._currentReadIndex, READ_IDX_INDICATING_PURGE_PROGRESS);
            //at this point reading blocks, because it cannot exchange _currentReadIndex

            //this is exclusive boundary of where the purging starts
            int lastPublishIndexBeforePurge = Volatile.Read(ref this._nextPublishIndex);

            SkipIndicationItem[] thisRunSkipItems = this._skipItems[Interlocked.Increment(ref this._purgeRunCount)%2];
            //zero out skipping counts
            for (int grpIdx = 0; grpIdx < _numberOfGroups; grpIdx++)
            {
                this._itemsPerGroup[grpIdx] = 0;
                thisRunSkipItems[grpIdx].ItemsOverwriten = 0;
            }

            //We traverse the buffer with two indexes - one for current item and another for the destination after comapcting
            // after the loop is done destination index points one before last valid item after compacting - so it's new _currentReadIndex
            int destinationItemIdx = GetRingBufferDecrement(lastPublishIndexBeforePurge);
            for (int purgingItemIdx = GetRingBufferDecrement(lastPublishIndexBeforePurge);
                purgingItemIdx != lastReadIdxBeforePurge;
                purgingItemIdx = GetRingBufferDecrement(purgingItemIdx),
                destinationItemIdx = GetRingBufferDecrement(destinationItemIdx))
            {
                //over budget, needs skipping
                if (++_itemsPerGroup[_ringBuffer[purgingItemIdx].IdOfGroup] > this._maxItemsAfterPurgePerGroup)
                {
                    //Current item is either data - then we're skipping 1 item, 
                    // or it's skipping item - then we need to add previous skip count
                    int skipCount = 1;
                    if (_ringBuffer[purgingItemIdx] is SkipIndicationItem)
                    {
                        skipCount = (_ringBuffer[purgingItemIdx] as SkipIndicationItem).ItemsOverwriten;
                    }

                    //if we haven't changed skip count yet it means that we haven't inserted it yet - so insert it now
                    if (thisRunSkipItems[_ringBuffer[purgingItemIdx].IdOfGroup].ItemsOverwriten == 0)
                    {
                        //we need to use destination idx!!
                        _ringBuffer[destinationItemIdx] = thisRunSkipItems[_ringBuffer[purgingItemIdx].IdOfGroup];
                    }
                    //if we are not exchanging skipping item for SkipCounting item, then we need to skip it and increase compacting
                    else
                    {
                        //TODO: this is just temporary validation code (no need to nullify, but it will help to fail fast)
                        _ringBuffer[destinationItemIdx] = null;

                        //since we go backwards, gap is increased by incrementing idx; 
                        // after next decrement in for loop header it will point to current (unwanted) item
                        destinationItemIdx = GetRingBufferIncrement(destinationItemIdx);
                    }

                    //increment skipping count for current item appropriately
                    thisRunSkipItems[_ringBuffer[purgingItemIdx].IdOfGroup].ItemsOverwriten += skipCount;
                }
                //not over budget - we want to keep current item, but we might needto compact it
                else
                {
                    if (purgingItemIdx != destinationItemIdx)
                    {
                        _ringBuffer[destinationItemIdx] = _ringBuffer[purgingItemIdx];
                    }
                }
            }

            //this resumes reading
            Interlocked.Exchange(ref this._currentReadIndex, destinationItemIdx);
        }

        public void PublishInitializedItem(IOverwrittableRingBufferItem item)
        {
            // We need to be sure that publishing is serialized - only one thread can increment ring buffer index and check if buffer is full
            //   other threads needs to wait, but they can actively wait (CompareExchange + SpinWait) - therefore we have lock-free synchronization
            // During publishing (exclusive access needs to be guaranteed as menitoned) updator should check if buffer is full.
            //  if it's full than it needs to indicate to other publishers (with full memory fence!) so that they can start pasive wait (Monitor.Wait)
            //  After purging the buffer, if there are waiters - they need to be signalled (Monitor.Pulse)
            // Publisher can check if thebuffer is full at the end of updating - as can only empty buffer, not fill it.
            //  But again - publisher needs to read actualized value from wirtter (memory fencing)


            if (item.IdOfGroup >= this._numberOfGroups)
            {
                throw new RingBufferException(RingBufferException.ReasonStatus.ItemWithHighGroupId);
            }

            if (item == null)
            {
                throw new RingBufferException(RingBufferException.ReasonStatus.InsertedItemIsNull);
            }

            bool published = false;
            bool purgeNeeded = false;

            do
            {
                //If purging is in progress then wait; we need to pass dummy cancellation token otherwise CLR would allocate one
                this._purgingDoneResetEvent.Wait(CancellationToken.None);


                bool lockTaken = false;
                try
                {
                    _lock.Enter(ref lockTaken);

                    //Now we have exclusive access, check the flag once again in case someone is performing purge in the meantime
                    if (this._purgingDoneResetEvent.IsSet)
                    {
                        int nextPublishIndexLocal = Volatile.Read(ref this._nextPublishIndex);
                        int currentReadIndexLocal = Volatile.Read(ref this._currentReadIndex);

                        //TODO: just testing verification code
                        if (_ringBuffer[nextPublishIndexLocal] == null)
                        {
                            throw new RingBufferException(RingBufferException.ReasonStatus.SilentOverwriting);
                        }

                        _ringBuffer[nextPublishIndexLocal] = item;
                        published = true;

                        nextPublishIndexLocal = GetRingBufferIncrement(nextPublishIndexLocal);
                        Interlocked.Exchange(ref this._nextPublishIndex, nextPublishIndexLocal);

                        if (nextPublishIndexLocal == currentReadIndexLocal)
                        {
                            //if purge is needed, let other threads wait, but exit spinlock region swiftly and perform
                            // purging outside of spinlock
                            this._purgingDoneResetEvent.Reset();
                            purgeNeeded = true;
                        }
                    }
                }
                finally
                {
                    if (lockTaken)
                        _lock.Exit();
                }
            } while (!published);


            if (purgeNeeded)
            {
                //Nobody should be accessing _nextPublishIndex here, as other threads are blocked by event
                PerformPurge();

                this._purgingDoneResetEvent.Set();
            }

            //signal to readers that we've inserted something (we need to signal always as buffer might get empty without us knowing)
            this._bufferHasItemsResetEvent.Set();
        }

        public IOverwrittableRingBufferItem GetNextInitializedItemBlocking()
        {
            IOverwrittableRingBufferItem item = null;

            do
            {
                //If purging is in progress then wait; we need to pass dummy cancellation token otherwise CLR would allocate one
                this._purgingDoneResetEvent.Wait(CancellationToken.None);

                int nextPublishIndexLocal = Volatile.Read(ref this._nextPublishIndex);
                int currentReadIndexLocal = Volatile.Read(ref this._currentReadIndex);
                int originalCurrentReadIndexLocal = currentReadIndexLocal;
                currentReadIndexLocal = GetRingBufferIncrement(currentReadIndexLocal);

                if (originalCurrentReadIndexLocal == READ_IDX_INDICATING_PURGE_PROGRESS)
                {
                    continue;
                }
                else if (currentReadIndexLocal == nextPublishIndexLocal)
                {
                    //empty
                    _bufferHasItemsResetEvent.Reset();
                    if(GetRingBufferIncrement(Volatile.Read(ref this._currentReadIndex)) == Volatile.Read(ref this._nextPublishIndex))
                    {
                        //wait only after doublecheck, as someone might attempt to set event right befeore we resetted it
                        _bufferHasItemsResetEvent.Wait(CancellationToken.None);
                    }
                }
                else
                {
                    if (
                        Interlocked.CompareExchange(ref this._currentReadIndex, currentReadIndexLocal,
                                                    originalCurrentReadIndexLocal) == originalCurrentReadIndexLocal)
                    {
                        //we are the only one here, UNLESS ABA problem occured (see comments below)
                        // current thread would have to be preempted and other threads would need to read the whole buffer around to the same index 
                        // (so some another threadX would be also inside this block with the item of same index) - this is not very likely, therefore we don't
                        // use spinlock to avoid it, but we need to check for this situation by allowing only one thread to aquire the reference

                        item = Interlocked.Exchange(ref _ringBuffer[currentReadIndexLocal], null);
                        //check for null resolves posible race due to ABA threading issue
                    }

                    //if we ended up here than it means that oher thread was quicker - we will try again
                }
            } while (item == null);

            return item;
        }

        public bool TryGetNextInitializedItem(out IOverwrittableRingBufferItem item)
        {
            do
            {
                if (this._purgingDoneResetEvent.IsSet)
                {
                    int nextPublishIndexLocal = Volatile.Read(ref this._nextPublishIndex);
                    int currentReadIndexLocal = Volatile.Read(ref this._currentReadIndex);
                    int originalCurrentReadIndexLocal = currentReadIndexLocal;
                    currentReadIndexLocal = GetRingBufferIncrement(currentReadIndexLocal);

                    if (currentReadIndexLocal == nextPublishIndexLocal ||
                        originalCurrentReadIndexLocal == READ_IDX_INDICATING_PURGE_PROGRESS)
                    {
                        //empty
                        item = null;
                        return false;
                    }
                    else
                    {
                        if (
                            Interlocked.CompareExchange(ref this._currentReadIndex, currentReadIndexLocal,
                                                        originalCurrentReadIndexLocal) == originalCurrentReadIndexLocal)
                        {
                            //we are the only one here, UNLESS ABA problem occured (see comments below)
                            // current thread would have to be preempted and other threads would need to read the whole buffer around to the same index 
                            // (so some another threadX would be also inside this block with the item of same index) - this is not very likely, therefore we don't
                            // use spinlock to avoid it, but we need to check for this situation by allowing only one thread to aquire the reference

                            item = Interlocked.Exchange(ref _ringBuffer[currentReadIndexLocal], null);
                            //this resolves posible race due to ABA threading issue
                            return item != null;
                        }

                        //if we ended up here than it means that oher thread was quicker - we will try again
                    }
                }
                else
                {
                    item = null;
                    return false;
                }
            } while (true);
        }

        public void ReturnItem(IOverwrittableRingBufferItem item)
        {
            this._uninitializedItemsPool.Return(item);
        }
    }


    public interface IObjectPool<T>
    {
        /// <summary>
        /// Indicates whether the pool is empty.
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// Gets the number of items currently available in the pool.
        /// </summary>
        int Count { get; }

        bool TryFetch(out T item);
        T Fetch();
        void Return(T item);
    }

    public class GCFreeObjectPool<T>: IObjectPool<T>
    {
        //Initial creations of items (or potentially when growing the pool if allowed)
        private readonly Func<T> _factoryMethod;

        private readonly Action<T> _releaseItemMethod;

        //LIFO usage is more friendly to caches - therefore stack
        private T[] _items;

        //needs to be accessed thrue Interlocked methods to ensure memory bariers
        // Marking volatile wouldn't be enaugh (as _currentSize++ is translated to multiple instructions) 
        private int _currentSize = 0;

        private readonly int _initialSize;

        //Spin lock is a lock free user-mode synchronization mechanism
        // To avoid memory allocations and ABA problems (see eg. http://www.codeproject.com/Articles/190200/Locking-free-synchronization)
        // See comments inside TryFetch_WRONG for more detailed info
        private SpinLock _lock = new SpinLock(false);


        public GCFreeObjectPool(Func<T> factoryMethod, Action<T> releaseItemMethod, int initialSize)
        {
            _factoryMethod = factoryMethod;
            _releaseItemMethod = releaseItemMethod;

            _items = new T[initialSize];
            for (int i = 0; i < initialSize; i++)
            {
                _items[i] = Create();
            }
            _currentSize = initialSize;
            _initialSize = initialSize;
        }

        /// <summary>
        /// Provides information about pool emptiness
        /// This is concurrent method so it can be used only is information
        /// </summary>
        public bool IsEmpty
        {
            get { return Volatile.Read(ref _currentSize) == 0; }
        }

        /// <summary>
        /// Provides information about pool fulness
        /// This is concurrent method so it can be used only is information
        /// </summary>
        public bool IsFull
        {
            get { return Volatile.Read(ref _currentSize) == _initialSize; }
        }

        public int Count
        {
            get { return Volatile.Read(ref _currentSize); }
        }

        /// <summary>
        /// Attempts to fetch next item from the pool, returns false if pool is empty
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool TryFetch(out T item)
        {
            //this read might be satisfied from the local cache
            int sizeLocal = this._currentSize - 1;

            if (sizeLocal < 0)
            {
                item = default(T);
                return false;
            }

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                //Entering spinlock doesn't guarantee memory barier so we need to force by Interlocked methods
                sizeLocal = Interlocked.Decrement(ref this._currentSize);
                if (sizeLocal < 0)
                {
                    item = default(T);
                    Interlocked.Increment(ref this._currentSize);
                    return false;
                }

                item = this._items[sizeLocal];
                this._items[sizeLocal] = default(T);
            }
            finally 
            {
                if(lockTaken)
                    _lock.Exit();
            }

            return true;
        }

        //public bool TryFetch_WRONG(out T item)
        //{
        //    int sizeLocal = this._currentSize - 1;

        //    if (sizeLocal < 0)
        //    {
        //        item = default(T);
        //        return false;
        //    }

        //    if (Interlocked.CompareExchange(ref this._currentSize, sizeLocal, sizeLocal + 1) == sizeLocal + 1)
        //    {
        //        //Here we are the only one accessing index sizeLocal...
        //        //UNLESS we hit ABA problem. E.g.:
        //        // We try to change index from 5 to 4 and access index 5. We are preempted thread B manage to change index to 4 start accessing index 5, 
        //        //  then it's preempted by thread C which pops - changing the index back to 5. When we are woken up, we see the index as 5 and thought nothing changed,
        //        //  we successfuly change it to 4 and start accessing index 5 - but now we are doing it concurrently with thread B! We are in race!!!
        //        // Since this is a stack (top of stack index moves back and forth) - we are in even higher risk to hit this

        //        //Conclusion: Serialized access is needed!

        //        //Options: - either use CompareExchange with bool _changingStack variable
        //        //         - or use SpinLock - SpinLock is more sofisticated version of CompareExchange strategy, and more readable!
        //        //         - during high contention it's actually better to use locking (not to waste CPU by just spinning)!!!
        //    }

        //}

        public T Fetch()
        {
            T item;

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                int sizeLocal = Interlocked.Decrement(ref this._currentSize);
                if (sizeLocal < 0)
                {
                    Interlocked.Increment(ref this._currentSize);
                    throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsEmpty);
                }

                item = this._items[sizeLocal];
                this._items[sizeLocal] = default(T);
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }

            return item;
        }

        public void Return(T item)
        {
            if (_releaseItemMethod != null)
                _releaseItemMethod(item);

            bool lockTaken = false;
            try
            {
                _lock.Enter(ref lockTaken);

                //Even though we are in synchronized region we need to be sure that we are not reading value from local cache
                // Volatile or MemoryBarier is required
                int sizeLocal = Volatile.Read(ref this._currentSize);

                if (sizeLocal == _initialSize)
                    throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsFull);

                this._items[sizeLocal] = item;
                Interlocked.Increment(ref this._currentSize);
            }
            finally
            {
                if (lockTaken)
                    _lock.Exit();
            }
        }

        private T Create()
        {
            return _factoryMethod.Invoke();
        }
    }

    public class ObjectPool<T> //where T : class
        : IObjectPool<T>
    {
        //Initial creations of items (or potentially when growing the pool if allowed)
        private readonly Func<T> _factoryMethod;

        private readonly Action<T> _releaseItemMethod;

        //LIFO usage is more friendly to caches - therefore stack
        private ConcurrentStack<T> _items = new ConcurrentStack<T>();

        public ObjectPool(Func<T> factoryMethod, Action<T> releaseItemMethod, int initialSize)
        {
            _factoryMethod = factoryMethod;
            _releaseItemMethod = releaseItemMethod;

            for (int n = 0; n < initialSize; n++)
                Return(Create());
        }

        public bool AllowGrowing { get; set; }

        /// <summary>
        /// Indicates whether the pool is empty.
        /// </summary>
        public bool IsEmpty { get { return _items.IsEmpty; } }

        public bool TryFetch(out T item)
        {
            if (!_items.TryPop(out item))
            {
                //if Growing would be allowed, then we could use just
                //item = Create();
                return false;
            }

            return true;
        }

        public T Fetch()
        {
            T item;

            if (!_items.TryPop(out item))
            {
                if (AllowGrowing)
                    item = Create();
                else
                {
                    throw new ObjectPoolException(ObjectPoolException.ReasonStatus.PoolIsEmpty);
                }
            }

            return item;
        }

        //public T FetchBlocking()
        //{
        //    T item;

        //    if (!_items.TryPop(out item))
        //    {
        //        //if Growing would be allowed, then we could use just
        //        //item = Create();
        //    }

        //    return item;
        //}

        public void Return(T item)
        {
            if (_releaseItemMethod != null)
                _releaseItemMethod(item);

            //bool shouldSignal = _items.IsEmpty;
            _items.Push(item); 
        }

        /// <summary>
        /// Gets the number of items currently available in the pool.
        /// </summary>
        public int Count { get { return _items.Count; } }

        private T Create()
        {
            return _factoryMethod.Invoke();
        }
    }

}
