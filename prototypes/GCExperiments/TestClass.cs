﻿namespace GCExperiments
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;

    class NonblittableBugReproClass
    {
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private unsafe struct FixedSizeArrayStruct
        {
            private const int ARRAY_SIZE = 8;
            private fixed byte _buffer[ARRAY_SIZE];

            public void SetBuffer(string asciiString)
            {
                if (asciiString.Length > ARRAY_SIZE)
                    throw new Exception("Too long string");

                fixed (byte* bytePtr = this._buffer)
                {
                    for (int strIdx = 0; strIdx < asciiString.Length; strIdx++)
                    {
                        if (asciiString[strIdx] > byte.MaxValue)
                            throw new Exception(string.Format("Passed ascii string has nonascii char at position {0}. {1}",
                                                              strIdx, asciiString));

                        bytePtr[strIdx] = (byte)asciiString[strIdx];
                    }

                    for (int bufferIdx = asciiString.Length; bufferIdx < ARRAY_SIZE; bufferIdx++)
                    {
                        bytePtr[bufferIdx] = (byte)0;
                    }

                }
            }

            public string ContentAsAsciiString
            {
                set
                {
                    this.SetBuffer(value);
                }

                get
                {
                    byte[] bytes = new byte[ARRAY_SIZE];

                    unsafe
                    {
                        fixed (byte* bytePtr = this._buffer)
                        {
                            for (int i = 0; i < ARRAY_SIZE; i++)
                            {
                                bytes[i] = *(bytePtr + i);
                            }
                        }
                    }

                    return Encoding.ASCII.GetString(bytes).TrimEnd('\0');
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private unsafe class BlittableType
        {
            public int I;
            public FixedSizeArrayStruct FixedBuffer;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private unsafe class NonBlittableType
        {
            public bool B;
            public FixedSizeArrayStruct FixedBuffer;
        }

        public static void AppendTypeIntoBuffer<T>(byte[] buffer, T value)
        {
            int typeSize = Marshal.SizeOf(typeof(T));

            if (typeSize > buffer.Length)
                throw new Exception("Not enaugh space in buffer");

            unsafe
            {
                fixed (byte* b = &buffer[0])
                {
                    IntPtr p = new IntPtr(b);
                    Marshal.StructureToPtr(value, p, false);
                }
            }
        }

        public static T ReadType<T>(byte[] buffer, T uninitializedInstance) where T : class
        {
            int typeSize = Marshal.SizeOf(typeof(T));

            if (buffer.Length < typeSize)
                throw new Exception("Buffer contains less data than what's needed for the type");

            unsafe
            {
                fixed (byte* b = &buffer[0])
                {
                    IntPtr p = new IntPtr(b);

                    //this doesn't accept value arguments
                    Marshal.PtrToStructure(p, uninitializedInstance);
                }
            }

            return uninitializedInstance;
        }


        static void Main2(string[] args)
        {
            byte[] bytes = new byte[20];

            BlittableType originlBlittableType = new BlittableType() { I = 10 };
            originlBlittableType.FixedBuffer.ContentAsAsciiString = "FooBar";
            AppendTypeIntoBuffer(bytes, originlBlittableType);

            BlittableType deserializedBlittableType = new BlittableType();
            ReadType(bytes, deserializedBlittableType);

            Console.WriteLine("BlittableType.FixedBuffer: {0}", deserializedBlittableType.FixedBuffer.ContentAsAsciiString);

            Array.Clear(bytes, 0, bytes.Length);


            NonBlittableType originlNonBlittableType = new NonBlittableType() { B = false };
            originlNonBlittableType.FixedBuffer.ContentAsAsciiString = "FooBar";
            AppendTypeIntoBuffer(bytes, originlNonBlittableType);

            NonBlittableType deserializedNonBlittableType = new NonBlittableType();
            ReadType(bytes, deserializedNonBlittableType);

            Console.WriteLine("NonBlittableType.FixedBuffer: {0}", deserializedNonBlittableType.FixedBuffer.ContentAsAsciiString);

            Console.ReadKey();
        }
    }
}
