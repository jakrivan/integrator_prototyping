﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace HighResolutionTimestampCalibrationTest
{
    public class DateTimeServiceProxy : System.ServiceModel.ClientBase<IDateTimeService>, IDateTimeService
    {
        public DateTimeServiceProxy(Binding binding, EndpointAddress remoteAddress)
        :base(binding, remoteAddress)
        {
            
        }

        public DateTime GetServerTime()
        {
            return Channel.GetServerTime();
        }

        public void ReceiveClientTime(DateTime clientTime)
        {
            Channel.ReceiveClientTime(clientTime);
        }


        public void CleanDeltasCache()
        {
            Channel.CleanDeltasCache();
        }

        public TimeSpan GetServerDelta()
        {
            return Channel.GetServerDelta();
        }
    }
}
