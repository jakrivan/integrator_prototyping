﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace HighResolutionTimestampCalibrationTest
{
    public class DateTimeServiceClient
    {
        private ILogger _logger = LogFactory.Instance.GetLogger("DateTimeClientTest");

        private readonly Uri _connectAddress;
        private readonly Binding _binding;

        public DateTimeServiceClient(string serverIp)
        {

                    _connectAddress =
                        new Uri("net.tcp://" + serverIp + ":" +
                                55555 + "/DateTimeService");
                    _binding = new NetTcpBinding(SecurityMode.None);

            
        }

        private DateTimeServiceProxy _serviceProxy;

        public void RunTest()
        {
            _serviceProxy = new DateTimeServiceProxy(this._binding,
                                                                     new EndpointAddress(this._connectAddress));

            List<TimeSpan> delays = new List<TimeSpan>();
            for (int i = 0; i < 50; i++)
            {
                try
                {
                    DateTime serverTime = _serviceProxy.GetServerTime();
                    delays.Add(serverTime - HighResolutionDateTime.UtcNow);
                    Console.WriteLine("Server timeoffset: {0}", delays.Last());
                    Thread.Sleep(20);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Error, "Failed", e);
                    throw;
                }
            }

            TimeSpan averageSpan = TimeSpan.FromTicks((long)delays.Select(dl => Math.Abs(dl.Ticks)).Average());
            Console.WriteLine();
            Console.WriteLine("Average delta: {0}", averageSpan);

            _serviceProxy.CleanDeltasCache();
            for (int i = 0; i < 50; i++)
            {
                try
                {
                    _serviceProxy.ReceiveClientTime(HighResolutionDateTime.UtcNow);
                    Thread.Sleep(20);
                }
                catch (Exception e)
                {
                    this._logger.LogException(LogLevel.Error, "Failed", e);
                    throw;
                }
            }
            TimeSpan serverOffset = _serviceProxy.GetServerDelta();

            Console.WriteLine();
            Console.WriteLine("Average server delta: {0}", serverOffset);
        }
    }
}
