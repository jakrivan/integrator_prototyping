﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.BusinessLayer;
using Kreslik.Integrator.BusinessLayer.Fakes;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;

namespace Kreslik.Integrator.PriceBookDriver
{
    public class AssemblyLoadingTest
    {
        static IIntegratorClient CreateIntegratorClientInstance(string assemblyFullPath, string typeName, ILogger tradingClientServiceLogger)
        {
            IIntegratorClient integratorClient = null;
            try
            {
                Type tradingClientType;

                Assembly clientAssembly = Assembly.LoadFrom(assemblyFullPath);
                tradingClientType = clientAssembly.GetType(typeName, true);


                integratorClient = Activator.CreateInstance(tradingClientType) as IIntegratorClient;
            }
            catch (Exception e)
            {
                tradingClientServiceLogger.LogException(LogLevel.Fatal, e,
                                                        "Couldn't create the TradingClient object of type {0}", typeName);
            }

            return integratorClient;
        }

        private IBookTopProvidersStore _bookTopProvidersStore;
        private IVenueDataProviderStore _venueDataProviderStore;
        private IOrderManagementEx _orderManagement;
        private IRiskManager _riskManager;

        public AssemblyLoadingTest()
        {
            _bookTopProvidersStore = new FakeBookTopProvidersStore();
            _venueDataProviderStore = new FakeVenueDataProviderStore();
            _orderManagement = new FakeOrderManagement();
            _riskManager = new NullRiskManager();
        }

        public InProcessClientGateway CreateClientGateway(string clientIdentity)
        {
            return new InProcessClientGateway((LogFactory.Instance).GetLogger(clientIdentity), _bookTopProvidersStore, _venueDataProviderStore,
                                              _orderManagement, new List<IBroadcastInfosStore>() {_riskManager},
                                              clientIdentity);
        }

        public void Test()
        {
            System.Threading.ThreadPool.QueueUserWorkItem(obj => TestInternal(1));
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(3));
            System.Threading.ThreadPool.QueueUserWorkItem(obj => TestInternal(2));
        }

        public void CloseAll()
        {
            foreach (InProcessClientGateway inProcessClientGateway in _inProcessClientGateways.Where(gtw => !gtw.IsClosed))
            {
                inProcessClientGateway.CloseLocalResourcesOnly("Integrator closing");
            }
        }

        private List<InProcessClientGateway> _inProcessClientGateways = new List<InProcessClientGateway>();

        public void TestInternal(int testNum)
        {
            //string assemblyFullPath =
            //    @"E:\KGT\src\Integrator_daily\build\bin\Debug\Prototype\Kreslik.Integrator.BusinesLayerDriver.exe";
            string assemblyFullPath =
                @"Kreslik.Integrator.BusinesLayerDriver.exe";
            string typeName = @"Kreslik.Integrator.BusinesLayerDriver.NullGUIClient";


            IIntegratorClient integratorClient = CreateIntegratorClientInstance(assemblyFullPath, typeName,
                                                                                (LogFactory.Instance).GetLogger(
                                                                                    "tradingService" + testNum));

            InProcessClientGateway clientGateway = null;
            try
            {
                clientGateway = CreateClientGateway("tradingclient" + testNum);
                integratorClient.Configure((LogFactory.Instance).GetLogger("tradingclient_" + testNum), clientGateway, string.Empty);
                integratorClient.Start();
            }
            catch (Exception e)
            {
                //tradingClientLogger.LogException(LogLevel.Fatal, e, "Unexpected exception during Trading Client configuration and starting");
            }

            _inProcessClientGateways.Add(clientGateway);

            Form form = Form.ActiveForm;
            if (form == null)
            {
                if (Application.OpenForms.Count > testNum - 1)
                {
                    form = Application.OpenForms[testNum - 1];
                }
            }

            if (form == null)
            {
                throw new Exception("no form");
            }

            form.FormClosed += (sender, arguments) => clientGateway.CloseLocalResourcesOnly("Gateway is closing (probably client disconnected)");

            Application.Run(form);

        }
    }
}
