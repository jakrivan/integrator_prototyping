﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Xml;
using System.Xml.Serialization;
using Kreslik.Integrator.BusinessLayer;
using Kreslik.Integrator.BusinessLayer.Fakes;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.DAL.DataCollection;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.LowLatencyUtils;
using Kreslik.Integrator.NMbgDevIO;
using Kreslik.Integrator.QuickItchN;
using Kreslik.Integrator.RiskManagement;
using Kreslik.Integrator.SessionManagement;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX42;
using LogFactory = Kreslik.Integrator.Common.LogFactory;
using Symbol = Kreslik.Integrator.Common.Symbol;

namespace Kreslik.Integrator.PriceBookDriver
{
    public static class ConsoleInteropHelper
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        [DllImport("kernel32")]
        private static extern bool AllocConsole();

        private const int MF_BYCOMMAND = 0x00000000;
        private const int SC_CLOSE = 0xF060;

        [DllImport("user32.dll")]
        private static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        public static void PreventConsoleClose()
        {
            AllocConsole();
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
            SetConsoleCtrlHandler(null, true);
        }
    }

    class Program
    {
        static void DoAllocations()
        {
            List<byte[]> buffers;
            List<byte[]> oldBuffers;
            List<byte[]> agingBuffers;
            Random rnd = new Random(Environment.TickCount);

            while (true)
            {
                buffers = new List<byte[]>();

                for (int i = 0; i < 1000; i++)
                {
                    buffers.Add(new byte[500]);
                }

                if (rnd.Next(100) > 97)
                {
                    agingBuffers = buffers;
                }
                else
                {
                    oldBuffers = buffers;
                }
            }
        }

        private const uint TICKS_PER_MILLISECOND = 10000;

        public static void TestContractsSerialization<T>(T @object)
        {
            MemoryStream ms = new MemoryStream();
            DataContractSerializer ser = new DataContractSerializer(typeof(T));
            ser.WriteObject(ms, @object);
            ms.Seek(0, SeekOrigin.Begin);
            T objectDeserialized = (T)ser.ReadObject(ms);
        }


        public class DeserializeAsBaseResolver : DataContractResolver
        {
            public DeserializeAsBaseResolver()
            {
                XmlDictionary dictionary = new XmlDictionary();
                _typeName = dictionary.Add("Price");
                _typeNamespace = dictionary.Add("Integrator");
            }

            XmlDictionaryString _typeName;
            private XmlDictionaryString _typeNamespace;

            public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
            {
                if (declaredType.Equals(typeof (PriceObjectInternal)))
                {
                    typeName = null;//_typeName;
                    typeNamespace = null;//_typeNamespace;
                    return true;
                }

                return knownTypeResolver.TryResolveType(type, declaredType, null, out typeName, out typeNamespace);
            }

            public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
            {
                //if (declaredType.Equals(typeof (WritablePriceObject)))
                //{
                //    return typeof(WritablePriceObject);
                //}

                //if (typeName == "Price" && typeNamespace == "Integrator")
                //{
                //    return typeof(LegacyPriceObject);
                //}

                return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null) ?? declaredType;
            }
        }

        public class TinnyOfflinePriceObject: PriceObject
        {
            public string SubscriptionIdentity { get; private set; }

            public TinnyOfflinePriceObject(decimal price, decimal sizeBaseAbsInitial, PriceSide side, Symbol symbol,
            Counterparty counterparty, string counterpartyIdentity, string subscriptionIdentiy, MarketDataRecordType marketDataRecordType,
            DateTime integratorReceivedTime, DateTime counterpartySentTime)
            {
                this.OverrideContentInternal(new AtomicDecimal(price), sizeBaseAbsInitial, null, null, side, symbol,
                    counterparty, marketDataRecordType, null, counterpartyIdentity.ToAsciiBuffer(), integratorReceivedTime,
                    counterpartySentTime);
                this.SubscriptionIdentity = subscriptionIdentiy;
            }

            public TinnyOfflinePriceObject(decimal price, decimal sizeBaseAbsInitial, PriceSide side, Symbol symbol,
            Counterparty counterparty, string counterpartyIdentity, Guid integratorIdentity, string subscriptionIdentiy, MarketDataRecordType marketDataRecordType,
            DateTime integratorReceivedTime, DateTime counterpartySentTime)
            {
                this.DangerousTestingOnly__OverrideContentInternal(new AtomicDecimal(price), sizeBaseAbsInitial, null, null, side, symbol,
                    counterparty, marketDataRecordType, null, counterpartyIdentity.ToAsciiBuffer(), integratorIdentity, integratorReceivedTime,
                    counterpartySentTime);
                this.SubscriptionIdentity = subscriptionIdentiy;
            }
        }

        public class BasicStreamingPricesProducer : IStreamingPricesProducer
        {

            public BasicStreamingPricesProducer(Symbol symbol, Counterparty counterparty, decimal size, decimal price, decimal spread, IStreamingChannel streamingChannel)
            {
                this.Symbol = symbol;
                this._counterparty = counterparty;
                this._size = size;
                this._price = price;
                this._spread = spread;
                this._streamingChannel = streamingChannel;
                this._streamingChannelProxy = streamingChannel.GetStreamingChannelProxy(this);

                this.IntegratedTradingSystemIdentification = new IntegratedTradingSystemIdentification(78,
                    IntegratedTradingSystemType.Cross, Counterparty.FA1, new TradingSystemGroup(1, "Calm Market IV"));
            }

            private decimal _price;
            private decimal _size;
            private decimal _spread;
            private IStreamingChannel _streamingChannel;
            private IStreamingChannelProxy _streamingChannelProxy;
            private Counterparty _counterparty;

            public void Register()
            {
                bool b = this._streamingChannelProxy.RegisterStreamingPricesProducer();
                this.StartStreaming();
            }

            public void UnRegister()
            {
                this._streamingChannelProxy.UnregisterStreamingPricesProducer();
            }

            public Symbol Symbol { get; private set; }
            public DealDirection? WillProduceOrdersOnSide { get { return null; } }
            public IntegratedTradingSystemIdentification IntegratedTradingSystemIdentification { get; private set; }

            public void StartStreaming()
            {
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < 2*60*60*1 /*10*/ /*60*5*/; i++)
                    {
                        var price1 = new StreamingPrice();

                        if (i%2 == 0)
                        {
                            _streamingChannelProxy.OverrideStreamingPrice(price1, this.Symbol,
                                this._counterparty, PriceSide.Ask, _size, 5000, this._price + _spread);
                            _streamingChannel.SendPrice(price1);
                        }
                        else
                        {
                            _streamingChannelProxy.OverrideStreamingPrice(price1, this.Symbol,
                                this._counterparty, PriceSide.Bid, _size, 5000, this._price - _spread);
                            _streamingChannel.SendPrice(price1);
                        }

                        Thread.Sleep(500);
                    }

                    //this._streamingChannelProxy.UnregisterStreamingPricesProducer();

                    //(this._streamingChannel as FXCMStreamingChannel).SendSessionStatus(false);

                    //Thread.Sleep(TimeSpan.FromSeconds(5));

                    //(this._streamingChannel as FXCMStreamingChannel).SendSessionStatus(true);

                    //Thread.Sleep(TimeSpan.FromSeconds(5));

                    //for (int i = 0; i < 2; i++)
                    //{
                    //    _streamingChannel.SendPrice(new StreamingPrice(this.Symbol, this._counterparty, PriceSide.Ask, 100000, this._price + _spread,
                    //        this._producerIndex));
                    //    _streamingChannel.SendPrice(new StreamingPrice(this.Symbol, this._counterparty, PriceSide.Bid, 100000, this._price - _spread,
                    //        this._producerIndex));

                    //    Thread.Sleep(5);
                    //}

                });
            }

            public void StopStreaming()
            {
                //
            }
            
            Random _rnd = new Random();
            private int _cnt = 0;
            public bool HandleNewUnconfirmedDeal(RejectableMMDeal deal, out RejectableMMDeal.DealRejectionReason rejectionReason)
            {
                rejectionReason = RejectableMMDeal.DealRejectionReason.SystemFailedToProcess;
                //Task.Delay(TimeSpan.FromSeconds(5)).ContinueWith(t => _streamingChannel.AcceptDeal(deal));
                //Task.Factory.StartNew(() => _streamingChannel.AcceptDeal(deal));
                //return false;
                //return true;

                _streamingChannel.AcceptDeal(deal);
                return true;

                if (_cnt++ > 2)
                {
                    _streamingChannel.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed);
                }
                else
                {
                    _streamingChannel.AcceptDeal(deal);
                }

                return true;


                int rand = 4;//_rnd.Next(10);

                if (rand >= 8)
                    return false;

                if (rand > 2)
                    _streamingChannel.AcceptDeal(deal);
                else
                    _streamingChannel.RejectDeal(deal, RejectableMMDeal.DealRejectionReason.DealMissed);

                return true;
            }

            public bool TryHandleCounterpartyRejectedExecutedDeal(Symbol symbol, decimal sizeBasePol)
            {
                return false;
            }

            public bool TryHandleCounterpartyRejectedExecutedDeal(RejectableMMDeal deal)
            {
                throw new NotImplementedException();
            }
        }

        public static void OnTimer()
        {
            Console.WriteLine("Timer invoked: {0:HH:mm:ss.fff}", DateTime.UtcNow);
            Thread.Sleep(TimeSpan.FromSeconds(2));
            Console.WriteLine("Timer  ending: {0:HH:mm:ss.fff}", DateTime.UtcNow);
        }



        private class TimerWrapObject
        {
            private DateTime _shouldFireTime;
            private ILogger _log;
            private int _state = -1; //-1 nothing, 0 cancelled, 1 fired
            private IAccurateTimer _timer;
            public int InvokeCnt = 0;

            public TimerWrapObject(int millisecondsToFireAfter, ILogger log)
            {
                this._log = log;
                this._shouldFireTime = HighResolutionDateTime.UtcNow +
                                       TimeSpan.FromMilliseconds(millisecondsToFireAfter);

                /*_timer = */
                HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(TimerCallback, this._shouldFireTime, TimerTaskFlagUtils.WorkItemPriority.Highest, TimerTaskFlagUtils.TimerPrecision.Highest, false, false);
            }

            private void TimerCallback()
            {
                Interlocked.Increment(ref InvokeCnt);
                DateTime now = HighResolutionDateTime.UtcNow;

                this._log.Log(LogLevel.Info, "Should fire {0:HH-mm-ss.fffffff}, fired {1:HH-mm-ss.fffffff} - diff {2}",
                    _shouldFireTime, now, (now-_shouldFireTime).Duration());
                //_timer = null;
            }
        }

        public static void HighResTimerTest()
        {
            //return HighResolutionDateTime.TryRegisterTimerIfInFuture(this.TimeoutExpired,
            //    this.CounterpartyDealTimeUtc + maxProcessingTime, TimerExecutionType.FastSynchronousHighPrecison,
            //    out _timer);

            //HighResolutionDateTime.RegisterTimer(() => ProcessUnconfirmedDealAfterDelay(deal), deal.CounterpartyDealTimeUtc + _settings.KGTLLTime,
            //    TimerExecutionType.FastSynchronousHighPrecison);

            ILogger log = LogFactory.Instance.GetLogger("TimersTest");
            Random rand = new Random(DateTime.UtcNow.Millisecond);
            List<TimerWrapObject> _timers = new List<TimerWrapObject>();
            for (int i = 0; i < 10; i++)
            {
                _timers.Add(new TimerWrapObject(rand.Next(300), log));
            }

            Thread.Sleep(TimeSpan.FromSeconds(2));

            foreach (var timerWrapObject in _timers)
            {
                if(timerWrapObject.InvokeCnt != 1)
                    log.Log(LogLevel.Fatal, "Timer invoked {0} times", timerWrapObject.InvokeCnt);
            }
        }

        public static void ThreadPoolTest()
        {
            var tp = ThreadPoolEx.Instance;

            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(1000), TimerTaskFlagUtils.WorkItemPriority.Lowest));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(900), TimerTaskFlagUtils.WorkItemPriority.Highest));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(1000), TimerTaskFlagUtils.WorkItemPriority.Normal));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(10), TimerTaskFlagUtils.WorkItemPriority.Normal));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(10), TimerTaskFlagUtils.WorkItemPriority.Highest));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(100), TimerTaskFlagUtils.WorkItemPriority.Lowest));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(10), TimerTaskFlagUtils.WorkItemPriority.Lowest));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(1), TimerTaskFlagUtils.WorkItemPriority.Lowest));
            tp.QueueWorkItem(new WorkItem(() => Thread.Sleep(1000), TimerTaskFlagUtils.WorkItemPriority.Normal));

            Thread.Sleep(5000);
        }

        public static void FS_Test(Counterparty cpt)
        {
            var priceStreamMonitor = DiagnosticsManager.CreatePriceStreamMonitor();
            var symbolsInfoProvider = new SymbolsInfoProvider(LogFactory.Instance.GetLogger("SymbolsProvider"), new PriceHistoryProvider());
            IRiskManager riskManager = new RiskManager(priceStreamMonitor, symbolsInfoProvider.SymbolsInfo, DiagnosticsManager.GetSettlementDatesKeeper(FXCMMMCounterparty.FS1));
            IDealStatistics dealStatistics = new DealStatistics(priceStreamMonitor);

            IPriceDelayEvaluatorProvider priceDelayEvaluatorProvider = new DelayEvaluatorDal(DiagnosticsManager.DiagLogger, riskManager);
            /*MakerConnectionObjectsKeeper*/
            var streamingObjects =
                MakerConnectionObjectsKeeper.CreateMakerConnectionObjectsKeeper(cpt, dealStatistics,
                    riskManager, symbolsInfoProvider.SymbolsInfo,
                    new UnicastInfoForwarder(UnicastInfoForwardingHub.Instance, (ITakerUnicastInfoForwarder)null),
                    priceDelayEvaluatorProvider.GetDelayEvaluator(cpt)) as
                    MakerConnectionObjectsKeeper;
            var stpSessions = StpConnectionObjectsKeeper.CreateStpSessions(dealStatistics);
            foreach (IStpFlowSession stpFlowSession in stpSessions)
            {
                stpFlowSession.Start();

            }

            StreamingChannel streamChannel = streamingObjects.StreamingChannel as StreamingChannel;


            //ILogger fsmPricingLogger = LogFactory.Instance.GetLogger("FSMM_QUO");
            //ILogger fsmOrdersLogger = LogFactory.Instance.GetLogger("FSMM_ORD");

            //FIXStreamingChannelPricing_FSM fsmQuo =
            //    new FIXStreamingChannelPricing_FSM(new PersistedFixConfigsReader("FCMMMQuoBackup", fsmPricingLogger), fsmPricingLogger,
            //        Counterparty.FC1, new UnexpectedMessagesTreatingSettings() { }, SessionsSettings.FIXChannel_FS1Behavior);
            //FIXStreamingChannelOrders_FSM fsmOrd =
            //    new FIXStreamingChannelOrders_FSM(new PersistedFixConfigsReader("FCMMMOrdBackup", fsmOrdersLogger), fsmOrdersLogger,
            //        Counterparty.FC1, new UnexpectedMessagesTreatingSettings() { }, SessionsSettings.FIXChannel_FS1Behavior);

            //FXCMStreamingChannel streamChannel = new FXCMStreamingChannel(fsmQuo, fsmOrd, null, fsmOrdersLogger);

            streamChannel.Start();

            Console.ReadKey();

            BasicStreamingPricesProducer producer1 = new BasicStreamingPricesProducer(Symbol.EUR_USD, cpt, 100000, 1.113m, /*0.0005m*/ 0m, streamChannel);
            //BasicStreamingPricesProducer producer2 = new BasicStreamingPricesProducer(Symbol.EUR_USD, Counterparty.FS1, 1.0877m, 0.002m, streamChannel);

            producer1.Register();
            //producer2.Register();

            Console.ReadKey();

            
            
            
            Console.WriteLine("Cnceling prices");

            producer1.UnRegister();
            //producer2.UnRegister();

            Console.ReadKey();


            streamChannel.Stop();

            Thread.Sleep(1000);
            Console.ReadKey();

            return;
        }

        public static void FA1_Test()
        {
            Counterparty cpt = Counterparty.FL1;

            var priceStreamMonitor = DiagnosticsManager.CreatePriceStreamMonitor();
            var symbolsInfoProvider = new SymbolsInfoProvider(LogFactory.Instance.GetLogger("SymbolsProvider"), new PriceHistoryProvider());
            IRiskManager riskManager = new RiskManager(priceStreamMonitor, symbolsInfoProvider.SymbolsInfo, DiagnosticsManager.GetSettlementDatesKeeper(FXCMMMCounterparty.FS1));
            IDealStatistics dealStatistics = new DealStatistics(priceStreamMonitor);

            IPriceDelayEvaluatorProvider priceDelayEvaluatorProvider = new DelayEvaluatorDal(DiagnosticsManager.DiagLogger, riskManager);
            /*MakerConnectionObjectsKeeper*/
            var streamingObjects =
                MakerConnectionObjectsKeeper.CreateMakerConnectionObjectsKeeper(cpt, dealStatistics,
                    riskManager, symbolsInfoProvider.SymbolsInfo,
                    new UnicastInfoForwarder(UnicastInfoForwardingHub.Instance, (ITakerUnicastInfoForwarder)null),
                    priceDelayEvaluatorProvider.GetDelayEvaluator(cpt)) as
                    MakerConnectionObjectsKeeper;

            StreamingChannel streamChannel = streamingObjects.StreamingChannel as StreamingChannel;

            //ILogger fsmPricingLogger = LogFactory.Instance.GetLogger("FSMM_QUO");
            //ILogger fsmOrdersLogger = LogFactory.Instance.GetLogger("FSMM_ORD");

            //FIXStreamingChannelPricing_FSM fsmQuo =
            //    new FIXStreamingChannelPricing_FSM(new PersistedFixConfigsReader("FCMMMQuoBackup", fsmPricingLogger), fsmPricingLogger,
            //        Counterparty.FC1, new UnexpectedMessagesTreatingSettings() { }, SessionsSettings.FIXChannel_FS1Behavior);
            //FIXStreamingChannelOrders_FSM fsmOrd =
            //    new FIXStreamingChannelOrders_FSM(new PersistedFixConfigsReader("FCMMMOrdBackup", fsmOrdersLogger), fsmOrdersLogger,
            //        Counterparty.FC1, new UnexpectedMessagesTreatingSettings() { }, SessionsSettings.FIXChannel_FS1Behavior);

            //FXCMStreamingChannel streamChannel = new FXCMStreamingChannel(fsmQuo, fsmOrd, null, fsmOrdersLogger);

            streamChannel.Start();

            Console.ReadKey();

            BasicStreamingPricesProducer producer1 = new BasicStreamingPricesProducer(Symbol.USD_JPY, cpt, 150000000, 121.385m, /*0.0005m*/ /*0m*/ 0.005m, streamChannel);
            BasicStreamingPricesProducer producer2 = new BasicStreamingPricesProducer(Symbol.EUR_USD, cpt, 1000000, 1.101m, 0.0002m, streamChannel);
            //BasicStreamingPricesProducer producer3 = new BasicStreamingPricesProducer(Symbol.EUR_USD, cpt, 1000000, 1.0877m, 0.0005m, streamChannel);
            BasicStreamingPricesProducer producer4 = new BasicStreamingPricesProducer(Symbol.USD_CAD, cpt, 1000000, 1.25m, 0.0005m, streamChannel);

            producer1.Register();
            producer2.Register();
            //producer3.Register();
            producer4.Register();

            Console.ReadKey();




            Console.WriteLine("Cnceling prices");

            producer1.UnRegister();
            producer2.UnRegister();
            //producer3.UnRegister();
            producer4.UnRegister();

            Console.ReadKey();


            streamChannel.Stop();

            Thread.Sleep(1000);
            Console.ReadKey();

            return;
        }

        //static void Dummy_CompileTestOf_Net_4_5_2_presence()
        //{
        //    System.Transactions.Transaction tr = new System.Transactions.CommittableTransaction();
        //    tr.PromoteAndEnlistDurable(Guid.Empty, null, null, System.Transactions.EnlistmentOptions.None);
        //}

        //static void Dummy_CompileTestOf_Net_4_6_presence()
        //{
        //    GC.TryStartNoGCRegion(5);
        //    GC.EndNoGCRegion();
        //}


        static void Main(string[] args)
        {
            //Dummy_CompileTestOf_Net_4_6_presence();
            Console.ReadKey();
            return;

            FA1_Test();


            //UpdateExecutedDealsWithSettlmentInfo();

            //Console.WriteLine(sw.Elapsed);
            return;
        }
    }
}
