﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.NMbgDevIO;

namespace Kreslik.Integrator.PriceBookDriver
{
    public class HighResTimerTest
    {
        public void Run()
        {
            DisposingScenario();
            SpecialCasesScenario();
            CancelTestSceanrio();
            NegativeSpanTest();
            LowStressTestScenario();
            Console.WriteLine("StressTestScenario after keypress...");
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            Console.ReadKey();
            StressTestScenario();
        }

        private IAccurateTimer RegisterPreciseTimer(TimeSpan dueSpan)
        {
            return HighResolutionDateTime.HighResolutionTimerManager.RegisterTimer(new TimerCallBackWrapper(dueSpan).TimerCallback,
                dueSpan, TimerTaskFlagUtils.WorkItemPriority.Highest, TimerTaskFlagUtils.TimerPrecision.Highest, true, false);
        }

        private void DisposingScenario()
        {
            TimerCallBackWrapper.TimerExecutions = 0;
            DisposingScenarioInternal();
            Thread.Sleep(300);
            GC.Collect();
            if (TimerCallBackWrapper.TimerExecutions != 4)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 4", TimerCallBackWrapper.TimerExecutions);
        }

        private void DisposingScenarioInternal()
        {
            var timer1 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(45));
            var timer2 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(21));
            var timer3 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(101));
            var timer4 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(200));
        }

        private void SpecialCasesScenario()
        {
            TimerCallBackWrapper.TimerExecutions = 0;
            var timer5 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(25));
            var timer6 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(11));
            Thread.Sleep(8);
            var timer7 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(101));
            Thread.Sleep(50);
            var timer8 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(2));
            Thread.Sleep(100);
            GC.KeepAlive(timer5);
            GC.KeepAlive(timer6);
            GC.KeepAlive(timer7);
            GC.KeepAlive(timer8);
            if (TimerCallBackWrapper.TimerExecutions != 4)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 4", TimerCallBackWrapper.TimerExecutions);
        }

        private void CancelTestSceanrio()
        {
            TimerCallBackWrapper.TimerExecutions = 0;
            var timer9 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(5));
            var timer10 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(4));
            HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref timer9);
            HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref timer10);
            var timer11 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(101));
            var timer12 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(200));
            Thread.Sleep(50);
            HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref timer11);
            HighResolutionDateTime.HighResolutionTimerManager.CancelTimer(ref timer12);
            Thread.Sleep(20);
            if (TimerCallBackWrapper.TimerExecutions != 0)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 4", TimerCallBackWrapper.TimerExecutions);
        }

        private void NegativeSpanTest()
        {
            TimerCallBackWrapper.TimerExecutions = 0;
            var timer13 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(-101));
            var timer14 = RegisterPreciseTimer(TimeSpan.FromMilliseconds(-1));
            Thread.Sleep(20);
            if (TimerCallBackWrapper.TimerExecutions != 2)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 2", TimerCallBackWrapper.TimerExecutions);
        }

        private void LowStressTestScenario()
        {
            int a, b;
            ThreadPool.GetAvailableThreads(out a, out b);

            TimerCallBackWrapper.TimerExecutions = 0;
            for (int i = 0; i < 100; i++)
            {
                RegisterPreciseTimer(TimeSpan.FromMilliseconds(61).Subtract(TimeSpan.FromTicks(i * 10)));
            }

            for (int i = 0; i < 100; i++)
            {
                RegisterPreciseTimer(TimeSpan.FromMilliseconds(5).Subtract(TimeSpan.FromTicks(i * 10)));
            }

            Thread.Sleep(100);
            if (TimerCallBackWrapper.TimerExecutions != 200)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 200", TimerCallBackWrapper.TimerExecutions);


            TimerCallBackWrapper.TimerExecutions = 0;
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                RegisterPreciseTimer(TimeSpan.FromMilliseconds(rnd.Next(102)).Subtract(TimeSpan.FromTicks(i * 10)));
            }
            Thread.Sleep(200);
            if (TimerCallBackWrapper.TimerExecutions != 100)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 100", TimerCallBackWrapper.TimerExecutions);
        }

        private void StressTestScenario()
        {
            int a, b;
            ThreadPool.GetAvailableThreads(out a, out b);

            TimerCallBackWrapper.TimerExecutions = 0;
            for (int i = 0; i < 10000; i++)
            {
                RegisterPreciseTimer(TimeSpan.FromMilliseconds(61).Subtract(TimeSpan.FromTicks(i*10)));
            }

            for (int i = 0; i < 10000; i++)
            {
                RegisterPreciseTimer(TimeSpan.FromMilliseconds(5).Subtract(TimeSpan.FromTicks(i*10)));
            }

            Thread.Sleep(100);
            if (TimerCallBackWrapper.TimerExecutions != 20000)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 20000", TimerCallBackWrapper.TimerExecutions);


            TimerCallBackWrapper.TimerExecutions = 0;
            Random rnd = new Random();
            for (int i = 0; i < 10000; i++)
            {
                RegisterPreciseTimer(TimeSpan.FromMilliseconds(rnd.Next(102)).Subtract(TimeSpan.FromTicks(i*10)));
            }
            Thread.Sleep(200);
            if (TimerCallBackWrapper.TimerExecutions != 10000)
                LogFactory.Instance.GetLogger(null)
                    .Log(LogLevel.Fatal, "Timer Executions: {0}, Expected 10000", TimerCallBackWrapper.TimerExecutions);
        }

        private class TimerCallBackWrapper
        {
            public static int TimerExecutions { get; set; }

            private static TimeSpan MaxDeviation = TimeSpan.FromMilliseconds(1);
            private static int instancesCnt;

            private DateTime _creationTime;
            private TimeSpan _dueSpan;
            private int _instanceId;

            public TimerCallBackWrapper(TimeSpan dueSpan)
            {
                this._creationTime = HighResolutionDateTime.UtcNow;
                this._dueSpan = dueSpan;
                this._instanceId = Interlocked.Increment(ref instancesCnt);
            }

            public void TimerCallback()
            {
                TimerExecutions++;
                DateTime now = HighResolutionDateTime.UtcNow;
                TimeSpan deviation = (now - _creationTime - (_dueSpan < TimeSpan.Zero ? TimeSpan.Zero : _dueSpan)).Duration();
                LogFactory.Instance.GetLogger(null)
                    .Log(deviation > MaxDeviation ? LogLevel.Fatal : LogLevel.Trace,
                        "Timer {0} fired {1:HH:mm:ss.fffffff}, due: {2}, deviation: {3}",
                        _instanceId, now, _dueSpan, deviation);
            }
        }
    }
}
