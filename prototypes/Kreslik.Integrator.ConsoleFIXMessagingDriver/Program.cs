﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.Contracts;
using Kreslik.Integrator.Contracts.Internal;
using Kreslik.Integrator.DAL;
using Kreslik.Integrator.Diagnostics;
using Kreslik.Integrator.FIXMessaging;
using Kreslik.Integrator.FIXMessaging.CounterpartSpecific;
using Kreslik.Integrator.RiskManagement;
using Kreslik.Integrator.SessionManagement;

namespace Kreslik.Integrator.ConsoleFIXMessagingDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            PriceObjectPool.InitializeInstance(500);

            bool resubscribeOnReconnect = true;
            var priceStreamMonitor = DiagnosticsManager.CreatePriceStreamMonitor();
            var symbolsInfoProvider = new SymbolsInfoProvider(LogFactory.Instance.GetLogger("SymbolsProvider"), new PriceHistoryProvider());
            IRiskManager riskManager = new RiskManager(priceStreamMonitor, symbolsInfoProvider.SymbolsInfo, DiagnosticsManager.GetSettlementDatesKeeper(FXCMMMCounterparty.FS1));
            IDealStatistics dealStatistics = new DealStatistics(priceStreamMonitor);

            DelayEvaluatorDal delayEvaluatorDal = new DelayEvaluatorDal(DiagnosticsManager.DiagLogger, riskManager);
            TakerConnectionObjectsKeeper LM3connectionObjectsKeeper =
                TakerConnectionObjectsKeeper.CreateConnectionObjectsKeeper(Counterparty.LM3, resubscribeOnReconnect, riskManager,
                    dealStatistics,
                    delayEvaluatorDal.GetDelayEvaluator(Counterparty.LM3),
                    true, true);
            delayEvaluatorDal.SetSessionStateWatchers(new List<IFIXChannel>() { LM3connectionObjectsKeeper.MarketDataSession });

            TakerConnectionObjectsKeeper FA1connectionObjectsKeeper =
                TakerConnectionObjectsKeeper.CreateConnectionObjectsKeeper(Counterparty.FA1, resubscribeOnReconnect, riskManager,
                    dealStatistics,
                    delayEvaluatorDal.GetDelayEvaluator(Counterparty.FA1),
                    true, true);
            delayEvaluatorDal.SetSessionStateWatchers(new List<IFIXChannel>() { LM3connectionObjectsKeeper.MarketDataSession });

            TakerConnectionObjectsKeeper FC1connectionObjectsKeeper =
                TakerConnectionObjectsKeeper.CreateConnectionObjectsKeeper(Counterparty.FC1, resubscribeOnReconnect, riskManager,
                    dealStatistics,
                    delayEvaluatorDal.GetDelayEvaluator(Counterparty.FC1),
                    true, true);
            delayEvaluatorDal.SetSessionStateWatchers(new List<IFIXChannel>() { LM3connectionObjectsKeeper.MarketDataSession });

            TakerConnectionObjectsKeeper FC2connectionObjectsKeeper =
                TakerConnectionObjectsKeeper.CreateConnectionObjectsKeeper(Counterparty.FC2, resubscribeOnReconnect, riskManager,
                    dealStatistics,
                    delayEvaluatorDal.GetDelayEvaluator(Counterparty.FC2),
                    true, true);
            delayEvaluatorDal.SetSessionStateWatchers(new List<IFIXChannel>() { LM3connectionObjectsKeeper.MarketDataSession });

            //stages - scenarios + 1
            Barrier scenarioBarrier = new Barrier(5);
            Task.Factory.StartNew(() => SubscriptionOnlyScenario(LM3connectionObjectsKeeper, scenarioBarrier));
            Task.Factory.StartNew(() => SubscriptionOnlyScenario(FA1connectionObjectsKeeper, scenarioBarrier));
            Task.Factory.StartNew(() => SubscriptionOnlyScenario(FC1connectionObjectsKeeper, scenarioBarrier));
            Task.Factory.StartNew(() => SubscriptionOnlyScenario(FC2connectionObjectsKeeper, scenarioBarrier));

            Console.ReadKey();
            scenarioBarrier.SignalAndWait();
            scenarioBarrier.SignalAndWait();

            Console.WriteLine("DONE, press key to exit...");
            Console.ReadKey();


            //HotOrdersHighRateTestScenario(connectionObjectsKeeper);


            //SubscriptionOnlyScenario(testObjectsFactory);
            //  SubscriptionAndOrderingScenario(testObjectsFactory);
            //  AbortedOrderingScenario(testObjectsFactory);

            // OrderStressScenario(testObjectsFactory);

        }

        //public static void HotOrdersHighRateTestScenario(ConnectionObjectsKeeper connectionsObjectsKeeper)
        //{
        //    VenueOrderFlowSession venueOrdersSession = connectionsObjectsKeeper.OrderFlowSession as VenueOrderFlowSession;
        //    if (venueOrdersSession == null)
        //        throw new Exception("Not a valid VenueOrderFlowSession passed into test");
        //    venueOrdersSession.Start();

        //    Console.ReadKey();

        //    for (int i = 0; i < 3; i++)
        //    {
        //        for (int j = 0; j < 13; j++)
        //        {
        //            for (int h = 0; h < 9; h++)
        //            {
        //                OrderRequestInfo orderInfo =
        //                    OrderRequestInfo.CreateLimit(1000m, 1.37m, DealDirection.Buy, Symbol.EUR_USD,
        //                                                 TimeInForce.Day);
        //                var orderExternal = venueOrdersSession.SendOrder(orderInfo);
        //                Thread.Sleep(1);
        //            }

        //            Thread.Sleep(10);
        //        }

        //        Thread.Sleep(1000);
        //    }

        //    Console.ReadKey();

        //    venueOrdersSession.Stop();

        //    Thread.Sleep(1000);

        //    //any outstanding orders during start?
        //    venueOrdersSession.Start();

        //    Thread.Sleep(3000);

        //    venueOrdersSession.Stop();

        //    Console.ReadKey();

        //    //venueOrdersSession.SubmitOrder()
        //}

        public static void SubscriptionOnlyScenario(TakerConnectionObjectsKeeper connectionsObjectsKeeper, Barrier barrier)
        {
            IMarketDataSession marketDataSession = connectionsObjectsKeeper.MarketDataSession;
            ILogger loggerMD = connectionsObjectsKeeper.QuotesLogger;

            marketDataSession.OnStarted += () => loggerMD.Log(LogLevel.Info, "SESSION STARTED (from main)");
            marketDataSession.OnStopped += () => loggerMD.Log(LogLevel.Info, "SESSION STOPPED (from main)");
            marketDataSession.OnSubscriptionChanged += subscription => loggerMD.Log(LogLevel.Info, "(from main) subscription [{0}] changed to [{1}]", subscription.Identity, subscription.SubscriptionStatus);
            marketDataSession.OnNewPrice += (PriceObjectInternal price, ref bool call) => loggerMD.Log(LogLevel.Info, "(from main) new PRICE: {0}", price);
            marketDataSession.OnIgnoredPrice += (PriceObjectInternal price, ref bool call) => loggerMD.Log(LogLevel.Info, "(from main) new IGNORED price: {0}", price);
            marketDataSession.OnCancelLastQuote += (s, c) => loggerMD.Log(LogLevel.Info, "(from main) cancelling quote from subscription {0}", s);

            marketDataSession.Start();

            if (connectionsObjectsKeeper.Counterparty == Counterparty.HSB)
            {
                System.Threading.Thread.Sleep(TimeSpan.FromMinutes(1));
            }
            else
            {
                for (int i = 0; i < 15; i++)
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                    if(marketDataSession.State == SessionState.Running)
                        break;
                }
            }

            List<ISubscription> subscriptions = new List<ISubscription>();

            foreach (Symbol symbol in Symbol.Values)
            {
                var sub = marketDataSession.Subscribe(new SubscriptionRequestInfo(symbol, 1000000, connectionsObjectsKeeper.Counterparty == Counterparty.HSB ? "KGT3" : null));
                if (sub != null)
                {
                    subscriptions.Add(sub);
                }
                else
                {
                    loggerMD.Log(LogLevel.Error, "Subscription for [{0}] failed", symbol);
                }
            }

            //var sub = marketDataSession.Subscribe(Symbol.EUR_USD, 1000000, "KGT3");

            //System.Threading.Thread.Sleep(TimeSpan.FromMinutes(2));

            barrier.SignalAndWait();


            foreach (var subscription in subscriptions)
            {
                loggerMD.Log(LogLevel.Trace, "(from main) Subscription [{0}] is now in state [{1}]",
                            subscription.Identity, subscription.SubscriptionStatus);

                if (subscription.SubscriptionStatus != SubscriptionStatus.Rejected &&
                    subscription.SubscriptionStatus != SubscriptionStatus.NotSubscribed &&
                    subscription.SubscriptionStatus != SubscriptionStatus.Broken)
                {
                    if (!marketDataSession.Unsubscribe(subscription))
                    {
                        loggerMD.Log(LogLevel.Error, "Unsubscription for [{0}] failed", subscription.Identity);
                    }
                }
            }

            //marketDataSession.Unsubscribe(sub);

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5));

            marketDataSession.Stop();

            barrier.SignalAndWait();
        }


        //public static void SubscriptionAndOrderingScenario(ConnectionObjectsKeeper connectionObjectsKeeper)
        //{
        //    QuoteBase lastQuote = null;

        //    IMarketDataSession marketDataSession = connectionObjectsKeeper.MarketDataSession;
        //    IOrderFlowSession orderFlowSession = connectionObjectsKeeper.OrderFlowSession;
        //    ILogger loggerMD = connectionObjectsKeeper.QuotesLogger;
        //    ILogger loggerORD = connectionObjectsKeeper.OrdersLogger;

        //    marketDataSession.OnStarted += () => loggerMD.Log(LogLevel.Info, "MD SESSION STARTED (from main)");
        //    marketDataSession.OnStopped += () => loggerMD.Log(LogLevel.Info, "MD SESSION STOPPED (from main)");
        //    marketDataSession.OnSubscriptionChanged += subscription => loggerMD.Log(LogLevel.Info, "(from main) subscription [{0}] changed to [{1}]", subscription.Identity, subscription.SubscriptionStatus);
        //    marketDataSession.OnNewQuote += quote =>
        //        {
        //            loggerMD.Log(LogLevel.Info, "(from main) new QUOTE: {0}", quote);
        //            lastQuote = quote;
        //        };
        //    marketDataSession.OnCancelLastQuote += (s, c) => loggerMD.Log(LogLevel.Info, "(from main) cancelling quote from subscription {0}", s);

        //    orderFlowSession.OnStarted += () => loggerORD.Log(LogLevel.Info, "ORD SESSION STARTED (from main)");
        //    orderFlowSession.OnStopped += () => loggerORD.Log(LogLevel.Info, "ORD SESSION STOPPED (from main)");
        //    orderFlowSession.OnOrderChanged +=
        //        (order, eventArgs) => loggerORD.Log(LogLevel.Info, "(from main) order changed: {0}", order);

        //    orderFlowSession.Start();
        //    marketDataSession.Start();

        //    if (connectionObjectsKeeper.Counterparty == Counterparty.HSB)
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(40));
        //    }
        //    else
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(7));
        //    }


        //    var sub = marketDataSession.Subscribe(new SubscriptionRequestInfo(Symbol.EUR_USD, 1000000, connectionObjectsKeeper.Counterparty == Counterparty.HSB ? "KGT3" : null));

        //    if (sub == null)
        //    {
        //        loggerMD.Log(LogLevel.Error, "Couldn't subscribe to price stream");
        //    }
        //    else
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(20));
        //        IIntegratorOrderExternal ord = null;
        //        IIntegratorOrderExternal ordInvalid = null;
        //        if (lastQuote != null && lastQuote.AskPrice != null)
        //        {
        //            ord =
        //                orderFlowSession.SendOrder(OrderRequestInfo.CreateQuoted(100000, lastQuote.AskPrice.Price, DealDirection.Buy,
        //                                                                lastQuote));

        //            ordInvalid =
        //                orderFlowSession.SendOrder(OrderRequestInfo.CreateQuoted(100000, lastQuote.AskPrice.Price - 1, DealDirection.Buy,
        //                                                                lastQuote));
        //        }

        //        if (ordInvalid == null)
        //        {
        //            loggerMD.Log(LogLevel.Error, "Sample invalid Order was not created");
        //        }

        //        if (ord == null)
        //        {
        //            loggerMD.Log(LogLevel.Error, "Order was not created");
        //        }
        //        else
        //        {
        //            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(10));

        //            if (ord.OrderStatus != IntegratorOrderExternalStatus.Filled)
        //            {
        //                loggerMD.Log(LogLevel.Error, "Order was not filled.");
        //            }
        //        }

        //        if (sub.SubscriptionStatus == SubscriptionStatus.Unknown)
        //        {
        //            loggerMD.Log(LogLevel.Error, "Subscription [{0}] is unconfirmed", sub);
        //        }

        //        marketDataSession.Unsubscribe(sub);
        //    }

        //    orderFlowSession.Stop();
        //    marketDataSession.Stop();

        //    Console.WriteLine("Press a key");
        //    Console.ReadKey();
        //}



        //public static void OrderStressScenario(ConnectionObjectsKeeper connectionObjectsKeeper)
        //{
        //    const int ORDERS_COUNT = 50;

        //    Random r = new Random();

        //    ConcurrentDictionary<string, IIntegratorOrderExternal> orders = new ConcurrentDictionary<string, IIntegratorOrderExternal>(2, ORDERS_COUNT);
        //    int sentOrders = 0;
        //    int doneOrders = 0;

        //    DateTime lastOrderSend = DateTime.UtcNow;

        //    ManualResetEvent allSent = new ManualResetEvent(false);
        //    ManualResetEvent allDone = new ManualResetEvent(false);

        //    IMarketDataSession marketDataSession = connectionObjectsKeeper.MarketDataSession;
        //    IOrderFlowSession orderFlowSession = connectionObjectsKeeper.OrderFlowSession;
        //    ILogger loggerMD = connectionObjectsKeeper.QuotesLogger;
        //    ILogger loggerORD = connectionObjectsKeeper.OrdersLogger;

        //    marketDataSession.OnStarted += () => loggerMD.Log(LogLevel.Info, "MD SESSION STARTED (from main)");
        //    marketDataSession.OnStopped += () => loggerMD.Log(LogLevel.Info, "MD SESSION STOPPED (from main)");
        //    marketDataSession.OnSubscriptionChanged += subscription => loggerMD.Log(LogLevel.Info, "(from main) subscription [{0}] changed to [{1}]", subscription.Identity, subscription.SubscriptionStatus);
        //    marketDataSession.OnNewQuote += quote =>
        //    {
        //        loggerMD.Log(LogLevel.Info, "(from main) new QUOTE: {0}", quote);

        //        DateTime now = DateTime.UtcNow;

        //        //if (lastOrderSend.Add(TimeSpan.FromMilliseconds(200)) > now)
        //        //{
        //        //    return;
        //        //}

        //        //if (r.Next(0, 2) > 0)
        //        //{
        //        if (quote.AskPrice != null && quote.AskPrice.Price > 0 && quote.AskPrice.SizeBaseAbsRemaining > 0 && sentOrders < ORDERS_COUNT)
        //        {
        //            IIntegratorOrderExternal ord =
        //                orderFlowSession.SendOrder(OrderRequestInfo.CreateQuoted(quote.AskPrice.SizeBaseAbsRemaining, quote.AskPrice.Price,
        //                                                                   DealDirection.Buy,
        //                                                                   quote));

        //            if (ord == null)
        //            {
        //                loggerORD.Log(LogLevel.Error, "Couldn't create buy order for quote [{0}]", quote);
        //            }
        //            else
        //            {
        //                if (!orders.TryAdd(ord.Identity, ord))
        //                {
        //                    loggerORD.Log(LogLevel.Error, "Error during adding order [{0}]", ord.Identity);
        //                }

        //                sentOrders++;
        //                lastOrderSend = DateTime.UtcNow;
        //            }
        //        }
        //        //}
        //        //else
        //        //{
        //        if (quote.BidPrice != null && quote.BidPrice.Price > 0 && quote.BidPrice.SizeBaseAbsRemaining > 0 && sentOrders < ORDERS_COUNT)
        //        {
        //            IIntegratorOrderExternal ord =
        //                orderFlowSession.SendOrder(OrderRequestInfo.CreateQuoted(quote.BidPrice.SizeBaseAbsRemaining, quote.BidPrice.Price,
        //                                                                   DealDirection.Sell,
        //                                                                   quote));

        //            if (ord == null)
        //            {
        //                loggerORD.Log(LogLevel.Error, "Couldn't create sell order for quote [{0}]", quote);
        //            }
        //            else
        //            {
        //                if (!orders.TryAdd(ord.Identity, ord))
        //                {
        //                    loggerORD.Log(LogLevel.Error, "Error during adding order [{0}]", ord.Identity);
        //                }

        //                sentOrders++;
        //                lastOrderSend = DateTime.UtcNow;
        //            }
        //        }
        //        //}

        //        if (sentOrders >= ORDERS_COUNT)
        //        {
        //            allSent.Set();
        //        }
        //    };
        //    marketDataSession.OnCancelLastQuote += (s, c) => loggerMD.Log(LogLevel.Info, "(from main) cancelling quote from subscription {0}", s);

        //    orderFlowSession.OnStarted += () => loggerORD.Log(LogLevel.Info, "ORD SESSION STARTED (from main)");
        //    orderFlowSession.OnStopped += () => loggerORD.Log(LogLevel.Info, "ORD SESSION STOPPED (from main)");
        //    orderFlowSession.OnOrderChanged +=
        //        (order, eventArgs) =>
        //        {
        //            loggerORD.Log(LogLevel.Info, "(from main) order changed: {0}", order);

        //            if (order.OrderStatus == IntegratorOrderExternalStatus.Filled || order.OrderStatus == IntegratorOrderExternalStatus.Rejected || order.OrderStatus == IntegratorOrderExternalStatus.InBrokenState)
        //            {
        //                doneOrders++;

        //                if (doneOrders == ORDERS_COUNT)
        //                {
        //                    allDone.Set();
        //                }
        //            }

        //        };

        //    orderFlowSession.Start();
        //    marketDataSession.Start();

        //    if (connectionObjectsKeeper.Counterparty == Counterparty.HSB)
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(40));
        //    }
        //    else
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(7));
        //    }


        //    var sub = marketDataSession.Subscribe(new SubscriptionRequestInfo(Symbol.EUR_USD, 1000000, connectionObjectsKeeper.Counterparty == Counterparty.HSB ? "KGT3" : null));

        //    var sub2 = marketDataSession.Subscribe(new SubscriptionRequestInfo(Symbol.AUD_USD, 1000000, connectionObjectsKeeper.Counterparty == Counterparty.HSB ? "KGT3" : null));

        //    var sub3 = marketDataSession.Subscribe(new SubscriptionRequestInfo(Symbol.AUD_JPY, 1000000, connectionObjectsKeeper.Counterparty == Counterparty.HSB ? "KGT3" : null));

        //    if (sub == null || sub2 == null || sub3 == null)
        //    {
        //        loggerMD.Log(LogLevel.Error, "Couldn't subscribe to price stream");
        //    }
        //    else
        //    {
        //        allSent.WaitOne(TimeSpan.FromMinutes(5));
        //        loggerORD.Log(LogLevel.Info, "{0} Orders sent", sentOrders);

        //        allDone.WaitOne(TimeSpan.FromSeconds(20));
        //        loggerORD.Log(LogLevel.Info, "{0} Orders done", doneOrders);

        //        marketDataSession.Unsubscribe(sub);

        //        marketDataSession.Unsubscribe(sub2);

        //        marketDataSession.Unsubscribe(sub3);
        //    }

        //    orderFlowSession.Stop();
        //    marketDataSession.Stop();

        //    Console.WriteLine("Press a key");
        //    Console.ReadKey();
        //}


        //public static void AbortedOrderingScenario(ConnectionObjectsKeeper connectionObjectsKeeper)
        //{
        //    QuoteBase lastQuote = null;

        //    IMarketDataSession marketDataSession = connectionObjectsKeeper.MarketDataSession;
        //    IOrderFlowSession orderFlowSession = connectionObjectsKeeper.OrderFlowSession;
        //    ILogger loggerMD = connectionObjectsKeeper.QuotesLogger;
        //    ILogger loggerORD = connectionObjectsKeeper.OrdersLogger;

        //    marketDataSession.OnStarted += () => loggerMD.Log(LogLevel.Info, "MD SESSION STARTED (from main)");
        //    marketDataSession.OnStopped += () => loggerMD.Log(LogLevel.Info, "MD SESSION STOPPED (from main)");
        //    marketDataSession.OnSubscriptionChanged += subscription => loggerMD.Log(LogLevel.Info, "(from main) subscription [{0}] changed to [{1}]", subscription.Identity, subscription.SubscriptionStatus);
        //    marketDataSession.OnNewPrice += quote =>
        //    {
        //        loggerMD.Log(LogLevel.Info, "(from main) new QUOTE: {0}", quote);
        //        lastQuote = quote.;
        //    };
        //    marketDataSession.OnCancelLastQuote += (s, c) => loggerMD.Log(LogLevel.Info, "(from main) cancelling quote from subscription {0}", s);

        //    orderFlowSession.OnStarted += () => loggerORD.Log(LogLevel.Info, "ORD SESSION STARTED (from main)");
        //    orderFlowSession.OnStopped += () => loggerORD.Log(LogLevel.Info, "ORD SESSION STOPPED (from main)");
        //    orderFlowSession.OnOrderChanged +=
        //        (order, eventArgs) => loggerORD.Log(LogLevel.Info, "(from main) order changed: {0}", order);

        //    orderFlowSession.Start();
        //    marketDataSession.Start();

        //    if (connectionObjectsKeeper.Counterparty == Counterparty.HSB)
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(40));
        //    }
        //    else
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(7));
        //    }


        //    var sub = marketDataSession.Subscribe(new SubscriptionRequestInfo(Symbol.EUR_USD, 1000000, connectionObjectsKeeper.Counterparty == Counterparty.HSB ? "KGT3" : null));

        //    if (sub == null)
        //    {
        //        loggerMD.Log(LogLevel.Error, "Couldn't subscribe to price stream");
        //    }
        //    else
        //    {
        //        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(20));
        //        IIntegratorOrderExternal ord = null;
        //        if (lastQuote != null)
        //        {
        //            ord =
        //                orderFlowSession.SendOrder(OrderRequestInfo.CreateQuoted(100000, lastQuote.AskPrice.Price, DealDirection.Buy,
        //                                                                lastQuote));
        //        }

        //        if (ord == null)
        //        {
        //            loggerMD.Log(LogLevel.Error, "Order was not created");
        //        }
        //        else
        //        {
        //            Environment.FailFast("Simulating forcefull abort");
        //        }

        //        if (sub.SubscriptionStatus == SubscriptionStatus.Unknown)
        //        {
        //            loggerMD.Log(LogLevel.Error, "Subscription [{0}] is unconfirmed", sub);
        //        }

        //        marketDataSession.Unsubscribe(sub);
        //    }

        //    orderFlowSession.Stop();
        //    marketDataSession.Stop();

        //    Console.WriteLine("Press a key");
        //    Console.ReadKey();
        //}
    }
}
