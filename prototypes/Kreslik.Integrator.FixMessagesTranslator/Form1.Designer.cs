﻿namespace Kreslik.Integrator.FixMessagesTranslator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRawText = new System.Windows.Forms.TextBox();
            this.txtParsedText = new System.Windows.Forms.TextBox();
            this.btnParse = new System.Windows.Forms.Button();
            this.chckWrap = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtRawText
            // 
            this.txtRawText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRawText.Location = new System.Drawing.Point(13, 13);
            this.txtRawText.Multiline = true;
            this.txtRawText.Name = "txtRawText";
            this.txtRawText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRawText.Size = new System.Drawing.Size(811, 293);
            this.txtRawText.TabIndex = 0;
            this.txtRawText.WordWrap = false;
            // 
            // txtParsedText
            // 
            this.txtParsedText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParsedText.Location = new System.Drawing.Point(13, 340);
            this.txtParsedText.Multiline = true;
            this.txtParsedText.Name = "txtParsedText";
            this.txtParsedText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtParsedText.Size = new System.Drawing.Size(811, 369);
            this.txtParsedText.TabIndex = 1;
            this.txtParsedText.WordWrap = false;
            // 
            // btnParse
            // 
            this.btnParse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnParse.Location = new System.Drawing.Point(749, 312);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(75, 23);
            this.btnParse.TabIndex = 2;
            this.btnParse.Text = "Parse";
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.btnParse_Click);
            // 
            // chckWrap
            // 
            this.chckWrap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckWrap.AutoSize = true;
            this.chckWrap.Location = new System.Drawing.Point(632, 316);
            this.chckWrap.Name = "chckWrap";
            this.chckWrap.Size = new System.Drawing.Size(76, 17);
            this.chckWrap.TabIndex = 3;
            this.chckWrap.Text = "Wrap lines";
            this.chckWrap.UseVisualStyleBackColor = true;
            this.chckWrap.CheckedChanged += new System.EventHandler(this.chckWrap_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 721);
            this.Controls.Add(this.chckWrap);
            this.Controls.Add(this.btnParse);
            this.Controls.Add(this.txtParsedText);
            this.Controls.Add(this.txtRawText);
            this.Name = "Form1";
            this.Text = "Fix Messages Logs Parser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRawText;
        private System.Windows.Forms.TextBox txtParsedText;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.CheckBox chckWrap;
    }
}

