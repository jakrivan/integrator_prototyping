﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kreslik.Integrator.FIXMessaging;

namespace Kreslik.Integrator.FixMessagesTranslator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            StringBuilder parsedOutputBuilder = new StringBuilder();

            foreach (string line in txtRawText.Text.Split(_linesSeparator, StringSplitOptions.None))
            {
                parsedOutputBuilder.AppendLine(line);
                this.AppendParsedMessage(line, parsedOutputBuilder);
            }

            txtParsedText.Text = parsedOutputBuilder.ToString();
        }

        private static char[] _tagsSeparator = new char[] { '\u0001' };
        private static string[] _linesSeparator = new string[] { Environment.NewLine };
        private static string _fixMessageStarter = "8=FIX";
        private static string _fixMessageEnd = "10=";

        private void AppendParsedMessage(string line, StringBuilder parsedOutputBuilder)
        {
            int fixMessageStartIndex = line.IndexOf(_fixMessageStarter);

            if(fixMessageStartIndex < 0)
                return;

            parsedOutputBuilder.Append("PARSED: ");

            try
            {
                bool parsingDone = false;
                foreach (string part in line.Substring(fixMessageStartIndex).Split(_tagsSeparator, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (parsingDone)
                    {
                        parsedOutputBuilder.Append(part);
                    }
                    else
                    {
                        string[] parts = part.Split('=');

                        QuickFixUtils.AppendFormattedTag(parsedOutputBuilder, int.Parse(parts[0]), parts[1]);
                        parsedOutputBuilder.Append(" # ");
                    }

                    if (part.Contains(_fixMessageEnd))
                    {
                        parsedOutputBuilder.Append("## AFTER MESSAGE END: ");
                        parsingDone = true;
                    }
                }
            }
            catch (Exception e)
            {
                parsedOutputBuilder.AppendLine();
                parsedOutputBuilder.AppendLine("Error during parsing:");
                parsedOutputBuilder.Append(e.ToString());
            }

            parsedOutputBuilder.AppendLine();
        }

        private void chckWrap_CheckedChanged(object sender, EventArgs e)
        {
            txtParsedText.WordWrap = chckWrap.Checked;
            txtRawText.WordWrap = chckWrap.Checked;
        }
    }
}
